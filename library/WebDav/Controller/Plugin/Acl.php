<?php

class WebDav_Controller_Plugin_Acl extends Zend_Controller_Plugin_Abstract
{

    public function preDispatch(Zend_Controller_Request_Abstract $request) {

        $roleName = (( @Zend_Auth::getInstance()->hasIdentity()) &&
                (@Zend_Auth::getInstance()->getStorage()->read()->role != '')) ?

            Zend_Auth::getInstance()->getStorage()->read()->role : 'guest';

        if(!$this->isCurrentPageAllowed($request, $roleName)) {
            $request->setModuleName('default');
            $request->setControllerName('Error');
            $request->setActionName('notallowed');
        }
    }

    private function isCurrentPageAllowed($request, $roleName) {
//		return true;

        $acl = Zend_Registry::get('acl');
        
        $requestModule      =   strtolower($request->getModuleName());
        $requestController  =   strtolower($request->getControllerName());
        $resourceName       =   'RES_PAGE::' . $requestModule . '::' . $requestController;

        if (!$acl->has($resourceName)) return false;

        return $acl->isAllowed($roleName, $resourceName);
    }
}
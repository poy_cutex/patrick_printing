<?php
/**
 * Description of LanguageSelector
 *
 * @author jon
 */
class WebDav_Controller_Plugin_LanguageSelector
    extends Zend_Controller_Plugin_Abstract
{
    public function preDispatch(Zend_Controller_Request_Abstract $request)
    {

        Zend_Session::start();
        $namespace_locale = new Zend_Session_Namespace("Namespace_Locale");

        $default_locale = (isset($namespace_locale->locale))? $namespace_locale->locale : 'en';
        $locale = $request->getParam('locale',$default_locale);

        $c = new Model_Countries();
        $locales = $c->getTable()->findAll();
        foreach($locales as $key=>$value)
        {
            $aliases[] = $value->alias;
        }
        if(!in_array($locale,$aliases))
        {
            /*failback to us*/
            $locale = 'us';
        }
        $namespace_locale->locale = $locale;
        Zend_Registry::set('locale',$locale);
    }

}

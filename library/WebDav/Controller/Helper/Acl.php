<?php

class WebDav_Controller_Helper_Acl
{

    private $resources = array(
        'RES_PAGE::admin::admin',
        'RES_PAGE::admin::combo',
        'RES_PAGE::admin::contacts',
        'RES_PAGE::admin::doctrine',
        'RES_PAGE::admin::groups',
        'RES_PAGE::admin::index',
        'RES_PAGE::admin::navibox',
        'RES_PAGE::admin::news',
        'RES_PAGE::admin::provision-scheme',
        'RES_PAGE::admin::provision-abc',
        'RES_PAGE::admin::settings',
        'RES_PAGE::admin::skel',
        'RES_PAGE::admin::translation',
        'RES_PAGE::admin::users',
        'RES_PAGE::admin::message',
        'RES_PAGE::admin::accounting',
        'RES_PAGE::admin::advertisement',
        'RES_PAGE::admin::cache-manager',
        'RES_PAGE::admin::business-partners',
        'RES_PAGE::default::accounts',
        'RES_PAGE::default::activity',
        'RES_PAGE::default::authentication',
        'RES_PAGE::default::combo',
        'RES_PAGE::default::competitors',
        'RES_PAGE::default::competitor-deals',
        'RES_PAGE::default::deals',
        'RES_PAGE::default::error',
        'RES_PAGE::default::index',
        'RES_PAGE::default::leads',
        'RES_PAGE::default::locale',
        'RES_PAGE::default::merchant',
        'RES_PAGE::default::permission',
        'RES_PAGE::default::provisions',
        'RES_PAGE::default::private',
        'RES_PAGE::default::direct',
        'RES_PAGE::default::clever-buy',
        'RES_PAGE::default::clever-travel',
        'RES_PAGE::default::search',
        'RES_PAGE::default::settings',
        'RES_PAGE::default::test',
        'RES_PAGE::default::calendar',
        'RES_PAGE::default::affiliate-leads',
        'RES_PAGE::default::affiliate-accounts',
        'RES_COLUMN::lead_status',
        'RES_COLUMN::lead_rating',
        'RES_COLUMN::lead_type',
        'RES_COLUMN::deal_status',

        'RES_ADD::dealtimeline',

        'RES_EDIT::dealtimeline',
        'RES_EDIT::leads',

        'RES_DELETE::contacts',
        'RES_DELETE::dealtimeline',
        'RES_DELETE::leads'
    );

    private $deniedResources = array(
        'superadmin'    => array(
                        ),
        'admin'         => array(
                        ),
        'salesmanager'  => array(
                            'RES_PAGE::admin::provision-scheme'
                        ),
        'salessupport'  => array(
                            'RES_COLUMN::deal_status',
                            'RES_DELETE::dealtimeline',
                            'RES_PAGE::admin::provision-scheme',
                            'RES_COLUMN::lead_status',
                            'RES_COLUMN::lead_rating',
                            'RES_COLUMN::lead_type',
                            'RES_COLUMN::deal_status',
                            'RES_DELETE::contacts',
                            'RES_ADD::dealtimeline',
                            'RES_EDIT::dealtimeline',
                            'RES_DELETE::dealtimeline',
                            //'RES_EDIT::leads',
                            'RES_DELETE::leads'
                        ),
        'dealmaker'     => array(
                            'RES_PAGE::admin::provision-scheme',
                            'RES_COLUMN::lead_status',
                            'RES_COLUMN::lead_rating',
                            'RES_COLUMN::lead_type',
                            'RES_COLUMN::deal_status',
                            'RES_DELETE::contacts',
                            'RES_ADD::dealtimeline',
                            'RES_EDIT::dealtimeline',
                            'RES_DELETE::dealtimeline',
                            //'RES_EDIT::leads',
                            'RES_DELETE::leads'
                        ),
        'user'          => array(
                            'RES_PAGE::admin::provision-scheme',
                            'RES_COLUMN::lead_status',
                            'RES_COLUMN::lead_rating',
                            'RES_COLUMN::lead_type',
                            'RES_COLUMN::deal_status',
                            'RES_DELETE::contacts',
                            'RES_ADD::dealtimeline',
                            'RES_EDIT::dealtimeline',
                            'RES_DELETE::dealtimeline',
                            //'RES_EDIT::leads',
                            'RES_DELETE::leads'
                        )
    );

    private $guestAllowedResources = array(
        'guest' => array(
                'RES_PAGE::admin::index',
                'RES_PAGE::default::authentication',
                'RES_PAGE::default::merchant',
                'RES_PAGE::default::combo',
                'RES_PAGE::default::error',
                'RES_PAGE::default::index',
                'RES_PAGE::default::locale',
                'RES_PAGE::default::permission',
                'RES_PAGE::default::private',
                'RES_PAGE::default::settings'
        ),
    );
    public $acl;

    public function __construct() {
        $this->acl = new Zend_Acl();
    }

    public function setRoles() {
        $this->acl->addRole(new Zend_Acl_Role('superadmin'));
        $this->acl->addRole(new Zend_Acl_Role('admin'));
        $this->acl->addRole(new Zend_Acl_Role('salesmanager'));
        $this->acl->addRole(new Zend_Acl_Role('salessupport'));
        $this->acl->addRole(new Zend_Acl_Role('dealmaker'));
        $this->acl->addRole(new Zend_Acl_Role('user'));
        $this->acl->addRole(new Zend_Acl_Role('guest'));
    }

    public function setResources() {
        foreach($this->resources as $resourceName) {
            $this->acl->add(new Zend_Acl_Resource($resourceName));
        }
    }

    public function setPrivileges() {
        $this->acl->deny('guest');
        $this->acl->allow('user');
        $this->acl->allow('dealmaker');
        $this->acl->allow('salessupport');
        $this->acl->allow('salesmanager');
        $this->acl->allow('admin');
        $this->acl->allow('superadmin');

        foreach($this->deniedResources as $role => $resourcesToDeny) {
            foreach($resourcesToDeny as $resourceToDeny) {
                $this->acl->deny($role, $resourceToDeny);
            }
        }

        foreach($this->guestAllowedResources as $resourceToAllowToGuest) {
            $this->acl->allow('guest', $resourceToAllowToGuest);
        }
    }

    public function setAcl() {
        Zend_Registry::set('acl',$this->acl);
    }
}

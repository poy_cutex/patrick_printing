<?php

class WebDav_Controller_Action_Helper_LayoutLoader extends Zend_Controller_Action_Helper_Abstract
{

	public function preDispatch()
	{
        $bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap');
		$config = $bootstrap->getOptions();
		$module = $this->getRequest()->getModuleName();
		if (isset($config[$module]['resources']['layout']['layout'])) {
			$layoutScript = $config[$module]['resources']['layout']['layout'];
			$this->getActionController()
				 ->getHelper('layout')
				 ->setLayout($layoutScript);
		}
	}

}

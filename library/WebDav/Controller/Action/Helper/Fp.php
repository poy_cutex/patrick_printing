<?php

class WebDav_Controller_Action_Helper_Fp extends Zend_Controller_Action_Helper_Abstract {

    public function __construct() {
        $this->fp = Zend_Registry::get('fp');
    }

    public function log($message, $prioritylevel = 6) {
        $this->fp->log($message, $prioritylevel);
    }


}
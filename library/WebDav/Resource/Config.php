<?php
class WebDav_Resource_Config extends Zend_Application_Resource_ResourceAbstract
{
    public function init()
    {
         $options = $this->getOptions();
         $config = new Zend_Config_Ini($options['iniPath'],$options['env']);
         /* Preserved the line below for backward compatibility */
         Zend_Registry::set('config', $config);
         return $config;
    }
}
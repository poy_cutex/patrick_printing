<?php
class WebDav_Model_NewsMapper
{
    protected $_dbTable;
 
    public function setDbTable($dbTable)
    {
        if (is_string($dbTable)) {
          $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
          throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }
 
    public function getDbTable()
    {
        if (null === $this->_dbTable) {
          $this->setDbTable('WebDav_Model_DbTable_News');
        }
        return $this->_dbTable;
    }
 
    public function save($newsInfo)
    {
        $newsInfo['last_update_dt'] = null;

        if (0 >= @$newsInfo['id']) {
            unset($newsInfo['id']);
            $res = $this->getDbTable()->insert($newsInfo);
        } else {
            $res = $this->getDbTable()->update($newsInfo, array('id = ?' => $newsInfo['id']));
        }

        return $res;
    }

    public function fetchAll($start = null, $limit = null)
    {
        return $this->getDbTable()->fetchAll(NULL, 'last_update_dt desc', $limit, $start);
    }

    public function delete($id)
    {
        $this->getDbTable()->delete('id = ' . $id);
    }

}

<?php
/*
 *@package webdav_models_dbtable
 *@author Kristel Magnaye
 */
class WebDav_Model_DbTable_Users extends Zend_Db_Table_Abstract {

  /** Table name */
  protected $_name    = 'users';

}
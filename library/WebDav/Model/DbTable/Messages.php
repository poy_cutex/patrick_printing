<?php
/*
 *@package webdav_models_dbtable
 *@author edd
 */
class WebDav_Model_DbTable_Messages extends WebDav_Model_BaseModel {

  /** Table name */
  protected $_name    = 'messages';
  protected $_userTable ;

  public function init(){
      $this->userTable = new WebDav_Db_Users();
  }

}
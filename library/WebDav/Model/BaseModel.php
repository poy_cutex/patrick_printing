<?php
    class WebDav_Model_BaseModel extends Zend_Db_Table_Abstract {
        protected $_db;

        public function init(){
         $this->_db =   Zend_Registry::get('db');
        }

        public function getAll($start=null,$limit=null){
            if($start==null && $limit==null){
                $select = $this->_db->select()->from($this->_name);
            }else{
                $select = $this->_db->select()
                            ->from($this->_name)
                            ->limit($limit,$start);
            }
            $select->order(array('id desc'));
            return $this->_db->query($select)->fetchAll();
        }
        public function getColumnById($column,$id){
        $name = $this->find($id);
        return  $name->current()->__get($column) ;
    }
    }

?>

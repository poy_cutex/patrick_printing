<?php
class WebDav_Model_NaviboxesMapper
{
    protected $_dbTable;
 
    public function setDbTable($dbTable)
    {
        if (is_string($dbTable)) {
          $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
          throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }
 
    public function getDbTable()
    {
        if (null === $this->_dbTable) {
          $this->setDbTable('WebDav_Model_DbTable_Naviboxes');
        }
        return $this->_dbTable;
    }

    public function fetchAll($whereParams=null,$start = null, $limit = null)
    {
        $select = $this->getDbTable()->select();
                   
        if( $whereParams['position'] != null AND  in_array($whereParams['position'],array('L','R')) ){
            $select->where('position = ?', $whereParams['position']);
        }

        if($whereParams['position']){
             $select->where('active_indicator = ?', 1);
        }
        $select->limit($limit, $start);

        $records = $this->getDbTable()->fetchAll($select);
                
        return $records;
    }

    public function save($data)
    {
        $id = $data['id'];
        if($id > 0) {
            $this->getDbTable()->update($data, 'id = ' . $id);
        } else {
            unset($data['id']);
            $this->getDbTable()->insert($data);
        }
    }

    public function delete($id)
    {
        $this->getDbTable()->delete('id = ' . $id);
    }

}

<?php
class WebDav_Model_GroupsNaviboxesMapper
{
    protected $_dbTable;

    public function setDbTable($dbTable)
    {
        if (is_string($dbTable)) {
          $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
          throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }

    public function getDbTable()
    {
        if (null === $this->_dbTable) {
          $this->setDbTable('WebDav_Model_DbTable_GroupsNaviboxes');
        }
        return $this->_dbTable;
    }

    public function getGroupNaviboxes($groupId)
    {
        return $this->getDbTable()->fetchAll('group_id = ' . $groupId);
    }

    public function delete(Default_Model_GroupsNaviboxes $groupNavibox)
    {
        $this->getDbTable()->delete('group_id = ' .$groupNavibox->getGroupId());
    }
}

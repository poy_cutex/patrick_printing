<?php
class WebDav_Model_UsersMapper
{
    protected $_dbTable;

    public function setDbTable($dbTable)
    {
        if (is_string($dbTable)) {
          $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
          throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }

    public function getDbTable()
    {
        if (null === $this->_dbTable) {
          $this->setDbTable('WebDav_Model_DbTable_Users');
        }
        return $this->_dbTable;
    }

    public function updatePassword($userId, $password)
    {
        $data['salt'] = $this->_createSalt();
        $data['password'] = sha1($password . $data['salt']);
        $data['last_password_upd_dt'] = null;
        $this->getDbTable()->update($data, 'id = ' . $userId);
    }

    public function findUserByEmail($email) {
        return $this->getDbTable()->fetchRow(
                $this->getDbTable()
                    ->select()
                    ->setIntegrityCheck(false)
                    ->from('users')
                    ->where('users.email = ?', $email)
        );
    }

    private function _createSalt(){
        $dynamicSalt = '';
        for ($i = 0; $i < 50; $i++) {
            $dynamicSalt .= chr(rand(33, 126));
        }
        return $dynamicSalt;
    }

    public function isPasswordExpired($userId)
    {
        $result = $this->getDbTable()->fetchRow(
                $this->getDbTable()
                        ->select()
                        ->from('users', array('diff' => 'datediff(curdate(), last_password_upd_dt)'))
                        ->where('id = ' . $userId)
                );

        if($result->diff >= 14)
            return true;

        return false;
    }
    /**
     *
     * @param type $bodyText
     * @param type $bodyHtml
     * @param type $fromEmail
     * @param type $fromName
     * @param type $toEmail
     * @param type $toName
     * @param type $subject
     * @param type $transport 
     */
    public function sendMail(
            $bodyText, $bodyHtml, $fromEmail, $fromName,
            $toEmail, $toName, $subject, $transport)
    {
        $mail = new Zend_Mail();
        $mail->setBodyText($bodyText);
        $mail->setBodyHtml($bodyHtml);
        $mail->setFrom($fromEmail, $fromName);
        $mail->addTo($toEmail, $toName);
        $mail->setSubject($subject);
        $mail->send($transport);
    }

    public function mailPassword($userId, $recipient, $name)
    {
        $subject = 'Password Reset';

        $password = $this->_createRandomPassword();
        $this->updatePassword($userId, $password);

        $config = array('auth' => 'login',
                'username' => 'project1onet@gmail.com',
                'password' => 'project1one',
                'ssl'=>"tls",
                'port'=> 587
            );

        $transport = new Zend_Mail_Transport_Smtp('smtp.gmail.com', $config);

        $bodyHtml  = "Hello ". $name . ",";
        $bodyHtml .= "<br><br>Your password has been reset to <b>{$password}</b>.";
        $bodyHtml .= "<br><br>Thank you,";
        $bodyHtml .= "<br>Project One Team";

        $this->sendMail(null, $bodyHtml, 'Project-One-Team@do-not-reply.com', 'Project One Team',
                $recipient, $name, $subject, $transport);
        return array("name"=>$name,"password"=>$password,"email"=>$recipient);
    }

    public function _createRandomPassword() {
        $chars = "abcdefghijkmnopqrstuvwxyz023456789";
        srand((double)microtime()*1000000);
        $i = 0;
        $pass = '' ;
        while ($i <= 7) {
            $num = rand() % 33;
            $tmp = substr($chars, $num, 1);
            $pass = $pass . $tmp;
            $i++;
        }
        return $pass;
    }

    public function fetchAll()
    {
        return $this->getDbTable()->fetchAll();
    }

    public function resetAllPasswords() {
        $users = WebDav_Db_Profiles::getAll();
        foreach ($users as $user) {
            $this->mailPassword(
                    $user['id'],
                    $user['email'],
                    ucwords($user['firstname'] . ' ' . $user['lastname']));
        }
    }
}

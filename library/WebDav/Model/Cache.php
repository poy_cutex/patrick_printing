<?php

/*
 * @package webdav_model
 * @author Edd Aranza
 */

class WebDav_Model_Cache {

    /**
     * @var boolean
     */
    static protected $_enabled = false;

    /**
     * @var Zend_Cache_Core
     */
    static protected $_cache;

    static function init($enabled, $dir, $lifetime=7200, $type='FILE') {
        //die($dir);

        self::$_enabled = $enabled;
        if (self::$_enabled) {
            //require_once 'Zend/Cache.php';

            $frontendOptions = array(
                'lifetime' => $lifetime,
                'automatic_serialization' => true,
            );
            $backendOptions = array(
                'cache_dir' => $dir,
                'file_name_prefix' => 'thecache',
                'hashed_directory_level' => 2
            );
            if ($type == 'FILE')
                self::$_cache = Zend_Cache::factory('Core', $type, $frontendOptions, $backendOptions);
            else
                self::$_cache = Zend_Cache::factory('Core', $type);
        }
    }

    static function getInstance() {
        if (self::$_enabled == false) {
            return false;
        }
        return self::$_cache;
    }

    static function load($keyName) {
        if (self::$_enabled == false) {
            return false;
        }
        return self::$_cache->load($keyName);
    }

    static function save($dataToStore, $keyName) {
        if (self::$_enabled == false) {
            return true;
        }

        return self::$_cache->save($dataToStore, $keyName);
    }

    static function clean() {
        if (self::$_enabled == false) {
            return;
        }
        self::$_cache->clean();
    }

}

<?php
class WebDav_Model_LoginJnlMapper
{
    protected $_dbTable;
 
    public function setDbTable($dbTable)
    {
        if (is_string($dbTable)) {
          $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
          throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }
 
    public function getDbTable()
    {
        if (null === $this->_dbTable) {
          $this->setDbTable('WebDav_Model_DbTable_LoginJnl');
        }
        return $this->_dbTable;
    }
 
    public function getLatestLogins($userId, $count = null) {
        return $this->getDbTable()->fetchAll('user_id = ' . $userId, 'login_dt desc', $count);
    }

}

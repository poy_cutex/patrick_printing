<?php
/*
 *@package webdav_model
 *@author Kristel Magnaye
 */
class WebDav_Model_Groups
{
  protected $_id;
  protected $_name;
  protected $_description;
  protected $_passwordResetDays;
  protected $_passwordResetDaysLastUpd;
 
  public function __construct(array $options = null)
  {
    if (is_array($options)) {
      $this->setOptions($options);
    }
  }
 
  public function __set($name, $value)
  {
    $method = 'set' . $name;
    if (('mapper' == $name) || !method_exists($this, $method)) {
      throw new Exception('Invalid property');
    }
    $this->$method($value);
  }
 
  public function __get($name)
  {
    $method = 'get' . $name;
    if (('mapper' == $name) || !method_exists($this, $method)) {
      throw new Exception('Invalid property');
    }
    return $this->$method();
  }
 
  public function setOptions(array $options)
  {
    $methods = get_class_methods($this);
    foreach ($options as $key => $value) {
      $method = 'set' . ucfirst($key);
      if (in_array($method, $methods)) {
        $this->$method($value);
      }
    }
    return $this;
  }
 
  public function setId($id)
  {
    $this->_id = $id;
    return $this;
  }
 
  public function getId()
  {
    return $this->_id;
  }

  public function setName($name)
  {
      $this->_name =  $name;
      return $this;
  }

  public function getName()
  {
      return $this->_name;
  }

  public function setDescription($description)
  {
      $this->_description =  $description;
      return $this;
  }

  public function getDescription()
  {
      return $this->_description;
  }

  public function setPasswordResetDays($passwordResetDays)
  {
      $this->_passwordResetDays =  $passwordResetDays;
      return $this;
  }

  public function getPasswordResetDays()
  {
      return $this->_passwordResetDays;
  }

  public function setPasswordResetDaysLastUpd($passwordResetDaysLastUpd)
  {
      $this->_passwordResetDaysLastUpd =  $passwordResetDaysLastUpd;
      return $this;
  }

  public function getPasswordResetDaysLastUpd()
  {
      return $this->_passwordResetDaysLastUpd;
  }

}

<?php
class WebDav_Model_TranslationMapper
{
    private $_db;
 
    private function getDb()
    {
        if (null === $this->_db) {
          $this->_db = Zend_Registry::get('db');
        }
        return $this->_db;
    }

    public function getAllTranslations($category = null, $status = null)
    {
        if($category == 'F' && !$status) $status = 'all';
        $sql = $this->getDb()->select()
                ->from('countries', 'id');
        $result = $this->getDb()->query($sql)->fetchAll();

        $sql = $this->getDb()->select()
                ->from(array('k' => 'lang_keywords'), array('keyword_id' => 'id', 'keyword', 'category', 'url', 'status'));
        $sql = $category ? $sql->where("k.category = '$category'") : $sql;
        $sql = $status ? (($status == 'all') ? $sql->where("k.status != 9") : $sql->where("k.status = '$status'")) : $sql;
        $sql = $sql->order('keyword');

        foreach($result as $country) {
            $alias = 't' . $country['id'];
            $subselect = $this->getDb()->select()
                            ->from('lang_translation')
                            ->where('country_id = ' . $country['id']);
            $sql = $sql->joinLeft(
                    array($alias => new Zend_Db_Expr("($subselect)")),
                    'k.id = ' . $alias . ' .keyword_id',
                    array('id_' . $alias => 'id', 'translation_' . $alias => 'translation')
                    );
        }
        $stmt = $this->getDb()->query($sql);
        $result = $stmt->fetchAll();
        if($result){
            return  $result;
        }
        return null;
    }
 
    public function save($data)
    {
        $id = $data['id'];
        if (0 >= $id) {
            unset($data['id']);
            $res = $this->getDb()->insert('lang_translation', $data);
        } else {
            $res = $this->getDb()->update('lang_translation', $data, array('id = ?' => $id));
        }

        return $res;
    }

    public function delete($translationId)
    {
        $res = $this->getDb()->delete('lang_translation', array('id = ?' => $translationId));
    }

    public function saveKeyword($data)
    {
        $id = $data['id'];
        $sql = $this->getDb()
                ->select()
                ->from('lang_keywords', array('cnt' => new Zend_Db_Expr('count(1)')))
                ->where('keyword = \'' . $data['keyword'] . '\'')
                ->where('category = \'' . $data['category'] . '\'');
        $sql = $id ? $sql->where('id != ' . $id) : $sql;
        $stmt = $this->getDb()->query($sql);
        $result = $stmt->fetchAll();
        if($result[0]['cnt'] > 0) unset($data['keyword']);
        if($id)
            $this->getDb()->update('lang_keywords', $data, 'id = ' . $id);
        else {
            $this->getDb()->insert('lang_keywords', $data);
            return $this->getDb()->lastInsertId();
        }

        return true;
    }

    public function getAllKeywords() {
        $sql = $this->getDb()->select()
                ->from('lang_keywords');
        $stmt = $this->getDb()->query($sql);
        $result = $stmt->fetchAll();
        if($result){
            return  $result;
        }
        return null;
    }

    public function deleteKeyword($data)
    {
        $this->getDb()->delete('lang_translation', 'keyword_id = ' . $data['id']);
        $this->getDb()->delete('lang_keywords', 'id = ' . $data['id']);
    }

}

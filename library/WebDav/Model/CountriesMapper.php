<?php
class WebDav_Model_CountriesMapper
{
    protected $_dbTable;
 
    public function setDbTable($dbTable)
    {
        if (is_string($dbTable)) {
          $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
          throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }
 
    public function getDbTable()
    {
        if (null === $this->_dbTable) {
          $this->setDbTable('WebDav_Model_DbTable_Countries');
        }
        return $this->_dbTable;
    }

    public function fetchAll($start = null, $limit = null)
    {
        return $this->getDbTable()->fetchAll(NULL, 'name', $limit, $start);
    }

}

<?php
class WebDav_Model_CategoriesMapper
{
    protected $_dbTable;
 
    public function setDbTable($dbTable)
    {
        if (is_string($dbTable)) {
          $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
          throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }
 
    public function getDbTable()
    {
        if (null === $this->_dbTable) {
          $this->setDbTable('WebDav_Model_DbTable_Categories');
        }
        return $this->_dbTable;
    }

    public function fetchAll($start = null, $limit = null)
    {
        $select = $this->getDbTable()->select()
                    ->setIntegrityCheck(false)
                    ->from(array('c' => 'categories'))
                    ->joinLeft(array('p' => 'categories'), 'c.parent = p.id', array('parent_name' => 'name'))
                    ->order('c.name');
        
        if(!is_null($start) && !is_null($limit))
            $select->limit ($limit, $start);

        $resultSet = $this->getDbTable()->fetchAll($select);

        return $resultSet;
    }

    public function getTreeChildren($node)
    {
        $childTbl = $this->getDbTable()->select()
                ->from('categories', array('parent', 'cnt' => new Zend_Db_Expr('count(*)')))
                ->group('parent');
        $select = $this->getDbTable()->select()
                ->setIntegrityCheck(false)
                ->from(array('c' => 'categories'), array(
                    'id', 'order', 'url', 'name', 'text' => 'name', 
                    'parent', 'description', 'keywords'
                ))
                ->joinLeft(array('ch' => $childTbl), 'c.id = ch.parent', array('childrenCnt' => 'cnt'))
                ->where('c.parent = ' . $node)
                ->order('c.name');
        $result = $this->getDbTable()->fetchAll($select);
        return $result;
    }

    public function save($data)
    {
        $id = $data['id'];
        $res['parent'] = $data['parent'];
        if (0 >= $id) {
            unset($data['id']);
            $res['saveAction'] = 'add';
            $res['id'] = $this->getDbTable()->insert($data);
        } else {
            $this->getDbTable()->update($data, array('id = ?' => $id));
            $res['saveAction'] = 'update';
            $res['id'] = $id;
        }

        return $res;
    }

    public function fetchRow($where)
    {
        return $this->getDbTable()->fetchRow($where);
    }

    public function delete($id)
    {
        $this->getDbTable()->delete('parent = ' . $id);
        $this->getDbTable()->delete('id = ' . $id);
    }
}

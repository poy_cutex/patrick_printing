<?php

class WebDav_Db_Profiles
{
    public static function getAll2($start=null,$limit=null) {
        $db = Zend_Registry::get('db');

        if ($start==null && $limit==null) {
            $select = $db->select()->from('userprofiles');
        }
        else {
            $select =   $db->select()
                        ->from('userprofiles')
                        ->order('username asc ')
                        ->limit($limit,$start);
        }
        $select->order(array('id desc'));
        $stmt = $db->query($select);
        $result = $stmt->fetchAll();
        if ($result) {
            return  $result;
        }
        return null;
    }

    public static function getAll($start=null,$limit=null) {
        $db = Zend_Registry::get('db');

        $select =   $db->select()
                    ->from('users',array('*','status_text' => new Zend_Db_Expr('CASE users.active WHEN 0 THEN "Not Active" WHEN 1 THEN "Active" WHEN 2 THEN "Confirmed" WHEN 3 THEN "Blocked" ELSE "Not Active" END')))
                    ->join('user_profiles'  ,   'user_profiles.user_id = users.id'  ,   array(  'user_id', 'firstname', 'lastname', 'email',
                                                                                                'phone', 'mobile_phone', 'gender', 'date_birth',
                                                                                                'home_address', 'country', 'homepage', 'icq', 'msn','provisionscheme_id'))
                    ->joinLeft('login_jnl'  ,   'login_jnl.user_id = users.id'      ,   array(  'last_login' => new Zend_Db_Expr('max(login_dt)')))
                    ->joinLeft('groups'     ,   'groups.id = users.group_id'        ,   array(  'role' => 'name'))                    
                    ->group('users.id');
        if (!($start==null && $limit==null)) {
            $select = $select->limit($limit,$start);
        }
        $select->order(array('users.id desc'));
        $stmt   =   $db->query($select);
        $result =   $stmt->fetchAll();
        if ($result) {
            return  $result;
        }
        return null;
    }
    
    public static function getAllNonStaticPasswords() {
        $db = Zend_Registry::get('db');

        $select =   $db->select()
                    ->from('users')
                    ->join('user_profiles'  ,   'user_profiles.user_id = users.id'  ,   
                            array(  'user_id', "concat(firstname,' ',lastname) name", 'email',))
                    ->where('users.static_password = 0 and users.active = 1');                
        $stmt   =   $db->query($select);
        $result =   $stmt->fetchAll();
        if ($result) {
            return  $result;
        }
        return null;
    }

    public static function getUserDetails($userId) {
        $db = Zend_Registry::get('db');

        $select =   $db->select()
                        ->from('users', array('username',"date_format(date_created,'%M %e, %Y') as date_created"))
                        ->join('user_profiles', 'user_profiles.user_id = users.id', array(
                        'user_id', 'firstname', 'lastname', 'email',
                        'phone', 'mobile_phone', 'gender', 'date_birth',
                        'home_address', 'country', 'homepage', 'icq', 'msn',
                        'picture', 'hide_numbers', 'default_language'))
                        ->where('user_id = ' . $userId);
        $stmt   =   $db->query($select);
        return $stmt->fetchAll();
    }
}
?>

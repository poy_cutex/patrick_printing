<?php
class WebDav_Db_Users extends Zend_Db_Table_Abstract
{
    protected $_name = 'users';

    public static function getIdByUsername($username)
    {
        $db = Zend_Registry::get('db');
        $select = $db->select()->from('users')->where('username=?',$username);
        $stmt = $db->query($select);
        $result = $stmt->fetch();
        if($result['id']){
            return $result['id'];
        }
        return false;
    }
    public static function recordExist($username){
        return WebDav_Db_Users::getIdByUsername($username);
    }
}
?>

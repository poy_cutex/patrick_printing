<?php
class WebDav_View_Helper_LoginToggle extends Zend_View_Helper_Abstract
{
    public function loginToggle ()
    {
        $auth = Zend_Auth::getInstance();
        if ($auth->hasIdentity()) {
            $username = $auth->getIdentity()->username;
            return '<a href="/authentication/logout">Logout</a>';
        }
        return '<a href="/authentication/login">Login</a>';
    }
}
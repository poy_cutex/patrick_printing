<?php
class WebDav_View_Helper_ViewUtility extends Zend_View_Helper_Abstract
{
    public function viewUtility () {
        return $this;
    }


    public function getTotalLogin($userIdparam) {
        $objWebDav_DbTable_LoginJnl = new WebDav_Model_DbTable_LoginJnl();

        $userData = $objWebDav_DbTable_LoginJnl->fetchRow($objWebDav_DbTable_LoginJnl
                    ->select()
                    ->from($objWebDav_DbTable_LoginJnl,array('total_login' => new Zend_Db_Expr ("COUNT(*)")))
                    ->where('user_id = ?', $userIdparam));
        return $userData->total_login;
    }
}
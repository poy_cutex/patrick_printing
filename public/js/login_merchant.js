setLabel = function(loginForm, elemId, text) {
    loginForm.findById(elemId).label.update(text + ':');
}

setLanguage = function(lang, loginForm, win) {
    //if(lang == null) lang = 'us';
    Ext.Ajax.request({
        url: '/default/locale/index/locale/' + (lang == null ? 'us' : lang),
        success: function(response) {
            eval(response.responseText);
            setLabel(loginForm, 'username', (LANG.FLD_USERNAME ? LANG.FLD_USERNAME : 'Username'));
            setLabel(loginForm, 'password', (LANG.FLD_PASSWORD ? LANG.FLD_PASSWORD : 'Password'));
            Ext.getCmp('login').setText(LANG.BTN_LOGIN ? LANG.BTN_LOGIN : 'Login');
            Ext.getCmp('reset').setText(LANG.BTN_RESET ? LANG.BTN_RESET : 'Reset');
            Ext.getCmp('forgotPassword').setText(LANG.BTN_FORGOT_PASSWORD ? LANG.BTN_FORGOT_PASSWORD : 'Forgot Password?');
            loginForm.getForm().findField('locale').setValue(lang);
            win.setTitle(LANG.PNL_LOGIN_TITLE ? LANG.PNL_LOGIN_TITLE : 'Please log in');
            Ext.destroy(Ext.getCmp('helpIcon'));
            Ext.getCmp('bottomPanel').add({
                id: 'helpIcon',
                autoEl  :   {
                    tag     :   'img',
                    src     :   '/img/icons/help.png',
                    title   :   LANG.TIP_FORGOT_PASSWORD ? LANG.TIP_FORGOT_PASSWORD : ''
                },
                xtype   :   'box'
            });
            win.doLayout();
        },
        failure: function() {
            Ext.Msg.alert('Failure', 'This language is not available.');
        }
    });
}

Ext.onReady(function() {
    var loginForm = new Wd.forms.login();
    var win = new Ext.Window({
        id          :   'loginWindow',
        width       :   '330',
        draggable   :   false,
        resizable   :   false,
        closable    :   false,
        listeners   : {
            beforerender : {
                fn : function(window) {
                    setLanguage(null, loginForm, window);
                }
            }
        },
        items       :   [
            loginForm,
            {
                id: 'bottomPanel',
                layout  :   'column',
                border  :   true,
                frame   :   true,
                margins :   {
                    top     :   5,
                    right   :   5,
                    bottom  :   5,
                    left    :   5
                },
                items   :   [
                    {
                        xtype       :   'combo',
                        emptyText   :   'Choose Language',
                        name        :   'language',
                        id          :   'language',
                        editable    :   false,
                        lazyRender  :   true,
                        displayField    :   'language',
                        valueField      :   'id',
                        hiddenValue     :   'id',
                        hiddenName      :   'language1',
                        plugins         :   new Ext.ux.plugins.IconCombo(),
                        iconClsField    :   'alias',
                        triggerAction   :   'all',
                        width:          150,
                        store       :   new Ext.data.JsonStore({
                            url             :   '/combo/countries',
                            root            :   'records',
                            fields          :   ['id','language', 'alias', 'language_alias']
                        }),
                        listeners   : {
                            select  : {
                                fn  : function(combo, record, index) {
                                    setLanguage(record.data.alias, loginForm, win);
                                    loginForm.doLayout();
                                }
                            }
                        }
                    },
                    {
                        xtype       :   'box',
                        html        :   '<span>&nbsp;</span>',
                        columnWidth :   .3
                    },
                    {
                        xtype   :   'button',
                        id      :   'forgotPassword',
                        cls     :   'button-link',
                        handler :   function() {
                            new Ext.Window({
                                title   :   'Forgot Password',
                                id      :   'forgotPasswordWindow',
                                modal   :   true,
                                closable:   true,
                                items   :   [
                                    new Wd.forms.forgotpassword({
                                        width   :   400,
                                        frame   :   false,
                                        border  :   false
                                    })
                                ]
                            }).show();
                        }
                    },
                    {
                        id: 'helpIcon',
                        autoEl  :   {
                            tag     :   'img',
                            src     :   '/img/icons/help.png'
                        },
                        xtype   :   'box'
                    }
                ]
            }
        ]
    }).show()
});
Ext.onReady(function() {
    Ext.BLANK_IMAGE_URL = '/ext/resources/images/default/s.gif';

    Ext.QuickTips.init();

    Ext.form.Field.prototype.msgTarget = 'side';

    var centerContainer = new Ext.Container({
        items: [
            new Ext.FormPanel({
                title: 'New Navigation Box',
                url: '',
                labelWidth: 100,
                frame:true,
                cls: 'panelMargin',
                id: 'naviboxForm',
                items: [
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Name',
                        name: 'name',
                        width: 400,
                        allowBlank: false
                    }, {
                        xtype: 'radiogroup',
                        fieldLabel: 'Location',
                        width: 150,
                        items: [
                            {boxLabel: 'Left', name: 'location', inputValue: 1, checked: true},
                            {boxLabel: 'Right', name: 'location', inputValue: 2}
                        ]
                    }, {
                        xtype: 'numberfield',
                        fieldLabel: 'Order from Top',
                        name: 'order',
                        width: 50,
                        minValue: 1,
                        maxValue: 99,
                        allowBlank: false,
                        allowNegative: false,
                        allowDecimals: false
                    }, {
                        xtype: 'radiogroup',
                        fieldLabel: 'Is Active',
                        width: 150,
                        items: [
                            {boxLabel: 'Yes', name: 'active', inputValue: 1, checked: true},
                            {boxLabel: 'No', name: 'active', inputValue: 0}
                        ]
                    }
                ],
                buttons: [
                    {
                        text: 'Save',
                        id: 'savebox',
                        handler: function(b, e) {
                            Ext.getCmp('naviboxForm').getForm().submit();
                        }
                    },
                    { text: 'Cancel'}
                ]
            })
       ]
    });
    
    var pageLayout = new Wd.panels.layout();
    var centerPanel = Ext.getCmp('centerpanel');
    
    centerPanel.insert(1, centerContainer);
    centerPanel.doLayout();

    pageLayout.render('layoutContainer');
});
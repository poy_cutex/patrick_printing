Ext.ns('Wd.panels');

manageCategories = function(anElement) {
    var tree = Ext.getCmp('categoryTree');
    var selectedNode = tree.getSelectionModel().getSelectedNode();
    selectedNode = (selectedNode == null) ? tree.getRootNode() : selectedNode;
    var categoryForm = new Wd.forms.categories();
    var loadForm = false;

    if('add' == anElement.itemAction) {
        categoryForm.getForm().findField('parent').setValue(selectedNode.id);
    } else if('edit' == anElement.itemAction) {
        loadForm = true;
    } else if('delete' == anElement.itemAction) {
        Ext.apply(categoryForm, { formType: 'delete' });
        loadForm = true;
    }

    new Ext.Window({
        title: (
            ('delete' == anElement.itemAction) ?
                (LANG.MSG_DELETE_CATEGORY ? LANG.MSG_DELETE_CATEGORY : 
                    'Are you sure you want to delete this category?') :
                (LANG.PNL_CATEGORY ? LANG.PNL_CATEGORY : 'Category')),
        closable: false,
        modal: true,
        width: 500,
        items: categoryForm
    }).show(null, function() {
        if(loadForm) {
            categoryForm.el.mask('Loading...', 'x-mask-loading');
            categoryForm.getForm().load({
                url: '/admin/categories/find',
                params: {
                    id: selectedNode.id
                },
                success: function() {
                    categoryForm.el.unmask();
                },
                failure: function(form, action) {
                    categoryForm.el.unmask();
                    Ext.Msg.alert("Load failed", action.result.errorMessage);
                }
            });
        }
    });
}



Wd.panels.categories = Ext.extend(Ext.Container,{
        initComponent : function(){

            this.treeTb = new Ext.Toolbar({
                items:[{
                    text: LANG.BTN_NEW_CATEGORY ? LANG.BTN_NEW_CATEGORY : 'New Category',
                    icon: '/img/icons/tag_blue_add.png',
                    itemAction: 'add',
                    handler: function(thisBtn){
                        manageCategories(thisBtn);
                    }
                }]
            });

            Ext.apply(this,{
                id: 'categoriesPanel',
                autoScroll: true,
                autoWidth: true,
                frame: true,
                items: [
                    new Ext.Panel({
                        title: LANG.PNL_CATEGORIES ? LANG.PNL_CATEGORIES : 'Categories',
                        layout: 'border',
                        height: 400,
                        items: [
                            new Ext.tree.TreePanel({
                                // tree
                                id: 'categoryTree',
                                animate:true,
                                enableDD:true,
                                containerScroll: true,
                                rootVisible: true,
                                root: {
                                    nodeType: 'async',
                                    text: 'Categories',
                                    draggable: false,
                                    id: '0',
                                    expanded: true
                                },
                                loader: new Ext.tree.TreeLoader({dataUrl: '/admin/categories/gettreechildren'}),
                                contextMenu: new Ext.menu.Menu({
                                    addItem: {
                                        id: 'addCategoryCntxt',
                                        text: LANG.BTN_ADD ? LANG.BTN_ADD : 'Add',
                                        itemAction: 'add',
                                        icon: '/img/icons/tag_blue_add.png'
                                    },
                                    editItem: {
                                        id: 'editCategoryCntxt',
                                        text: LANG.BTN_EDIT ? LANG.BTN_EDIT : 'Edit',
                                        itemAction: 'edit',
                                        icon: '/img/icons/tag_blue_edit.png'
                                    },
                                    deleteItem: {
                                        id: 'deleteCategoryCntxt',
                                        text: LANG.BTN_DELETE ? LANG.BTN_DELETE : 'Delete',
                                        itemAction: 'delete',
                                        icon: '/img/icons/tag_blue_delete.png'
                                    },
                                    listeners: {
                                        itemclick: function(item) {
                                            manageCategories(item);
                                        }
                                    }
                                }),
                                listeners: {
                                    click: {
                                        fn: function(thisNode) {
                                            thisNode.expand();
                                        }
                                    },
                                    contextmenu: function(node, e) {
                                        node.select();
                                        node.expand();
                                        var c = node.getOwnerTree().contextMenu;
                                        c.removeAll();
                                        c.add(c.addItem);
                                        if(node.id > '0') {
                                            c.add(c.editItem);
                                            c.add(c.deleteItem);
                                        }

                                        c.contextNode = node;
                                        c.showAt(e.getXY());
                                    }
                                },

                                // layout
                                region:'west',
                                width:150,
                                split: true,

                                // panel
                                autoScroll:true,
                                tbar: this.treeTb
                            }),
                            new Wd.grids.categories({
                                region: 'center'
                            })
                        ]
                    })
                    
                ]
            });

            Wd.panels.categories.superclass.initComponent.apply(this,arguments);
        }
});

Ext.reg('wdpanelscategories', Wd.panels.categories);
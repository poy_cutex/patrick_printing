Ext.ns('Wd.panels');

var username = '';

Wd.panels.userdetails = Ext.extend(Ext.Container,{
        initComponent : function(){
            Ext.apply(this,{
                items: [
                    {
                        xtype: 'form',
                        title: 'Profile of ',
                        frame: true,
                        labelWidth: 90,
                        cls: 'panelMargin',
                        defaults: {
                            xtype: 'displayfield',
                            labelStyle: 'margin-left: 5px;'
                        },
                        listeners: {
                            'beforerender' : {
                                fn: function() {
                                    var thisPanel = this;
                                    Ext.Ajax.request({
                                        url: '/admin/users/getuserdetails',
                                        method: 'post',
                                        params: {userId : userId},
                                        success: function(result, request) {
                                            var userDetailsRes =  Ext.util.JSON.decode(result.responseText);
                                            var userDetails = userDetailsRes.userdetails[0];
                                            username = userDetails.username;

                                            thisPanel.setTitle('Profile of ' + username);
                                            thisPanel.add(
                                                {
                                                    xtype:'box'
                                                    ,anchor:''
                                                    ,autoEl:{
                                                        tag:'div', children:[{
                                                             tag:'img'
                                                            ,src: '/' + userDetails.picture
                                                            ,height: '200',
                                                            width: '200'
                                                        }]
                                                    }
                                                }, {
                                                    fieldLabel: 'Username',
                                                    value: userDetails.username
                                                }, {
                                                    fieldLabel: 'First Name',
                                                    value: userDetails.firstname
                                                }, {
                                                    fieldLabel: 'Last Name',
                                                    value: userDetails.lastname
                                                }, {
                                                    fieldLabel: 'Date of Birth',
                                                    value: userDetails.date_birth
                                                }, {
                                                    fieldLabel: 'Gender',
                                                    value: userDetails.gender
                                                }, {
                                                    fieldLabel: 'Phone',
                                                    value: userDetails.phone
                                                }, {
                                                    fieldLabel: 'Mobile Phone',
                                                    value: userDetails.mobile_phone
                                                }, {
                                                    fieldLabel: 'Email',
                                                    value: userDetails.email
                                                }, {
                                                    fieldLabel: 'Country',
                                                    value: userDetails.country
                                                }, {
                                                    fieldLabel: 'Homepage',
                                                    value: userDetails.homepage
                                                }, {
                                                    fieldLabel: 'ICQ',
                                                    value: userDetails.icq
                                                }, {
                                                    fieldLabel: 'MSN',
                                                    value: userDetails.msn
                                                }
                                            );
                                            thisPanel.doLayout();
                                        }
                                    });
                                }
                            }
                        }
                    }, {
                        title: 'Last Logins from ',
                        frame: true,
                        cls: 'panelMargin',
                        listeners: {
                            'beforerender': {
                                fn: function() {
                                    var thisPanel = this;
                                    Ext.Ajax.request({
                                        url: '/admin/users/getlatestlogins',
                                        method: 'post',
                                        params: {userId : userId, limit : 20},
                                        success: function(result, request) {
                                            var response =  Ext.util.JSON.decode(result.responseText);

                                            var logins = new Array();
                                            for(var i=0; i<response.records.length; i++) {
                                                logins.push({
                                                    html: '<div class="lastLogin">' + response.records[i].login_dt + '</div>'
                                                });
                                            }

                                            thisPanel.setTitle('Last Logins from ' + username);
                                            thisPanel.add(logins);
                                            thisPanel.doLayout();
                                        }
                                    });
                                }
                            }
                        }
                    }
                ]
            });

            Wd.panels.userdetails.superclass.initComponent.apply(this,arguments);
        }
});

Ext.reg('wdbuserdetails', Wd.panels.userdetails);
Ext.ns('Wd.panels');

Wd.panels.backendNavibox = Ext.extend(Ext.Panel,{
        initComponent : function(){
            Ext.apply(this,{
                title       :   LANG.PNL_BACKEND_MANAGEMENT ? LANG.PNL_BACKEND_MANAGEMENT : 'Backend Managemnet' ,
                autoHeight  :   true,
                collapsible :   true,
                collapsed   :   true,
                titleCollapse : true,
                defaults    :   {
                    width       :   '100%',
                    xtype       :   'cbbutton',
                    iconAlign   :   'left',
                    listeners   :   {
                        'click'     :   {
                            fn  :   function() {
                                var ctrregion = Ext.getCmp('centerregion');
                                var removePrevious = true;
                                switch (this.id) {
                                    case 'userlist'         :   component = new Wd.grids.users({id:'usergrid'}); break;
                                    case 'createuser'       :   component = new Wd.forms.users();       break;
                                    case 'designgroup'      :   component = new Wd.grids.groups();      break;
                                    case 'listnavibox'      :   component = new Wd.grids.navibox();     break;
                                    case 'provisionschemes'      :   component = new CB.grids.ProvisionSchemes();     break;
                                    case 'newsformBtn'      :
                                        removePrevious = false;
                                        new Ext.Window({
                                            title       :   'Create News',
                                            width       :   550,
                                            modal       :   true,
                                            closable    :   false,
                                            items       :   [
                                                new Wd.forms.news({
                                                    border  :   false
                                                })
                                            ]
                                        }).show();
                                        break;
                                }
                                if (removePrevious == true) {
                                    ctrregion.items.items[2].removeAll();
                                    ctrregion.items.items[2].add(component);
                                    ctrregion.items.items[2].doLayout();
                                }
                            }
                        }
                    }
                },

                items   :   [
                    {
                        text        :   LANG.BTN_BACKEND_NEWS ? LANG.BTN_BACKEND_NEWS :'Backend News',
                        id          :   'newsformBtn',
                        icon        :   '/img/icons/page.png',
                        menuBtn     :   true
                    },
                    {
                        text        :   LANG.BTN_CHANGE_GROUP ? LANG.BTN_CHANGE_GROUP : 'Change Group',
                        id          :   'designgroup',
                        icon        :   '/img/icons/group.png',
                        menuBtn     :   true
                    },
                    {
                        text        :   LANG.BTN_NAVIGATION_BOXES ? LANG.BTN_NAVIGATION_BOXES : 'Navigation Boxes',
                        id          :   'listnavibox',
                        icon        :   '/img/icons/application_double.png',
                        menuBtn     :   true
                    },
                    {
                        text        :   LANG.BTN_USER_LIST ? LANG.BTN_USER_LIST :'User List',
                        id          :   'userlist',
                        icon        :   '/img/icons/user.png',
                        menuBtn     :   true
                    },
                    {
                        text        :   'Provision Schemes',
                        id          :   'provisionschemes',
                        icon        :   '/img/icons/folder_page.png',
                        menuBtn     :   true
                    }
                ]
            });

            Wd.panels.backendNavibox.superclass.initComponent.apply(this,arguments);
        }
});

Ext.reg('wdpanelsbackendnavibox', Wd.panels.backendNavibox);
Ext.ns('Wd.panels');

Wd.panels.homeStatistics   =   Ext.extend(Ext.Panel, {
    initComponent : function () {Ext.apply(this, Ext.apply(this.initialConfig, {
        title       :   LANG.PNL_STATISTICS ? LANG.PNL_STATISTICS : 'Statistics',
        cls         :   'panelMargin',
        autoHeight  :   true,
        contentEl   :   'stat'
    }));
    Wd.panels.homeStatistics.superclass.initComponent.apply(this,arguments);
}});

Ext.reg('wdpanelhomestatistics', Wd.panels.homeStatistics);

Ext.ns('Wd.panels');
Wd.panels.Skel = Ext.extend(Ext.Panel,{
        initComponent : function(){
            Ext.apply(this,{});

            Wd.panels.Skel.superclass.initComponent.apply(this,arguments);
        }
});
Ext.reg('wdpanelskel', Wd.panels.Skel);

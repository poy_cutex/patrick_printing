Ext.ns('Wd.panels');

Wd.panels.navibox = Ext.extend(Ext.Container,{
        initComponent : function(){
            Ext.apply(this,{
                layout: 'column',
                items: [
                    new Wd.grids.navibox({
                        cls: 'panelMargin',
                        id: 'naviboxGrid',
                        autoScroll: true,
                        height: 200,
                        sm : new Ext.grid.RowSelectionModel({
                            singleSelect: true,
                            listeners: {
                                rowselect: function(sm, row, rec) {
                                    Ext.getCmp('naviboxForm').getForm().loadRecord(rec);
                                }
                            }
                        })
                    }),
                    new Wd.forms.navibox({
                        id: 'naviboxForm',
                        listeners: {
                            'beforerender': {
                                fn: function() {
                                    Ext.apply(Ext.getCmp('saveboxBtn'), {
                                        handler: function() {
                                        var thisForm = this.findParentByType('wdformnavibox').getForm();
                                        thisForm.submit({
                                            method:'POST',
                                            waitTitle:'Please wait...',
                                            waitMsg:'Saving data...',
                                            url: '/private/savenavibox/',
                                            clientValidation: true,

                                            success: function(form, action) {
                                                Ext.Msg.alert('Status', action.result.msg, function(btn) {
                                                    if (btn == 'ok') {
                                                       Ext.getCmp('naviboxGrid').getStore().reload();
                                                    }
                                                });
                                            },

                                            failure: function(form, action) {
                                                //console.log(action.result);
                                                if(action.failureType == 'server') {
                                                    obj = Ext.util.JSON.decode(action.response.responseText);
                                                    Ext.Msg.alert('Error!', obj.errors.reason);
                                                } else {
                                                    Ext.Msg.alert('Warning!', 'Authentication server is unreachable : ' + action.response.responseText + "abcd");
                                                }

                                                thisForm.reset();
                                            }
                                        });
                                    }
                                    });
                                }
                            }
                        }
                    })
                ]
            });

            Wd.panels.navibox.superclass.initComponent.apply(this,arguments);
        }
});

Ext.reg('wdpanelsnavibox', Wd.panels.navibox);
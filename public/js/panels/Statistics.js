Ext.ns('Wd.panels');

Wd.panels.Statistics = Ext.extend(Ext.Panel,{
        tplData : {id:'tplData'},
        initComponent : function(){
            var data = this.tplData;    
            Ext.apply(this,{
                title : 'Statistics',
                collapsed: false,
                height : 120,
                style : 'padding:0px 5px 5px 10px;',
                items : [
                    {
                        data : this.tplData,
                        baseCls : '',
                        style   : 'padding : 10px',
                        tpl : new Ext.XTemplate(
                            '<table>'+
                            '<tr><td width="100">Total Logins</td><td>:</td><td style="padding-left:5px"> {totalLogin}</td><tr>'+
                            '<tr><td>Registered on <td>:</td></td><td style="padding-left:5px"> {registeredDate}</td></tr>' +
                            '<tr><td>Position </td><td>:</td><td style="padding-left:5px">{group}</td></tr>'+
                            '</table>'
                            ),
                        listeners : {
                            beforerender : function(){
                                var tpl = this.tpl;
                                var pnl = this;
                                Ext.Ajax.request({
                                   url: '/default/private/stat',
                                   success: function(res){
                                        var tplData = Ext.decode(res.responseText);
                                        tpl.overwrite(pnl.body,tplData);
                                   },
                                   failure: function(){}
                                });
                            }
                        }
                    }
                ]
            });
            Wd.panels.Statistics.superclass.initComponent.apply(this,arguments);
        }
});
Ext.reg('wdpanelstatistics', Wd.panels.Statistics);

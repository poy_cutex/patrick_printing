Ext.ns('Wd.panels');

Wd.panels.faq = Ext.extend(Ext.Container,{
        initComponent : function(){
            Ext.apply(this,{
                autoScroll: true,
                items: [
                    new Wd.grids.faq(),
                    new Wd.grids.faq_deleted()
                ]
            });

            Wd.panels.faq.superclass.initComponent.apply(this,arguments);
        }
});

Ext.reg('wdpanelsfaq', Wd.panels.faq);
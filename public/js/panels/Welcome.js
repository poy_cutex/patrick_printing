Ext.ns('Wd.panels');
Wd.panels.Welcome = Ext.extend(Ext.Panel,{
        initComponent : function(){
            Ext.apply(this,{
                title : '',
                baseCls : '',
                style : 'padding: 0px 0px 15px 5px',
                collapsed : false,
                collapsible : false,  
                items : [
                {
                        baseCls : '',     
                        id      :   'welcomeContainer',
                        autoEl  :   {
                        tag     :   'div',
                        html    :   '<h3>' +( LANG.HDR_WELCOME_ADMIN_PAGE ? LANG.HDR_WELCOME_ADMIN_PAGE : 'Welcome to the Administration Page' ) +'</h3>'
                        }
                }
                ]
            });
            Wd.panels.Welcome.superclass.initComponent.apply(this,arguments);
        }
});
Ext.reg('wdpanelwelcome', Wd.panels.Welcome);

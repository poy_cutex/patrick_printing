Ext.ns('Wd.panels');

var translationgroupstore = new Ext.data.JsonStore({
    xtype: 'jsonstore',
    url: '/admin/combo/translation-groups',
    root: 'records',
    fields: ['id', 'name'],
    autoLoad: false
});

Wd.panels.Translation = Ext.extend(Ext.Panel,{
    initComponent : function(){
        var cfg = {
                loadMask : true,
                items :[],
                listeners : {
                    beforerender : function(panel){
                         Ext.Ajax.request({
                            url: '/admin/translation/get-table',
                            success: function(res){
                                var editor = new Ext.ux.grid.RowEditor({
                                            saveText: 'Update'
                                        });
                                cfg = Ext.decode(res.responseText);
                                var checkboxSel = new Ext.grid.CheckboxSelectionModel();
                               // cfg.fields.unshift(checkboxSel);
                                var initCfg = {
                                        id  :'dynamic_language_table',
                                        autoScroll: true,
                                        sm: checkboxSel,
                                        store : new Ext.data.JsonStore({
                                            url  : '/admin/translation/get-records',
                                            root : 'records',
                                            fields : cfg.fields,
                                            autoLoad : true,
                                            listeners : {
                                                
                                                load : function(){
                                                    this.commitChanges();
                                                }
                                            }
                                        }),
                                        tbar : [
                                            {xtype:'button',
                                            icon :'/img/icons/world_add.png',
                                            text:'Add New Keyword',
                                            handler : function(){
                                                egrid = Ext.getCmp('dynamic_language_table');
                                                var LangRecord = Ext.data.Record.create([
                                                    cfg.fields
                                                ]);
                                                var recCfg = {};
                                                     Ext.each(cfg.fields,function(item){
                                                        switch(item)
                                                        {case  'kid' :
                                                             case  'kw' :
                                                             case  'us' :
                                                             case  'group_id' :
                                                             case  'group_name' :
                                                                break;
                                                            default:
                                                            recCfg[item] = '-';
                                                        }
                                                    });
                                                var rec = new LangRecord(recCfg);
                                                editor.stopEditing();
                                                egrid.getStore().insert(0, rec);
                                                egrid.getView().refresh();
                                                egrid.getSelectionModel().selectRow(0);
                                                editor.startEditing(0);
                                            }
                                            },'-',
                                            {
                                                text : 'Save Modified Rows',
                                                icon :'/img/icons/disk.png',
                                                handler : function(){
                                                    var grid = panel.get(0);
                                                    grid.onSave();
                                                }
                                            },'-',
                                            {
                                                text : 'Delete Selected Rows',
                                                icon :'/img/icons/world_delete.png',
                                                handler : function(){
                                                    var selectedRows = panel.get(0).getSelectionModel().getSelections();
                                                    var grid = panel.get(0);
                                                    var params = [];
                                                    Ext.Msg.show({
                                                        title:'Delete Row/s?',
                                                        msg: 'You are about to delete the selected rows, are you sure?',
                                                        buttons: Ext.Msg.OKCANCEL,
                                                        fn: function(){
                                                            Ext.each(selectedRows,function(item){
                                                                params.push(item.data.kid);
                                                            })
                                                            Ext.Ajax.request({
                                                                url: '/admin/translation/delete-rows',
                                                                success: function(){
                                                                    grid.getStore().reload();

                                                                    var tree = Ext.getCmp('langGroupsTree');
                                                                    if(tree != null) {
                                                                        var rootNode = tree.getRootNode();
                                                                        tree.getLoader().load(rootNode);
                                                                        rootNode.expand();
                                                                    }
                                                                },
                                                                failure: function(){
                                                                    alert('palpaks');
                                                                },
                                                                params: {ids: Ext.encode(params)}
                                                            });
                                                        },
                                                        animEl: 'elId',
                                                        icon: Ext.MessageBox.QUESTION
                                                     });
                                                },
                                                scope : this 
                                            }
                                        ],
                                        listeners: {
                                            beforerender: function(thisCmp) {
                                                translationgroupstore.load();

                                                var colModel = thisCmp.getColumnModel();
                                                var groupNameColIndex = colModel.getIndexById('group_name');
                                                var categoryEditor = {
                                                    xtype: 'combo',
                                                    allowBlank: 'false',
                                                    store: translationgroupstore,
                                                    displayField: 'name',
                                                    valueField: 'id',
                                                    hiddenValue: 'id',
                                                    hiddenName: 'gid',
                                                    triggerAction: 'all',
                                                    id: 'categoryEditor'
                                                }

                                                colModel.setEditor(groupNameColIndex, categoryEditor);
                                                colModel.setRenderer(groupNameColIndex, function(value){
                                                    index = translationgroupstore.find('id', value);
                                                    if (index == -1) return value;

                                                    rec = translationgroupstore.getAt(index);
                                                    return rec.get('name');
                                                });
                                                
                                            }
                                        },
                                        plugins: [
                                            new Ext.ux.grid.Search({
                                                iconCls:'',
                                                position: 'top',
                                                mode: 'local',
                                                width: 150
                                            }), 
                                            editor
                                        ],
                                        onSave : function() {
                                            var grid = this;
                                            var modified =this.getStore().getModifiedRecords(); 
                                            if (modified.length > 0) {
                                                var recordsToSend = [];
                                                Ext.each(modified, function(record) {            
                                                    recordsToSend.push(record.data);
                                                });
                                                this.stopEditing();
                                                recordsToSend = Ext.encode(recordsToSend);       
                                                Ext.Ajax.request({
                                                    url     : '/admin/translation/insert-update',
                                                    params : {
                                                        recordsToInsertUpdate : recordsToSend
                                                    },
                                                    success : function(response) {
                                                        res = Ext.decode(response.responseText);
                                                        if(res.success===false){
                                                            Ext.Msg.show({
                                                               title:'Save Changes?',
                                                               msg: '[ '+res.e + ' ] Invalid Keyword found, accepted prefixes are BTN,FAQ<br>(More will be added later ...)',
                                                               buttons: Ext.Msg.OK,
                                                               fn : function(ans){
                                                                    if(ans=='ok') {
                                                                    //console.log(ans);
                                                                    }
                                                               },
                                                               animEl: 'elId',
                                                               icon: Ext.MessageBox.ERROR
                                                            });
                                                        }

                                                        var tree = Ext.getCmp('langGroupsTree');
                                                        if(tree != null) {
                                                            var rootNode = tree.getRootNode();
                                                            tree.getLoader().load(rootNode);
                                                            rootNode.expand();
                                                        }

                                                        grid.getStore().commitChanges();
                                                        grid.getStore().reload();
                                                    }
                                                });
                                            }
                                 
                                        }
                                                
                                    };
                                panel.add(new Ext.grid.EditorGridPanel(Ext.apply(initCfg,cfg)));                              
                                panel.doLayout();
                            },
                            failure: function(){}
                         });
            
                    } 
    } // eo initComponent()
                
            }
        Ext.apply(this,cfg);

        Wd.panels.Translation.superclass.initComponent.apply(this,arguments);
    }
});
Ext.reg('wdpaneltranslation', Wd.panels.Translation);
Ext.ns('Wd.panels');

manageTranslationGroups = function(anElement) {
    var tree = Ext.getCmp('langGroupsTree');
    var rootNode = tree.getRootNode();
    var selectedNode = tree.getSelectionModel().getSelectedNode();
    selectedNode = (selectedNode == null) ? tree.getRootNode() : selectedNode;
    var editSelectedNode = true;

    var treeEditor = Ext.getCmp('langGroupTreeEditor');

    if(anElement.itemAction == 'add') {
        var node = rootNode.appendChild(new Ext.tree.TreeNode({
            text:'New Group',
            allowDrag:false,
            leaf: false,
            cls:'x-tree-node-collapsed'
        }));
        tree.getSelectionModel().select(node);
        selectedNode = node;
    } else if(anElement.itemAction == 'delete') {
        editSelectedNode = false;
        Ext.Msg.show({
            title:'Delete',
            msg: 'Are you sure you want to delete this group?',
            buttons: Ext.Msg.OKCANCEL,
            fn: function(btn){
                if(btn=='ok')
                    Ext.Ajax.request({
                        url : '/admin/translation/delete-group',
                        params: { id: selectedNode.id },
                        success: function ( result, request ) {
                            var jsonData = Ext.util.JSON.decode(result.responseText);
                            if(jsonData.success == false) {
                                Ext.Msg.show({
                                    title:'Delete Failed!',
                                    msg: 'This group is not empty.',
                                    buttons: Ext.Msg.OK,
                                    fn : function(ans){
                                         if(ans=='ok') {
                                         //console.log(ans);
                                         }
                                    },
                                    animEl: 'elId',
                                    icon: Ext.MessageBox.ERROR
                                });
                            } else {
                                tree.getLoader().load(rootNode);
                                rootNode.expand();
                                Ext.getCmp('categoryEditor').getStore().reload();
                            }
                        },
                        failure: function ( result, request ) {
                            var jsonData = Ext.util.JSON.decode(result.responseText);
                            var resultMessage = jsonData.data.result;
                        }
                    });
            },
            animEl: 'elId',
            icon: Ext.MessageBox.QUESTION
        });
    }

    if(editSelectedNode) {
        setTimeout(function(){
            treeEditor.editNode = selectedNode;
            treeEditor.startEdit(selectedNode.ui.textNode);
        }, 10);
    }
}



Wd.panels.systemtranslation = Ext.extend(Ext.Container,{
        initComponent : function(){
            this.treeTb = new Ext.Toolbar({
                items:[{
                    text: LANG.BTN_NEW_GROUP ? LANG.BTN_NEW_GROUP : 'New Group',
                    icon: '/img/icons/tag_blue_add.png',
                    itemAction: 'add',
                    handler: function(thisBtn){
                        manageTranslationGroups(thisBtn);
                    }
                }]
            });

            Ext.apply(this,{
                id: 'langGroupsPanel',
                autoScroll: true,
                autoWidth: true,
                frame: true,
                items: [
                    new Ext.Panel({
                        title: LANG.PNL_TRANSLATION_SYSTEM ? LANG.PNL_TRANSLATION_SYSTEM : 'Translation System',
                        layout: 'border',
                        height: 400,
                        style : 'padding : 0px 5px 5px 10px;margin-top:0px;margin-bottom:10px',
                        items: [
                            new Ext.tree.TreePanel({
                                // tree
                                id: 'langGroupsTree',
                                animate:true,
                                enableDD:false,
                                containerScroll: true,
                                rootVisible: true,
                                root: {
                                    id: '0',
                                    nodeType: 'async',
                                    text: 'Groups',
                                    draggable: false,
                                    expanded: true,
                                    editable: false
                                },
                                loader: new Ext.tree.TreeLoader({dataUrl: '/admin/translation/gettreechildren'}),
                                contextMenu: new Ext.menu.Menu({
                                    addItem: {
                                        id: 'addGroupCntxt',
                                        text: LANG.BTN_ADD ? LANG.BTN_ADD : 'Add',
                                        itemAction: 'add',
                                        icon: '/img/icons/tag_blue_add.png'
                                    },
                                    editItem: {
                                        id: 'editGroupCntxt',
                                        text: LANG.BTN_EDIT ? LANG.BTN_EDIT : 'Edit',
                                        itemAction: 'edit',
                                        icon: '/img/icons/tag_blue_edit.png'
                                    },
                                    deleteItem: {
                                        id: 'deleteGroupCntxt',
                                        text: LANG.BTN_DELETE ? LANG.BTN_DELETE : 'Delete',
                                        itemAction: 'delete',
                                        icon: '/img/icons/tag_blue_delete.png'
                                    },
                                    listeners: {
                                        itemclick: function(item) {
                                            manageTranslationGroups(item);
                                        }
                                    }
                                }),
                                listeners: {
                                    click: {
                                        fn: function(thisNode) {
                                            var groupId = null;
                                            var keywordId = null;
                                            if(!thisNode.isLeaf()) {
                                                thisNode.expand();
                                                groupId = thisNode.id;
                                            } else {
//                                                var translationTable = Ext.getCmp('dynamic_language_table');
//                                                var selectRow = translationTable.getStore().find('kid', thisNode.id);
//                                                translationTable.getSelectionModel().selectRow(selectRow);
                                                keywordId = thisNode.id
                                            }

                                            var translationGridStore = Ext.getCmp('dynamic_language_table').getStore();
                                            translationGridStore.removeAll();
                                            translationGridStore.reload({
                                                params: {
                                                    groupId: groupId,
                                                    keywordId: keywordId
                                                }
                                            });
                                        }
                                    },
                                    contextmenu: function(node, e) {
                                        if(!node.isLeaf()) {
                                            node.select();
                                            var c = node.getOwnerTree().contextMenu;
                                            c.removeAll();
                                            c.add(c.addItem);
                                            if(node.id > '0') {
                                                c.add(c.editItem);
                                                c.add(c.deleteItem);
                                            }

                                            c.contextNode = node;
                                            c.showAt(e.getXY());
                                        }
                                    },
                                    beforerender: function(thisCmp) {
                                        new Ext.tree.TreeEditor(thisCmp, { allowBlank: false }, {
                                            id: 'langGroupTreeEditor',
                                            allowBlank:false,
                                            blankText:'A name is required',
                                            selectOnFocus:true,
                                            cancelOnEsc: true,
                                            listeners: {
                                                complete: function(editor, value) {
                                                    Ext.Ajax.request({
                                                        url : '/admin/translation/save-group',
                                                        params: { id: editor.editNode.id, name: value },
                                                        success: function ( result, request ) {
                                                            Ext.getCmp('categoryEditor').getStore().reload();
                                                            var nodeId = editor.editNode.id.slice(0, (editor.editNode.id.slice.length - 1));
                                                            if(nodeId > 0) {
                                                                Ext.getCmp('dynamic_language_table').getStore().reload();
                                                            }
                                                        },
                                                        failure: function ( result, request ) {
                                                            var jsonData = Ext.util.JSON.decode(result.responseText);
                                                            var resultMessage = jsonData.data.result;
                                                        }
                                                    });

                                                    var rootNode = thisCmp.getRootNode();
                                                    thisCmp.getLoader().load(rootNode);
                                                    rootNode.expand();
                                                }
                                            }
                                        });
                                    }
                                },

                                // layout
                                region:'west',
                                width:150,
                                split: true,

                                // panel
                                autoScroll:true,
                                tbar: this.treeTb
                            }),
                            new Wd.panels.Translation({
                                region: 'center'
                            })
                        ]
                    })
                    
                ]
            });

            Wd.panels.systemtranslation.superclass.initComponent.apply(this,arguments);
        }
});

Ext.reg('wdpanelssystemtranslation', Wd.panels.systemtranslation);
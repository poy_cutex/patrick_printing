Ext.ns('Wd.panels');

Wd.panels.passwordsettings = Ext.extend(Ext.Container,{
        initComponent : function(){
            Ext.apply(this,{
                style               : 'padding : 0px 5px 5px 10px;margin-top:0px;margin-bottom:10px',
                items: [
                    {
                        title: 'System Einstellungen / System Settings',
                        frame: true,
                        labelWidth: 90,
                        cls: 'panelMargin',
                        items: [
                            {
                                xtype: 'button',
                                text: 'Grundeinstellungen / Basic Settings',
                                cls: 'button-link',
                                handler: function() {
                                    new Ext.Window({
                                        title: 'Basic Settings',
                                        modal: true,
                                        resizable: false,
                                        closable: false,
                                        width: 320,
                                        items: [ new Wd.forms.passwordsettings() ]
                                    }).show();
                                }
                            }, {
                                xtype: 'button',
                                text: 'Mailtext Anmeldung',
                                cls: 'button-link',
                                handler: function() {
                                    new Ext.Window({
                                        title: 'Mail Settings',
                                        modal: true,
                                        resizable: false,
                                        closable: false,
                                        width: 440,
                                        items: [ new Wd.forms.mailsettings() ]
                                    }).show();
                                }
                            }, {
                                xtype: 'button',
                                text: 'alle passwörter andern / change all Passwords',
                                handler: function() {
                                    Ext.Msg.confirm('Password Reset', 'Are you sure you want to reset ALL passwords?', function(btn){
                                        if (btn == 'yes') {
                                            Ext.Msg.confirm('Password Reset', 'Do you really want to reset ALL passwords?', function(btn){
                                                if(btn == 'yes') {
                                                    Ext.Ajax.request({
                                                        url: '/admin/groups/resetallpasswords',
                                                        success: function(result, request) {
                                                            
                                                        },
                                                        failure: function() {

                                                        }
                                                    });
                                                }
                                            });
                                        }
                                    });
                                }
                            }
                        ]
                    }
                ]
            });

            Wd.panels.passwordsettings.superclass.initComponent.apply(this,arguments);
        }
});

Ext.reg('wdpasswordsettings', Wd.panels.passwordsettings);
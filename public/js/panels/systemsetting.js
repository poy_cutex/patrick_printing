Ext.ns('Wd.panels');

Wd.panels.systemSettingNavibox = Ext.extend(Ext.Panel,{
        initComponent : function(){
            Ext.apply(this,{
                title       :   LANG.PNL_SYSTEM_SETTING ? LANG.PNL_SYSTEM_SETTING :'System Settings',
                collapsed   :   true,
                collapsible :   true,
                titleCollapse : true,
                autoHeight  :   true,
                defaults    :   {
                    width       :   '100%',
                    xtype       :   'cbbutton',
                    iconAlign   :   'left',
                    listeners   :   {
                        'click' :   {
                            fn  :   function() {
                                var ctrregion = Ext.getCmp('centerregion');
                                var removePrevious = true;
                                switch(this.id) {
                                    case 'passwordsettings'     :   component = new Wd.panels.passwordsettings();       break;
                                    case 'translations'         :   component = new Wd.grids.translations();            break;
                                    case 'translationsMgr'      :   component = new Wd.panels.systemtranslation();      break;
                                }

                                if(removePrevious == true) {
                                    ctrregion.items.items[2].removeAll();
                                    ctrregion.items.items[2].add(component);
                                    ctrregion.items.items[2].doLayout();
                                }
                            }
                        }
                    }
                },
                items   :   [
                    {
                        text    :   LANG.BTN_SYSTEM_SETTING ? LANG.BTN_SYSTEM_SETTING :'System Setting',
                        id      :   'passwordsettings',
                        icon    :   '/img/icons/key.png',
                        menuBtn :   true
                    },
                    {
                        text    :   LANG.BTN_TRANSLATION_SYSTEM ? LANG.BTN_TRANSLATION_SYSTEM :'Translation System',
                        id      :   'translationsMgr',
                        icon    :   '/img/icons/help.png',
                        menuBtn :   true
                    }
                ],
                collapsible :   true
            });

            Wd.panels.systemSettingNavibox.superclass.initComponent.apply(this,arguments);
        }
});

Ext.reg('wdpanelssystemsettingnavibox', Wd.panels.systemSettingNavibox);
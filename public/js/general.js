function get_time() {
	var d = new Date();
	var monthname = new Array("January","February","March","April","May","June","July","August","September","October","November","December")
	var gmtHours = (-1) * (d.getTimezoneOffset()/60);
	var gmtLabel = '';

	var txt_month = monthname[d.getMonth()];
	var txt_date = d.getDate();
	var txt_year = d.getFullYear();

	var txt_hour = d.getHours();
	var txt_min  = d.getMinutes();
	var txt_sec  = d.getSeconds();
	var txt_TOD = ''
	if (txt_hour >= 12) {
		txt_TOD = 'PM'
		txt_hour -= 12;
	}
	else {
		txt_TOD = 'AM'
	}
	if (txt_hour == 0) txt_hour = 12;
	if (txt_hour < 10) txt_hour = '0' + txt_hour;
	if (txt_min < 10) txt_min = '0' + txt_min;
	if (txt_sec < 10) txt_sec = '0' + txt_sec;
	if (gmtHours < 10) gmtHours = '0' + gmtHours;

	if (gmtHours == 0) gmtHours = '';
	if (gmtHours <  0) gmtLabel = '-';
	if (gmtHours >  0) gmtLabel = '+';

	return txt_month + ' ' + txt_date + ', ' + txt_year + ' | ' + txt_hour + ':' + txt_min + ':' + txt_sec + ' ' + txt_TOD + '   (GMT' + gmtLabel + gmtHours + ')';
}

function trim (str){
	return str.replace(/^\s+|\s+$/g, '');
}

function setInnerHTML(text, id) {
	try {
		document.getElementById(id). innerHTML = text;
	} catch (e) {}
}

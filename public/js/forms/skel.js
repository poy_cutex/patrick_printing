Ext.ns('Wd.forms');

/*Template for form*/

Wd.forms.skel = Ext.extend(Ext.form.FormPanel,{
        initComponent : function(){
            Ext.apply(this,{});

            Wd.forms.login.superclass.initComponent.apply(this,arguments);
        }
});
Ext.reg('wdskelform', Wd.forms.skel);

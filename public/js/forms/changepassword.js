Ext.ns('Wd.forms');

Wd.forms.changepassword = Ext.extend(Ext.form.FormPanel,{
        initComponent : function(){
            Ext.apply(this,{
                id: 'changepasswordForm',
                frame: true,
                labelWidth: 130,
                defaults: {
                    xtype: 'textfield',
                    inputType: 'password'
                },
                items: [
                    {
                        fieldLabel: 'Current Password',
                        name: 'curPassword',
                        allowBlank: false,
                        anchor: '100%'
                    }, {
                        fieldLabel: 'New Password',
                        name: 'newPassword',
                        id: 'newPassword',
                        allowBlank: false,
                        vtype: 'password',
                        anchor: '100%'
                    }, {
                        fieldLabel: 'Confirm New Password',
                        name: 'newPasswordConf',
                        initialPasswordField: 'newPassword',
                        allowBlank: false,
                        vtype: 'password',
                        anchor: '100%'
                    }
                ],
                buttons: [
                    {
                        text: 'Logout',
                        handler: function() {
                            window.location = '/authentication/logout';
                        }
                    }, {
                        text: 'Save',
                        handler: function(b, e) {
                            var thisPanel = this.findParentByType('wdformchangepassword');
                            var thisForm = thisPanel.getForm();

                            if(thisForm.isValid()) {
                                thisForm.submit({
                                    method:'POST',
                                    waitMsg:'Please wait...',
                                    url: '/authentication/changeexpiredpassword/',
                                    clientValidation: true,

                                    success: function(form, action) {
                                        Ext.Msg.alert('Status', 'You have successfully changed your password. Please log-in again.', function(btn) {
                                            if (btn == 'ok') {
                                               window.location = '/authentication/logout';
                                            }
                                        });
                                    },

                                    failure: function(form, action) {
                                        Ext.Msg.alert('Error!', 'The password you entered is incorrect.');
                                        thisForm.reset();
                                    }
                                });
                            }
                        }
                    }
                ]
            });

            Wd.forms.changepassword.superclass.initComponent.apply(this,arguments);
        }
});

Ext.reg('wdformchangepassword', Wd.forms.changepassword);

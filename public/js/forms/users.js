Ext.apply(Ext.form.VTypes, {
    password    :   function (value, field) {
        if (field.initialPasswordField) {
            var pwd             =   Ext.getCmp(field.initialPasswordField);
            this.passwordText   =   'Confirmation does not match your intial password entry.';
            return (value == pwd.getValue());
        }
        this.passwordText = 'Passwords must be at least 5 characters, containing either a number, or a valid special character (!@#$%^&*()-_=+)';
        var hasSpecial  =   value.match(/[0-9!@#\$%\^&\*\(\)\-_=\+]+/i);
        var hasLength   =   (value.length >= 5);

        return (hasSpecial && hasLength);
    },

    passwordText    :   'Passwords must be at least 5 characters, containing either a number, or a valid special character (!@#$%^&*()-_=+)'
});

Ext.ns('Wd.forms');

Wd.forms.users = Ext.extend(Ext.form.FormPanel,{
    initComponent   :   function () {
     var userForm = this;
     Ext.apply(this, Ext.apply(this.initialConfig, {
        defaults    :   {
            xtype   :   'textfield'
        },
        autoScroll  :   true,
        width       :   450,
        bodyStyle   :   'padding:15px;',
        frame       :   true,
        fileUpload  :   true,
        listeners   :   {
            'beforerender'  :   {
                fn  :   function() {
                            switch (this.formType) {
                                case 'completeUserProfile' :
                                    this.remove('username');
                                    this.remove('role');
                                    break;
                                case 'editOwnProfile' :
                                    this.remove('password');
                                    this.remove('role');
                                    this.remove('email2');

                                    this.add({
                                        xtype       :   'fileuploadfield',
                                        fieldLabel  :   LANG.FLD_PICTURE 
                                                        ? LANG.FLD_PICTURE 
                                                        :'Picture',
                                        name        :   'picture_upload',
                                        anchor      :   '100%'

                                    });

                                    this.add({
                                        xtype       :   'hidden',
                                        name        :   'MAX_FILE_SIZE',
                                        value       :   '150000'
                                    });

                                    this.insert(7, {
                                        name        :   'hide_numbers',
                                        xtype       :   'checkbox',
                                        fieldLabel  :   LANG.FLD_HIDE_PHONE_LIST 
                                                        ? LANG.FLD_HIDE_PHONE_LIST 
                                                        :'Hide phone list?',
                                        inputValue  :   '1',
                                        labelSeparator: ''
                                    });
                                    
                                    this.add({
                                        name        :   'default_lang',
                                        id          :   'default_lang',
                                        xtype       :   'combo',
                                        fieldLabel  :   LANG.FLD_DEFAULT_LANGUAGE 
                                                        ? LANG.FLD_DEFAULT_LANGUAGE 
                                                        :'Default Language',
                                        editable    :   false,
                                        store       :   new Ext.data.JsonStore({
                                            url         :   '/admin/combo/countries',
                                            root        :   'records',
                                            fields      :   ['alias', 'language']
                                        }),
                                        forceSelection  :   true,
                                        displayField    :   'language',
                                        valueField      :   'alias',
                                        hiddenValue     :   'alias',
                                        hiddenName      :   'default_language',
                                        triggerAction   :   'all',
                                        anchor          :   '100%'
                                    });
                                    
                                    this.load({
                                        url     :   '/admin/users/getuserdetails',
                                        waitMsg :   'Loading...',
                                        failure :   function(form, result) {}
                                    });
                                    break;
                            }

                            var id  =   this.findById('id').getValue();
                            if (id > 0) {
                                this.remove('email2');
                                this.get('password').show();
                                this.get('active').show();
                                this.get('static_password').show();
                            }
                            this.doLayout();
                }
            }
        },
        
        items   :   [
            {
                name    :   'id',
                id      :   'id',
                hidden  :   true
            },
            {
                fieldLabel  : LANG.FLD_USERNAME 
                              ? LANG.FLD_USERNAME 
                              : 'Username *',
                name        :   'username',
                id          :   'username',
                allowBlank  :   false,
                anchor      :   '100%'
            },
            {
                xtype       :   'textfield',
                inputType   :   'password',
                fieldLabel  :   LANG.FLD_PASSWORD 
                                ? LANG.FLD_PASSWORD 
                                : 'Password',
                name        :   'password',
                id          :   'password',
                anchor      :   '100%',
                hidden      :   true
            },
            {
                xtype       :   'checkbox',
                fieldLabel  :   'Permanent Password',
                name        :   'static_password',
                id          :   'static_password',
                anchor      :   '100%',
                hidden      :   true,
                listeners   :   {
                    'afterrender' : function(){
                        if(userForm.rec_static_password == 1){
                            this.setValue(true);
                        }
                    }
                }
            },
            {
                xtype           :   'combo',
                fieldLabel      :   LANG.FLD_GROUP 
                                    ? LANG.FLD_GROUP 
                                    : 'Group *',
                name            :   'role',
                id              :   'role',
                displayField    :   'name',
                valueField      :   'id',
                hiddenName      :   'role',
                hiddenValue     :   this.rec_group_id,
                triggerAction   :   'all',
                anchor          :   '100%',
                allowBlank      :   false,
                editable        :   false,
                store           :   new Ext.data.JsonStore({
                    url     :   '/admin/combo/groups',
                    root    :   'records',
                    fields  :   ['id','name']
                })
            },
            {
            fieldLabel  :   LANG.FLD_FIRST_NAME 
                            ? LANG.FLD_FIRST_NAME 
                            :'First name *',
            name        :   'firstname',
            allowBlank  :   false,
            anchor      :   '100%'
            },
            {
                fieldLabel  :   LANG.FLD_LAST_NAME 
                                ? LANG.FLD_LAST_NAME 
                                : 'Last name *',
                name        :   'lastname',
                allowBlank  :   false,
                anchor      :   '100%'
            },
            {
                fieldLabel  :   LANG.FLD_EMAIL 
                                ? LANG.FLD_EMAIL 
                                : 'Email *',
                name        :   'email',
                id          :   'email',
                allowBlank  :   false,
                vtype       :   'email',
                anchor      :   '100%'
            },
            {
                fieldLabel  :   LANG.FLD_VERIFY_EMAIL 
                                ? LANG.FLD_VERIFY_EMAIL 
                                : 'Verify Email *',
                name        :   'email2',
                id          :   'email2',
                vtype       :   'email',
                allowBlank  :   false,
                anchor      :   '100%'
            },
            {
                fieldLabel  :   LANG.FLD_PHONE 
                                ? LANG.FLD_PHONE 
                                : 'Phone *',
                name        :   'phone',
                id          :   'phone',
                allowBlank  :   false,
                anchor      :   '100%'
            },
            {
                fieldLabel  :   LANG.FLD_MOBILE_PHONE 
                                ? LANG.FLD_MOBILE_PHONE 
                                :'Mobile Phone *',
                name        :   'mobile_phone',
                id          :   'mobile_phone',
                allowBlank  :   false,
                anchor      :   '100%'
            },
            {
                xtype       :   'combo',
                fieldLabel  :   LANG.FLD_GENDER 
                                ? LANG.FLD_GENDER 
                                : 'Gender *',
                name        :   'gender',
                allowBlank  :   false,
                store       :   ['Male', 'Female', 'Other'],
                anchor      :   '100%',
                value   : 'Male',
                forceSelection  :   true,
                triggerAction   :   'all'
            },
            {
                xtype       :   'datefield',
                fieldLabel  :   LANG.FLD_BIRTH_DATE 
                                ? LANG.FLD_BIRTH_DATE 
                                : 'Date of Birth *',
                allowBlank  :   false,
                name        :   'date_birth',
                anchor      :   '100%',
                format      :   'Y-m-d'
            },
            {
                xtype       :   'combo',
                fieldLabel  :   LANG.FLD_LAND 
                                ? LANG.FLD_LAND 
                                :'Land *',
                name        :   'country',
                store       :   new Ext.data.JsonStore({
                    url         :   '/admin/combo/countries',
                    root        :   'records',
                    fields      :   ['id','name']
                }),
                displayField    :   'name',
                valueField      :   'id',
                triggerAction   :   'all',
                allowBlank      :   false,
                anchor          :   '100%'
            },
            {
                fieldLabel  :   LANG.FLD_HOME_PAGE 
                                ? LANG.FLD_HOME_PAGE 
                                : 'Homepage',
                name        :   'homepage',
                anchor      :   '100%'
            },
            {
                fieldLabel  :   LANG.FLD_ICQ 
                                ? LANG.FLD_ICQ 
                                : 'ICQ',
                name        :   'icq',
                anchor      :   '100%'
            },
            {
                fieldLabel  :   LANG.FLD_MSN 
                                ? LANG.FLD_MSN 
                                : 'MSN',
                name        :   'msn',
                anchor      :   '100%'
            },
            {
                xtype       :   'combo',
                fieldLabel  :   LANG.FLD_STATUS 
                                ? LANG.FLD_STATUS 
                                : 'Status',
                name        :   'active',
                id          :   'active',
                editable    :   false,
                lazyRender  :   true,
                autoSelect  :   true,
                store       :   new Ext.data.JsonStore({
                    url         :   '/admin/combo/userstatus',
                    root        :   'records',
                    fields      :   ['id','name']
                }),
                displayField    :   'name',
                valueField      :   'id',
                hiddenValue     :   this.rec_status,
                hiddenName      :   'active',
                triggerAction   :   'all',
                anchor          :   '100%',
                hidden          :   true
            },{
                xtype       :   'cbcomboprovisionschemes',
                fieldLabel  :   'Provision Scheme',
                name        :   'scheme',
                id          :   'scheme',
                anchor      :   '100%',
                allowBlank  :   false
            },
            {
            name    :   'picture',
            id      :   'picture',
            hidden  :   true
            }
        ],

        buttons     :   [
            {
                text    : LANG.BTN_SAVE 
                          ? LANG.BTN_SAVE 
                          :'Save',
                handler :   function() {
                    Ext.Msg.wait('Please wait while the system processes your request');
                    var fp          =   this.findParentByType('wdformusers');
                    var form        =   fp.getForm();
                    var mainLayout  =   this.findParentByType('wdlayout');

                    if (form.isValid()) {
                        if (form.findField('email2') === null || form.findField('email').getValue() == form.findField('email2').getValue()) {
                            form.submit({
                                clientValidation    :   true,
                                url                 :   '/admin/users/add',
                                success             :   function() {
                                    Ext.Msg.hide();
                                    var ctrregion   =   Ext.getCmp('centerregion');
                                    if (fp.formType == 'completeUserProfile') {
                                        var disabledBtns    =   mainLayout.findBy(function(c) {
                                            return (c.menuBtn == true) && (c.alwaysShow != true);
                                        });

                                        for (var i=0; i<disabledBtns.length; i++) {
                                            disabledBtns[i].setDisabled(false);
                                        }

                                        ctrregion.remove(ctrregion.get(1));
                                        ctrregion.insert(1, new Wd.grids.users({id: 'usergrid'}));
                                        ctrregion.doLayout();
                                    }
                                    else if (fp.formType == 'editOwnProfile') {
                                        ctrregion.remove(ctrregion.get(1));
                                        ctrregion.insert(1, new Wd.panels.userdetails());
                                        ctrregion.doLayout();
                                    }
                                    else {
                                        fp.findParentByType('window').close();
                                    }
                                    userstore.reload();
                                },
                                failure : function(frm,response) {
                                    console.log(response);
                                    if (response.result.error) {
                                        Ext.Msg.hide();
                                        Ext.Msg.alert(response.result.error);
                                        frm.findField('username').markInvalid('Username already exists');
                                    }
                                    else {
                                        Ext.Msg.alert('Error!', response.result.msg);
                                    }
                                }
                            });
                        }
                        else {
                            Ext.Msg.show({
                                title   :   'Email Mismatch',
                                msg     :   'The emails that you entered do not match',
                                buttons :   Ext.Msg.OK,
                                fn      :   function(){},
                                animEl  :   'elId',
                                icon    : Ext.MessageBox.WARNING
                            });
                            form.findField('email').markInvalid('emails do not match');
                            form.findField('email2').markInvalid('emails do not match');
                        }
                    }
                    else {
                        Ext.Msg.alert('Error!', (LANG.MSG_FILL_REQUIRED? LANG.MSG_FILL_REQUIRED : 'Please fill in required fields.'));
                        return;
                    }
                }
            }
            ,
            {
                text    :   LANG.BTN_CANCEL
                            ? LANG.BTN_CANCEL
                            :'Cancel',
                id      :   'cancelBtn',
                handler :   function() {
                    var win =   this.findParentByType('window')
                    if (win) {
                        win.close();
                    }
                    else {
                        var ctrregion = Ext.getCmp('centerregion');
                        component =  new Wd.panels.NaviboxesContainer({
                            id:'naviBoxContainersCenter',
                            pos:'C',
                            autoHeight : true,
                            collapsed : false
                        });
                        ctrregion.remove(ctrregion.get(1));
                        ctrregion.insert(1,component);
                        ctrregion.doLayout();
                    }
                },
                scope : this
            }
        ]
    }));
    Wd.forms.users.superclass.initComponent.apply(this,arguments);
}});

Ext.reg('wdformusers', Wd.forms.users);

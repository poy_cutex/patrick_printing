Ext.ns('Wd.forms');

Wd.forms.navibox = Ext.extend(Ext.form.FormPanel,{
        initComponent : function(){
            Ext.apply(this,{
                id: 'naviboxForm',
                labelWidth: 100,
                autoWidth: true,
                frame:true,
                cls: 'panelMargin',
                items: [
                    {
                        name: 'id',
                        xtype: 'hidden'
                    }, {
                        name: 'name',
                        xtype: 'textfield',
                        fieldLabel: 'Name',
                        width: 400,
                        allowBlank: false
                    }, {
                        name: 'position',
                        xtype: 'radiogroup',
                        fieldLabel: 'Position',
                        allowBlank: false,
                        width: 150,
                        items: [
                            {boxLabel: 'Left', name: 'position', inputValue: 'L'},
                            {boxLabel: 'Right', name: 'position', inputValue: 'R'}
                        ]
                    }, {
                        name: 'order',
                        xtype: 'numberfield',
                        fieldLabel: 'Order from Top',
                        width: 50,
                        minValue: 1,
                        maxValue: 99,
                        allowBlank: false,
                        allowNegative: false,
                        allowDecimals: false
                    }, {
                        name: 'activeIndicator',
                        xtype: 'radiogroup',
                        fieldLabel: 'Is Active',
                        allowBlank: false,
                        width: 150,
                        items: [
                            {boxLabel: 'Yes', name: 'activeIndicator', inputValue: '1'},
                            {boxLabel: 'No', name: 'activeIndicator', inputValue: '2'}
                        ]
                    },{
                        //xtype : 'box',
                        layout : 'column',
                        style : 'padding : 10px',
                        defaults : {
                            width : 120
                        },
                        items :[
                            {
                                column  : 1,
                                xtype : 'button',
                                text: 'Save',
                                width : 50,
                                id: 'saveboxBtn',
                                handler: function(b, e) {
                                    var thisForm = this.findParentByType('wdformnavibox');
                                    thisForm.getForm().submit({
                                        method:'POST',
                                        waitTitle:'Please wait...',
                                        waitMsg:'Saving data...',
                                        url: '/private/savenavibox/',
                                        clientValidation: true,

                                        success: function(form, action) {
                                            Ext.Msg.alert('Status', action.result.msg, function(btn) {
                                                if (btn == 'ok') {
                                                   navistore.reload();
                                                   thisForm.findParentByType('window').close();
                                                }
                                            });
                                          
                                        },

                                        failure: function(form, action) {
                                            //console.log(action.result);
                                            if(action.failureType == 'server') {
                                                obj = Ext.util.JSON.decode(action.response.responseText);
                                                Ext.Msg.alert('Error!', obj.errors.reason);
                                            } else {
                                                Ext.Msg.alert('Warning!', 'Authentication server is unreachable : ' + action.response.responseText + "abcd");
                                            }

                                            thisForm.reset();
                                        }
                                    });
                                }
                            }, {
                                column  : 1,
                                xtype : 'button',
                                text: 'Cancel',
                                width : 100,
                                handler: function() {
                                    this.findParentByType('wdformnavibox').findParentByType('window').close();
                                }
                            }
                        ]
                    }
                ]



            });
            Wd.forms.navibox.superclass.initComponent.apply(this,arguments);
        }
});

Ext.reg('wdformnavibox', Wd.forms.navibox);

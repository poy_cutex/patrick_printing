Ext.ns('Wd.forms');

Wd.forms.forgotpassword = Ext.extend(Ext.form.FormPanel,{
        initComponent : function(){
            Ext.apply(this,{
                url: '',
                labelWidth: 100,
                id: 'forgotpasswordForm',
                defaultType: 'textfield',
                bodyStyle   : 'margin : 10px;background:transparent',
                items: [
                    {
                        fieldLabel: 'E-mail',
                        name: 'email',
                        vtype: 'email',
                        allowBlank: false,
                        width: 250
                    }
                ],
                buttons: [
                    {
                        text: 'Request new Password',
                        handler: function(b, e) {
                            var thisForm = this.findParentByType('wdformforgotpassword').getForm();
                            thisForm.submit({
                                method:'POST',
                                waitMsg:'Please wait...',
                                url: '/authentication/getpassword/',
                                clientValidation: true,

                                success: function(form, action) {
                                    Ext.Msg.alert('Status', 'Your new password has been sent to your e-mail address.', function(btn) {
                                        if (btn == 'ok') {
                                           Ext.getCmp('forgotPasswordWindow').close();
                                        }
                                    });
                                },
                                
                                failure: function(form, action) {
                                    Ext.Msg.alert('Error!', 'The e-mail address you entered is not registered.');
                                    
                                    thisForm.reset();
                                }
                            });
                        }
                    }
                ]
            });

            Wd.forms.forgotpassword.superclass.initComponent.apply(this,arguments);
        }
});

Ext.reg('wdformforgotpassword', Wd.forms.forgotpassword);

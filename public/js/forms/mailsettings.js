Ext.ns('Wd.forms');

Wd.forms.mailsettings = Ext.extend(Ext.form.FormPanel,{
        initComponent : function(){
            Ext.apply(this,{
                url: '',
                labelWidth: 100,
                frame:true,
                defaults: {
                    xtype: 'textarea',
                    width: 300,
                    height: 250
                },
                listeners: {
                    'afterrender' : {
                        fn: function() {
                            var thisPanel = this;

                            thisPanel.el.mask('Loading...', 'x-mask-loading');

                            // basic settings fields
                            Ext.Ajax.request({
                                url: '/admin/settings/list-fields',
                                params: { type: 'M' },
                                success: function(result, request) {
                                    var records =  Ext.util.JSON.decode(result.responseText);
                                    for(var i=0; i<records.length; i++) {
                                        if(i==0) {
                                            thisPanel.add({
                                                xtype: 'box',
                                                autoEl: {
                                                    tag: 'span',
                                                    html: 'Variables',
                                                    cls: 'settingsLabel'
                                                }
                                            });
                                        }

                                        thisPanel.add(
                                            {
                                                name: records[i].name,
                                                fieldLabel: records[i].name,
                                                value: records[i].text_value
                                            }, {
                                                xtype: 'hidden',
                                                name: records[i].name + '_text',
                                                value: true
                                            }
                                        );
                                    }

                                    thisPanel.doLayout();
                                    thisPanel.el.unmask();
                                },
                                failure: function() {

                                }
                            });
                        }
                    }
                },
                buttons: [
                    {
                        text: 'Save',
                        handler: function(b, e) {
                            var fp = this.findParentByType('wdmailsettings');
                            var form = fp.getForm();

                            if(form.isValid()){
                                form.submit({
                                    method:'POST',
                                    waitTitle:'Please wait...',
                                    waitMsg:'Saving data...',
                                    url: '/admin/settings/save',
                                    clientValidation: true,

                                    success: function(form, action) {
                                        fp.findParentByType('window').close();
                                    },

                                    failure: function(form, action) {
                                        //console.log(action.result);
                                        if(action.failureType == 'server') {
                                            obj = Ext.util.JSON.decode(action.response.responseText);
                                            Ext.Msg.alert('Error!', obj.errors.reason);
                                        } else {
                                            Ext.Msg.alert('Warning!', 'Authentication server is unreachable : ' + action.response.responseText + "abcd");
                                        }

                                        thisForm.reset();
                                    }
                                });
                            }
                        }
                    }, {
                        text: 'Cancel',
                        handler: function() {
                            this.findParentByType('window').close();
                        }
                    }
                ]
            });

            Wd.forms.mailsettings.superclass.initComponent.apply(this,arguments);
        }
});

Ext.reg('wdmailsettings', Wd.forms.mailsettings);

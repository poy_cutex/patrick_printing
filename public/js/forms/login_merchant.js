Ext.ns('Wd.forms');

showLoginErrorMsg = function(msg, parentWindow, showTimer, intTimeout) {
    var lang = Ext.getCmp('locale').getValue();
    Ext.Ajax.request({
        url: '/default/locale/index/locale/' + ((lang == null || lang == '') ? 'us' : lang),
        success: function(response) {
            eval(response.responseText);
            Ext.Msg.show({
                title   :   LANG['PNL_LOGIN_ERROR'] ? LANG['PNL_LOGIN_ERROR'] : 'Login Error',
                msg     :   LANG[msg],
                buttons :   (showTimer == true ? null : Ext.Msg.OK),
                closable: false,
                fn      :   function(){
                    parentWindow.show();
                },
                animEl  :   'elId',
                icon    :   Ext.MessageBox.WARNING
            });

            if (showTimer) {
                for(var i=0; i<intTimeout; i++) {
                    setTimeout("Ext.MessageBox.updateText('"+LANG[msg]+" in " + (intTimeout - i) + " secs');", (i*1000));
                }
                setTimeout("Ext.MessageBox.hide();", (intTimeout * 1000));
            }
        },
        failure: function() {
            Ext.Msg.alert('Failure', 'This language is not available.');
        }
    });
    
}

Wd.forms.login = Ext.extend(Ext.form.FormPanel,{
        initComponent : function(){
            Ext.apply(this,{
                id      : 'loginForm',
                border  : false,
                frame   : false,
                width: 300,
                listeners : {
                    afterrender: function() {
                        Ext.getCmp('username').focus('',500);
                    }
                },
                defaults: {
                    labelWidth: 80,
                    anchor      : '95%'
                },
                bodyStyle   : 'margin : 10px;background:transparent',
                items   : [{
                    xtype       : 'textfield',
                    name        : 'username',
                    id          : 'username',
                    allowBlank  : false
                 },{
                    xtype       : 'textfield',
                    name        : 'password',
                    id          : 'password',
                    inputType   : 'password',
                    allowBlank  : false
                 },{
                    xtype       : 'hidden',
                    name        : 'locale',
                    id          : 'locale'
                 }],
            buttons : [{
                    id          : 'login',
                    handler     : function(){
                         var parentWindow =  this.findParentByType('window');
                         var form = this.findParentByType('wdlogin').getForm().submit({
                                clientValidation: true,
                                url: '/merchant/login',
                                success: function(form, action) {
                                    var res = Ext.util.JSON.decode(action.response.responseText);
                                    Ext.Msg.wait('Please wait while you are being redirected to the member\'s area');
                                    window.location = res.redirect
                                },
                                failure: function(form, action) {

                                    var blnShowTimer = false;
                                    switch (action.failureType) {
                                        case Ext.form.Action.CLIENT_INVALID:
                                            showLoginErrorMsg('MSG_LOGIN_FIELDS_REQUIRED', parentWindow);
                                            //form.reset();
                                            break;
                                        case Ext.form.Action.CONNECT_FAILURE:
                                            showLoginErrorMsg('MSG_LOGIN_CON_ERROR', parentWindow);
                                            //form.reset();
                                            break;
                                        case Ext.form.Action.SERVER_INVALID:
                                           var res = Ext.util.JSON.decode(action.response.responseText);
                                           var msg = '';
                                           var intTimeout = 0;
                                           switch(res.error_code){
                                               case 3 :
                                                    showLoginErrorMsg('MSG_ACCT_DISABLED', parentWindow);
                                                    break;
                                               case 2 :
                                                    showLoginErrorMsg('MSG_MAX_LOGIN', parentWindow);
                                                    break;
                                               case 1 :
                                               default  :
                                                   msg = '';

                                                   Ext.Ajax.request({
                                                        url: '/settings/get',
                                                        params: {name: 'LOGIN_TIMEOUT'},
                                                        scope: this,
                                                        success: function(result, request) {
                                                            var loginTimeout = Ext.util.JSON.decode(result.responseText);
                                                            if(loginTimeout.enabled == '1') {
                                                                blnShowTimer = true;
                                                                intTimeout = loginTimeout.value;
                                                            }

                                                            showLoginErrorMsg('MSG_LOGIN_INVALID', parentWindow, blnShowTimer, intTimeout);
                                                        },
                                                        failure: function() {

                                                        }
                                                    });
                                                    break;
                                           }
                                   }

                                   form.findField('username').reset();
                                   form.findField('username').focus();
                                   form.findField('password').reset();
                                }
                            });
                    }
                },{
                    id      : 'reset',
                    handler : function(){
                        this.findParentByType('wdlogin').getForm().reset();
                    }
                }],
                keys    :   [
                    {
                        key: [Ext.EventObject.ENTER], handler: function() {
                            Ext.getCmp('login').handler();
                        }
                    }
                ]
            });

            Wd.forms.login.superclass.initComponent.apply(this,arguments);
        }
});
Ext.reg('wdlogin', Wd.forms.login);

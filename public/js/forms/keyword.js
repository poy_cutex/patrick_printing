Ext.ns('Wd.forms');

Wd.forms.keyword = Ext.extend(Ext.form.FormPanel,{
        initComponent : function(){

            this.keyword = {
                fieldLabel: 'Keyword',
                name: 'keyword',
                id: 'keyword',
                allowBlank: false,
                width: 250,
                tabIndex: 1
            };

            this.keyword_id = {
                xtype: 'hidden',
                name: 'id',
                id: 'id'
            };

            this.keywordCb = {
                xtype: 'combo',
                fieldLabel: 'Keyword',
                id: 'keywordCb',
                store: new Ext.data.JsonStore({
                    url: '/admin/combo/keywords',
                    root: 'records',
                    fields: ['id', 'keyword']
                }),
                allowBlank: false,
                displayField: 'keyword',
                valueField: 'id',
                hiddenValue: 'id',
                hiddenName: 'id',
                triggerAction: 'all',
                editable: false
            };

            this.saveBtn = {
                xtype: 'button',
                text: 'Save',
                tabIndex: 2,
                handler: function(b, e) {
                    var thisFormPanel = this.findParentByType('wdformkeyword');
                    var thisForm = thisFormPanel.getForm();

                    if(thisForm.isValid()) {
                        thisForm.submit({
                            method:'POST',
                            waitMsg:'Please wait...',
                            url: '/admin/translation/savekeyword',
                            clientValidation: true,
                            success: function(form, action) {
                                thisFormPanel.findParentByType('window').close();
                                store.reload();
                            },

                            failure: function(form, action) {
                                thisForm.reset();
                                thisForm.findField('keyword').markInvalid(action.result['msg']);
                            }
                        });
                    }
                }
            };

            this.deleteBtn = {
                xtype: 'button',
                text: 'Delete',
                handler: function() {
                    var thisFormPanel = this.findParentByType('wdformkeyword');
                    var thisForm = thisFormPanel.getForm();

                    Ext.apply(thisFormPanel.get('keyword'), { allowBlank: true });

                    if(thisForm.isValid()) {
                        Ext.Msg.confirm('Delete', 'Are you sure you want to delete this keyword?', function(btn){
                            if (btn == 'yes') {
                                thisForm.submit({
                                    method:'POST',
                                    waitMsg:'Please wait...',
                                    url: '/admin/translation/deletekeyword',
                                    clientValidation: true,
                                    success: function(form, action) {
                                        thisFormPanel.findParentByType('window').close();
                                        store.reload();
                                    },
                                    failure: function(form, action) {
                                    }
                                });
                            }
                        });
                    }
                }
            };

            this.cancelBtn = {
                xtype: 'button',
                text: 'Cancel',
                tabIndex: 3,
                handler: function() {
                    this.findParentByType('window').close();
                }
            };

            Ext.apply(this,{
                labelWidth: 90,
                id: 'keywordForm',
                defaultType: 'textfield',
                frame: true,
                listeners: {
                    beforerender: {
                        fn: function() {
                            switch(this.formType) {
                                case 'add':
                                    this.add(this.keyword);
                                    this.addButton(this.cancelBtn);
                                    this.addButton(this.saveBtn);
                                    break;
                                case 'edit':
                                    Ext.apply(this.keyword, { fieldLabel: 'New Keyword' });
                                    this.add(this.keywordCb);
                                    this.add(this.keyword);
                                    this.addButton(this.cancelBtn);
                                    this.addButton(this.deleteBtn);
                                    this.addButton(this.saveBtn);
                                    break;
                            }

                            this.doLayout();
                        }
                    }
                }
            });

            Wd.forms.keyword.superclass.initComponent.apply(this,arguments);
        }
});

Ext.reg('wdformkeyword', Wd.forms.keyword);

Ext.ns('Wd.forms');

Wd.forms.news = Ext.extend(Ext.form.FormPanel,{
        initComponent : function(){
            Ext.apply(this,{
                url: '',
                labelWidth: 100,
                anchor: '100%',
                frame: true,
                id: 'newsForm',
                defaultType: 'textfield',
                items: [
                    {
                        xtype: 'hidden',
                        name: 'id',
                        id: 'id'
                    }, {
                        fieldLabel: 'Subject',
                        name: 'subject',
                        allowBlank: false,
                        anchor: '100%'
                    }, {
                        xtype: 'textarea',
                        name: 'description',
                        fieldLabel: 'Description',
                        allowBlank: false,
                        height: 200,
                        anchor: '100%'
                    }
                ],
                buttons: [
                    {
                        text: 'Cancel',
                        handler: function() {
                            this.findParentByType('window').close();
                        }
                    }, {
                        text: 'Save',
                        handler: function(b, e) {
                            var fp = this.findParentByType('wdformnews');
                            var form = fp.getForm();

                            if(form.isValid()){
                                form.submit({
                                    method:'POST',
                                    waitTitle:'Please wait...',
                                    waitMsg:'Saving data...',
                                    url: '/admin/news/save',
                                    clientValidation: true,
                                    success: function(form, action) {
                                        newsstore.reload();
                                        fp.findParentByType('window').close();
                                    },
                                    failure: function(form, action) {
                                        //console.log(action.result);
                                        if(action.failureType == 'server') {
                                            obj = Ext.util.JSON.decode(action.response.responseText);
                                            Ext.Msg.alert('Error!', obj.errors.reason);
                                        } else {
                                            Ext.Msg.alert('Warning!', 'Authentication server is unreachable : ' + action.response.responseText + "abcd");
                                        }

                                        thisForm.reset();
                                    }
                                });
                            }
                        }
                    }
                ]
            });

            Wd.forms.news.superclass.initComponent.apply(this,arguments);
        }
});

Ext.reg('wdformnews', Wd.forms.news);

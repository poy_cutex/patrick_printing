Ext.ns('Wd.forms');

if (!Array.prototype.indexOf)
{
  Array.prototype.indexOf = function(elt /*, from*/)
  {
    var len = this.length;

    var from = Number(arguments[1]) || 0;
    from = (from < 0)
         ? Math.ceil(from)
         : Math.floor(from);
    if (from < 0)
      from += len;

    for (; from < len; from++)
    {
      if (from in this &&
          this[from] === elt)
        return from;
    }
    return -1;
  };
}

Wd.forms.groups = Ext.extend(Ext.form.FormPanel,{
        initComponent : function(){
            Ext.apply(this,{
                url: '',
                labelWidth: 100,
                frame:true,
                cls: 'panelMargin',
                id: 'groupForm',
                defaultType: 'textfield',
                listeners: {
                    'beforerender' : {
                        fn: function() {
                            var grp = Ext.getCmp('EdtGrpFldNme').getValue();
                            var fp          =   this;
                            var groupId = this.findById('id').getValue();
                            var groupNaviboxes = '';
                            if(groupId > 0) {
                                Ext.Ajax.request({
                                    url: '/admin/groups/getgroupnaviboxes',
                                    method: 'POST',
                                    params: { groupId: groupId },
                                    success: function(result_o, request_o) {
                                        Ext.Ajax.request({
                                            url : '/private/naviboxlist',
                                            method: 'POST',
                                            scope: this,
                                            success: function ( result, request ) {
                                                groupNaviboxes = Ext.util.JSON.decode(result_o.responseText);
                                               
                                                var naviboxes = Ext.util.JSON.decode(result.responseText);
                                                var checkboxes = new Array();
                                               // console.log('naviboxes',naviboxes);
                                                for(var i=0; i<naviboxes.records.length; i++) {
                                                    checkboxes.push(new Ext.form.Checkbox({
                                                        boxLabel: naviboxes.records[i].name,
                                                        inputValue: naviboxes.records[i].box_id,
                                                        name: 'navibox' + naviboxes.records[i].box_id,
                                                        checked: (groupNaviboxes.indexOf(naviboxes.records[i].box_id) >= 0) ? true: false
                                                       ,hidden : (grp === 'Superadmin' && (naviboxes.records[i].name === 'BackEnd Management')) ? true : false
                                                    }));
                                                }

                                                fp.add(new Ext.form.CheckboxGroup({
                                                    fieldLabel: 'Navigation Boxes',
                                                     
                                                    items: checkboxes,
                                                    columns: 2,
                                                        name: 'naviboxes'
                                                }));

                                                //fp.add(new Ext.form.CheckboxGroup({
                                                //    fieldLabel: 'Startpage Information',
                                                //    items: [
                                                //        new Ext.form.Checkbox({
                                                //            boxLabel    :   'Info',
                                                //            inputValue  :   'pnl_info',
                                                //            name        :   'pnl_info',
                                                //            checked     :   (fp.rec.data.pnl_info == 1)? true:false
                                                //        }),
                                                //       new Ext.form.Checkbox({
                                                //            boxLabel    :   'Info',
                                                //            inputValue  :   'pnl_info',
                                                //            name        :   'pnl_info',
                                                //            checked     :   (fp.rec.data.pnl_info == 1)? true:false
                                                //        })
                                                //    ]}));
//                                                fp.add(new Ext.form.CheckboxGroup({
//                                                    fieldLabel: 'Startpage Information',
//                                                    items: [
//                                                        new Ext.form.Checkbox({
//                                                            boxLabel    :   'Info',
//                                                            inputValue  :   'pnl_info',
//                                                            name        :   'pnl_info',
//                                                            checked     :   (fp.rec.data.pnl_info == 1)? true:false
//                                                        }),
//                                                        new Ext.form.Checkbox({
//                                                            boxLabel    :   'Back End Management',
//                                                            inputValue  :   'pnl_backendmgt',
//                                                            name        :   'pnl_backendmgt',
//                                                            checked     :   (fp.rec.data.pnl_backendmgt == 1)? true:false
//                                                        }),
//                                                        new Ext.form.Checkbox({
//                                                            boxLabel    :   'Content Management',
//                                                            inputValue  :   'pnl_contentmgt',
//                                                            name        :   'pnl_contentmgt',
//                                                            checked     :   (fp.rec.data.pnl_contentmgt == 1)? true:false
//                                                        }),
//                                                        new Ext.form.Checkbox({
//                                                            boxLabel    :   'System Setting',
//                                                            inputValue  :   'pnl_systemsetting',
//                                                            name        :   'pnl_systemsetting',
//                                                            checked     :   (fp.rec.data.pnl_systemsetting == 1)? true:false
//                                                        })
//                                                    ],
//                                                    columns: 2,
//                                                        name: 'naviboxes'
//                                                }
//));
                                                
                                                fp.doLayout();
                                                
                                            },
                                            failure: function ( result, request ) {
                                                var jsonData = Ext.util.JSON.decode(result.responseText);
                                                var resultMessage = jsonData.data.result;
                                            }
                                        });
                                    }
                                });
                            }
                        }
                    }
                },
                items: [
                    {
                        xtype: 'hidden',
                        name: 'id',
                        id: 'id'
                    }, {
                        fieldLabel: 'Name',
                        id : 'EdtGrpFldNme',
                        name: 'name',
                        allowBlank: false,
                        width: 300
                    }, {
                        xtype: 'textarea',
                        name: 'description',
                        fieldLabel: 'Description',
                        width: 500
                    }
                ],
                buttons: [
                    {
                        text: LANG.BTN_CANCEL?  LANG.BTN_CANCEL : 'Cancel',
                        handler: function() {
                            this.findParentByType('window').close();
                        }
                    }, {
                        text: LANG.BTN_SAVE?  LANG.BTN_SAVE : 'Save',
                        id: 'savebox',
                        handler: function(b, e) {
                            var fp = this.findParentByType('wdformgroups');
                            var form = fp.getForm();

                            if(form.isValid()){
                                form.submit({
                                    method:'POST',
                                    waitTitle:'Please wait...',
                                    waitMsg:'Saving data...',
                                    url: '/private/savegroup/',
                                    clientValidation: true,

                                    success: function(form, action) {
                                         fp.findParentByType('window').close();
                                         groupstore.reload();
                                         var nvr =  Ext.getCmp('naviBoxContainerRight');
                                         if(nvr){
                                          nvr.updateNavBoxes();
                                         }
                                         var nvl = Ext.getCmp('naviBoxContainers');
                                         if(nvl){
                                          nvl.updateNavBoxes();
                                         }
                                    },

                                    failure: function(form, action) {
                                        //console.log(action.result);
                                        if(action.failureType == 'server') {
                                            obj = Ext.util.JSON.decode(action.response.responseText);
                                            Ext.Msg.alert('Error!', obj.errors.reason);
                                        } else {
                                            Ext.Msg.alert('Warning!', 'Authentication server is unreachable : ' + action.response.responseText + "abcd");
                                        }

                                        thisForm.reset();
                                       
                                    }
                                });
                            }
                        }
                    }
                ]
            });

            Wd.forms.groups.superclass.initComponent.apply(this,arguments);
        }
});

Ext.reg('wdformgroups', Wd.forms.groups);

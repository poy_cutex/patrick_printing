Ext.ns('Wd.forms');

Wd.forms.passwordsettings = Ext.extend(Ext.form.FormPanel,{
        initComponent : function(){
            Ext.apply(this,{
                url: '',
                labelWidth: 150,
                frame:true,
                id: 'groupForm',
                defaults: {
                    xtype: 'textfield',
                    width: 50
                },
                listeners: {
                    'afterrender' : {
                        fn: function() {
                            var thisPanel = this;

                            thisPanel.el.mask('Loading...', 'x-mask-loading');

                            // basic settings fields
                            Ext.Ajax.request({
                                url: '/admin/settings/list-fields',
                                params: { type: 'S' },
                                success: function(result, request) {
                                    var records =  Ext.util.JSON.decode(result.responseText);
                                    for(var i=0; i<records.length; i++) {
                                        if(i==0) {
                                            thisPanel.add({
                                                xtype: 'box',
                                                autoEl: {
                                                    tag: 'span',
                                                    html: '<br/>Variables',
                                                    cls: 'settingsLabel'
                                                }
                                            });
                                        }
                                        
                                        thisPanel.add(new Ext.Panel({
                                            layout: 'column',
                                            anchor: '100%',
                                            items: [
                                                {
                                                    layout: 'form',
                                                    columnWidth: .75,
                                                    labelWidth: 150,
                                                    items: [{
                                                        xtype: 'textfield',
                                                        width: 50,
                                                        name: records[i].name,
                                                        fieldLabel: records[i].name,
                                                        value: records[i].value
                                                    }]
                                                }, {
                                                    columnWidth: .25,
                                                    items: [{
                                                        xtype: 'checkbox',
                                                        checked: (records[i].enabled == 1 ? true : false),
                                                        boxLabel: 'Enabled',
                                                        name: records[i].name + '_enabled',
                                                        inputValue: '1'
                                                    }]
                                                }
                                            ]
                                        }));
                                    }

                                    thisPanel.doLayout();
                                    thisPanel.el.unmask();
                                },
                                failure: function() {

                                }
                            });
                            
                            //retrieves all groups
                            Ext.Ajax.request({
                                url: '/admin/groups/listall',
                                success: function(result, request) {
                                    var response =  Ext.util.JSON.decode(result.responseText);

                                    for(var i=0; i<response.records.length; i++) {
                                        if(i==0) {
                                            thisPanel.add({
                                                xtype: 'box',
                                                autoEl: {
                                                    tag: 'span',
                                                    html: '<br/>Password Reset Interval',
                                                    cls: 'settingsLabel'
                                                }
                                            });
                                        }
                                        
                                        thisPanel.add({
                                            name: response.records[i].id + '_g',
                                            fieldLabel: response.records[i].name,
                                            value: response.records[i].password_reset_interval_days,
                                            emptyText: '(days)'
                                        });
                                    }

                                    thisPanel.doLayout();
                                    thisPanel.el.unmask();
                                },
                                failure: function() {

                                }
                            });
                        }
                    }
                },
                buttons: [
                    {
                        text: 'Save',
                        id: 'savebox',
                        handler: function(b, e) {
                            var fp = this.findParentByType('wdpasswordsettings');
                            var form = fp.getForm();

                            if(form.isValid()){
                                form.submit({
                                    method:'POST',
                                    waitTitle:'Please wait...',
                                    waitMsg:'Saving data...',
                                    url: '/admin/settings/save',
                                    clientValidation: true,

                                    success: function(form, action) {
                                        fp.findParentByType('window').close();
                                    },

                                    failure: function(form, action) {
                                        //console.log(action.result);
                                        if(action.failureType == 'server') {
                                            obj = Ext.util.JSON.decode(action.response.responseText);
                                            Ext.Msg.alert('Error!', obj.errors.reason);
                                        } else {
                                            Ext.Msg.alert('Warning!', 'Authentication server is unreachable : ' + action.response.responseText + "abcd");
                                        }

                                        thisForm.reset();
                                    }
                                });
                            }
                        }
                    }, {
                        text: 'Cancel',
                        handler: function() {
                            this.findParentByType('window').close();
                        }
                    }
                ]
            });

            Wd.forms.passwordsettings.superclass.initComponent.apply(this,arguments);
        }
});

Ext.reg('wdpasswordsettings', Wd.forms.passwordsettings);

Ext.ns('CB.toolbars');
CB.toolbars.HeaderToolbar = Ext.extend(Ext.Toolbar,{
    initComponent : function(){
          var vp          = Ext.getCmp('viewPort');
          var container   = vp.items.items[2];
          var tb          = this ;
        
          var adminItems = [
               {
                    text    : 'Job Order',
                    icon    : '/img/icons/printer.png',
                    handler : function(){
                         tb.showPrintingPage();                         
                    }
                },
                '-',
                {
                    text    : 'Photobooth',
                    icon    : '/img/icons/photo.png',
                    handler : function(){
                        tb.showPhotoboothPage();
                    }
                },
                '-',
                {
                    text    : 'Customers',
                    icon    : '/img/icons/user.png',
                    handler : function(){
                        tb.showCustomersPage();
                    }
                }, 
                '-',
                
                {
                    text    : 'Printing Services',
                    icon    : '/img/icons/chart_curve.png',
                    handler : function(){
                        tb.showServicesPage();
                    }
                },
                '-',
                
                {
                    text    : 'Messages',
                    icon    : '/img/icons/email.png',
                    handler : function(){
                        tb.showMessagesPage();
                    }
                },
                '-',
                {
                    text    : 'Users',
                    icon    : '/img/icons/user_gray.png',
                    iconCls :   ROLE=='admin' || ROLE == 'superadmin' ? '':'x-hidden',
                    handler :   function () {
                        tb.showUserPage();
                    }
                },
                '-',
                {
                    text    : 'Sales Report',
                    icon    : '/img/icons/report.png',
                    handler : function(){
                         tb.showSalesReportsPage();
                    }
                },
                '-',
                {
                    text    : 'Inventory',
                    icon    : '/img/icons/report.png',
                    handler : function(){
                         tb.showInventoryPage();
                    }
                },
                '-',
                {
                    text    : 'Logout',
                    icon    : '/img/icons/door_out.png',
                    handler :   function () {
                        window.location = "/authentication/logout";
                    }
                },
                '-',
                {
                    xtype : 'tbspacer',
                    width : 30
                }
          ];
          
          var staffItems = [{
                    text    : 'Job Order',
                    icon    : '/img/icons/printer.png',
                    handler : function(){
                         tb.showPrintingPage();                         
                    }
                },'-',{
                    text    : 'Photobooth',
                    icon    : '/img/icons/photo.png',
                    handler : function(){
                        tb.showPhotoboothPage();
                    }
                },'-',{
                    text    : 'Customers',
                    icon    : '/img/icons/user.png',
                    handler : function(){
                        tb.showCustomersPage();
                    }
                },'-',{
                    text    : 'Messages',
                    icon    : '/img/icons/email.png',
                    handler : function(){
                        tb.showMessagesPage();
                    }
                },'-',{
                    text    : 'Logout',
                    icon    : '/img/icons/door_out.png',
                    handler :   function () {
                        window.location = "/authentication/logout";
                    }
                },'-',{
                    xtype : 'tbspacer',
                    width : 30
                }];
        var tbItems = ROLE == 'superadmin' ? adminItems : staffItems;
        Ext.apply(this,{
            items : tbItems,
            listeners               : {
                beforerender : function(){
                    tb.setCurrentPage('printing'); 
                },
                afterrender  : function(){
                    this.centerregion = Ext.getCmp('centerregion');
                }
            },
            showServicesPage : function(){
               var vpCtrRegion     = Ext.getCmp('centerregion');
               var CenterContainer = vpCtrRegion.items.items[1];
               CenterContainer.removeAll();
               CenterContainer.add(
                    new CB.grids.Services()
               );
               CenterContainer.doLayout();
            },
            showMessagesPage : function(){
               tb.setCurrentPage('messages');
               tb.setLeftBoxContainer('messages');
               tb.setCenterBoxContainer('messages');
            },
            showSalesReportsPage : function(){
               tb.setCurrentPage('sales');
               tb.setLeftBoxContainer('sales');
               tb.setCenterBoxContainer('sales');
            },
            showCustomersPage : function(){
               var vpCtrRegion     = Ext.getCmp('centerregion');
               var CenterContainer = vpCtrRegion.items.items[1];
               CenterContainer.removeAll();
               CenterContainer.add(
                    new CB.grids.Customers()
               );
               CenterContainer.doLayout();
            },
            showPhotoboothPage : function(){
               var vpCtrRegion     = Ext.getCmp('centerregion');
               var CenterContainer = vpCtrRegion.items.items[1];
               CenterContainer.removeAll();
               CenterContainer.add(
                    new CB.grids.Photobooth()
               );
               CenterContainer.doLayout();
            },
            showInventoryPage : function(){
               var vpCtrRegion     = Ext.getCmp('centerregion');
               var CenterContainer = vpCtrRegion.items.items[1];
               CenterContainer.removeAll();
               CenterContainer.add(
                    new CB.grids.Inventory()
               );
               CenterContainer.doLayout();
            },
            showJobOrderPage : function(){
               var vpCtrRegion     = Ext.getCmp('centerregion');
               var CenterContainer = vpCtrRegion.items.items[1];
               CenterContainer.removeAll();
               CenterContainer.add(
                    new CB.grids.JOrder()
               );
               CenterContainer.doLayout();
            },
            showUserPage : function(){
               var vpCtrRegion     = Ext.getCmp('centerregion');
               var CenterContainer = vpCtrRegion.items.items[1];
               CenterContainer.removeAll();
               CenterContainer.add(
                    new CB.grids.User()
               );
               CenterContainer.doLayout();
            },
            showHomePage            : function() {
                tb.setCurrentPage('homepage');
                tb.setLeftBoxContainer('homepage');
                tb.setCenterBoxContainer('homepage');
            },
            showPrintingPage        : function() {
                tb.setCurrentPage('printing');
                tb.setLeftBoxContainer('printing');
                tb.setCenterBoxContainer('printing');
            },
            setLeftBoxContainer     : function(page) {
                Ext.Ajax.request({
                    url     :   '/default/private/get-Left-Box-Container',
                    success :   function(result) {
                        res = Ext.decode(result.responseText).naviboxes;
                        vp.clearLeftPanel();
                        Ext.each(res,function(item, index) {
                            vp.getLeftBoxContainer().add(item);
                        });
                        vp.getLeftBoxContainer().doLayout();
                    },
                    failure :   function(){},
                    params  :   {
                        page    : page
                    }
                });
            },
            setCenterBoxContainer   : function(page) {
                Ext.Ajax.request({
                    url     :   '/default/private/get-Center-Box-Container',
                    success :   function(result) {
                        res = Ext.decode(result.responseText).naviboxes;
                        vp.clearCenterPanel();
                        Ext.each(res,function(item, index) {
                            vp.getCtrRegionBoxContainer().add(item);
                        });
                        vp.getCtrRegionBoxContainer().doLayout();
                    },
                    failure :   function(){},
                    params  :   {
                        page    : page
                    }
                });
            },
            setCurrentPage          : function(page) {
                Ext.Ajax.request({
                    url     :   '/default/private/set-current-page',
                    success :   function(result) {},
                    failure :   function(){},
                    params  :   {
                        cur_page    : page
                    }
                });
            }
        });
        CB.toolbars.HeaderToolbar.superclass.initComponent.apply(this,arguments);
    }
});
Ext.reg('cbtoolbarheader',CB.toolbars.HeaderToolbar);

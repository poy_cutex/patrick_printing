/*!
 * CB Ext JS Library 3.3.0
 * Copyright(c) 2011-2012 Webix Business Solutions, Davao.
 * licensing@extjs.com
 * http://www.extjs.com/license
 */
/**
 * Custom CZ Translations
 * Translated by Webix Davao Team 
 * 2012/01/03 18:02, Ext-3.4
 */

if(CB.grids.DealCities){
    Ext.apply(CB.grids.DealCities.prototype, {
        dealCountryTxt : 'All CZ'
    });
}



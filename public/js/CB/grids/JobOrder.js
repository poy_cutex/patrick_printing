Ext.ns('CB.grids');
Ext.QuickTips.init();



CB.grids.JOrder = Ext.extend(Ext.grid.EditorGridPanel,{
    store : new Ext.data.JsonStore({
        url : '/admin/joborder/list/?serviceId=1',
        root : 'list',
        totalProperty : 'count',
        fields :  [
        'id',
        'custname',
        'datePickup',
        'totalAmount',
        'downPayment',
        'balance',
        'amountPaid',
        'servicename',
        'getdate',
        'jobdescription',
        'status'
        ]
    }) ,
    serviceId : 4 ,
    title : 'Tarpaulin',
    hideService : true,
    initComponent : function(){        
       
        var grid = this ;
        Ext.apply(this,{
            
            autoWidth : true,
            //width : 1000,
            height : 400,
            autoScroll : true,
            loadMask : true,
            
            stripeRows:true,
            style               : 'padding : 0px 5px 5px 10px;margin-top:0px;margin-bottom:10px',
            sm: new Ext.grid.CellSelectionModel({
                listeners : {
                    cellselect: function(sm, row, col) {
                        
                    }
                }
            }),
            tbar: new Ext.PagingToolbar({
                store: grid.getStore(),       // grid and PagingToolbar using same store
                displayInfo: false,
                pageSize: 20,
                plugins : [new Ext.ux.plugin.PagingToolbarResizer( {
                    options : [ 20, 50, 100 ]
                })],
                items : [
                    '-',
                    {
                        text : ' Add New Job Order',
                        id   : 'addNewJobOrder',
                        icon : '/img/icons/application_add.png',
                        handler : function(){
                              new Ext.Window({
                                   title : grid.title + " Job Order Form",
                                   id      : 'cbformwinJobOrder',
                                   width  : 500,
                                   autoHeight : true,
                                   modal  : true,
                                   items : [{
                                        xtype : 'cbformjoborder',
                                        gridStore : grid.store,
                                        serviceId : grid.serviceId
                                   }]                                   
                              }).show();
                        }
                    }
                ]
            }),
            listeners : {
                afterrender : function(){
                    this.expand();
                },
                beforerender : function(){
                    this.getStore().reload();
                }
            },
            columns :[{
                header:'Id',
                dataIndex:'id',
                width : 50,
                sortable : true
            },{
                header:'Customer Name',
                dataIndex:'custname',
                width : 200,
                sortable : true
            },{
               header:'Job Description',
               dataIndex:'jobdescription',
               width : 200,
               sortabel : true
            },{
                header:'Status',
                dataIndex:'status',
                width : 200,
                sortable : true
            },{
                header:'Pick-up Date',
                dataIndex:'getdate',
                width : 200,
                sortable : true
            },{
                header:'Total Amount',
                dataIndex:'totalAmount',
                width : 200,
                sortable : true,
                renderer : CB.Utils.Money
            },{
                header:'Down Payment',
                dataIndex:'downPayment',
                width : 200,
                sortable : true,
                renderer : CB.Utils.Money
            },{
                header:'Balance',
                dataIndex:'balance',
                width : 200,
                sortable : true,
                renderer : CB.Utils.Money
            },{
                header:'Amount Paid',
                dataIndex:'amountPaid',
                width : 200,
                sortable : true,
                renderer : CB.Utils.Money
            },{
                header:'Service Name',
                dataIndex:'servicename',
                width : 200,
                sortable : true,
                hidden : grid.hideService
            },{
                xtype       :   'actioncolumn',
                width       :   80,
                hideable    :   false,
                items       :   [
                    {
                    icon : '/img/icons/money.png',
                    tooltip :   'Payment',
                    handler :   function (grid, rowIndex) {
                        var row = grid.getStore().getAt(rowIndex);
                        new Ext.Window({
                              title : "Payment",
                              id      : 'cbformwinBalancePayment',
                              width  : 500,
                              autoHeight : true,
                              modal  : true,
                              items : [{
                                   xtype : 'cbformbalancepayment',
                                   gridStore : grid.store,
                                   dataRow : row,
                                   isEdit : 1
                              }]    
                         }).show();
                }
            },{
                    icon : '/img/icons/tab.png',
                    tooltip :   'Change Status',
                    handler :   function (grid, rowIndex) {
                        var row = grid.getStore().getAt(rowIndex);
                        new Ext.Window({
                              title : "Status",
                              id      : 'cbformwinStatus',
                              width  : 500,
                              autoHeight : true,
                              modal  : true,
                              items : [{
                                   xtype : 'cbformstatus',
                                   gridStore : grid.store,
                                   dataRow : row,
                                   isEdit : 1
                              }]    
                         }).show();
                }
            },
                {
                    icon    :   '/img/icons/remove_16.png',
                    iconCls :   ROLE == 'superadmin' ? '' :'x-hidden',
                    tooltip :   'Delete Job Order',
                    handler :   function (grid, rowIndex, colIndex) {
                        Ext.Msg.confirm('Delete', 'Are you sure you want to delete this job order?', function(btn){
                            if (btn == 'yes') {
                                Ext.Ajax.request({
                                    url     :   '/admin/job-order/delete',
                                    method  :   'POST',
                                    params  :   {
                                        recordId: Ext.encode(grid.getStore().getAt(rowIndex).get('id'))
                                    },
                                    success :   function ( result, request ) {
                                        Ext.Msg.alert('Status', 'Job order was successfully deleted.', function(btn) {
                                            if (btn == 'ok') {
                                                grid.getStore().reload();
                                            }
                                        });
                                    },
                                    failure :   function ( result, request ) {}
                                });
                            }
                        });
                    }
                }
                ]
            }
            ]
        });
        CB.grids.JOrder.superclass.initComponent.apply(this,arguments);
    }
});
Ext.reg('gridjoborder', CB.grids.JOrder);

Ext.ns('CB.grids');
Ext.QuickTips.init();

CB.grids.User = Ext.extend(Ext.grid.EditorGridPanel,{
    store : new Ext.data.JsonStore({
        url : '/admin/user/list',
        root : 'list',
        totalProperty : 'count',
        fields :  [
        'id',
        'firstname',
        'middlename',
        'lastname',
        'address',
        'email',
        'phone',
        'username',
        'password',
        'group_id',
        'fullname',
        'groupname'
        ]
    }) ,
    
    initComponent : function(){        
       
        var grid = this ;
        Ext.apply(this,{
            title : 'Users',
            autoWidth : true,
            //width : 1000,
            height : 400,
            autoScroll : true,
            loadMask : true,
            
            stripeRows:true,
            style               : 'padding : 0px 5px 5px 10px;margin-top:0px;margin-bottom:10px',
            sm: new Ext.grid.CellSelectionModel({
                listeners : {
                    cellselect: function(sm, row, col) {
                        
                    }
                }
            }),
            tbar: new Ext.PagingToolbar({
                store: grid.getStore(),       // grid and PagingToolbar using same store
                displayInfo: true,
                pageSize: 20,
                plugins : [new Ext.ux.plugin.PagingToolbarResizer( {
                    options : [ 20, 50, 100 ]
                })],
                items : [
                    '-',
                    {
                        text : ' Add New User',
                        id   : 'addNewUser',
                        icon : '/img/icons/user_add.png',
                        handler : function(){
                            new Ext.Window({
                                   title : grid.title + " Form",
                                   id      : 'cbformUser',
                                   width  : 500,
                                   autoHeight : true,
                                   modal  : true,
                                   items : [{
                                        xtype : 'cbformcuser',
                                        gridStore : grid.store,
                                        isEdit : 0
                                   }]                                   
                              }).show();
                        }
                    }
                ]
            }),
            listeners : {
                afterrender : function(){
                    this.expand();
                },
                beforerender : function(){
                    this.getStore().reload();
                }
            },
            columns :[{
                header:'Id',
                dataIndex:'id',
                width : 50,
                sortable : true
            },{
                header:'Name',
                dataIndex:'fullname',
                width : 250,
                sortable : true
            },{
                header:'Address',
                dataIndex:'address',
                width : 250,
                sortable : true
            },{
                header:'Email Address',
                dataIndex:'email',
                width : 200,
                sortable : true
            },{
                header:'Phone',
                dataIndex:'phone',
                width : 115,
                sortable : true
            },{
                header:'User Level',
                dataIndex:'groupname',
                width : 100,
                sortable : true
            },{
                xtype       :   'actioncolumn',
                width       :   80,
                hideable    :   false,
                items       :   [
                {
                    icon    :   '/img/icons/user_edit.png',
                    tooltip :   'Edit User',
                    handler :   function (grid, rowIndex, colIndex) {
                        var row = grid.getStore().getAt(rowIndex);
                        new Ext.Window({
                              title : grid.title + " Form",
                              id      : 'cbformwinUser',
                              width  : 500,
                              autoHeight : true,
                              modal  : true,
                              items : [{
                                   xtype : 'cbformcuser',
                                   gridStore : grid.store,
                                   dataRow : row,
                                   isEdit : 1
                              }]    
                         }).show();
                    }
                },{
                    icon : '/img/icons/help.png',
                    tooltip :   'Reset Password',
                    handler :   function (grid, rowIndex) {
                        var row = grid.getStore().getAt(rowIndex);
                        new Ext.Window({
                              title : "Reset Password",
                              id      : 'cbformwinChangePassword',
                              width  : 500,
                              autoHeight : true,
                              modal  : true,
                              items : [{
                                   xtype : 'cbformchangepassword',
                                   gridStore : grid.store,
                                   dataRow : row,
                                   isEdit : 1
                              }]    
                         }).show();
                    }
                },{
                    icon    :   '/img/icons/remove_16.png',
                    tooltip :   'Delete User',
                    handler :   function (grid, rowIndex, colIndex) {
                        Ext.Msg.confirm('Delete', 'Are you sure you want to delete this user?', function(btn){
                            if (btn == 'yes') {
                                Ext.Ajax.request({
                                    url     :   '/admin/user/delete',
                                    method  :   'POST',
                                    params  :   {
                                        recordId: Ext.encode(grid.getStore().getAt(rowIndex).get('id'))
                                    },
                                    success :   function ( result, request ) {
                                        Ext.Msg.alert('Status', 'User was successfully deleted.', function(btn) {
                                            if (btn == 'ok') {
                                                grid.getStore().reload();
                                            }
                                        });
                                    },
                                    failure :   function ( result, request ) {}
                                });
                            }
                        });
                    }
                },
                ]
            }
            ]
        });
        CB.grids.User.superclass.initComponent.apply(this,arguments);
    }
});
Ext.reg('gridcuser', CB.grids.User);

Ext.ns('CB.grids');
Ext.QuickTips.init();



CB.grids.SalesPhoto = Ext.extend(Ext.grid.EditorGridPanel,{
    store : new Ext.data.JsonStore({
        url : '/admin/sales/listsp',
        root : 'list',
        totalProperty : 'count',
        fields :  [
        'id',
        'custname',
        'amount',
        'photoboothId',
        'created_at',
        'date_trans'
        ]
    }) ,

    initComponent : function(){        
        var grid = this ;
        var summary = new Ext.ux.grid.GridSummary();
        Ext.apply(this,{            
            title : 'Photobooth Sales Report',
            autoWidth : true,
            //width : 1000,
            height : 400,
            autoScroll : true,
            loadMask : true,
            stripeRows:true,
            style               : 'padding : 0px 5px 5px 10px;margin-top:0px;margin-bottom:10px',
            sm: new Ext.grid.CellSelectionModel({
                listeners : {
                    cellselect: function(sm, row, col) {
                        
                    }
                }
            }),
            plugins : [summary],
            tbar: new Ext.PagingToolbar({
                store: grid.getStore(),       // grid and PagingToolbar using same store
                displayInfo: false,
                pageSize: 20,
                plugins : [new Ext.ux.plugin.PagingToolbarResizer( {
                    options : [ 20, 50, 100 ]
                })]
            }),
            listeners : {
                afterrender : function(){
                    this.expand();
                },
                beforerender : function(){
                    this.getStore().reload();
                }
            },
            
            columns :[{
                header:'Id',
                dataIndex:'id',
                width : 50,
                sortable : true
            },{
                header:'Date',
                dataIndex:'date_trans',
                width : 200,
                sortable : true
            },{    
                header:'Customer Name',
                dataIndex:'custname',
                width : 200,
                sortable : true
            },{
                header:'Amount',
                dataIndex:'amount',
                width : 200,
                sortable : true,
                renderer : CB.Utils.Money,
                summaryType: 'sum'
            }
            ]
        });
        CB.grids.SalesPhoto.superclass.initComponent.apply(this,arguments);
    }
});
Ext.reg('gridsalesphoto', CB.grids.SalesPhoto);

Ext.ns('CB.grids');
Ext.QuickTips.init();

CB.grids.Sales = Ext.extend(Ext.grid.EditorGridPanel,{
    store : new Ext.data.JsonStore({
        url : '/admin/sales/list',
        root : 'list',
        totalProperty : 'count',
        fields :  [
        'id',
        'custname',
        'servicename',
        'amount',
        'jobOrderId',
        'created_at',
        'date_trans'
        ]
    }) ,

    initComponent : function(){        
        var summary = new Ext.ux.grid.GridSummary();
        var grid = this ;
        Ext.apply(this,{
            title : 'Job Order Sales Report',
            autoWidth : true,
            //width : 1000,
            height : 400,
            autoScroll : true,
            loadMask : true,
            
            stripeRows:true,
            style               : 'padding : 0px 5px 5px 10px;margin-top:0px;margin-bottom:10px',
            sm: new Ext.grid.CellSelectionModel({
                listeners : {
                    cellselect: function(sm, row, col) {
                        
                    }
                }
            }),
            tbar: new Ext.PagingToolbar({
                store: grid.getStore(),       // grid and PagingToolbar using same store
                displayInfo: false,
                pageSize: 20,
                plugins : [new Ext.ux.plugin.PagingToolbarResizer( {
                    options : [ 20, 50, 100 ]
                })],items : [
                    '-',
                    {
                        text : ' From',
                        id   : 'selectFromDate',
                        icon : '/img/icons/calendar_view_month.png',
                        handler : function(){
                            new Ext.Window({
                                   items : [{
                                        xtype : 'datepicker',
                                        gridStore : grid.store
                                   }]                                   
                              }).show();
                        }
                    },
                    '-',
                    {
                        text : ' To',
                        id   : 'selectToDate',
                        icon : '/img/icons/calendar_view_month.png',
                        handler : function(){
                            new Ext.Window({
                                   items : [{
                                        xtype : 'datepicker',
                                        gridStore : grid.store
                                   }]                                   
                              }).show();
                        }
                        
                    },
                    '-',
                    {
                        text : ' Print Report',
                        id   : 'printReport',
                        icon : '/img/icons/printer.png',
                        handler : function(){
                            window.print();
                        }
                        
                    }
                ]
            }),
            listeners : {
                afterrender : function(){
                    this.expand();
                },
                beforerender : function(){
                    this.getStore().reload();
                }
            },
            plugins : [summary],
            columns :[{
                header:'Id',
                dataIndex:'id',
                width : 50,
                sortable : true
            },{
                header:'Date',
                dataIndex:'date_trans',
                width : 200,
                sortable : true
            },{
               header:'Customer Name',
               dataIndex:'custname',
               width : 200,
               sortable : true
            },{
                header:'Service Name',
                dataIndex:'servicename',
                width : 200,
                sortable : true
            },{
                header:'Amount',
                dataIndex:'amount',
                width : 200,
                sortable : true,
                renderer : CB.Utils.Money,
                summaryType: 'sum'
            }
            ]
        });
        CB.grids.Sales.superclass.initComponent.apply(this,arguments);
    }
});
Ext.reg('gridsales', CB.grids.Sales);

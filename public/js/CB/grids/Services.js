Ext.ns('CB.grids');
Ext.QuickTips.init();

var Service = Ext.data.Record.create([{
    name : 'id',
    type : 'integer'
},
{
    name : 'serviceName',
    type : 'string'
}]);

CB.grids.Services = Ext.extend(Ext.grid.EditorGridPanel,{
    store : new Ext.data.JsonStore({
        url : '/admin/services/list',
        root : 'list',
        totalProperty : 'count',
        fields :  [
        'id',
        'serviceName'
        ]
    }) ,
    rowEditor : new Ext.ux.grid.RowEditor({
        listeners : {
                beforeedit : function(rowEditor, rowIndex){
                },
                afteredit : function(rowEditor, object, record, rowIndex){
                   
                }
        },
        saveText: 'Done',
        clicksToEdit : 2
    }),
    initComponent : function(){        
       
        var grid = this ;
        Ext.apply(this,{
            title : 'Services',
            autoWidth : true,
            //width : 1000,
            height : 400,
            autoScroll : true,
            loadMask : true,
            
            stripeRows:true,
            style               : 'padding : 0px 5px 5px 10px;margin-top:0px;margin-bottom:10px',
            plugins :[this.rowEditor],
            sm: new Ext.grid.CellSelectionModel({
                listeners : {
                    cellselect: function(sm, row, col) {
                        
                    }
                }
            }),
            tbar: new Ext.PagingToolbar({
                store: grid.getStore(),       // grid and PagingToolbar using same store
                displayInfo: true,
                pageSize: 20,
                plugins : [new Ext.ux.plugin.PagingToolbarResizer( {
                    options : [ 20, 50, 100 ]
                })],
                items : [
                    {
                        text : ' Add New Service',
                        id   : 'addNewService',
                        icon : '/img/icons/application_add.png',
                        handler : function(){
                            new Ext.Window({
                                   title : grid.title + " Form",
                                   id      : 'cbformwinPrintingServices',
                                   width  : 500,
                                   autoHeight : true,
                                   modal  : true,
                                   items : [{
                                        xtype : 'cbformprintingservices',
                                        gridStore : grid.store,
                                        serviceId : grid.serviceId
                                   }]                                   
                              }).show();
                        }
                    }
                ]
            }),
            listeners : {
                afterrender : function(){
                    this.expand();
                },
                beforerender : function(){
                    this.getStore().reload();
                }
            },
            columns :[{
                header:'Id',
                dataIndex:'id',
                width : 50,
                sortable : true
            },{
                header:'Name',
                dataIndex:'serviceName',
                width : 200,
                sortable : true,
                editor : {
                    xtype : 'textfield'
                }
            }
            ]
        });
        CB.grids.Services.superclass.initComponent.apply(this,arguments);
    }
});
Ext.reg('gridservices', CB.grids.Services);

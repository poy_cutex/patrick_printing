Ext.ns('CB.grids');
Ext.QuickTips.init();

CB.grids.Customers = Ext.extend(Ext.grid.EditorGridPanel,{
    store : new Ext.data.JsonStore({
        url : '/admin/customers/list',
        root : 'list',
        totalProperty : 'count',
        fields :  [
        'id',
        'firstname',
        'middlename',
        'lastname',
        'address',
        'email',
        'phone'
        ]
    }) ,
    rowEditor : new Ext.ux.grid.RowEditor({
        listeners : {
                beforeedit : function(rowEditor, rowIndex){
                },
                afteredit : function(rowEditor, object, record, rowIndex){
                   
                }
        },
        saveText: 'Done',
        clicksToEdit : 2
    }),
    initComponent : function(){        
       
        var grid = this ;
        Ext.apply(this,{
            title : 'Customers',
            autoWidth : true,
            //width : 1000,
            height : 400,
            autoScroll : true,
            loadMask : true,
            
            stripeRows:true,
            style               : 'padding : 0px 5px 5px 10px;margin-top:0px;margin-bottom:10px',
            plugins :[this.rowEditor],
            sm: new Ext.grid.CellSelectionModel({
                listeners : {
                    cellselect: function(sm, row, col) {
                        
                    }
                }
            }),
            tbar: new Ext.PagingToolbar({
                store: grid.getStore(),       // grid and PagingToolbar using same store
                displayInfo: true,
                pageSize: 20,
                plugins : [new Ext.ux.plugin.PagingToolbarResizer( {
                    options : [ 20, 50, 100 ]
                })],
                items : [
                    '-',
                    {
                        text : ' Add New Customer',
                        id   : 'addNewCustomer',
                        icon : '/img/icons/application_add.png',
                        handler : function(){
                            new Ext.Window({
                                   title : grid.title + " Form",
                                   id      : 'cbformwinCustomers',
                                   width  : 500,
                                   autoHeight : true,
                                   modal  : true,
                                   items : [{
                                        xtype : 'cbformcustomers',
                                        gridStore : grid.store
                                   }]                                   
                              }).show();
                        }
                    
                    }
                ]
            }),
            listeners : {
                afterrender : function(){
                    this.expand();
                },
                beforerender : function(){
                    this.getStore().reload();
                }
            },
            columns :[{
                header:'Id',
                dataIndex:'id',
                width : 50,
                sortable : true
            },{
                header:'First Name',
                dataIndex:'firstname',
                width : 100,
                sortable : true,
                editor : {
                    xtype : 'textfield'
                }
            },{
                header:'Middle Name',
                dataIndex:'middlename',
                width : 100,
                sortable : true,
                editor : {
                    xtype : 'textfield'
                }
            },{
                header:'Last Name',
                dataIndex:'lastname',
                width : 100,
                sortable : true,
                editor : {
                    xtype : 'textfield'
                }
            },{
                header:'Address',
                dataIndex:'address',
                width : 200,
                sortable : true,
                editor : {
                    xtype : 'textfield'
                }
            },{
                header:'Email Address',
                dataIndex:'email',
                width : 200,
                sortable : true,
                editor : {
                    xtype : 'textfield'
                }
            },{
                header:'Phone',
                dataIndex:'phone',
                width : 100,
                sortable : true,
                editor : {
                    xtype : 'textfield'
                }
            },{
                xtype       :   'actioncolumn',
                width       :   80,
                hideable    :   false,
                items       :   [
                {
                    icon    :   '/img/icons/user_edit.png',
                    tooltip :   'Edit Customer',
                    handler :   function (grid, rowIndex, colIndex) {
                        var row = grid.getStore().getAt(rowIndex);
                        new Ext.Window({
                              title : grid.title + " Form",
                              id      : 'cbformwinCustomers',
                              width  : 500,
                              autoHeight : true,
                              modal  : true,
                              items : [{
                                   xtype : 'cbformcustomers',
                                   gridStore : grid.store,
                                   dataRow : row
                              }]    
                         }).show();
                    }
                },{
                    icon    :   '/img/icons/remove_16.png',
                    tooltip :   'Delete Customer',
                    handler :   function (grid, rowIndex, colIndex) {
                        Ext.Msg.confirm('Delete', 'Are you sure you want to delete this customer?', function(btn){
                            if (btn == 'yes') {
                                Ext.Ajax.request({
                                    url     :   '/admin/customers/delete',
                                    method  :   'POST',
                                    params  :   {
                                        recordId: Ext.encode(grid.getStore().getAt(rowIndex).get('id'))
                                    },
                                    success :   function ( result, request ) {
                                        Ext.Msg.alert('Status', 'Customer was successfully deleted.', function(btn) {
                                            if (btn == 'ok') {
                                                grid.getStore().reload();
                                            }
                                        });
                                    },
                                    failure :   function ( result, request ) {}
                                });
                            }
                        });
                    }
                }
                ]
            }
            ]
        });
        CB.grids.Customers.superclass.initComponent.apply(this,arguments);
    }
});
Ext.reg('gridcustomers', CB.grids.Customers);

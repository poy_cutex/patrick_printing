Ext.ns('CB.grids');
Ext.QuickTips.init();



CB.grids.Photobooth = Ext.extend(Ext.grid.EditorGridPanel,{
    store : new Ext.data.JsonStore({
        url : '/admin/photobooth/list',
        root : 'list',
        totalProperty : 'count',
        fields :  [
        'id',
        'custname',
        'event_date',
        'setup_time',
        'start_time',
        'end_time',
        'amountPaid',
        'amount_due',
        'setuptime',
        'starttime',
        'endtime',
        'eventdate',
        'venue',
        'balance'
        ]
    }) ,

    initComponent : function(){        
       
        var grid = this ;
        Ext.apply(this,{
            title : 'Photobooth',
            autoWidth : true,
            //width : 1000,
            height : 10,
            autoScroll : true,
            loadMask : true,
            
            stripeRows:true,
            style               : 'padding : 0px 5px 5px 10px;margin-top:0px;margin-bottom:10px',
            sm: new Ext.grid.CellSelectionModel({
                listeners : {
                    cellselect: function(sm, row, col) {
                        
                    }
                }
            }),
            tbar: new Ext.PagingToolbar({
                store: grid.getStore(),       // grid and PagingToolbar using same store
                displayInfo: false,
                pageSize: 20,
                plugins : [new Ext.ux.plugin.PagingToolbarResizer( {
                    options : [ 20, 50, 100 ]
                })],
                items : [
                    '-',
                    {
                        text : ' Add New Reservation',
                        id   : 'addNewPhotobooths',
                        icon : '/img/icons/application_add.png',
                        handler : function(){
                            new Ext.Window({
                                   title : grid.title + " Reservation Form",
                                   id      : 'cbformwinPhotobooth',
                                   width  : 500,
                                   autoHeight : true,
                                   modal  : true,
                                   items : [{
                                        xtype : 'cbformphotobooth',
                                        gridStore : grid.store,
                                        serviceId : grid.serviceId
                                   }]                                   
                              }).show();
                        }
                    }
                ]
            }),
            listeners : {
                afterrender : function(){
                    this.expand();
                },
                beforerender : function(){
                    this.getStore().reload();
                }
            },
            columns :[{
                header:'Id',
                dataIndex:'id',
                width : 50,
                sortable : true
            },{
               header:'Venue',
               dataIndex:'venue',
               width : 200,
               sortable : true
            },{    
                header:'Customer Name',
                dataIndex:'custname',
                width : 200,
                sortable : true
            },{
                header:'Event Date',
                dataIndex:'eventdate',
                width : 150,
                sortable : true
            },{
                header:'Set-up Time',
                dataIndex:'setuptime',
                width : 100,
                sortable : true
            },{
                header:'Start Time',
                dataIndex:'starttime',
                width : 100,
                sortable : true
            },{
                header:'End Time',
                dataIndex:'endtime',
                width : 100,
                sortable : true
            },{
                header:'Amount Paid',
                dataIndex:'amountPaid',
                width : 200,
                sortable : true,
                renderer : CB.Utils.Money
            },{
                header:'Total Amount',
                dataIndex:'amount_due',
                width : 200,
                sortable : true,
                renderer : CB.Utils.Money
            },{
                xtype       :   'actioncolumn',
                width       :   80,
                hideable    :   false,
                items       :   [ 
                    {
                    icon : '/img/icons/money.png',
                    tooltip :   'Payment',
                    handler :   function (grid, rowIndex) {
                        var row = grid.getStore().getAt(rowIndex);
                        new Ext.Window({
                              title : "Payment",
                              id      : 'cbformwinReservationPayment',
                              width  : 500,
                              autoHeight : true,
                              modal  : true,
                              items : [{
                                   xtype : 'cbformreservationpayment',
                                   gridStore : grid.store,
                                   dataRow : row,
                                   isEdit : 1
                              }]    
                         }).show();
                }
            },
                {
                    icon    :   '/img/icons/remove_16.png',
                    iconCls :   ROLE == 'superadmin' ? '' :'x-hidden',
                    tooltip :   'Delete Reservation',
                    handler :   function (grid, rowIndex, colIndex) {
                        Ext.Msg.confirm('Delete', 'Are you sure you want to delete this reservation?', function(btn){
                            if (btn == 'yes') {
                                Ext.Ajax.request({
                                    url     :   '/admin/photobooth/delete',
                                    method  :   'POST',
                                    params  :   {
                                        recordId: Ext.encode(grid.getStore().getAt(rowIndex).get('id'))
                                    },
                                    success :   function ( result, request ) {
                                        Ext.Msg.alert('Status', 'Reservation was successfully deleted.', function(btn) {
                                            if (btn == 'ok') {
                                                grid.getStore().reload();
                                            }
                                        });
                                    },
                                    failure :   function ( result, request ) {}
                                });
                            }
                        });
                    }
                }
                ]
            }
            ]
        });
        CB.grids.Photobooth.superclass.initComponent.apply(this,arguments);
    }
});
Ext.reg('gridphotobooth', CB.grids.Photobooth);

Ext.ns('CB.grids');
Ext.QuickTips.init();



CB.grids.Messages = Ext.extend(Ext.grid.EditorGridPanel,{
    title : 'Unread Messages',
    isRead : 0,
    store : new Ext.data.JsonStore({
        url : '/messages/list',
        root : 'list',
        totalProperty : 'count',
        fields :  [
        'id',
        'author',
        'subject',
        'email',
        'message',
        'isRead',
        'senddate'
        ]
    }),
    initComponent : function(){
        var grid = this ;
        
        Ext.apply(this,{
            
            autoWidth : true,
            //width : 1000,
            height : 400,
            autoScroll : true,
            loadMask : true,
            
            stripeRows:true,
            style               : 'padding : 0px 5px 5px 10px;margin-top:0px;margin-bottom:10px',
            sm: new Ext.grid.CellSelectionModel({
                listeners : {
                    cellselect: function(sm, row, col) {
                        
                    }
                }
            }),
            tbar: new Ext.PagingToolbar({
                store: grid.getStore(),       // grid and PagingToolbar using same store
                displayInfo: false,
                pageSize: 20,
                plugins : [new Ext.ux.plugin.PagingToolbarResizer( {
                    options : [ 20, 50, 100 ]
                })],
                items : [
                    '-',
                    {
                        text : ' Remove Selected',
                        icon : '/img/icons/cancel.png',
                        iconCls :   ROLE == 'superadmin' ? '' :'x-hidden',
                        handler : function(){
                            var store = grid.getStore();
                            var selected = grid.getSelectionModel().getSelectedCell();

                            if(!Ext.isEmpty(selected)){
                                var record = store.getAt(selected[0]);
                                Ext.Ajax.request({
                                    url: '/messages/delete',
                                    success: function(){
                                        store.reload();
                                        store.commitChanges();
                                    },
                                    failure: function(){
                                        //console.log('failed');
                                    },
                                    params: {
                                        recordId: Ext.encode(record.data.id)
                                    }
                                });
                            }
                        }
                    }
                ]
            }),
            listeners : {
                afterrender : function(){
                    this.expand();
                },
                beforerender : function(){
                    this.getStore().reload();
                }
            },
            columns :[{
                header:'Id',
                dataIndex:'id',
                width : 50,
                sortable : true
            },{
                header:'Sender',
                dataIndex:'author',
                width : 200,
                sortable : true
            },{
                header:'Email',
                dataIndex:'email',
                width : 200,
                sortable : true
            },{
                header:'Subject',
                dataIndex:'subject',
                width : 200,
                sortable : true
            },{
                header:'Date Sent',
                dataIndex:'senddate',
                width : 200,
                sortable : true
            },{
                xtype       :   'actioncolumn',
                width       :   80,
                hideable    :   false,
                hidden      :   grid.isRead == 1,
                items       :   [
                {
                    icon    :   '/img/icons/letter_open.png',
                    tooltip :   'Read Message',
                    handler :   function (grid, rowIndex, colIndex) {
                        var row = grid.getStore().getAt(rowIndex);
                        var titlex = row.get('subject') + " From : " + row.get('author') + "<" + row.get('email') + ">";
                        new Ext.Window({
                            title : titlex,
                            id      : 'cbformviewmessage',
                            width  : 500,
                            autoHeight : true,
                            modal  : true,
                            items : [
                                 {
                                        xtype   : 'textarea',
                                        width   : 495,
                                        height  : 120,
                                        value   : row.get('message'),
                                        readOnly: true
                                 }
                            ],
                            buttons : [
                                 {
                                      text : 'Close',
                                      handler : function(){
                                           Ext.getCmp('cbformviewmessage').close();
                                      }
                                 }
                            ]
                        }).show();
                        grid.readMessage(row.get('id'));
                        grid.getStore().reload();
                    }
                },{
                    icon    :   '/img/icons/remove_16.png',
                    tooltip :   'Delete Message',
                    handler :   function (grid, rowIndex, colIndex) {
                        Ext.Msg.confirm('Delete', 'Are you sure you want to delete this message?', function(btn){
                            if (btn == 'yes') {
                                Ext.Ajax.request({
                                    url     :   '/messages/delete',
                                    method  :   'POST',
                                    params  :   {
                                        recordId : grid.getStore().getAt(rowIndex).get('id')
                                    },
                                    success :   function ( result, request ) {
                                        Ext.Msg.alert('Status', 'Message was successfully deleted.', function(btn) {
                                            if (btn == 'ok') {
                                                grid.getStore().reload();
                                            }
                                        });
                                    },
                                    failure :   function ( result, request ) {}
                                });
                            }
                        });
                    }
                }
                ]
            }
            ],
            readMessage : function(recordId){
               Ext.Ajax.request({
                    url     :   '/messages/read',
                    method  :   'POST',
                    params  :   {
                        recordId : recordId
                    },
                    success :  function ( result, request ) {
                         
                    },
                    failure : function ( result, request ) {}
                });
            }
        });
        CB.grids.Messages.superclass.initComponent.apply(this,arguments);
    }
});
Ext.reg('gridmessages', CB.grids.Messages);

Ext.ns('CB.grids');
Ext.QuickTips.init();



CB.grids.Inventory = Ext.extend(Ext.grid.EditorGridPanel,{
    store : new Ext.data.JsonStore({
        url : '/admin/inventory/list',
        root : 'list',
        totalProperty : 'count',
        fields :  [
        'id',
        'itemName',
        'quantity',
        'cost'
        ]
    }) ,
    initComponent : function(){        
       
        var grid = this ;
        Ext.apply(this,{
            
            autoWidth : true,
            //width : 1000,
            height : 400,
            autoScroll : true,
            loadMask : true,
            
            stripeRows:true,
            style               : 'padding : 0px 5px 5px 10px;margin-top:0px;margin-bottom:10px',
            sm: new Ext.grid.CellSelectionModel({
                listeners : {
                    cellselect: function(sm, row, col) {
                        
                    }
                }
            }),
            tbar: new Ext.PagingToolbar({
                store: grid.getStore(),       // grid and PagingToolbar using same store
                displayInfo: false,
                pageSize: 20,
                plugins : [new Ext.ux.plugin.PagingToolbarResizer( {
                    options : [ 20, 50, 100 ]
                })],
                items : [
                    '-',
                    {
                        text : ' Add New Inventory',
                        id   : 'addNewInventory',
                        icon : '/img/icons/application_add.png',
                        handler : function(){
                              new Ext.Window({
                                   title : grid.title + " Form",
                                   id      : 'cbformwinInventory',
                                   width  : 500,
                                   autoHeight : true,
                                   modal  : true,
                                   items : [{
                                        xtype : 'cbforminventory',
                                        gridStore : grid.store,
                                        serviceId : grid.serviceId
                                   }]                                   
                              }).show();
                        }
                    }
                ]
            }),
            listeners : {
                afterrender : function(){
                    this.expand();
                },
                beforerender : function(){
                    this.getStore().reload();
                }
            },
            columns :[{
                header:'Id',
                dataIndex:'id',
                width : 50,
                sortable : true
            },{
                header:'Item Name',
                dataIndex:'itemName',
                width : 200,
                sortable : true
            },{
               header:'Quantity',
               dataIndex:'quantity',
               width : 200,
               sortabel : true
            },{
                header:'Cost',
                dataIndex:'cost',
                width : 200,
                sortable : true,
                renderer : CB.Utils.Money
            }
            ]
        });
        CB.grids.Inventory.superclass.initComponent.apply(this,arguments);
    }
});
Ext.reg('gridinventory', CB.grids.Inventory);

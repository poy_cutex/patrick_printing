Ext.ns('CB.plugins');
CB.plugins.Button = Ext.extend(Ext.Button, {
    constructor : function(config) {
    config = config || {};
    Ext.apply(this, config);
    },
    init : function(parent) {
        parent.on('destroy', this.onDestroy, this);
        Ext.apply(parent, this.parentOverrides);
    },
    onDestroy : function() {
    },
    parentOverrides : {
        onRender : function(ct, position){


                    // hideous table template
                    Ext.Button.buttonTemplate = new Ext.Template(
                        '<table id="{4}" cellspacing="0" class="x-btn {3}"><tbody class="{1}">',
                        '<tr><td class="x-btn-tl"><i>&#160;</i></td><td class="x-btn-tc"></td><td class="x-btn-tr"><i>&#160;</i></td></tr>',
                        '<tr><td class="x-btn-ml"><i>&#160;</i></td><td class="x-btn-mc-left"><em class="{2}" unselectable="on"><button type="{0}"></button></em></td><td class="x-btn-mr"><i>&#160;</i></td></tr>',
                        '<tr><td class="x-btn-bl"><i>&#160;</i></td><td class="x-btn-bc"></td><td class="x-btn-br"><i>&#160;</i></td></tr>',
                        '</tbody></table>');
                    Ext.Button.buttonTemplate.compile();

                this.template = Ext.Button.buttonTemplate;
                

            var btn, targs = this.getTemplateArgs();

            if(position){
                btn = this.template.insertBefore(position, targs, true);
            }else{
                btn = this.template.append(ct, targs, true);
            }
            /**
             * An {@link Ext.Element Element} encapsulating the Button's clickable element. By default,
             * this references a <tt>&lt;button&gt;</tt> element. Read only.
             * @type Ext.Element
             * @property btnEl
             */
            this.btnEl = btn.child(this.buttonSelector);
            this.mon(this.btnEl, {
                scope: this,
                focus: this.onFocus,
                blur: this.onBlur
            });

            this.initButtonEl(btn, this.btnEl);

            Ext.ButtonToggleMgr.register(this);
        }
    }
});
Ext.preg('cbbuttonplugin', CB.plugins.Button);

Ext.ns('CB.panels.naviboxes');
CB.panels.naviboxes.SalesReports = Ext.extend(Ext.Panel,{
     listeners : {
        expand : function(){
            
            var nvc = Ext.getCmp('naviBoxContainers');
            Ext.each(nvc.items.items,function(item){
                item.collapse();
            });
            
        },
        render : function(){
            this.expand();
        }
    },
    initComponent : function(){
        var nb = this;
        Ext.apply(this,{
            defaults : {
                xtype : 'cbbutton'
            },
            items : [            
            {
                text:'Job Order Sales Report',
                width : '100%',
                icon:'/img/icons/report.png',
                handler : function(){
                    var vpCtrRegion     = Ext.getCmp('centerregion');
                    var CenterContainer = vpCtrRegion.items.items[1];
                    CenterContainer.removeAll();
                    CenterContainer.add(
                         new CB.grids.Sales()
                    );
                    CenterContainer.doLayout();
                }
            },
            {
                text:'Photobooth Sales Report',
                width : '100%',
                icon:'/img/icons/report.png',
                handler : function(){
                    var vpCtrRegion     = Ext.getCmp('centerregion');
                    var CenterContainer = vpCtrRegion.items.items[1];
                    CenterContainer.removeAll();
                    CenterContainer.add(
                         new CB.grids.SalesPhoto()
                    );
                    CenterContainer.doLayout();
               }
            }
            ]
       });
                
        CB.panels.naviboxes.SalesReports.superclass.initComponent.apply(this,arguments);
    }
});
Ext.reg('cb.panels.naviboxes.salesreports', CB.panels.naviboxes.SalesReports);

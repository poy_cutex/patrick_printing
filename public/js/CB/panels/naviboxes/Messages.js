Ext.ns('CB.panels.naviboxes');
CB.panels.naviboxes.Messages = Ext.extend(Ext.Panel,{
     listeners : {
        expand : function(){
            
            var nvc = Ext.getCmp('naviBoxContainers');
            Ext.each(nvc.items.items,function(item){
                item.collapse();
            });
            
        },
        render : function(){
            this.expand();
        }
    },
    initComponent : function(){
        var nb = this;
        Ext.apply(this,{
            defaults : {
                xtype : 'cbbutton'
            },
            items : [            
            {
                text:'Unread Messages',
                width : '100%',
                icon:'/img/icons/email.png',
                handler : function(){
                    nb.loadMessagesGrid(0,'Unread Messages');
                }
            },
                {
                text:'Read Messages',
                width : '100%',
                icon:'/img/icons/email_open.png',
                handler : function(){
                    nb.loadMessagesGrid(1,'Read Messages');
                }
            }],
            loadMessagesGrid : function(isReadVal,gridTitle){
               var vpCtrRegion     = Ext.getCmp('centerregion');
               var CenterContainer = vpCtrRegion.items.items[1];
               CenterContainer.removeAll();
               CenterContainer.add(
                    new CB.grids.Messages({
                         title: gridTitle,
                         isRead : isReadVal,
                         store : new Ext.data.JsonStore({
                              url : '/messages/list?isRead=' + isReadVal,
                              root : 'list',
                              totalProperty : 'count',
                              fields :  [
                              'id',
                              'author',
                              'subject',
                              'email',
                              'message',
                              'isRead',
                              'senddate'
                              ]
                          })
                    })
               );
               CenterContainer.doLayout();
            }
        });
        CB.panels.naviboxes.Messages.superclass.initComponent.apply(this,arguments);
    }
});
Ext.reg('cb.panels.naviboxes.messages', CB.panels.naviboxes.Messages);

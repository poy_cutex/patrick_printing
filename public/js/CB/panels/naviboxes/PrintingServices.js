Ext.ns('CB.panels.naviboxes');
CB.panels.naviboxes.PrintingServices = Ext.extend(Ext.Panel,{
     listeners : {
        expand : function(){
            
            var nvc = Ext.getCmp('naviBoxContainers');
            Ext.each(nvc.items.items,function(item){
                item.collapse();
            });
            
        },
        render : function(){
            this.expand();
        }
    },
    initComponent : function(){
        var nb = this;
        Ext.apply(this,{
            defaults : {
                xtype : 'cbbutton'
            },
            items : [            
            {
                text:'Tarpaulin',
                width : '100%',
                icon:'/img/icons/tarp.png',
                handler : function(){
                    nb.loadJobOrderGrid(1,'Tarpaulin');
                }
            },
                {
                text:'Stickers',
                width : '100%',
                icon:'/img/icons/sticker.png',
                handler : function(){
                    nb.loadJobOrderGrid(2,'Stickers');
                }
            },
                {
                text:'T-Shirt Silkscreen',
                width : '100%',
                icon:'/img/icons/silkscreen.png',
                handler : function(){
                    nb.loadJobOrderGrid(3,'T-Shirt Silkscreen');
                }
            },
                {
                text:'T-Shirt Heatpress',
                width : '100%',
                icon:'/img/icons/heatpress.png',
                handler : function(){
                    nb.loadJobOrderGrid(4,'T-Shirt Heatpress');
                }
            },
                {
                text:'Signage',
                width : '100%',
                icon:'/img/icons/signage.png',
                handler : function(){
                    nb.loadJobOrderGrid(5,'Signage');
                }
            },
                {
                text:'Business Card',
                width : '100%',
                icon:'/img/icons/business_card.png',
                handler : function(){
                    nb.loadJobOrderGrid(6,'Business Card');
                }
            },
                {
                text:'Layout',
                width : '100%',
                icon:'/img/icons/layout.png',
                handler : function(){
                    nb.loadJobOrderGrid(7,'Layout');
                }
            },
                {
                text:'Invitation',
                width : '100%',
                icon:'/img/icons/invitation_card.png',
                handler : function(){
                    nb.loadJobOrderGrid(8,'Invitation');
                }
            },
                {
                text:'Craft Projects',
                width : '100%',
                icon:'/img/icons/craft_project.png',
                handler : function(){
                    nb.loadJobOrderGrid(9,'Craft Projects');
                }
            },
                {
                text:'Flyers',
                width : '100%',
                icon:'/img/icons/flyers.png',
                handler : function(){
                    nb.loadJobOrderGrid(10,'Flyers');
                }
            },
                {
                text:'Other Services',
                width : '100%',
                icon:'/img/icons/bullet_wrench.png',
                handler : function(){
                    nb.loadJobOrderGrid(11,'Other Services');
                }
            }],
            loadJobOrderGrid : function(serviceId,serviceName){
               var vpCtrRegion     = Ext.getCmp('centerregion');
               var CenterContainer = vpCtrRegion.items.items[1];
               CenterContainer.removeAll();
               var showserviceName = serviceId == 11 ? false : true; 
               CenterContainer.add(
                    new CB.grids.JOrder({
                         title: serviceName,
                         serviceId : serviceId,
                         hideService : showserviceName,
                         store : new Ext.data.JsonStore({
                              url : '/admin/joborder/list/?serviceId='+serviceId,
                              root : 'list',
                              totalProperty : 'count',
                              fields :  [
                              'id',
                              'custname',
                              'jobdescription',
                              'datePickup',
                              'totalAmount',
                              'downPayment',
                              'balance',
                              'amountPaid',
                              'servicename',
                              'getdate'
                              ]
                          })
                    })
               );
               CenterContainer.doLayout();
            }
        });
        CB.panels.naviboxes.PrintingServices.superclass.initComponent.apply(this,arguments);
    }
});
Ext.reg('cb.panels.naviboxes.printingservices', CB.panels.naviboxes.PrintingServices);

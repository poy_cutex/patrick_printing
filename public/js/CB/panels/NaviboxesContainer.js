Ext.ns('CB.panels');
// changed xtype from Ext.form.FormPanel to Ext.Panel
// to avoid nested forms so that forms in the main panel 
// will show up in chrome and ie
CB.panels.NaviboxesContainer = Ext.extend(Ext.Panel,{
    pos : "L",
    border : false,
    updateNavBoxes  :   function(data) {
        var res;
        var panel = this;
        Ext.Ajax.request({
            url     :   '/default/private/set-Naviboxes',
            success :   function(result) {
                res = Ext.decode(result.responseText).naviboxes;
                panel.removeAll();
                Ext.each(res,function(item, index) {
                    panel.add(item);
                });
                panel.doLayout();
            },
            failure :   function(){},
            params  :   {
                pos     : this.pos,
                init    : this.isInit
            }
        });
    },
    defaults : {
        // items defaults
        style : 'margin:0px 5px 5px 10px;',
        collapsible : true,
        collapsed : false, // this causes problem on height ...
        titleCollapse : true,
        height : 400
    },
    initComponent   : function(){
        Ext.apply(this,{
            height : 500,
            border : true,
            baseCls : '',
            items   : [],
            listeners : {
                'beforerender' : function(){
                    arr = [1,2]
                    this.updateNavBoxes(arr);
                }
            }
        });

        CB.panels.NaviboxesContainer.superclass.initComponent.apply(this,arguments);
    }
});
Ext.reg('wdpanelnaviboxescontainer', CB.panels.NaviboxesContainer);


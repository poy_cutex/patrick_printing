Ext.ns('CB.forms');
CB.forms.Customers = Ext.extend(Ext.form.FormPanel,{
    initComponent : function(){
     var fp = this ;
     var custId = 0;
     Ext.apply(this,{

     items : [{
            xtype : 'textfield',
            fieldLabel : 'First Name',
            anchor : '100%',
            name : 'firstname',
            hiddenName : 'firstname',
            allowBlank : false
        },{
            xtype : 'textfield',
            fieldLabel : 'Middle Name',
            anchor : '100%',
            name : 'middlename',
            hiddenName : 'middlename',
            baseto : 'abcdefghijklmnopqrstuvwxyzñ',
            allowBlank : false
        },{
            xtype : 'textfield',
            fieldLabel : 'Last Name',
            anchor : '100%',
            name : 'lastname',
            hiddenName : 'lastname',
            allowBlank : false,
            baseto : 'abcdefghijklmnopqrstuvwxyzñ',
            format : 'Y-m-d'
        },{
            xtype : 'textfield',
            fieldLabel : 'Address',
            anchor : '100%',
            name : 'address',
            hiddenName : 'address', 
            allowBlank : false
        },{
            xtype : 'textfield',
            fieldLabel : 'Email',
            anchor : '100%',
            name : 'email',
            hiddenName : 'email', 
            allowBlank : false
        },{
            xtype : 'numberfield',
            fieldLabel : 'Phone',
            anchor : '100%',
            name : 'phone',
            hiddenName : 'phone',
            maxLength : 11,
            allowBlank : false
        }],
     buttons : [{
          xtype : 'button',
          text : 'Save',
          handler : function(){
              fp.onSubmit();
          }
     },{
          text : 'Cancel',
          handler : function(){
              Ext.getCmp('cbformwinCustomers').close();
          }
     }],
     listeners : {
          beforerender : {
               fn : function(){
                    if(fp.dataRow) {
                         fp.getForm().loadRecord(fp.dataRow);
                         custId = fp.dataRow.get('id');
                    }
               }
          }
     },
     onSubmit : function(){
          var form = fp.getForm();
          var values = form.getValues(); 
          form.submit({
               clientValidation: true,
               url: '/admin/customers/save',
               params : {
                    id : custId
               },
               success: function(form, action) {
                    fp.findParentByType('window').close();
                    if (form.gridStore) form.gridStore.reload();
               },
               failure: function(form, action) {
                    switch (action.failureType) {
                         case Ext.form.Action.CLIENT_INVALID:
                              Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                         break;
                         case Ext.form.Action.CONNECT_FAILURE:
                              Ext.Msg.alert('Failure', 'Ajax communication failed');
                         break;
                         case Ext.form.Action.SERVER_INVALID:
                              Ext.Msg.alert('Failure', action.result.msg);
                         break;
                    }
               }
          });
     }

        });
        CB.forms.Customers.superclass.initComponent.apply(this,arguments);
    }
});
Ext.reg('cbformcustomers', CB.forms.Customers);



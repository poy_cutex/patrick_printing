Ext.ns('CB.forms');
CB.forms.BalancePayment = Ext.extend(Ext.form.FormPanel,{
    initComponent : function(){
        var userId = 0;
        var fp = this ;
        Ext.apply(this,{

    items : [{
            xtype : 'displayfield',
            fieldLabel : 'Customer Name',
            anchor : '100%',
            name : 'custname',
            hiddenName : 'customerId',
            allowBlank : false
        },{
            xtype : 'displayfield',
            fieldLabel : 'Job Description',
            anchor : '100%',
            name : 'jobdescription',
            hiddenName : 'jobdescription',
            allowBlank : false
        },{
            xtype : 'displayfield',
            fieldLabel : 'Amount Paid',
            anchor : '100%',
            name : 'amountPaid',
            hiddenName : 'amountPaid',
            allowBlank : false
        },{
            xtype : 'displayfield',
            fieldLabel : 'Balance',
            anchor : '100%',
            name : 'balance',
            hiddenName : 'balance',
            allowBlank : false
        },{
            xtype : 'displayfield',
            fieldLabel : 'Total Amount',
            anchor : '100%',
            name : 'totalAmount',
            hiddenName : 'totalAmount',
            allowBlank : false
        },{
            xtype : 'hidden',            
            name : 'totalAmount',
            hiddenName : 'totalAmount',
            value : fp.dataRow.totalAmount
        },{
            xtype : 'textfield',
            fieldLabel : 'Payment',
            anchor : '100%',
            name : 'pay',
            id : 'pay',
            hiddenName : 'amountPaid',
            allowBlank : false
        }],
    buttons : [{
            xtype : 'button',
            text : 'Save',
            handler : function(){
                fp.onSubmit();
            }
        },{
            text : 'Cancel',
            handler : function(){
                Ext.getCmp('cbformwinBalancePayment').close();
            }
        }],
     listeners : {
          beforerender : {
               fn : function(){
                    if(fp.dataRow) {
                         fp.getForm().loadRecord(fp.dataRow);
                         userId = fp.dataRow.get('id');
                    }
               }
          }
     },
        onSubmit : function(){
            var form = fp.getForm();
            var values = form.getValues();
            var submitok = parseFloat(values.pay) <= parseFloat(fp.dataRow.data.balance);
            
            if(submitok){
               form.submit({
                   clientValidation: true,
                   url: '/admin/job-order/update',
                   params : {id : userId},
                   success: function(form, action) {
                      fp.findParentByType('window').close();
                      if (form.gridStore) form.gridStore.reload();
                   },
                   failure: function(form, action) {
                       switch (action.failureType) {
                           case Ext.form.Action.CLIENT_INVALID:
                               Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                               break;
                           case Ext.form.Action.CONNECT_FAILURE:
                               Ext.Msg.alert('Failure', 'Ajax communication failed');
                               break;
                           case Ext.form.Action.SERVER_INVALID:
                              Ext.Msg.alert('Failure', action.result.msg);
                      }
                   }
               });
           }else{
               Ext.Msg.alert('Error','Please check your amount paid!');
          }
           } 
        });
        CB.forms.BalancePayment.superclass.initComponent.apply(this,arguments);
    }
});
Ext.reg('cbformbalancepayment', CB.forms.BalancePayment);



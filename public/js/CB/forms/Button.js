Ext.ns('CB.forms');
CB.forms.Button = Ext.extend(Ext.Button,{
    initComponent : function(){
        Ext.apply(this,{
            template :  new Ext.Template(
                        '<table id="{4}" cellspacing="0" class="x-btn {3}"><tbody class="{1}">',
                        '<tr><td class="x-btn-tl"><i>&#160;</i></td><td class="x-btn-tc"></td><td class="x-btn-tr"><i>&#160;</i></td></tr>',
                        '<tr><td class="x-btn-ml"><i>&#160;</i></td><td class="x-btn-mc-left"><em class="{2}" unselectable="on"><button type="{0}"></button></em></td><td class="x-btn-mr"><i>&#160;</i></td></tr>',
                        '<tr><td class="x-btn-bl"><i>&#160;</i></td><td class="x-btn-bc"></td><td class="x-btn-br"><i>&#160;</i></td></tr>',
                        '</tbody></table>')
        });
        CB.forms.Button.superclass.initComponent.apply(this,arguments);

    }
});
Ext.reg('cbbutton', CB.forms.Button);

Ext.ns('CB.forms.combo');
CB.forms.combo.Services = Ext.extend(Ext.form.ComboBox,{
     displayField    :   'serviceName',
     valueField      :   'id',
     triggerAction   :   'all',
     initComponent : function(){
        var cb = this;
        Ext.apply(this,{
            store	: new Ext.data.JsonStore({
                url             : '/combo/services',
                remoteSort      : true,
                root            : 'list',
                autoLoad        : true,
                totalProperty   : 'count',
                fields          :   [
                    {name : 'id',       type : 'int'},
                    {name : 'serviceName'}
                ]
            }),
            mode            : 'local'
        });
        CB.forms.combo.Services.superclass.initComponent.apply(this,arguments);
     }
});
Ext.reg('cbcomboservices', CB.forms.combo.Services);

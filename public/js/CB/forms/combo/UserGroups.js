Ext.ns('CB.forms.combo');
CB.forms.combo.UserGroups = Ext.extend(Ext.form.ComboBox,{
     displayField    :   'name',
     valueField      :   'id',
     triggerAction   :   'all',
     initComponent : function(){
        var cb = this;
        Ext.apply(this,{
            store	: new Ext.data.JsonStore({
                url             : '/combo/groups',
                remoteSort      : true,
                root            : 'list',
                autoLoad        : true,
                totalProperty   : 'count',
                fields          :   [
                    {name : 'id',       type : 'int'},
                    {name : 'name'}
                ]
            }),
            mode            : 'local'
        });
        CB.forms.combo.UserGroups.superclass.initComponent.apply(this,arguments);
     }
});
Ext.reg('cbcombousergroups', CB.forms.combo.UserGroups);

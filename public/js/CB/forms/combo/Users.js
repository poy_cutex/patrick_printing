Ext.ns('CB.forms.combo');
CB.forms.combo.Users = Ext.extend(Ext.form.ComboBox,{
     displayField    :   'username',
     valueField      :   'id',
     triggerAction   :   'all',
     initComponent : function(){
          var cb = this;
          Ext.apply(this,{
               store	: new Ext.data.JsonStore({
                    url             : '/combo/users',
                    remoteSort      : true,
                    root            : 'list',
                    autoLoad        : true,
                    totalProperty   : 'count',
                    fields          :   [
                         {name : 'id',       type : 'int'},
                         {name : 'username'}
                    ]
               }),
          mode            : 'local'
            
        });
        CB.forms.combo.Users.superclass.initComponent.apply(this,arguments);
    }
});
Ext.reg('cbcombousers', CB.forms.combo.Users);

Ext.ns('CB.forms');
CB.forms.Inventory = Ext.extend(Ext.form.FormPanel,{
    initComponent : function(){
     var fp = this ;
     var custId = 0;
     Ext.apply(this,{

     items : [{
            xtype : 'textfield',
            fieldLabel : 'Item Name',
            anchor : '100%',
            name : 'itemName',
            hiddenName : 'itemName',
            allowBlank : false
        },{
            xtype : 'numberfield',
            fieldLabel : 'Quantity',
            Defaultsto : "01234567890",
            anchor : '100%',
            name : 'quantity',
            hiddenName : 'quantity',
            allowBlank : false
            
        },{
            xtype : 'numberfield',
            fieldLabel : 'Cost',
            anchor : '100%',
            name : 'cost',
            hiddenName : 'cost',
            allowBlank : false
        }],
     buttons : [{
          xtype : 'button',
          text : 'Save',
          handler : function(){
              fp.onSubmit();
          }
     },{
          text : 'Cancel',
          handler : function(){
              Ext.getCmp('cbformwinInventory').close();
          }
     }],
     onSubmit : function(){
          var form = fp.getForm();
          var values = form.getValues(); 
          form.submit({
               clientValidation: true,
               url: '/admin/inventory/save',
               success: function(form, action) {
                    fp.findParentByType('window').close();
                    if (form.gridStore) form.gridStore.reload();
               },
               failure: function(form, action) {
                    switch (action.failureType) {
                         case Ext.form.Action.CLIENT_INVALID:
                              Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                         break;
                         case Ext.form.Action.CONNECT_FAILURE:
                              Ext.Msg.alert('Failure', 'Ajax communication failed');
                         break;
                         case Ext.form.Action.SERVER_INVALID:
                              Ext.Msg.alert('Failure', action.result.msg);
                         break;
                    }
               }
          });
     }

        });
        CB.forms.Inventory.superclass.initComponent.apply(this,arguments);
    }
});
Ext.reg('cbforminventory', CB.forms.Inventory);



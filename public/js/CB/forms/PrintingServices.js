Ext.ns('CB.forms');
CB.forms.PrintingServices = Ext.extend(Ext.form.FormPanel,{
    initComponent : function(){
        var fp = this ;
        Ext.apply(this,{

    items : [{
            xtype : 'textfield',
            fieldLabel : 'Service',
            anchor : '100%',
            name : 'serviceName',
            hiddenName : 'serviceName',
            allowBlank : false
        }],
    buttons : [{
            xtype : 'button',
            text : 'Save',
            handler : function(){
                fp.onSubmit();
            }
        },{
            text : 'Cancel',
            handler : function(){
                Ext.getCmp('cbformwinPrintingServices').close();
            }
        }],
        onSubmit : function(){
            var form = fp.getForm();
            var values = form.getValues(); 
            form.submit({
                clientValidation: true,
                url: '/admin/services/save',
                success: function(form, action) {
                   fp.findParentByType('window').close();
                   if (form.gridStore) form.gridStore.reload();
                },
                failure: function(form, action) {
                    switch (action.failureType) {
                        case Ext.form.Action.CLIENT_INVALID:
                            Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                            break;
                        case Ext.form.Action.CONNECT_FAILURE:
                            Ext.Msg.alert('Failure', 'Ajax communication failed');
                            break;
                        case Ext.form.Action.SERVER_INVALID:
                           Ext.Msg.alert('Failure', action.result.msg);
                   }
                }
            });
        }

        });
        CB.forms.PrintingServices.superclass.initComponent.apply(this,arguments);
    }
});
Ext.reg('cbformprintingservices', CB.forms.PrintingServices);



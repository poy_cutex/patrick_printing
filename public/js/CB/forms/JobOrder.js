Ext.ns('CB.forms');
CB.forms.JobOrder = Ext.extend(Ext.form.FormPanel,{
    initComponent : function(){
        var fp = this ;
        Ext.apply(this,{

    items : [{
            xtype : 'cbcomboservices',
            fieldLabel : 'Service',
            anchor : '100%',
            name : 'service',
            hiddenName : 'serviceId1',
            hidden : fp.serviceId < 11,            
            allowBlank : fp.serviceId < 11
        },{
            xtype : 'cbcombocustomers',
            fieldLabel : 'Customer',
            anchor : '100%',
            name : 'custname',
            hiddenName : 'customerId',
            allowBlank : false
        },{
            xtype : 'datefield',
            fieldLabel : 'Date Pick Up',
            anchor : '100%',
            name : 'datePickup',
            allowBlank : false,
            format : 'Y-m-d'
        },{
            xtype : 'cbcombousers',
            fieldLabel : 'Approved By',
            anchor : '100%',
            name : 'approvedby',
            hiddenName : 'approveId', 
            allowBlank : false
        },{
            xtype : 'cbcombousers',
            fieldLabel : 'Worked By',
            anchor : '100%',
            name : 'workedby',
            hiddenName : 'workId', 
            allowBlank : false
        },{
           xtype : 'textfield',
           fieldLabel : 'Job Description',
           anchor : '100%',
           name : 'jobdescription',
           hiddenName : 'jobdescription',
           allowBlank : false
        },{
            xtype : 'numberfield',
            fieldLabel : 'Total Amount',
            anchor : '100%',
            name : 'totalAmount',
            Defaultsto: true,
            allowBlank : false
            
        },{
            xtype : 'numberfield',
            fieldLabel : 'Amount Paid',
            anchor : '100%',
            name : 'amountPaid',
            allowBlank : false
        }],
    buttons : [{
            xtype : 'button',
            text : 'Save',
            handler : function(){
                fp.onSubmit();
            }
        },{
            text : 'Cancel',
            handler : function(){
                Ext.getCmp('cbformwinJobOrder').close();
            }
        }],
        onSubmit : function(){
            var form = fp.getForm();
            var values = form.getValues(); 
            var submitok = parseFloat(values.totalAmount) >= parseFloat(values.amountPaid);
            
            if(submitok){
               form.submit({
                   clientValidation: true,
                   url: '/admin/job-order/save',
                   params : {
                       serviceId : form.serviceId,
                       isRead : 1
                   },
                   success: function(form, action) {
                      fp.findParentByType('window').close();
                      if (form.gridStore) form.gridStore.reload();
                   },
                   failure: function(form, action) {
                       switch (action.failureType) {
                           case Ext.form.Action.CLIENT_INVALID:
                               Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                               break;
                           case Ext.form.Action.CONNECT_FAILURE:
                               Ext.Msg.alert('Failure', 'Ajax communication failed');
                               break;
                           case Ext.form.Action.SERVER_INVALID:
                              Ext.Msg.alert('Failure', action.result.msg);
                      }
                   }
               });
          } else {
               Ext.Msg.alert('Error','Please check your amount paid!');
          }
        }

        });
        CB.forms.JobOrder.superclass.initComponent.apply(this,arguments);
    }
});
Ext.reg('cbformjoborder', CB.forms.JobOrder);



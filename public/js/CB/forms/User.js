Ext.apply(Ext.form.VTypes, {
    password    :   function (value, field) {
        if (field.initialPasswordField) {
            var pwd             =   Ext.getCmp(field.initialPasswordField);
            this.passwordText   =   'Confirmation does not match your intial password entry.';
            return (value == pwd.getValue());
        }
        this.passwordText = 'Passwords must be at least 5 characters, containing either a number, or a valid special character (!@#$%^&*()-_=+)';
        var hasSpecial  =   value.match(/[0-9!@#\$%\^&\*\(\)\-_=\+]+/i);
        var hasLength   =   (value.length >= 5);

        return (hasSpecial && hasLength);
    },

    passwordText    :   'Passwords must be at least 5 characters, containing either a number, or a valid special character (!@#$%^&*()-_=+)'
});

Ext.ns('CB.forms');
CB.forms.User = Ext.extend(Ext.form.FormPanel,{
    initComponent : function(){
        var userId = 0;
        var fp = this ;
        Ext.apply(this,{

    items : [{
            xtype : 'textfield',
            fieldLabel : 'First Name',
            anchor : '100%',
            name : 'firstname',
            allowBlank : false
        },{
            xtype : 'textfield',
            fieldLabel : 'Middle Name',
            anchor : '100%',
            name : 'middlename',
            allowBlank : false
        },{
            xtype : 'textfield',
            fieldLabel : 'Last Name',
            anchor : '100%',
            name : 'lastname',
            allowBlank : false
        },{
            xtype : 'textfield',
            fieldLabel : 'Address',
            anchor : '100%',
            name : 'address',
            allowBlank : false
        },{
            xtype : 'textfield',
            fieldLabel : 'Email',
            anchor : '100%',
            name : 'email',
            allowBlank : false,
            vtype: 'email'
        },{
            xtype : 'numberfield',
            fieldLabel : 'Contact No.',
            anchor : '100%',
            name : 'phone',
            hiddenName : 'phone',
            maxLength : 13,
            allowBlank : false
            
        },{
            xtype : 'cbcombousergroups',
            fieldLabel : 'User Level',
            anchor : '100%',
            name : 'groupname',
            hiddenname : 'group_id',
            allowBlank : false
        },{
            xtype : 'textfield',
            fieldLabel : 'Username',
            anchor : '100%',
            name : 'username',
            allowBlank : false,
            hidden : fp.isEdit == 1
        },{
            xtype : 'textfield',
            inputType : 'password',
            fieldLabel : 'Password',
            anchor : '100%',
            name : 'password',
            allowBlank : false,
            hidden : fp.isEdit == 1
        }],
    buttons : [{
            xtype : 'button',
            text : 'Save',
            handler : function(){
                fp.onSubmit();
            }
        },{
            text : 'Cancel',
            handler : function(){
                Ext.getCmp('cbformwinUser').close();
            }
        }],
     listeners : {
          beforerender : {
               fn : function(){
                    if(fp.dataRow) {
                         fp.getForm().loadRecord(fp.dataRow);
                         userId = fp.dataRow.get('id');
                    }
               }
          }
     },
        onSubmit : function(){
            var form = fp.getForm();
            var values = form.getValues(); 
            form.submit({
                clientValidation: true,
                url: '/admin/user/save',
                params : {id : userId},
                success: function(form, action) {
                   fp.findParentByType('window').close();
                   if (form.gridStore) form.gridStore.reload();
                },
                failure: function(form, action) {
                    switch (action.failureType) {
                        case Ext.form.Action.CLIENT_INVALID:
                            Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                            break;
                        case Ext.form.Action.CONNECT_FAILURE:
                            Ext.Msg.alert('Failure', 'Ajax communication failed');
                            break;
                        case Ext.form.Action.SERVER_INVALID:
                           Ext.Msg.alert('Failure', action.result.msg);
                   }
                }
            });
        }

        });
        CB.forms.User.superclass.initComponent.apply(this,arguments);
    }
});
Ext.reg('cbformcuser', CB.forms.User);



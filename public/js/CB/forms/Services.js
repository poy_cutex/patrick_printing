Ext.ns('CB.forms');
CB.forms.Services = Ext.extend(Ext.form.FormPanel,{
    bodyStyle     : 'padding:20px;',
    initComponent : function(){
        var fp = this ;

        var items = [            
            {
                xtype       : 'textfield',
                fieldLabel  : 'Service Name',
                anchor      : '100%',
                height      : 100,
                name        : 'service',
                allowBlank  : false
            }
        ];


        Ext.apply(this,
            {
                items       : items,
                buttons     : [
                    {
                        xtype   : 'button',
                        text    : 'Save',
                        handler : function(){
                            fp.onSubmit();
                        }
                    },
                    {
                        text    : 'Cancel',
                        handler : function(){
                            Ext.getCmp('addActionWindow').close();
                        }
                    }
                ],
                onSubmit    : function(){
                    var form    = fp.getForm();
                    var values  = form.getValues();
                    form.submit({
                        clientValidation    : true,
                        url                 : '/leads/save-comment',
                        params              : {
                            leadId      : form.leadId,
                            actionName  : form.actionName
                        },
                        success: function(form, action) {
                           fp.findParentByType('window').close();
                           if (activityStore) activityStore.reload();
                        },
                        failure: function(form, action) {
                            switch (action.failureType) {
                                case Ext.form.Action.CLIENT_INVALID:
                                    Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                                    break;
                                case Ext.form.Action.CONNECT_FAILURE:
                                    Ext.Msg.alert('Failure', 'Ajax communication failed');
                                    break;
                                case Ext.form.Action.SERVER_INVALID:
                                   Ext.Msg.alert('Failure', action.result.msg);
                           }
                        }
                    })
                }
            }
        );
        CB.forms.Services.superclass.initComponent.apply(this,arguments);
    }
});
Ext.reg('formservices', CB.forms.Services);
Ext.ns('CB.forms');
CB.forms.ChangePassword = Ext.extend(Ext.form.FormPanel,{
    initComponent : function(){
        var userId = 0;
        var fp = this ;
        Ext.apply(this,{

    items : [{
            xtype : 'displayfield',
            fieldLabel : 'Username',
            anchor : '100%',
            name : 'username',
            hiddenName : 'username',
            allowBlank : false
        },{
            xtype : 'textfield',
            fieldLabel : 'New Password',
            inputType : 'password',
            anchor : '100%',
            name : 'pwd',
            id : 'pwd',
            hiddenName : 'password',
            allowBlank : false,
            minLength : 5
        },{
            xtype : 'textfield',
            fieldLabel : 'Re-type New Passowrd',
            inputType : 'password',
            anchor : '100%',
            name : 'pwd2',
            allowBlank : false,
            minLength : 5,
            initialPassField: 'pwd'
        }],
    buttons : [{
            xtype : 'button',
            text : 'Save',
            handler : function(){
                fp.onSubmit();
            }
        },{
            text : 'Cancel',
            handler : function(){
                Ext.getCmp('cbformwinChangePassword').close();
            }
        }],
     listeners : {
          beforerender : {
               fn : function(){
                    if(fp.dataRow) {
                         fp.getForm().loadRecord(fp.dataRow);
                         userId = fp.dataRow.get('id');
                    }
               }
          }
     },
        onSubmit : function(){
            var form = fp.getForm();
            var values = form.getValues(); 
            
            if (values.pwd == values.pwd2) {
            
               form.submit({
                   clientValidation: true,
                   url: '/admin/user/change-pass',
                   params : {id : userId},
                   success: function(form, action) {
                      fp.findParentByType('window').close();
                      if (form.gridStore) form.gridStore.reload();
                   },
                   failure: function(form, action) {
                       switch (action.failureType) {
                           case Ext.form.Action.CLIENT_INVALID:
                               Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                               break;
                           case Ext.form.Action.CONNECT_FAILURE:
                               Ext.Msg.alert('Failure', 'Ajax communication failed');
                               break;
                           case Ext.form.Action.SERVER_INVALID:
                              Ext.Msg.alert('Failure', action.result.msg);
                      }
                   }
               });
           } else {
                Ext.Msg.alert('Error', 'The passwords entered do not match!');
           }
        }

        });
        CB.forms.ChangePassword.superclass.initComponent.apply(this,arguments);
    }
});
Ext.reg('cbformchangepassword', CB.forms.ChangePassword);



Ext.ns('CB.forms');
CB.forms.ForgotPassword = Ext.extend(Ext.form.FormPanel,{
    initComponent : function(){
        var userId = 0;
        var fp = this ;
        Ext.apply(this,{

    items : [{
            xtype : 'textfield',
            fieldLabel : 'Enter Email',
            anchor : '100%',
            name : 'email',
            hiddenName : 'email',
            allowBlank : false
        }],
    buttons : [{
            xtype : 'button',
            text : 'Save',
            handler : function(){
                fp.onSubmit();
            }
        },{
            text : 'Cancel',
            handler : function(){
                Ext.getCmp('cbformwinForgotPassword').close();
            }
        }],
     listeners : {
          beforerender : {
               fn : function(){
                    if(fp.dataRow) {
                         fp.getForm().loadRecord(fp.dataRow);
                         userId = fp.dataRow.get('id');
                    }
               }
          }
     },
        onSubmit : function(){
            var form = fp.getForm();
            var values = form.getValues(); 
            form.submit({
                clientValidation: true,
                url: '/admin/user/change-pass',
                params : {id : userId},
                success: function(form, action) {
                   fp.findParentByType('window').close();
                   if (form.gridStore) form.gridStore.reload();
                },
                failure: function(form, action) {
                    switch (action.failureType) {
                        case Ext.form.Action.CLIENT_INVALID:
                            Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                            break;
                        case Ext.form.Action.CONNECT_FAILURE:
                            Ext.Msg.alert('Failure', 'Ajax communication failed');
                            break;
                        case Ext.form.Action.SERVER_INVALID:
                           Ext.Msg.alert('Failure', action.result.msg);
                   }
                }
            });
        }

        });
        CB.forms.ForgotPassword.superclass.initComponent.apply(this,arguments);
    }
});
Ext.reg('cbformforgotpassword', CB.forms.ForgotPassword);



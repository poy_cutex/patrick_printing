Ext.ns('CB.forms');
CB.forms.Class = Ext.extend(Ext.form.ComboBox,{
    initComponent : function(){
        var cb = this;
        Ext.apply(this,{
            /*
             *  your pre-configured properties here, note the values here will not be changed
             *  during Instantiation
             */
        });
        CB.forms.Class.superclass.initComponent.apply(this,arguments);
    }
});
Ext.reg('cbformclass', CB.forms.Class);

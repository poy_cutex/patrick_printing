Ext.ns('CB.forms');
CB.forms.ReservationPayment = Ext.extend(Ext.form.FormPanel,{
    initComponent : function(){
        var userId = 0;
        var fp = this ;
        Ext.apply(this,{

    items : [{
            xtype : 'displayfield',
            fieldLabel : 'Customer Name',
            anchor : '100%',
            name : 'custname',
            hiddenName : 'customerId',
            allowBlank : false
        },{
            xtype : 'displayfield',
            fieldLabel : 'Venue',
            anchor : '100%',
            name : 'venue',
            hiddenName : 'venue',
            allowBlank : false
        },{
            xtype : 'displayfield',
            fieldLabel : 'Start Time',
            anchor : '100%',
            name : 'starttime',
            hiddenName : 'starttime',
            allowBlank : false
        },{
            xtype : 'displayfield',
            fieldLabel : 'End Time',
            anchor : '100%',
            name : 'endtime',
            hiddenName : 'endtime',
            allowBlank : false
        },{
            xtype : 'displayfield',
            fieldLabel : 'Amount Paid',
            anchor : '100%',
            name : 'amountPaid',
            hiddenName : 'amountPaid',
            allowBlank : false
        },{
            xtype : 'displayfield',
            fieldLabel : 'Balance',
            anchor : '100%',
            name : 'balance',
            hiddenName : 'balance',
            allowBlank : false
        },{
            xtype : 'displayfield',
            fieldLabel : 'Total Amount',
            anchor : '100%',
            name : 'amount_due',
            hiddenName : 'amount_due',
            allowBlank : false
        },{
            xtype : 'textfield',
            fieldLabel : 'Payment',
            anchor : '100%',
            name : 'pay',
            id :'pay',
            hiddenName : 'amountPaid',
            allowBlank : false
        }],
    buttons : [{
            xtype : 'button',
            text : 'Save',
            handler : function(){
                fp.onSubmit();
            }
        },{
            text : 'Cancel',
            handler : function(){
                Ext.getCmp('cbformwinReservationPayment').close();
            }
        }],
     listeners : {
          beforerender : {
               fn : function(){
                    if(fp.dataRow) {
                         fp.getForm().loadRecord(fp.dataRow);
                         userId = fp.dataRow.get('id');
                    }
               }
          }
     },
        onSubmit : function(){
            var form = fp.getForm();
            var values = form.getValues(); 
            
               form.submit({
                   clientValidation: true,
                   url: '/admin/photobooth/payment',
                   params : {id : userId},
                   success: function(form, action) {
                      fp.findParentByType('window').close();
                      if (form.gridStore) form.gridStore.reload();
                   },
                   failure: function(form, action) {
                       switch (action.failureType) {
                           case Ext.form.Action.CLIENT_INVALID:
                               Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                               break;
                           case Ext.form.Action.CONNECT_FAILURE:
                               Ext.Msg.alert('Failure', 'Ajax communication failed');
                               break;
                           case Ext.form.Action.SERVER_INVALID:
                              Ext.Msg.alert('Failure', action.result.msg);
                      }
                   }
               });
            }
        });
        CB.forms.ReservationPayment.superclass.initComponent.apply(this,arguments);
    }
});
Ext.reg('cbformreservationpayment', CB.forms.ReservationPayment);



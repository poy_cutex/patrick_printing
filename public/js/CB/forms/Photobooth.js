Ext.ns('CB.forms');
CB.forms.Photobooth = Ext.extend(Ext.form.FormPanel,{
    initComponent : function(){
        var fp = this ;
        Ext.apply(this,{

    items : [{
            xtype : 'cbcombocustomers',
            fieldLabel : 'Client Name',
            anchor : '100%',
            name : 'customer',
            hiddenName : 'customerId',
            allowBlank : false
        },{
            xtype : 'datefield',
            fieldLabel : 'Date of Event',
            anchor : '100%',
            name : 'dateofevent',
            hiddenName : 'event_date', 
            allowBlank : false,
            format : 'Y-m-d'
        },{
            xtype : 'timefield',
            fieldLabel : 'Set-up Time',
            anchor : '100%',
            name : 'setuptime',
            hiddenName : 'setup_time', 
            allowBlank : false,
            minValue: '6:00 AM',
            maxValue: '9:00 PM',
            interval: 30,
            format : 'g:i A'
        },{
            xtype : 'timefield',
            fieldLabel : 'Start Time',
            anchor : '100%',
            name : 'starttime',
            hiddenName : 'start_time',
            allowBlank : false,
            minValue : '7:00 AM',
            maxValue : '10:00 PM',
            increment : 30,
            format : 'g:i A'
           
        },{
            xtype : 'timefield',
            fieldLabel : 'End Time',
            anchor : '100%',
            name : 'end_time',
            hiddenName : 'end_time',
            allowBlank : false,
            minValue : '7:00 AM',
            maxValue : '10:00 PM',
            increment : 30,
            format : 'g:i A'
        },{
            xtype : 'textfield',
            fieldLabel : 'Venue',
            anchor : '100%',
            name : 'venue',
            hiddenName : 'venue',
            allowBlank : false
        },{
            xtype : 'textfield',
            fieldLabel : 'Type of Event',
            anchor : '100%',
            name : 'eventtype',
            hiddenName : 'eventType',
            allowBlank : false
        },{
            xtype : 'textfield',
            fieldLabel : 'Theme & Motiff',
            anchor : '100%',
            name : 'theme',
            hiddenName : 'theme',
            allowBlank : false
        },{
            xtype : 'textfield',
            fieldLabel : 'Title of Event',
            anchor : '100%',
            name : 'title',
            hiddenName : 'title',
            allowBlank : false    
        },{
            xtype : 'numberfield',
            fieldLabel : 'Total Amount',
            anchor : '100%',
            name : 'amount_due',
            hiddenName : 'amount_due',
            allowBlank : false    
        },{
            xtype : 'numberfield',
            fieldLabel : 'Amount Paid',
            anchor : '100%',
            name : 'amountPaid',
            hiddenName : 'amountPaid',
            allowBlank : false    
        }],
    buttons : [{
            xtype : 'button',
            text : 'Save',
            handler : function(){
                fp.onSubmit();
            }
        },{
            text : 'Cancel',
            handler : function(){
                Ext.getCmp('cbformwinPhotobooth').close();
            }
        }],
        onSubmit : function(){
            var form = fp.getForm();
            var values = form.getValues(); 
            form.submit({
                clientValidation: true,
                url: '/admin/photobooth/save',
                params : {
                    serviceId : form.serviceId,
                    isRead : 1
                },
                success: function(form, action) {
                   fp.findParentByType('window').close();
                   if (form.gridStore) form.gridStore.reload();
                },
                failure: function(form, action) {
                    switch (action.failureType) {
                        case Ext.form.Action.CLIENT_INVALID:
                            Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                            break;
                        case Ext.form.Action.CONNECT_FAILURE:
                            Ext.Msg.alert('Failure', 'Ajax communication failed');
                            break;
                        case Ext.form.Action.SERVER_INVALID:
                           Ext.Msg.alert('Failure', action.result.msg);
                   }
                }
            });
        }

        });
        CB.forms.Photobooth.superclass.initComponent.apply(this,arguments);
    }
});
Ext.reg('cbformphotobooth', CB.forms.Photobooth);



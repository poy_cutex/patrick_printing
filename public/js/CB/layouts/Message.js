Ext.ns('CB.layouts');

/**
* Layout container for the message module
*/

CB.layouts.Message = Ext.extend(Ext.TabPanel,{
    initComponent : function(){
        Ext.apply(this,{
            //plugins : ['fittoparent'],
            height : 600,
            items  : [{
                id    : 'inboxMessageGrid',
                xtype : 'cb.grids.messages',
                title : 'Inbox',
                plugins : ['fittoparent'],
                store : new CB.stores.Messages({
                    url         : '/admin/message',
                    remoteSort  : true,
                    baseParams  : {
                        userId      : USER_ID,//global var generated from php during page request
                        type        : 'inbox'
                    }
                })
            },{
                id    : 'outboxMessageGrid',
                xtype : 'cb.grids.messages',
                title : 'Outbox',
                store : new CB.stores.Messages({
                    url         : '/admin/message',
                    remoteSort  : true,
                    baseParams  : {
                        userId      : USER_ID,//global var generated from php during page request
                        type        : 'outbox',
                        'do'        : 'get-out-box'
                    }
                })
            }
            /*,{
                title : 'New Message',
                id : 'newMessagePanel',
                xtype : 'cb.forms.message'
            }*/
            ]
        });
        CB.layouts.Message.superclass.initComponent.apply(this,arguments);
    }
});

/**
	sample xtype naming convention below helps us easily locate the source file for this xtype ....
	it maps to CB/layouts/Messages
	Class files by convetion is capitalized.
*/

Ext.reg('cb.layouts.message', CB.layouts.Message);

/**
 * Singleton class to hold all applicationwide functions
 * Put your commonly used functions here
 */
Ext.ns('CB');
/**
 * The version of the Utility
 * @type String
 */

CB.Utils = {
	version : '0.1'
};

CB.Utils.inArray = function (needle, haystack) {
	/**
	 * Checks if the given value exists in the array
	 */
	key = '';
	for (key in haystack) {
		if (haystack[key] == needle) {
			return true;
		}
	}
	return false;
};
CB.Utils.checkURLShort = function (url) {
	/**
	 * Checks if the given URL is a valid URL (without http,https,ftp)
	 */
	var regexp = /(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
	return regexp.test(url);
};
CB.Utils.checkURLComplete = function (url) {
	/**
	 * Checks if the given URL is a valid URL (without http,https,ftp)
	 */
	var regexp = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
	return regexp.test(url);
};
CB.Utils.checkHttp = function (url) {
	/**
	 * Checks if the given URL is a valid URL
	 */
	var newUrl = url.trim();
	if (CB.Utils.checkURLShort(newUrl)) {
		if (/(ftp|http|https):\/\//.test(newUrl)) {
			return url;
		} else {
			return ("http:\/\/"+newUrl);
		}
	}
	return false;
};
CB.Utils.getFlashVersion = function getFlashVersion() {
	// ie
	try {
		try {
			// avoid fp6 minor version lookup issues
			var axo = new ActiveXObject('ShockwaveFlash.ShockwaveFlash.6');
			try {
				axo.AllowScriptAccess = 'always';
			} catch(e) {
				return '6,0,0';
			}
		} catch(e) {
		}
		return new ActiveXObject('ShockwaveFlash.ShockwaveFlash').GetVariable('$version').replace(/\D+/g, ',').match(/^,?(.+),?$/)[1];
		// other browsers
	} catch(e) {
		try {
			if(navigator.mimeTypes["application/x-shockwave-flash"].enabledPlugin) {
				return (navigator.plugins["Shockwave Flash 2.0"] || navigator.plugins["Shockwave Flash"]).description.replace(/\D+/g, ",").match(/^,?(.+),?$/)[1];
			}
		} catch(e) {
		}
	}
	return '0,0,0';
}
CB.Utils.Round = function round (value, precision, mode) {
	// http://kevin.vanzonneveld.net
	// +   original by: Philip Peterson
	// +    revised by: Onno Marsman
	// +      input by: Greenseed
	// +    revised by: T.Wild
	// +      input by: meo
	// +      input by: William
	// +   bugfixed by: Brett Zamir (http://brett-zamir.me)
	// +      input by: Josep Sanz (http://www.ws3.es/)
	// +    revised by: Rafał Kukawski (http://blog.kukawski.pl/)
	// %        note 1: Great work. Ideas for improvement:
	// %        note 1:  - code more compliant with developer guidelines
	// %        note 1:  - for implementing PHP constant arguments look at
	// %        note 1:  the pathinfo() function, it offers the greatest
	// %        note 1:  flexibility & compatibility possible
	// *     example 1: round(1241757, -3);
	// *     returns 1: 1242000
	// *     example 2: round(3.6);
	// *     returns 2: 4
	// *     example 3: round(2.835, 2);
	// *     returns 3: 2.84
	// *     example 4: round(1.1749999999999, 2);
	// *     returns 4: 1.17
	// *     example 5: round(58551.799999999996, 2);
	// *     returns 5: 58551.8
	var m, f, isHalf, sgn; // helper variables
	precision |= 0; // making sure precision is integer
	m = Math.pow(10, precision);
	value *= m;
	sgn = (value > 0) | -(value < 0); // sign of the number
	isHalf = value % 1 === 0.5 * sgn;
	f = Math.floor(value);

	if (isHalf) {
		switch (mode) {
			case 'PHP_ROUND_HALF_DOWN':
				value = f + (sgn < 0); // rounds .5 toward zero
				break;
			case 'PHP_ROUND_HALF_EVEN':
				value = f + (f % 2 * sgn); // rouds .5 towards the next even integer
				break;
			case 'PHP_ROUND_HALF_ODD':
				value = f + !(f % 2); // rounds .5 towards the next odd integer
				break;
			default:
				value = f + (sgn > 0); // rounds .5 away from zero
		}
	}

	return (isHalf ? value : Math.round(value)) / m;
}

CB.Utils.str_pad = function str_pad (input, pad_length, pad_string, pad_type) {
    // http://kevin.vanzonneveld.net
    // +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // + namespaced by: Michael White (http://getsprink.com)
    // +      input by: Marco van Oort
    // +   bugfixed by: Brett Zamir (http://brett-zamir.me)
    // *     example 1: str_pad('Kevin van Zonneveld', 30, '-=', 'STR_PAD_LEFT');
    // *     returns 1: '-=-=-=-=-=-Kevin van Zonneveld'
    // *     example 2: str_pad('Kevin van Zonneveld', 30, '-', 'STR_PAD_BOTH');
    // *     returns 2: '------Kevin van Zonneveld-----'
    var half = '',
        pad_to_go;

    var str_pad_repeater = function (s, len) {
        var collect = '',
            i;

        while (collect.length < len) {
            collect += s;
        }
        collect = collect.substr(0, len);

        return collect;
    };

    input += '';
    pad_string = pad_string !== undefined ? pad_string : ' ';

    if (pad_type != 'STR_PAD_LEFT' && pad_type != 'STR_PAD_RIGHT' && pad_type != 'STR_PAD_BOTH') {
        pad_type = 'STR_PAD_RIGHT';
    }
    if ((pad_to_go = pad_length - input.length) > 0) {
        if (pad_type == 'STR_PAD_LEFT') {
            input = str_pad_repeater(pad_string, pad_to_go) + input;
        } else if (pad_type == 'STR_PAD_RIGHT') {
            input = input + str_pad_repeater(pad_string, pad_to_go);
        } else if (pad_type == 'STR_PAD_BOTH') {
            half = str_pad_repeater(pad_string, Math.ceil(pad_to_go / 2));
            input = half + input + half;
            input = input.substr(0, pad_length);
        }
    }

    return input;
}

CB.Utils.checkEmail = function (str) {
	/**
	 * Checks if the given URL is a valid URL (without http,https,ftp)
	 */
	var regexp = /^([\w\-\'\-]+)(\.[\w-\'\-]+)*@([\w\-]+\.){1,5}([A-Za-z]){2,4}$/;
	return regexp.test(str);
};

CB.Utils.Money = function (value){
     return Ext.util.Format.number(value,'Php 0,000.00');
};
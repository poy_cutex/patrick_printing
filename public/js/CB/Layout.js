Ext.ns('CB');
CB.Layout = Ext.extend(Ext.Viewport,{
    initComponent : function () {
        var vp = this ;
        Ext.apply(this, Ext.apply(this.initialConfig, {
            id          :   'mainLayout',
            layout      :   'border',
            autoScroll  : true,
            listeners   :   {
                render : function(){
                    //1. add Left Naviboxes
                    vp.createLeftPanel(1);
                    //2. add Left Naviboxes
                    vp.createCenterPanel(1);
                }
            },
            items   :   [
            {
                height      : 118,
                region      : 'north',
                frame       : true,
                split       : true,
                baseCls     : '',
                style       : 'background-color : #000',
                items       :   [
                    {
                        baseCls     :   '',
                        bodyStyle   :   'style : background-color : #000',
                        html        :   '<div>'+'<img src="/img/'+LOGO+'">'+'</div>'
                    }
                ],bbar      : {
                    xtype   : 'cbtoolbarheader'
                }
            },{
                title           : 'User Panel',
                titleCollapse   : true,
                region          : 'west',
                id              : 'westRegion',
                collapsible     : true,
                collapseMode    : 'mini',
                split           : true,
                width           : 230,
                maxSize         : 200,
                autoScroll      : true,
                defaults        : {
                    style       :   'padding:2px;margin:12px;',
                    bodyStyle   :   'margin:1px'
                },
                items       :   []
            },{
                id          :   'centerregion',
                style       :   'background : #f1f1f1',
                region      :   'center',
                autoScroll  :   true,
                bodyStyle   :   'padding : 8px 4px 2px 0px;',
                items       :   [
                    {
                        baseCls     :   '',
                        bodyStyle   :   'padding : 5px 5px 5px 10px;',
                        contentEl   :   'logged-in-as'
                    }
                ]
            } // eof centerRegion
            ],
            getCtrRegionBoxContainer    : function(){
                return Ext.getCmp('naviBoxContainersCenter');
            },
            getLeftBoxContainer         : function(){
                return Ext.getCmp('naviBoxContainers');
            },
            getWestRegion               : function(){
                return Ext.getCmp('westRegion');
            },
            getEastRegion               : function(){
                return Ext.getCmp('eastRegion');
            },
            getCenterRegion             : function(){
                return Ext.getCmp('centerregion');
            },
            getDefaultViewPortPanels: function(){
            // default Left,Center,Right Naviboxes/Panels
            },
            createLeftPanel : function(init){
                var wr = this.getWestRegion();
                wr.add(new CB.panels.NaviboxesContainer({
                    id          : 'naviBoxContainers',
                    pos         : 'L',
                    autoHeight  : true,
                    style       : 'margin:0px',
                    isInit      : (typeof init === "undefined") ? false : true,
                    defaults    : {
                        autoHeight      : true,
                        collapsible     : true,
                        titleCollapse   : true,
                        collapsed       : true,
                        border          : false
                    }
                }));
                wr.doLayout();
            },
            clearLeftPanel : function(){
                this.getLeftBoxContainer().removeAll();
            },
            clearWestRegion : function(){
                this.getWestRegion().removeAll(true);
            },
            createCenterPanel : function(init){
                var cr = this.getCenterRegion();
                cr.add(new CB.panels.NaviboxesContainer({
                    id          : 'naviBoxContainersCenter',
                    pos         : 'C',
                    collapsed   : false,
                    autoScroll  : false,
                    isInit      : (typeof init === "undefined") ? false : true,
                    defaults    : {
                        height      : 900,
                        collapsed   : false
                    }
                }));
                cr.doLayout();
            },
            clearCenterPanel : function(){
                this.getCtrRegionBoxContainer().removeAll(true);
            },
            clearCenterRegion : function(){
                this.getCtrRegionBoxContainer().destroy();
            }
        }));
        CB.Layout.superclass.initComponent.apply(this,arguments);
    }
});
Ext.reg('wdlayout', CB.Layout);

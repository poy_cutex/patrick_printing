/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


var LANG_USERNAME = 'Username';
var LANG_PASSWORD = 'Password';
var LANG_LOGIN = 'Login';
var LANG_RESET = 'Reset';
var LANG_FORGOT_PASSWORD = 'Forgot Password';
var LANG_LOGIN_TITLE = 'Please log in';
var LANG_FORGOT_PASSWORD_TOOLTIP = 'Forgotten Password? ' +
    'No Problem! Click on Link Forgotten Password and enter your E-mail adress. ' +
    'If you have entered this E-mail adress in our prime registration, we send you a new Password. Please look in your Mailbox. ' +
    'Please be sure, to use the same spelling as in the prime registration, as we otherwise System could not to find in our database.';
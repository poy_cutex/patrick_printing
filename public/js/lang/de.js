/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


var LANG_USERNAME = 'Benutzername';
var LANG_PASSWORD = 'Kennwort';
var LANG_LOGIN = 'Anmeldung';
var LANG_RESET = 'Nachstellung';
var LANG_FORGOT_PASSWORD = 'Vergessenen Kennwort';
var LANG_LOGIN_TITLE = 'Bitte melden Sie sich an';
var LANG_FORGOT_PASSWORD_TOOLTIP = 'Passwort vergessen ? ' +
    'Kein Problem, einfach Link Passwort vergessen anklicken und EMail-Adresse in das Feld eingeben. ' +
    'Wenn wir dich mit dieser Adresse in unserer Datenbank registriert haben, schicken wir dir ein neues Passwort per E-Mail zu. ' +
    'Bitte verwende unbedingt die gleiche Schreibweise wie bei der Anmeldung, da wir dich sonst in unserer Datenbank nicht finden k�nnen.';
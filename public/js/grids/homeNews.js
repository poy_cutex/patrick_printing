Ext.ns('Wd.grids');

var newsstore   =   new Ext.data.JsonStore({
    id              :   'newsStore',
    url             :   '/admin/news/list',
    autoLoad        :   true,
    root            :   'records',
    totalProperty   :   'count',
    fields          :   [
        {name : 'id'},
        {name : 'subject'},
        {name : 'description'},
        {name : 'shortDesc'},
        {name : 'last_update_dt'}
    ]
});

var newsTitleRenderer   =   function(val) {
    return '<span style="font-weight: bold">' + val + '</span>';
}

Wd.grids.homeNews   =   Ext.extend(Ext.grid.GridPanel, {
    initComponent : function () {Ext.apply(this, Ext.apply(this.initialConfig, {
        id              :   'newsGrid',
        title           :   LANG.PNL_NEWS ? LANG.PNL_NEWS : 'News',
        cls             :   'panelMargin',
        store           :   newsstore,
        trackMouseOver  :   false,
        stripeRows      :   true,
        autoExpandColumn: 'last_update_dt',
        height          :   400,
        collapsed       :  false,
        columns         :   [
            {
                dataIndex   :   'id',
                hidden      :   true
            },
            {
                header      :   LANG.COL_SUBJECT ? LANG.COL_SUBJECT : 'Subject',
                sortable    :   true,
                dataIndex   :   'subject',
                id          :   'subject',
                renderer    :   newsTitleRenderer,
                width       :   350
            },
            {
                header      :   LANG.COL_DATE ? LANG.COL_DATE : 'Date',
                dataIndex   :   'last_update_dt',
                id          :   'last_update_dt',
                sortable    :   true,
                renderer    :   newsTitleRenderer,
                autoWidth   :   true
            },
            {
                xtype       :   'actioncolumn',
                width       :   50,
                items       :   [
                    {
                        icon    :   '/img/icons/book_open.png',
                        tooltip :   'Read more',
                        handler :   function(grid, rowIndex, colIndex) {
                            var thisGrid    =   grid;
                            thisGrid.getView().getRowClass = function(record, index, rowParams, store) {
                                if (index == rowIndex) {
                                    rowParams.body = '<p class="newsDescripMargin">' + record.data.description + '</p>';
                                }
                                else {
                                    rowParams.body = '<p class="newsDescripMargin">' + record.data.shortDesc + '</p>';
                                }
                            }
                            thisGrid.getView().refresh();
                        }
                    },

                    {
                        icon    :   '/img/icons/page_edit.png',
                        tooltip :   'Edit',
                        handler :   function (grid, rowIndex, colIndex) {
                            var rec         =   newsstore.getAt(rowIndex);
                            var editForm    =   new Wd.forms.news({
                                border  :   false,
                                frame   :   false
                            });
                            editForm.getForm().loadRecord(rec);
                            new Ext.Window({
                                title       :   'Edit News',
                                width       :   550,
                                modal       :   true,
                                items       :   [editForm]
                            }).show();
                        }
                    },

                    {
                        icon    :   '/img/icons/page_delete.png',
                        tooltip :   'Delete',
                        handler :   function (grid, rowIndex, colIndex) {
                            Ext.Msg.confirm('Delete', 'Are you sure you want to delete this?', function(btn) {
                                if (btn == 'yes') {
                                    Ext.Ajax.request({
                                        url     :   '/admin/news/delete',
                                        method  :   'POST',
                                        params  :   {
                                            id      :   newsstore.getAt(rowIndex).get('id')
                                        },
                                        success :   function ( result, request) {
                                            newsstore.reload();
                                        },
                                        failure :   function ( result, request) {
                                            var jsonData        =   Ext.util.JSON.decode(result.responseText);
                                            var resultMessage   =   jsonData.data.result;
                                        }
                                    });
                                }
                            });
                        }
                    }
                ]
            }
        ],

        viewConfig  :   {
            forceFit        :   true,
            enableRowBody   :   true,
            getRowClass     :   function (record, rowIndex, p, store) {
                p.body = '<p class="newsDescripMargin">'+record.data.shortDesc+'</p>';
            }
        },
        bbar        :   new Ext.PagingToolbar({
            store       :   newsstore,
            pageSize    :   5,
            autoWidth   :   false
        })
    }));
    Wd.grids.homeNews.superclass.initComponent.apply(this,arguments);
}});

Ext.reg('wdgridshomenews', Wd.grids.homeNews);

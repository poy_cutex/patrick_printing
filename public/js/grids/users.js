Ext.ns('Wd.grids');

var usernameRenderer = function(val) {
    return '<span style="color: blue">' + val + '</span>';
}

var statusRenderer = function(val) {
    switch(val){
        case '0' :
        return '<img src="/img/icons/shape_square_error.png"> Disabled';
        break;
        case '1' :
        return '<img src="/img/icons/accept.png"> Active';
        break;
        case '2' :
        return '<img src="/img/icons/user_go.png"> Confirmed';
        break;
        case '3' :
        return '<img src="/img/icons/cancel.png"> Blocked';
        break;

    }
}


var dateRender = function(val) {
    alert(val);
    return Date.parseDate(val, 'Y-m-d g:i:s A');
}

var userstore = new Ext.data.JsonStore({
                    id           : 'userStore',
                    url          : '/admin/users/lists'
                    ,autoLoad       : false
                    ,root    : 'records'
                    ,totalProperty    : 'count'
                    ,fields  : [
                        {name : 'id', type : 'int'}
                        ,{name : 'status', mapping : 'active'}
                        ,{name : 'group', mapping : 'role'}
                        ,{name : 'group_id'}
                        ,{name : 'username'}
                        ,{name : 'password'}
                        ,{name : 'role'}
                        ,{name : 'firstname'}
                        ,{name : 'lastname'}
                        ,{name : 'email'}
                        ,{name : 'phone'}
                        ,{name : 'mobile_phone'}
                        ,{name : 'gender'}
//                        ,{name : 'date_birth', type: 'date', dateFormat: 'Y-m-d G:i:s'}
                        ,{name : 'date_birth'}
                        ,{name : 'home'}
                        ,{name : 'country'}
                        ,{name : 'homepage'}
                        ,{name : 'icq'}
                        ,{name : 'msn'}
                        ,{name : 'active', mapping: 'status_text'}
                        ,{name : 'last_login'}
                        ,{name : 'scheme'}
                        ,{name : 'provisionscheme_id'}
                        ,{name : 'static_password'}

                    ]
                }
            );

var userId;

Wd.grids.users = Ext.extend(Ext.grid.GridPanel,{
        initComponent : function(){
            Ext.apply(this,{
                title           : 'Übersicht Backenduser'
                //,autoHeight     : true
                //,width          : 400
                ,height         : 470
                ,autoScroll     : true
                ,autoWidth      : true
                ,frame          : true
                ,columnLines    : true
                ,stripeRows     : true
                ,store           : userstore
                ,style               : 'padding : 0px 5px 5px 10px;margin-top:0px;margin-bottom:10px'
                ,cellrenderer : function(val){
                    return val;
                }
                ,listeners           :   {
                    'beforerender'  :   {
                        fn  :   function(){
                            userstore.reload();
                        }
                    },
                    'afterrender'  :   {
                        fn  :   function(){
                        }
                    }
                }
                ,sm             : new Ext.grid.CellSelectionModel({
                    listeners : {
                        cellselect: function(sm, row, col) {
                            if(col == 2) {
                                userId = userstore.getAt(row).get('id');
                                var userDetailsPanel = new Wd.panels.userdetails();

                                ctrregion = Ext.getCmp('centerregion');
                                ctrregion.remove(ctrregion.get(1));
                                ctrregion.insert(1, userDetailsPanel);
                                ctrregion.doLayout();
                            }
                        }
                    }
                })
                ,columns    :[{
                     header     : 'ID',
                     dataIndex  :  'id',
                     sortable   : true,
                     width      :  40
                    },{
                     header     : 'Status',
                     dataIndex  :  'status',
                     sortable   : true,
                     renderer   : statusRenderer,
                     width      :  80
                    },{
                     header     : 'Username',
                     dataIndex  :  'username',
                     sortable   : true,
                     renderer   : usernameRenderer,
                     tooltip    : 'Click to view user details',
                     width      :  80
                    },{
                     header     : 'Password',
                     dataIndex  :  'password',
                     width      :  100,
                     hidden     : true
                    },{
                     header     : 'First Name',
                     dataIndex  :  'firstname',
                     width      :  130,
                     sortable   : true
                    },{
                     header     : 'Last name',
                     dataIndex  :  'lastname',
                     sortable   : true,
                     width      :  130
                    },{
                     id         : 'email',
                     header     : 'Email',
                     dataIndex  :  'email',
                     sortable   : true,
                     hidden     : true
                    },{
                     header     : 'Group',
                     dataIndex  :  'group',
                     sortable   : true,
                     width      :  75
                    },{
                     header     : 'Gender',
                     dataIndex  :  'gender',
                     hidden     : true,
                     width      :  100
                    },{
                     header     : 'Birth day',
                     dataIndex  :  'date_birth',
                     width      :  100,
                     hidden     : true
                    },{
                     header     : 'Home',
                     dataIndex  :  'home',
                     hidden     : true,
                     width      :  100
                    },{
                     header     : 'Land',
                     dataIndex  :  'country',
                     hidden     : true
                     },{
                     id         : 'homepage',
                     header     : 'Homepage',
                     hidden     : true,
                     dataIndex  :  'homepage'
                    },{
                     header     : 'ICQ',
                     hidden     : true,
                     dataIndex  :  'icq'
                    },{
                     header     : 'MSN',
                     hidden     : true,
                     dataIndex  :  'msn'
                    },{
                     dataIndex  : 'phone',
                     hidden     : true
                    },{
                     dataIndex  : 'mobile_phone',
                     hidden     : true
                    },{
                     dataIndex  : 'active',
                     hidden     : true
                    },{
                     dataIndex  : 'last_login',
                     header     : 'Last Login',
                     width      : 150,
                     sortable   : true
                    },{
                     xtype: 'actioncolumn',
                     width: 60,
                     items: [
                         {
                             icon: '/img/icons/user_edit.png',
                             tooltip: 'Edit',
                             handler: function(grid, rowIndex, colIndex) {
                                 var rec = grid.getStore().getAt(rowIndex);

//                                 console.log(rec);

                                var editForm = new Wd.forms.users({
                                                    border: false,
                                                    frame: false,
                                                    rec_group_id : rec.data.group_id,
                                                    rec_status : rec.data.status,
                                                    rec_static_password : rec.data.static_password
                                                });
                                editForm.getForm().loadRecord(rec);

                                new Ext.Window({
                                    title   : 'Edit User',
                                    autoWidth: true,
                                    modal   : true,
                                    items : [ editForm ]
                                }).show();
                             }
                         }, {
                             icon: '/img/icons/user_delete.png',
                             tooltip: 'Delete',
                             handler: function(grid, rowIndex, colIndex) {
                                 var username = userstore.getAt(rowIndex).get('username');
                                 Ext.Msg.confirm('Delete', 'Are you sure you want to delete ' + username + '?', function(btn){
                                    if (btn == 'yes') {
                                        Ext.Msg.confirm('Delete', 'Do you really want to delete ' + username + '?', function(btn){
                                            if(btn == 'yes') {
                                                Ext.Ajax.request({
                                                    url : '/admin/users/delete',
                                                    method: 'POST',
                                                    params: {userId: userstore.getAt(rowIndex).get('id')},
                                                    success: function ( result, request ) {
                                                        userstore.reload();
                                                    },
                                                    failure: function ( result, request ) {
                                                        var jsonData = Ext.util.JSON.decode(result.responseText);
                                                        var resultMessage = jsonData.data.result;
                                                    }
                                                });
                                            }
                                        });
                                    } //if btn yes
                                });
                            }
                         }
                     ]
                    }]
                ,tbar :  new Ext.PagingToolbar({
                       // id              : 'historyPageToolbar',
                        store           : userstore,
                        pageSize        : 20,
                        autoWidth       : false,
                        listeners       : {'render':function(){

                        }},
                       // style           : 'padding-bottom:8px;padding-left:8px;',
                        items           : [
                                            '-',
                                            {
                                            xtype   : 'button',
                                            text    : 'Create new user',
                                            icon    : '/img/icons/user_add.png',
                                            handler : function() {
                                                new Ext.Window({
                                                    title   : 'New User',
                                                    autoWidth   : true,
                                                    autoHeight  : true,
                                                    modal   : true,
                                                    items : [ new Wd.forms.users() ]
                                                }).show();
                                            }
                                          }
                                           ]
                    })

 //           ,applyTo   : 'usersContainer'
            });

            Wd.grids.users.superclass.initComponent.apply(this,arguments);
        }
});
Ext.reg('wdgridusers', Wd.grids.users);

//Ext.onReady(function() {
//    Ext.BLANK_IMAGE_URL = '/ext/resources/images/default/s.gif';
//    var usersGrid = new Wd.grids.users();
//});
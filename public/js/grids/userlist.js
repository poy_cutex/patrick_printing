Ext.ns('Wd.grids');
            var linkRenderer = function(val){
                return '<a href="/">'+ val+'</a>';
            }
var userListStore = new Ext.data.JsonStore({
                    id           : 'userStore',
                    url          : '/admin/users/lists'
                    ,autoLoad       : true
                    ,root    : 'records'
                    ,totalProperty    : 'count'
                    ,fields  : [
                        {name : 'id' , type : 'int'}
                        ,{name : 'status', mapping : 'active'}
                        ,{name : 'group', mapping : 'role'}
                        ,{name : 'username'}
                        ,{name : 'password'}
                        ,{name : 'role'}
                        ,{name : 'firstname'}
                        ,{name : 'lastname'}
                        ,{name : 'email'}
                        ,{name : 'gender'}
                        ,{name : 'birthday'}
                        ,{name : 'home'}
                        ,{name : 'land'}
                        ,{name : 'homepage'}
                        ,{name : 'icq'}
                        ,{name : 'msn'}

                    ]
                }
            );
Wd.grids.userlist = Ext.extend(Ext.grid.EditorGridPanel,{
        initComponent : function(){
            Ext.apply(this,{
                title           : 'Userübersicht'
                ,height         : 470
                ,autoScroll     : true
                ,autoWidth      : true
                ,frame          : true
                ,columnLines    : true
                ,stripeRows     : true
             //   ,autoExpandColumn    : 'email'
                ,store           : userstore

            ,columns    :[{
                 header     : 'ID',
                 dataIndex  :  'id',
                 sortable   : true,
                 hidden     : true,
                 width      :  40
                },{
                 header     : 'Status',
                 dataIndex  :  'status',
                 sortable   : true,
                 hidden     : true,
                 renderer   : statusRenderer,
                 width      :  100
                },{
                 header     : 'Username',
                 dataIndex  :  'username',
                 sortable   : true,
                 width      :  100,
                 renderer   : linkRenderer
                },{
                 header     : 'Password',
                 dataIndex  :  'password',
                 width      :  100,
                 hidden     : true
                },{
                 header     : 'First Name',
                 dataIndex  :  'firstname',
                 width      :  130,
                 sortable   : true
                },{
                 header     : 'Last name',
                 dataIndex  :  'lastname',
                 sortable   : true,
                 width      :  130
                },{
                 id         : 'email',
                 header     : 'Email',
                 dataIndex  :  'email',
                 hidden     : true,    
                 sortable   : true,
                 autoWidth      :  true
                },{
                 header     : 'Group',
                 dataIndex  :  'group',
                 sortable   : true,

                 width      :  100
                },{
                 header     : 'Gender',
                 dataIndex  :  'gender',
                 hidden     : true,
                 width      :  100
                },{
                 header     : 'Birth day',
                 dataIndex  :  'birthday',
                 hidden     : true,
                 width      :  100
                },{
                 header     : 'Home',
                 dataIndex  :  'home',
                 hidden     : true,
                 width      :  100
                },{
                 header     : 'Land',
                 dataIndex  :  'land',
                 hidden     : true
                 },{
                 id         : 'homepage',
                 header     : 'Homepage',
                 hidden     : true,
                 dataIndex  :  'homepage'
                },{
                 header     : 'ICQ',
                 hidden     : true,
                 dataIndex  :  'icq'
                },{
                 header     : 'MSN',
                 hidden     : true,
                 dataIndex  :  'msn'
                }]
            ,tbar :  new Ext.PagingToolbar({
                   // id              : 'historyPageToolbar',
                    store           : userstore,
                    pageSize        : 20,
                    autoWidth       : false,
                    listeners       : {'render':function(){

                    }},
                   // style           : 'padding-bottom:8px;padding-left:8px;',
                    items           : [
                                        '-',
//                                        {
//                                        xtype   : 'button',
//                                        text    : 'Create new user',
//                                        icon    : '/img/icons/user_add.png',
//                                        handler : function(){
//                                            new Ext.Window({
//                                                title   : 'Create New User',
//                                                autoWidth   : true,
//                                                autoHeight  : true,
//                                                modal   : true,
//                                                items : [ new Wd.forms.users() ]
//
//                                            }).show();
//                                        }
//                                      }
                                       ]
                })

 //           ,applyTo   : 'usersContainer'
            });

            Wd.grids.users.superclass.initComponent.apply(this,arguments);
        }
});
Ext.reg('wdgriduserlist', Wd.grids.userlist);

//Ext.onReady(function() {
//    Ext.BLANK_IMAGE_URL = '/ext/resources/images/default/s.gif';
//    var usersGrid = new Wd.grids.users();
//});
Ext.ns('Wd.grids');

var translationGridColumns = [
    {
        dataIndex: 'keyword_id',
        hidden: true
    }, {
        dataIndex: 'keyword',
        id: 'keyword',
        header: 'Keyword',
        sortable: true,
        editor: {
            xtype: 'textfield',
            allowBlank: false
        }
    }
];

var translationStoreFields = [
    {name : 'keyword_id'},
    {name : 'keyword'},
    {name : 'category'}
];

var translationstore;

Ext.Ajax.request({
    url : '/admin/translation/getcountries',
    method: 'POST',
    scope: this,
    success: function ( result, request ) {
        var resText = Ext.util.JSON.decode(result.responseText);
        for(var i=0; i<resText.countries.length; i++) {
            translationStoreFields.push({
                name: 'id_t' + resText.countries[i].id
            }, {
                name: 'translation_t' + resText.countries[i].id
            });

            translationGridColumns.push({
                dataIndex: 'translation_t' + resText.countries[i].id,
                header: resText.countries[i].language,
                sortable: true,
                width: 150,
                editor: {xtype: 'textfield'}
            });
        }

        translationGridColumns.push({
            xtype: 'actioncolumn',
            width: 60,
            items: [
                {
                    icon: '/img/icons/world_delete.png',
                    tooltip: 'Delete',
                    handler: function(grid, rowIndex, colIndex) {
                        Ext.Msg.confirm('Delete', 'Are you sure you want to delete this translation?', function(btn){
                            if (btn == 'yes') {
                                Ext.Ajax.request({
                                    url : '/admin/translation/deletekeyword',
                                    method: 'POST',
                                    params: {id: translationstore.getAt(rowIndex).get('keyword_id')},
                                    success: function ( result, request ) {
                                        translationstore.removeAll();
                                        translationstore.reload();
                                    },
                                    failure: function ( result, request ) {
                                        var jsonData = Ext.util.JSON.decode(result.responseText);
                                        var resultMessage = jsonData.data.result;
                                    }
                                });
                            }
                        });
                    }
                }
            ]
        });

        translationstore = new Ext.data.JsonStore({
            id           : 'translationstore',
            url          : '/admin/translation/list/category/B',
            root    : 'records',
            totalProperty    : 'count',
            autoLoad: true,
            fields: translationStoreFields
        });
    },
    failure: function ( result, request ) {
        var jsonData = Ext.util.JSON.decode(result.responseText);
        var resultMessage = jsonData.data.result;
    }
});

var onInsertRecordBtn = function(grid) {
    var selectedCell = grid.getSelectionModel().getSelectedCell();
    var selectedRowIndex = (selectedCell == null) ? 0 : selectedCell[0];
    var newRecord = new translationstore.recordType({
        category: 'B'
    });
    grid.stopEditing();
    translationstore.insert(selectedRowIndex, newRecord);
    grid.startEditing(selectedRowIndex,0);
}

Wd.grids.translations = Ext.extend(Ext.grid.EditorGridPanel ,{
        initComponent : function(){
            Ext.apply(this,{
                id: 'translationGrid',
                store: translationstore,
                columns: translationGridColumns,
                tbar: {
                    xtype: 'toolbar',
                    items: [
                        {
                            text : 'Add Translation',
                            icon: '/img/icons/world_add.png',
                            handler: function() {
                                onInsertRecordBtn(this.findParentByType('wdgridtranslations'));
                            }
                        },
                        '-',
                        {
                            text : 'Save Changes',
                            icon: '/img/icons/disk.png',
                            handler: function() {
                                var hasBlankKeyword = false;
                                var modified = translationstore.getModifiedRecords();
                                if (modified.length > 0) {
                                    var recordsToSend = [];
                                    Ext.each(modified, function(record) {
                                        if(record.data.keyword != null)
                                            recordsToSend.push(record.data);
                                        else {
                                            var recSize = Object.size(record.data);
                                            if(recSize > 1) { //has translation values
                                                hasBlankKeyword = true;
                                            }
                                        }
                                    });

                                    if(hasBlankKeyword == false) {
                                        var grid = this.findParentByType('wdgridtranslations');
                                        grid.el.mask('Updating', 'x-mask-loading');
                                        grid.stopEditing();
                                        recordsToSend = Ext.encode(recordsToSend);
                                        Ext.Ajax.request({
                                            url : '/admin/translation/save',
                                            params : {recordsToInsertUpdate : recordsToSend},
                                            success : function(response) {
                                                grid.el.unmask();
                                                translationstore.commitChanges();
                                                translationstore.reload();
                                            },
                                            failure: function(response) {
                                                alert('fail');
                                                //console.log(response);
                                            }
                                        });
                                    } else {
                                        Ext.Msg.show({
                                            msg: 'Keyword is a required field.',
                                            buttons: Ext.Msg.OK,
                                            animEl: 'elId',
                                            icon: Ext.MessageBox.ERROR,
                                            closable: false
                                        });
                                    }
                                }
                            }
                        },
                        '-',
                        {
                            text : 'Reject Changes',
                            icon: '/img/icons/cancel.png',
                            handler: function() {
                                Ext.Msg.show({
                                   title:'Reject Changes',
                                   msg: 'All your changes will be reverted. Do you want to continue?',
                                   buttons: Ext.Msg.YESNO,
                                   fn: function(btn){
                                       if(btn == 'yes')
                                           translationstore.reload();
                                   },
                                   animEl: 'elId',
                                   icon: Ext.MessageBox.WARNING
                                });
                            }
                        },
                        '-'
                    ]
                },
                listeners: {
                    cellcontextmenu: {
                        fn: function(editorGrid, rowIndex, cellIndex, evtObj) {
                            evtObj.stopEvent();
                            if (!editorGrid.rowCtxMenu) {
                                editorGrid.rowCtxMenu = new Ext.menu.Menu({
                                    items : [
                                        {
                                            text : 'Insert Record',
                                            handler : function() {
                                                onInsertRecordBtn(editorGrid);
                                            }
                                        }
                                    ]
                                });
                            }
                            editorGrid.getSelectionModel().select(rowIndex,cellIndex);
                            editorGrid.rowCtxMenu.showAt(evtObj.getXY());
                        }
                    }
                },
                autoExpandColumn: 'keyword',
                autoExpandMin: 100,
                frame:true,
                loadMask: true,
                height: 380,
                autoScroll: true,
                clicksToEdit: 1,
                cls: 'panelMargin',
                title: 'Translation'
            });

            Wd.grids.translations.superclass.initComponent.apply(this,arguments);
        }
});

Ext.reg('wdgridtranslations', Wd.grids.translations);
Ext.ns('Wd.grids');

var groupstore = new Ext.data.JsonStore({
                    id           : 'groupStore',
                    url          : '/admin/groups/list',
                    autoLoad       : false,
                    root    : 'records',
                    totalProperty    : 'count',
                    fields  : [
                        {name : 'id' , type : 'int'},
                        {name : 'name'},
                        {name : 'description'},
                        {name : 'pnl_info'},
                        {name : 'pnl_backendmgt'},
                        {name : 'pnl_contentmgt'},
                        {name : 'pnl_systemsetting'}
                    ]
                }
            );

Wd.grids.groups = Ext.extend(Ext.grid.GridPanel,{
        initComponent : function(){
            Ext.apply(this,{
                title          : LANG.PNL_CHANGE_GROUP ? LANG.PNL_CHANGE_GROUP : 'Change Group',
                height         : 470,
                autoScroll     : true,
                autoWidth      : true,
                frame          : true,
                columnLines    : true,
                stripeRows     : true,
                autoExpandColumn    : 'description',
                store           : groupstore,
                style               : 'padding : 0px 5px 5px 10px;margin-top:0px;margin-bottom:10px',
                listeners       : {
                    beforerender: function() {
                        groupstore.load();
                    }
                },
                cellrenderer : function(val){
                    return val;
                },
                columns    : [
                    {
                        header     :  'ID',
                        dataIndex  :  'id',
                        sortable   : true,
                        width      :  40
                    }, {
                        header     : LANG.COL_NAME ? LANG.COL_NAME : 'Name',
                        dataIndex  :  'name',
                        sortable   : true,
                        width      :  150
                    }, {
                        header     : LANG.COL_DESCRIPTION ? LANG.COL_DESCRIPTION : 'Description',
                        dataIndex  : 'description',
                        id         : 'description'
                    }, {
                        xtype: 'actioncolumn',
                        width: 60,
                        items: [
                            {
                                icon: '/img/icons/group_edit.png',
                                tooltip: LANG.TIP_EDIT ? LANG.TIP_EDIT : 'Edit',
                                handler: function(grid, rowIndex, colIndex) {
                                    var rec = groupstore.getAt(rowIndex);

                                    var editForm = new Wd.forms.groups({
                                                        border: false,
                                                        frame: false,
                                                        rec : rec
                                                    });
                                    editForm.getForm().loadRecord(rec);

                                    new Ext.Window({
                                        title   : LANG.PNL_EDIT_GROUP ? LANG.PNL_EDIT_GROUP : 'Edit Group',
                                        width: 650,
                                        modal   : true,
                                        closable: false,
                                        items : [ editForm ]
                                    }).show();
                                }
                            }, {
                                icon: '/img/icons/group_delete.png',
                                tooltip: LANG.TIP_DELETE ? LANG.TIP_DELETE : 'Delete',
                                handler: function(grid, rowIndex, colIndex) {
                                    Ext.Msg.confirm(
                                            LANG.PNL_DELETE ? LANG.PNL_DELETE : 'Delete',
                                            LANG.MSG_DELETE_GROUP ? LANG.MSG_DELETE_GROUP :
                                                'Are you sure you want to delete this group?',
                                            function(btn){
                                        if (btn == 'yes') {
                                            Ext.Ajax.request({
                                                url : '/admin/groups/delete',
                                                method: 'POST',
                                                params: { groupId: groupstore.getAt(rowIndex).get('id') },
                                                success: function ( result, request ) {
                                                    Ext.Msg.alert('Status', result.responseText, function(btn) {
                                                        if (btn == 'ok') {
                                                            groupstore.reload();
                                                        }
                                                    });
                                                },
                                                failure: function ( result, request ) {
                                                    var jsonData = Ext.util.JSON.decode(result.responseText);
                                                    var resultMessage = jsonData.data.result;
                                                }
                                            });
                                        }
                                    });
                                }
                            }
                        ]
                    }
                ],
                tbar :  new Ext.PagingToolbar({
                    store           : groupstore,
                    pageSize        : 20,
                    autoWidth       : false,
                    listeners       : {'render':function(){

                    }},
                    items           : [
                                        '-',
                                        {
                                            xtype   : 'button',
                                            text    : LANG.BTN_NEW_GROUP ? LANG.BTN_NEW_GROUP : 'New Group',
                                            icon    : '/img/icons/group_add.png',
                                            handler : function(){
                                                    new Ext.Window({
                                                        title   : LANG.PNL_NEW_GROUP ? LANG.PNL_NEW_GROUP : 'New Group',
                                                        width: 650,
                                                        modal   : true,
                                                        closable: false,
                                                        items : [
                                                            new Wd.forms.groups({
                                                                border: false,
                                                                frame: false
                                                            })
                                                        ]
                                                    }).show();
                                                }
                                        }
                                       ]
                })
            });

            Wd.grids.groups.superclass.initComponent.apply(this,arguments);
        }
});

Ext.reg('wdgridgroups', Wd.grids.groups);
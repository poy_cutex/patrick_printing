Ext.ns('Wd.grids');

var navistore = new Ext.data.JsonStore({
                    id: 'naviboxStore',
                    url: '/admin/navibox/list',
                    autoLoad: false,
                    root: 'records',
                    totalProperty: 'count',
                    fields: [
                        {name: 'id', type : 'int'},
                        {name: 'name'},
                        {name: 'position'},
                        {name: 'activeIndicator', mapping: 'active_indicator'},
                        {name: 'order'}
                    ]
                });

Wd.grids.navibox = Ext.extend(Ext.grid.EditorGridPanel, {
        initComponent : function() {
            this.onInsertRecord = function() {
                var selectedRowIndex;
                var selectedCell = this.getSelectionModel().getSelectedCell();
                if(selectedCell == null) {
                    selectedRowIndex = 0;
                } else {
                    selectedRowIndex = selectedCell[0];
                }

                var newRecord = new navistore.recordType();
                navistore.insert(selectedRowIndex, newRecord);
                this.startEditing(selectedRowIndex,0);
                
                var naviBoxContainers = Ext.getCmp('naviBoxContainers');
                if(naviBoxContainers)
                naviBoxContainers.updateNavBoxes();
            }

            Ext.apply(this, {
                title : 'Navigation Boxes',
                height: 400,
                loadMask: true,
                stripeRows: true,
                autoWidth: true,
                autoExpandColumn: 'name',
                style               : 'padding : 0px 5px 5px 10px;margin-top:0px;margin-bottom:10px',
                store: navistore,
                columns: [
                    {
                        header     :  'ID',
                        dataIndex: 'id',
                        id: 'id',
                        sortable    :   true,
                        width       :   40
                    }, {
                        header: 'Name',
                        dataIndex: 'name',
                        id: 'name',
                        sortable: true,
                        editor: {
                            xtype: 'textfield',
                            allowBlank: false
                        }
                    }, {
                        header: 'Position',
                        dataIndex: 'position',
                        sortable: true,
                        editor: {
                            xtype: 'combo',
                            store: new Ext.data.SimpleStore({
                                fields: ['val', 'desc'],
                                data : [
                                    ['L', 'Left'],
                                    ['R', 'Right'],
                                    ['C', 'Center']                                                                
                                ]
                            }),
                            displayField:'desc',
                            valueField: 'val',
                            hiddenValue: 'val',
                            hiddenName: 'position',
                            typeAhead: true,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            allowBlank: false
                        },
                        renderer: function(val) {
                            var nv;            
                            switch(val) {
                                case 'L':
                                    nv = 'Left';
                                    break;
                                case 'R':
                                    nv = 'Right';
                                    break;
                                case 'C':
                                    nv = 'Center';
                                    break;                                                                                
                            }
                            return nv;            
                        }
                    }, {
                        header: 'Active',
                        dataIndex: 'activeIndicator',
                        sortable: true,
                        editor: {
                            xtype: 'combo',
                            store: new Ext.data.SimpleStore({
                                fields: ['val', 'desc'],
                                data : [
                                    ['1', 'Yes'],
                                    ['2', 'No']
                                ]
                            }),
                            displayField:'desc',
                            valueField: 'val',
                            hiddenValue: 'val',
                            hiddenName: 'active',
                            typeAhead: true,
                            mode: 'local',
                            triggerAction: 'all',
                            selectOnFocus:true,
                            allowBlank: false
                        },
                        renderer: function(val) {
                            switch(val) {
                                case '1':
                                    return '<img src="/img/icons/accept.png" />';
                                    break;
                                case '2':
                                    return '<img src="/img/icons/delete.png" />';
                                    break;
                            }
                        }
                    },
                    {
                        header: 'Order',
                        dataIndex: 'order',
                        sortable: false,
                        hidden :true,
                        value   : 1,
                        editor: {
                            xtype: 'numberfield',
                            minValue: 1,
                            maxValue: 99,
                            allowBlank: false,
                            allowNegative: false,
                            allowDecimals: false
                        }
                    },

                    {
                        xtype: 'actioncolumn',
                        width: 60,
                        items: [
                            {
                                icon: '/img/icons/application_delete.png',
                                tooltip: 'Delete',
                                handler: function(grid, rowIndex, colIndex) {
                                    Ext.Msg.confirm('Delete', 'Are you sure you want to delete this navigation box?', function(btn){
                                        if (btn == 'yes') {
                                            Ext.Ajax.request({
                                                url : '/admin/navibox/delete',
                                                method: 'POST',
                                                params: { id: navistore.getAt(rowIndex).get('id') },
                                                success: function ( result, request ) {
                                                    navistore.reload();

                                                    var naviBoxContainers = Ext.getCmp('naviBoxContainers');
                                                    if(naviBoxContainers)
                                                    naviBoxContainers.updateNavBoxes();
                                                },
                                                failure: function ( result, request ) {
                                                    var jsonData = Ext.util.JSON.decode(result.responseText);
                                                    var resultMessage = jsonData.data.result;
                                                }
                                            });
                                        }
                                    });
                                }
                            }
                        ]
                    }
                ],
                listeners: {
                    beforerender: function() {
                        navistore.load();
                    },
                    cellcontextmenu: {
                        fn: function(editorGrid, rowIndex, cellIndex, evtObj) {
                            evtObj.stopEvent();
                            if (!editorGrid.rowCtxMenu) { // 2
                                editorGrid.rowCtxMenu = new Ext.menu.Menu({
                                    items : [
                                        {
                                            text : 'Insert Record',
                                            handler : function() {
                                                editorGrid.onInsertRecord();
                                            }
                                        }
                                    ]
                                });
                            }
                            editorGrid.getSelectionModel().select(rowIndex,cellIndex); // 3
                            editorGrid.rowCtxMenu.showAt(evtObj.getXY());
                        }
                    }
                },
                tbar :  new Ext.PagingToolbar({
                    store           : navistore,
                    pageSize        : 20,
                    autoWidth       : false,
                    listeners       : {'render':function(){

                    }},
                    items           : [
                                        '-',
                                        {
                                            xtype   : 'button',
                                            text    : 'New',
                                            icon    : '/img/icons/application_add.png',
                                            handler : function() {
                                                var thisGrid = this.findParentByType('wdgridnavibox');
                                                thisGrid.onInsertRecord();

                                            }
                                        },
                                        '-',
                                        {
                                            xtype   : 'button',
                                            text    : 'Reject Changes',
                                            icon    : '/img/icons/cancel.png',
                                            handler : function() {
                                                Ext.Msg.show({
                                                    title:'Reject Changes',
                                                    msg: 'All your changes will be reverted. Do you want to continue?',
                                                    buttons: Ext.Msg.YESNO,
                                                    fn: function(btn){
                                                        if(btn == 'yes')
                                                            navistore.reload();
                                                    },
                                                    animEl: 'elId',
                                                    icon: Ext.MessageBox.WARNING
                                                });
                                            }
                                        },
                                        '-',
                                        {
                                            xtype   : 'button',
                                            text    : 'Save Changes',
                                            icon    : '/img/icons/disk.png',
                                            handler : function() {
                                                var modified = navistore.getModifiedRecords();
                                                if (modified.length > 0) {
                                                    var recordsToSend = [];
                                                    Ext.each(modified, function(record) {
                                                        recordsToSend.push(record.data);
                                                    });

                                                    var grid = this.findParentByType('wdgridnavibox');
                                                    grid.el.mask('Updating', 'x-mask-loading');
                                                    grid.stopEditing();
                                                    recordsToSend = Ext.encode(recordsToSend);
                                                    Ext.Ajax.request({
                                                        url : '/admin/navibox/save',
                                                        params : {recordsToInsertUpdate : recordsToSend},
                                                        success : function(response) {
                                                            grid.el.unmask();
                                                            navistore.commitChanges();
                                                            navistore.reload();

                                                        var naviBoxContainers = Ext.getCmp('naviBoxContainers');
                                                        if(naviBoxContainers)
                                                        naviBoxContainers.updateNavBoxes();

                                                        },
                                                        failure: function(response) {
                                                            grid.el.unmask();
                                                            Ext.Msg.show({
                                                                title:'Error!',
                                                                msg: 'Please fill-up all fields.',
                                                                animEl: 'elId',
                                                                icon: Ext.MessageBox.ERROR,
                                                                buttons: Ext.Msg.OK,
                                                                closable: false
                                                            });
                                                        }
                                                    });
                                                }
                                                            var nvr =  Ext.getCmp('naviBoxContainerRight');
                                                             if(nvr){
                                                              nvr.updateNavBoxes();
                                                             }
                                            }
                                        }
                                       ]
                })
            });

            Wd.grids.navibox.superclass.initComponent.apply(this,arguments);
        }
});

Ext.reg('wdgridnavibox', Wd.grids.navibox);

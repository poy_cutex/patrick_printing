/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50534
Source Host           : localhost:3306
Source Database       : printing

Target Server Type    : MYSQL
Target Server Version : 50534
File Encoding         : 65001

Date: 2014-10-04 22:22:54
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `category`
-- ----------------------------
DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of category
-- ----------------------------
INSERT INTO category VALUES ('1', 'Bath & Shower');
INSERT INTO category VALUES ('2', 'Lotions & Creams');
INSERT INTO category VALUES ('5', 'Anti-Bacterial & Soaps');
INSERT INTO category VALUES ('6', 'Face Care');
INSERT INTO category VALUES ('7', 'Aromatherapy');
INSERT INTO category VALUES ('8', 'Spa & Treatments');

-- ----------------------------
-- Table structure for `countries`
-- ----------------------------
DROP TABLE IF EXISTS `countries`;
CREATE TABLE `countries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `alias` varchar(2) DEFAULT NULL,
  `language` varchar(45) DEFAULT NULL,
  `language_alias` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of countries
-- ----------------------------
INSERT INTO countries VALUES ('1', 'United States', 'us', 'English', 'en');
INSERT INTO countries VALUES ('2', 'Germany', 'de', 'German', 'de');
INSERT INTO countries VALUES ('4', 'Philippines', 'ph', 'Tagalog', 'fil');
INSERT INTO countries VALUES ('5', 'Czech Republic', 'cz', 'Czech', 'cz');
INSERT INTO countries VALUES ('6', 'Spain', 'es', 'Espanol', 'es');
INSERT INTO countries VALUES ('7', 'China', 'cn', 'Chinese', 'cn');

-- ----------------------------
-- Table structure for `customers`
-- ----------------------------
DROP TABLE IF EXISTS `customers`;
CREATE TABLE `customers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `firstname` varchar(100) NOT NULL,
  `middlename` varchar(100) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `address` varchar(200) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(30) DEFAULT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of customers
-- ----------------------------
INSERT INTO customers VALUES ('1', '2014-10-03 11:47:14', '2014-10-03 11:47:14', 'Ronnie', 'B', 'Castro', 'Pitogo', 'ronnie@yahoo.com', '09164729441', 'Ronnie', '123456');
INSERT INTO customers VALUES ('3', '2014-10-03 13:54:16', '2014-10-03 13:54:16', 'Joshua', 'C', 'Hisola', 'Kanto', 'kahit@no.yahoo.com', '12345678901', 'Joshua', '123456');
INSERT INTO customers VALUES ('4', '2014-10-03 15:38:15', '2014-10-03 15:38:15', 'Randy', 'L', 'Bolata', 'Makati City', 'randy@gmail.com', '117', 'Randy', '123456');
INSERT INTO customers VALUES ('5', '2014-10-03 15:50:37', '2014-10-03 15:50:37', '', '', '', '', '', '', '', '123456');
INSERT INTO customers VALUES ('6', '2014-10-03 15:50:37', '2014-10-03 15:50:37', 'Onin', 'V', 'Manalo', 'Baclaran', 'onin@yahoo.com', '4123', 'Onin', '123456');

-- ----------------------------
-- Table structure for `deliveries`
-- ----------------------------
DROP TABLE IF EXISTS `deliveries`;
CREATE TABLE `deliveries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of deliveries
-- ----------------------------
INSERT INTO deliveries VALUES ('1', '2011-09-21 00:12:42', '2011-09-21 00:12:42');
INSERT INTO deliveries VALUES ('2', '2011-11-22 11:36:17', '2011-11-22 11:36:17');
INSERT INTO deliveries VALUES ('3', '2011-11-22 11:40:40', '2011-11-22 11:40:40');
INSERT INTO deliveries VALUES ('4', '2011-11-28 17:08:57', '2011-11-28 17:08:57');
INSERT INTO deliveries VALUES ('5', '2011-11-28 17:09:00', '2011-11-28 17:09:00');
INSERT INTO deliveries VALUES ('6', '2011-11-28 17:17:36', '2011-11-28 17:17:36');
INSERT INTO deliveries VALUES ('7', '2011-11-28 17:26:07', '2011-11-28 17:26:07');

-- ----------------------------
-- Table structure for `deliverylist`
-- ----------------------------
DROP TABLE IF EXISTS `deliverylist`;
CREATE TABLE `deliverylist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `productID` int(11) NOT NULL,
  `deliveryID` int(11) NOT NULL DEFAULT '0',
  `quantity` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of deliverylist
-- ----------------------------
INSERT INTO deliverylist VALUES ('1', '3', '1', '90');
INSERT INTO deliverylist VALUES ('2', '22', '2', '14');
INSERT INTO deliverylist VALUES ('3', '23', '3', '100');
INSERT INTO deliverylist VALUES ('4', '22', '4', '10');
INSERT INTO deliverylist VALUES ('5', '22', '6', '50');
INSERT INTO deliverylist VALUES ('6', '24', '7', '50');

-- ----------------------------
-- Table structure for `errlogs`
-- ----------------------------
DROP TABLE IF EXISTS `errlogs`;
CREATE TABLE `errlogs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `val` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of errlogs
-- ----------------------------
INSERT INTO errlogs VALUES ('3', 'res = \' <br/>');
INSERT INTO errlogs VALUES ('4', 'payer_id ----> 8JEKCB5GJHXM8 <br />address_country_code ----> US <br />ipn_track_id ----> 9TlL6xYK7uNztsiB2y6rvg <br />address_zip ----> 95131 <br />charset ----> windows-1252 <br />payment_gross ----> 10.50 <br />address_status ----> confirmed <br />address_street ----> 1 Main St <br />verify_sign ----> Aj2e0X9nNB4.M-wErocdgw0ChnxEAyC76O7PY6nsxHRxd3JRnswf8Ok3 <br />test_ipn ----> 1 <br />txn_type ----> send_money <br />receiver_id ----> MWHFRU58WGJZ6 <br />payment_fee ----> 0.60 <br />mc_currency ----> USD <br />transaction_subject ---->  <br />protection_eligibility ----> Eligible <br />address_country ----> United States <br />payer_status ----> verified <br />first_name ----> Test <br />address_name ----> Test User <br />mc_gross ----> 10.50 <br />payment_date ----> 08:04:54 Nov 27, 2011 PST <br />payment_status ----> Completed <br />business ----> poy_cu_1247782578_biz@yahoo.com <br />last_name ----> User <br />address_state ----> CA <br />txn_id ----> 3M854443WK6326713 <br />mc_fee ----> 0.60 <br />resend ----> true <br />payment_type ----> instant <br />notify_version ----> 3.4 <br />payer_email ----> poy_cu_1247777456_per@yahoo.com <br />receiver_email ----> poy_cu_1247782578_biz@yahoo.com <br />address_city ----> San Jose <br />residence_country ----> US <br />sulod sa fpokres = VERIFIED <br/>');
INSERT INTO errlogs VALUES ('5', 'payer_id ----> 8JEKCB5GJHXM8 <br />address_country_code ----> US <br />ipn_track_id ----> 9TlL6xYK7uNztsiB2y6rvg <br />address_zip ----> 95131 <br />charset ----> windows-1252 <br />payment_gross ----> 10.50 <br />address_status ----> confirmed <br />address_street ----> 1 Main St <br />verify_sign ----> AJS0ZGLaMdPbPpAcRfJokwdB2HSbAk3SzqtMICrhBnXvRyOtSTB20w.M <br />test_ipn ----> 1 <br />txn_type ----> send_money <br />receiver_id ----> MWHFRU58WGJZ6 <br />payment_fee ----> 0.60 <br />mc_currency ----> USD <br />transaction_subject ---->  <br />protection_eligibility ----> Eligible <br />address_country ----> United States <br />payer_status ----> verified <br />first_name ----> Test <br />address_name ----> Test User <br />mc_gross ----> 10.50 <br />payment_date ----> 08:04:54 Nov 27, 2011 PST <br />payment_status ----> Completed <br />business ----> poy_cu_1247782578_biz@yahoo.com <br />last_name ----> User <br />address_state ----> CA <br />txn_id ----> 3M854443WK6326713 <br />mc_fee ----> 0.60 <br />resend ----> true <br />payment_type ----> instant <br />notify_version ----> 3.4 <br />payer_email ----> poy_cu_1247777456_per@yahoo.com <br />receiver_email ----> poy_cu_1247782578_biz@yahoo.com <br />address_city ----> San Jose <br />residence_country ----> US <br />sulod sa fppayment ok = <br/> txnok = 1<br/> mainemailok = 1<br/>\r\n                    totamount = <br />\r\n                    res = VERIFIED <br/>');
INSERT INTO errlogs VALUES ('6', 'payer_id ----> 8JEKCB5GJHXM8 <br />address_country_code ----> US <br />ipn_track_id ----> 9TlL6xYK7uNztsiB2y6rvg <br />address_zip ----> 95131 <br />charset ----> windows-1252 <br />payment_gross ----> 10.50 <br />address_status ----> confirmed <br />address_street ----> 1 Main St <br />verify_sign ----> A2B3v3sdHB3gvwF0gjR-Y6khoYFOATRPCItjWUZMwOMahRd2bFWRVE9- <br />test_ipn ----> 1 <br />txn_type ----> send_money <br />receiver_id ----> MWHFRU58WGJZ6 <br />payment_fee ----> 0.60 <br />mc_currency ----> USD <br />transaction_subject ---->  <br />protection_eligibility ----> Eligible <br />address_country ----> United States <br />payer_status ----> verified <br />first_name ----> Test <br />address_name ----> Test User <br />mc_gross ----> 10.50 <br />payment_date ----> 08:04:54 Nov 27, 2011 PST <br />payment_status ----> Completed <br />business ----> poy_cu_1247782578_biz@yahoo.com <br />last_name ----> User <br />address_state ----> CA <br />txn_id ----> 3M854443WK6326713 <br />mc_fee ----> 0.60 <br />resend ----> true <br />payment_type ----> instant <br />notify_version ----> 3.4 <br />payer_email ----> poy_cu_1247777456_per@yahoo.com <br />receiver_email ----> poy_cu_1247782578_biz@yahoo.com <br />address_city ----> San Jose <br />residence_country ----> US <br />sulod sa fppayment ok = 1<br/> txnok = 1<br/> mainemailok = 1<br/>\r\n                    totamount = 10.5<br />\r\n                    res = VERIFIED <br/>');
INSERT INTO errlogs VALUES ('7', 'mc_gross ----> 180.00 <br />protection_eligibility ----> Eligible <br />address_status ----> confirmed <br />payer_id ----> 8JEKCB5GJHXM8 <br />address_street ----> 1 Main St <br />payment_date ----> 23:24:51 Nov 27, 2011 PST <br />payment_status ----> Completed <br />charset ----> windows-1252 <br />address_zip ----> 95131 <br />first_name ----> Test <br />mc_fee ----> 5.52 <br />address_country_code ----> US <br />address_name ----> Test User <br />notify_version ----> 3.4 <br />payer_status ----> verified <br />business ----> poy_cu_1247782578_biz@yahoo.com <br />address_country ----> United States <br />address_city ----> San Jose <br />verify_sign ----> AS6mHAjw8Ml4Xchvl9ltLRMg2.FPAuBGSM3pSJ3I625uGUnd4i0DXeGP <br />payer_email ----> poy_cu_1247777456_per@yahoo.com <br />txn_id ----> 1EK17754404844424 <br />payment_type ----> instant <br />last_name ----> User <br />address_state ----> CA <br />receiver_email ----> poy_cu_1247782578_biz@yahoo.com <br />payment_fee ----> 5.52 <br />receiver_id ----> MWHFRU58WGJZ6 <br />txn_type ----> send_money <br />mc_currency ----> USD <br />residence_country ----> US <br />test_ipn ----> 1 <br />transaction_subject ---->  <br />payment_gross ----> 180.00 <br />ipn_track_id ----> vn3Mio3Ywq4nPrASXywYvA <br />sulod sa fppayment ok = 1<br/> txnok = 1<br/> mainemailok = 1<br/>\r\n                    totamount = 180<br />\r\n                    res = VERIFIED <br/>');
INSERT INTO errlogs VALUES ('8', 'mc_gross ----> 10.50 <br />protection_eligibility ----> Eligible <br />address_status ----> confirmed <br />payer_id ----> 8JEKCB5GJHXM8 <br />address_street ----> 1 Main St <br />payment_date ----> 00:06:52 Nov 28, 2011 PST <br />payment_status ----> Completed <br />charset ----> windows-1252 <br />address_zip ----> 95131 <br />first_name ----> Test <br />mc_fee ----> 0.60 <br />address_country_code ----> US <br />address_name ----> Test User <br />notify_version ----> 3.4 <br />payer_status ----> verified <br />business ----> poy_cu_1247782578_biz@yahoo.com <br />address_country ----> United States <br />address_city ----> San Jose <br />verify_sign ----> AFcWxV21C7fd0v3bYYYRCpSSRl31ACu-xMJYnHo-5xbiOGvQ9Fik99MW <br />payer_email ----> poy_cu_1247777456_per@yahoo.com <br />txn_id ----> 28J65505JH293134V <br />payment_type ----> instant <br />last_name ----> User <br />address_state ----> CA <br />receiver_email ----> poy_cu_1247782578_biz@yahoo.com <br />payment_fee ----> 0.60 <br />receiver_id ----> MWHFRU58WGJZ6 <br />txn_type ----> send_money <br />mc_currency ----> USD <br />residence_country ----> US <br />test_ipn ----> 1 <br />transaction_subject ---->  <br />payment_gross ----> 10.50 <br />ipn_track_id ----> P0MK9rzC50rKyjEbYVWAUA <br />sulod sa fppayment ok = <br/> txnok = 1<br/> mainemailok = 1<br/>\r\n                    totamount = 12<br />\r\n                    res = VERIFIED <br/>');
INSERT INTO errlogs VALUES ('9', 'mc_gross ----> 30.00 <br />protection_eligibility ----> Eligible <br />address_status ----> confirmed <br />payer_id ----> 8JEKCB5GJHXM8 <br />address_street ----> 1 Main St <br />payment_date ----> 00:53:53 Nov 28, 2011 PST <br />payment_status ----> Completed <br />charset ----> windows-1252 <br />address_zip ----> 95131 <br />first_name ----> Test <br />mc_fee ----> 1.17 <br />address_country_code ----> US <br />address_name ----> Test User <br />notify_version ----> 3.4 <br />payer_status ----> verified <br />business ----> poy_cu_1247782578_biz@yahoo.com <br />address_country ----> United States <br />address_city ----> San Jose <br />verify_sign ----> AZ-U2MXRH3AVvlsg3xQxVe3vInj9AqYHqoLKCmH1dxRWdBCWs9GZ16uq <br />payer_email ----> poy_cu_1247777456_per@yahoo.com <br />txn_id ----> 7R8990790C5047014 <br />payment_type ----> instant <br />last_name ----> User <br />address_state ----> CA <br />receiver_email ----> poy_cu_1247782578_biz@yahoo.com <br />payment_fee ----> 1.17 <br />receiver_id ----> MWHFRU58WGJZ6 <br />txn_type ----> send_money <br />mc_currency ----> USD <br />residence_country ----> US <br />test_ipn ----> 1 <br />transaction_subject ---->  <br />payment_gross ----> 30.00 <br />ipn_track_id ----> fkOJxXTO5AIDS.CjWtLjDg <br />sulod sa fppayment ok = <br/> txnok = 1<br/> mainemailok = 1<br/>\r\n                    totamount = 12<br />\r\n                    res = VERIFIED <br/>');
INSERT INTO errlogs VALUES ('10', 'mc_gross ----> 10.00 <br />protection_eligibility ----> Eligible <br />address_status ----> confirmed <br />payer_id ----> 8JEKCB5GJHXM8 <br />address_street ----> 1 Main St <br />payment_date ----> 01:00:35 Nov 28, 2011 PST <br />payment_status ----> Completed <br />charset ----> windows-1252 <br />address_zip ----> 95131 <br />first_name ----> Test <br />mc_fee ----> 0.59 <br />address_country_code ----> US <br />address_name ----> Test User <br />notify_version ----> 3.4 <br />payer_status ----> verified <br />business ----> poy_cu_1247782578_biz@yahoo.com <br />address_country ----> United States <br />address_city ----> San Jose <br />verify_sign ----> Ac9JCfIPcAZed16E-FLvbjdO.litAJH8lf8EP9-jocEJ9KtRUqwR3i5Z <br />payer_email ----> poy_cu_1247777456_per@yahoo.com <br />txn_id ----> 5TC601529R781492P <br />payment_type ----> instant <br />last_name ----> User <br />address_state ----> CA <br />receiver_email ----> poy_cu_1247782578_biz@yahoo.com <br />payment_fee ----> 0.59 <br />receiver_id ----> MWHFRU58WGJZ6 <br />txn_type ----> send_money <br />mc_currency ----> USD <br />residence_country ----> US <br />test_ipn ----> 1 <br />transaction_subject ---->  <br />payment_gross ----> 10.00 <br />ipn_track_id ----> CrFOJhFUpnen8vIIa.peFQ <br />sulod sa fppayment ok = <br/> txnok = 1<br/> mainemailok = 1<br/>\r\n                    totamount = 12<br />\r\n                    res = VERIFIED <br/>');
INSERT INTO errlogs VALUES ('11', 'mc_gross ----> 150.00 <br />protection_eligibility ----> Eligible <br />address_status ----> confirmed <br />payer_id ----> 8JEKCB5GJHXM8 <br />address_street ----> 1 Main St <br />payment_date ----> 01:21:49 Nov 28, 2011 PST <br />payment_status ----> Completed <br />charset ----> windows-1252 <br />address_zip ----> 95131 <br />first_name ----> Test <br />mc_fee ----> 4.65 <br />address_country_code ----> US <br />address_name ----> Test User <br />notify_version ----> 3.4 <br />payer_status ----> verified <br />business ----> poy_cu_1247782578_biz@yahoo.com <br />address_country ----> United States <br />address_city ----> San Jose <br />verify_sign ----> An5ns1Kso7MWUdW4ErQKJJJ4qi4-AwNhc9CBFzwPKlpuImh91cn6Gbm5 <br />payer_email ----> poy_cu_1247777456_per@yahoo.com <br />txn_id ----> 3C766401US805313C <br />payment_type ----> instant <br />last_name ----> User <br />address_state ----> CA <br />receiver_email ----> poy_cu_1247782578_biz@yahoo.com <br />payment_fee ----> 4.65 <br />receiver_id ----> MWHFRU58WGJZ6 <br />txn_type ----> send_money <br />mc_currency ----> USD <br />residence_country ----> US <br />test_ipn ----> 1 <br />transaction_subject ---->  <br />payment_gross ----> 150.00 <br />ipn_track_id ----> fXLhXpVi6VjbJcwgqaPCSA <br />sulod sa fppayment ok = 1<br/> txnok = 1<br/> mainemailok = 1<br/>\r\n                    totamount = 150<br />\r\n                    res = VERIFIED <br/>');
INSERT INTO errlogs VALUES ('12', 'mc_gross ----> 12.00 <br />protection_eligibility ----> Eligible <br />address_status ----> confirmed <br />payer_id ----> 8JEKCB5GJHXM8 <br />address_street ----> 1 Main St <br />payment_date ----> 21:38:38 Dec 05, 2011 PST <br />payment_status ----> Completed <br />charset ----> windows-1252 <br />address_zip ----> 95131 <br />first_name ----> Test <br />mc_fee ----> 0.65 <br />address_country_code ----> US <br />address_name ----> Test User <br />notify_version ----> 3.4 <br />payer_status ----> verified <br />business ----> poy_cu_1247782578_biz@yahoo.com <br />address_country ----> United States <br />address_city ----> San Jose <br />verify_sign ----> AU9WXDyS6Af6NrgDpFdO-gp6q6TYA5GPwfsRyMO3rW4VShaSh8TmbqC2 <br />payer_email ----> poy_cu_1247777456_per@yahoo.com <br />txn_id ----> 9E618097T46580048 <br />payment_type ----> instant <br />last_name ----> User <br />address_state ----> CA <br />receiver_email ----> poy_cu_1247782578_biz@yahoo.com <br />payment_fee ----> 0.65 <br />receiver_id ----> MWHFRU58WGJZ6 <br />txn_type ----> send_money <br />mc_currency ----> USD <br />residence_country ----> US <br />test_ipn ----> 1 <br />transaction_subject ---->  <br />payment_gross ----> 12.00 <br />ipn_track_id ----> Qx8C2EEWZ13xo7E1FJNqpA <br />sulod sa fppayment ok = 1<br/> txnok = 1<br/> mainemailok = 1<br/>\r\n                    totamount = 12<br />\r\n                    res = VERIFIED <br/>');

-- ----------------------------
-- Table structure for `forum`
-- ----------------------------
DROP TABLE IF EXISTS `forum`;
CREATE TABLE `forum` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `userID` int(11) NOT NULL,
  `title` varchar(200) NOT NULL,
  `message` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of forum
-- ----------------------------
INSERT INTO forum VALUES ('1', '2011-09-21 03:21:07', '2011-09-21 03:21:12', '1', 'What are soaps?', 'Can please anybody help me answer this question?');
INSERT INTO forum VALUES ('2', '2011-09-21 03:21:53', '2011-09-21 03:21:56', '4', 'Why do we need Soaps?', 'why the hell do i have to use soaps?');
INSERT INTO forum VALUES ('3', '2011-11-03 11:50:47', '2011-11-03 11:50:47', '1', 'hi', 'dsgfhgv');
INSERT INTO forum VALUES ('4', '2011-11-03 12:38:33', '2011-11-03 12:38:33', '1', 'sample topic', 'fdhkafaf');

-- ----------------------------
-- Table structure for `forumcomments`
-- ----------------------------
DROP TABLE IF EXISTS `forumcomments`;
CREATE TABLE `forumcomments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `comment` text NOT NULL,
  `userID` int(11) NOT NULL,
  `forumID` int(11) NOT NULL,
  `datetrans` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of forumcomments
-- ----------------------------
INSERT INTO forumcomments VALUES ('1', 'no comment', '4', '1', '2011-09-21 10:54:13');
INSERT INTO forumcomments VALUES ('2', 'i love soaps', '1', '2', '2011-09-21 10:54:16');
INSERT INTO forumcomments VALUES ('3', 'hello', '1', '1', '2011-11-17 14:01:59');

-- ----------------------------
-- Table structure for `groups`
-- ----------------------------
DROP TABLE IF EXISTS `groups`;
CREATE TABLE `groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `description` varchar(100) DEFAULT NULL,
  `password_reset_interval_days` smallint(5) DEFAULT NULL,
  `password_reset_last_upd_dt` datetime DEFAULT NULL,
  `pnl_info` smallint(1) DEFAULT '0',
  `pnl_backendmgt` smallint(1) DEFAULT '0',
  `pnl_contentmgt` smallint(1) DEFAULT '0',
  `pnl_systemsetting` smallint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of groups
-- ----------------------------
INSERT INTO groups VALUES ('1', 'Superadmin', '', '7', '2014-09-22 07:24:02', '0', '0', '0', '0');
INSERT INTO groups VALUES ('2', 'Admin', '', '0', '2014-09-22 07:24:02', '0', '0', '0', '0');

-- ----------------------------
-- Table structure for `groups_naviboxes`
-- ----------------------------
DROP TABLE IF EXISTS `groups_naviboxes`;
CREATE TABLE `groups_naviboxes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `navibox_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=503 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of groups_naviboxes
-- ----------------------------
INSERT INTO groups_naviboxes VALUES ('221', '5', '29');
INSERT INTO groups_naviboxes VALUES ('220', '5', '27');
INSERT INTO groups_naviboxes VALUES ('446', '2', '47');
INSERT INTO groups_naviboxes VALUES ('461', '1', '460');
INSERT INTO groups_naviboxes VALUES ('460', '1', '49');
INSERT INTO groups_naviboxes VALUES ('459', '1', '46');
INSERT INTO groups_naviboxes VALUES ('222', '5', '24');
INSERT INTO groups_naviboxes VALUES ('223', '5', '26');
INSERT INTO groups_naviboxes VALUES ('224', '5', '31');
INSERT INTO groups_naviboxes VALUES ('225', '5', '41');
INSERT INTO groups_naviboxes VALUES ('226', '5', '28');
INSERT INTO groups_naviboxes VALUES ('227', '5', '23');
INSERT INTO groups_naviboxes VALUES ('228', '5', '25');
INSERT INTO groups_naviboxes VALUES ('229', '5', '30');
INSERT INTO groups_naviboxes VALUES ('230', '5', '40');
INSERT INTO groups_naviboxes VALUES ('231', '5', '42');
INSERT INTO groups_naviboxes VALUES ('445', '2', '44');
INSERT INTO groups_naviboxes VALUES ('444', '2', '40');
INSERT INTO groups_naviboxes VALUES ('443', '2', '23');
INSERT INTO groups_naviboxes VALUES ('453', '2', '460');
INSERT INTO groups_naviboxes VALUES ('452', '2', '49');
INSERT INTO groups_naviboxes VALUES ('451', '2', '46');
INSERT INTO groups_naviboxes VALUES ('450', '2', '42');
INSERT INTO groups_naviboxes VALUES ('449', '2', '31');
INSERT INTO groups_naviboxes VALUES ('448', '2', '29');
INSERT INTO groups_naviboxes VALUES ('447', '2', '45');
INSERT INTO groups_naviboxes VALUES ('442', '2', '27');
INSERT INTO groups_naviboxes VALUES ('458', '1', '29');
INSERT INTO groups_naviboxes VALUES ('457', '1', '45');
INSERT INTO groups_naviboxes VALUES ('456', '1', '47');
INSERT INTO groups_naviboxes VALUES ('455', '1', '44');
INSERT INTO groups_naviboxes VALUES ('454', '1', '27');

-- ----------------------------
-- Table structure for `inventory`
-- ----------------------------
DROP TABLE IF EXISTS `inventory`;
CREATE TABLE `inventory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `productID` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `inx` int(11) DEFAULT NULL,
  `outx` int(11) DEFAULT NULL,
  `amount` float DEFAULT NULL,
  `rq` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of inventory
-- ----------------------------
INSERT INTO inventory VALUES ('1', '3', '2011-09-21 00:12:43', '2011-09-21 00:12:43', '90', null, '0', '90');
INSERT INTO inventory VALUES ('2', '3', '2011-09-21 00:15:12', '2011-09-21 00:15:12', null, '12', '600', '78');
INSERT INTO inventory VALUES ('3', '1', '2011-09-21 13:07:04', '2011-09-21 13:07:04', null, '2', '21', '48');
INSERT INTO inventory VALUES ('4', '22', '2011-11-22 11:36:18', '2011-11-22 11:36:18', '14', null, '0', '14');
INSERT INTO inventory VALUES ('5', '23', '2011-11-22 11:40:40', '2011-11-22 11:40:40', '100', null, '0', '100');
INSERT INTO inventory VALUES ('6', '5', '2011-11-28 01:54:58', '2011-11-28 01:54:58', null, '2', '24', '48');
INSERT INTO inventory VALUES ('7', '7', '2011-11-28 15:25:11', '2011-11-28 15:25:11', null, '2', '180', '48');
INSERT INTO inventory VALUES ('8', '22', '2011-11-28 17:08:58', '2011-11-28 17:08:58', '10', null, '0', '24');
INSERT INTO inventory VALUES ('9', '22', '2011-11-28 17:17:37', '2011-11-28 17:17:37', '50', null, '0', '74');
INSERT INTO inventory VALUES ('10', '22', '2011-11-28 17:22:06', '2011-11-28 17:22:06', null, '10', '150', '64');
INSERT INTO inventory VALUES ('11', '24', '2011-11-28 17:26:07', '2011-11-28 17:26:07', '50', null, '0', '50');
INSERT INTO inventory VALUES ('12', '3', '2011-12-06 13:38:49', '2011-12-06 13:38:49', null, '1', '12', '77');

-- ----------------------------
-- Table structure for `joborder`
-- ----------------------------
DROP TABLE IF EXISTS `joborder`;
CREATE TABLE `joborder` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customerId` int(11) NOT NULL,
  `totalAmount` float NOT NULL,
  `datePickup` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `serviceId` int(11) NOT NULL,
  `approveId` int(11) NOT NULL,
  `workId` int(11) NOT NULL,
  `downPayment` float NOT NULL,
  `balance` float NOT NULL,
  `amountPaid` float NOT NULL,
  `transactionId` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of joborder
-- ----------------------------
INSERT INTO joborder VALUES ('1', '0', '500', '2014-10-10 00:00:00', '2014-10-03 05:10:09', '2014-10-03 05:10:09', '1', '0', '0', '300', '200', '300', '');
INSERT INTO joborder VALUES ('2', '0', '500', '2014-10-10 00:00:00', '2014-10-03 05:10:35', '2014-10-03 05:10:35', '1', '0', '0', '300', '200', '300', '');
INSERT INTO joborder VALUES ('3', '0', '500', '2014-10-04 00:00:00', '2014-10-03 05:13:35', '2014-10-03 05:13:35', '2', '0', '0', '300', '200', '300', '');
INSERT INTO joborder VALUES ('4', '0', '500', '2015-12-01 00:00:00', '2014-10-03 11:41:01', '2014-10-03 11:41:01', '1', '0', '0', '300', '200', '300', '');
INSERT INTO joborder VALUES ('5', '1', '500', '2014-10-22 00:00:00', '2014-10-03 11:48:03', '2014-10-03 11:48:03', '1', '76', '76', '200', '300', '200', '');
INSERT INTO joborder VALUES ('7', '4', '6000', '2014-10-06 00:00:00', '2014-10-03 15:42:50', '2014-10-03 15:42:50', '1', '76', '91', '3000', '3000', '3000', '');
INSERT INTO joborder VALUES ('8', '4', '1000', '2014-10-07 00:00:00', '2014-10-03 15:44:56', '2014-10-03 15:44:56', '1', '76', '76', '1100', '-100', '1100', '');
INSERT INTO joborder VALUES ('9', '6', '2500', '2014-10-07 00:00:00', '2014-10-03 15:52:02', '2014-10-03 15:52:02', '2', '76', '76', '0', '0', '2500', '');

-- ----------------------------
-- Table structure for `lang_group`
-- ----------------------------
DROP TABLE IF EXISTS `lang_group`;
CREATE TABLE `lang_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of lang_group
-- ----------------------------
INSERT INTO lang_group VALUES ('1', 'Button');
INSERT INTO lang_group VALUES ('2', 'Panel');
INSERT INTO lang_group VALUES ('3', 'Field');
INSERT INTO lang_group VALUES ('4', 'Column');
INSERT INTO lang_group VALUES ('5', 'FAQ');
INSERT INTO lang_group VALUES ('6', 'Header');
INSERT INTO lang_group VALUES ('7', 'Message');
INSERT INTO lang_group VALUES ('8', 'Tab');
INSERT INTO lang_group VALUES ('9', 'Tooltip');

-- ----------------------------
-- Table structure for `lang_keywords`
-- ----------------------------
DROP TABLE IF EXISTS `lang_keywords`;
CREATE TABLE `lang_keywords` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `keyword` varchar(45) NOT NULL,
  `group_id` int(11) DEFAULT NULL,
  `url` varchar(100) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `keyword` (`keyword`)
) ENGINE=InnoDB AUTO_INCREMENT=260 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of lang_keywords
-- ----------------------------
INSERT INTO lang_keywords VALUES ('1', 'BTN_SAVE', '1', null, null);
INSERT INTO lang_keywords VALUES ('2', 'BTN_DELETE', '1', null, null);
INSERT INTO lang_keywords VALUES ('3', 'BTN_CANCEL', '1', null, null);
INSERT INTO lang_keywords VALUES ('4', 'BTN_UPDATE', '1', null, null);
INSERT INTO lang_keywords VALUES ('5', 'BTN_RESET', '1', null, null);
INSERT INTO lang_keywords VALUES ('86', 'BTN_HOME', '1', null, null);
INSERT INTO lang_keywords VALUES ('95', 'BTN_MESSAGES', '1', null, null);
INSERT INTO lang_keywords VALUES ('97', 'BTN_BACKEND_NEWS', '1', null, null);
INSERT INTO lang_keywords VALUES ('110', 'BTN_EDIT_PROFILE', '1', null, null);
INSERT INTO lang_keywords VALUES ('112', 'BTN_CHANGE_GROUP', '1', null, null);
INSERT INTO lang_keywords VALUES ('113', 'BTN_NAVIGATION_BOXES', '1', null, null);
INSERT INTO lang_keywords VALUES ('129', 'PNL_STATISTICS', '2', null, null);
INSERT INTO lang_keywords VALUES ('130', 'PNL_UPDATES', '2', null, null);
INSERT INTO lang_keywords VALUES ('131', 'BTN_LOGOUT', '1', null, null);
INSERT INTO lang_keywords VALUES ('132', 'BTN_USER_LIST', '1', null, null);
INSERT INTO lang_keywords VALUES ('133', 'PNL_BACKEND_MANAGEMENT', '2', null, null);
INSERT INTO lang_keywords VALUES ('134', 'PNL_CONTENT_MANAGEMENT', '2', null, null);
INSERT INTO lang_keywords VALUES ('135', 'PNL_SYSTEM_SETTING', '2', null, null);
INSERT INTO lang_keywords VALUES ('136', 'BTN_ACTORS', '1', null, null);
INSERT INTO lang_keywords VALUES ('137', 'BTN_CATEGORIES', '1', null, null);
INSERT INTO lang_keywords VALUES ('138', 'BTN_PICTURE_MANAGEMENT', '1', null, null);
INSERT INTO lang_keywords VALUES ('139', 'HDR_WELCOME_ADMIN_PAGE', '6', null, null);
INSERT INTO lang_keywords VALUES ('140', 'PNL_MAIN_MENU', '2', null, null);
INSERT INTO lang_keywords VALUES ('141', 'PNL_NEWS', '2', null, null);
INSERT INTO lang_keywords VALUES ('142', 'BTN_SYSTEM_SETTING', '1', null, null);
INSERT INTO lang_keywords VALUES ('143', 'BTN_FAQ_HELP', '1', null, null);
INSERT INTO lang_keywords VALUES ('144', 'BTN_TRANSLATION_SYSTEM', '1', null, null);
INSERT INTO lang_keywords VALUES ('145', 'COL_SUBJECT', '4', null, null);
INSERT INTO lang_keywords VALUES ('146', 'COL_DATE', '4', null, null);
INSERT INTO lang_keywords VALUES ('147', 'TAB_NEW', '8', null, null);
INSERT INTO lang_keywords VALUES ('148', 'TAB_READ_MESSAGES', '8', null, null);
INSERT INTO lang_keywords VALUES ('149', 'TAB_SENT_MESSAGES', '8', null, null);
INSERT INTO lang_keywords VALUES ('150', 'TAB_ALL_MESSAGES', '8', null, null);
INSERT INTO lang_keywords VALUES ('151', 'BTN_SEND_NEW_MESSAGE', '1', null, null);
INSERT INTO lang_keywords VALUES ('152', 'COL_TYPE', '4', null, null);
INSERT INTO lang_keywords VALUES ('153', 'COL_SENDER', '4', null, null);
INSERT INTO lang_keywords VALUES ('157', 'FLD_PICTURE', '3', null, null);
INSERT INTO lang_keywords VALUES ('159', 'FLD_HIDE_PHONE_LIST', '3', null, null);
INSERT INTO lang_keywords VALUES ('160', 'FLD_DEFAULT_LANGUAGE ', '3', null, null);
INSERT INTO lang_keywords VALUES ('161', 'FLD_USERNAME', '3', null, null);
INSERT INTO lang_keywords VALUES ('162', 'FLD_PASSWORD ', '3', null, null);
INSERT INTO lang_keywords VALUES ('163', 'FLD_GROUP', '3', null, null);
INSERT INTO lang_keywords VALUES ('164', 'FLD_FIRST_NAME', '3', null, null);
INSERT INTO lang_keywords VALUES ('165', 'FLD_LAST_NAME', '3', null, null);
INSERT INTO lang_keywords VALUES ('166', 'FLD_EMAIL', '3', null, null);
INSERT INTO lang_keywords VALUES ('167', 'FLD_VERIFY_EMAIL ', '3', null, null);
INSERT INTO lang_keywords VALUES ('168', 'FLD_PHONE', '3', null, null);
INSERT INTO lang_keywords VALUES ('169', 'FLD_MOBILE_PHONE', '3', null, null);
INSERT INTO lang_keywords VALUES ('170', 'FLD_GENDER', '3', null, null);
INSERT INTO lang_keywords VALUES ('171', 'FLD_BIRTH_DATE', '3', null, null);
INSERT INTO lang_keywords VALUES ('172', 'FLD_LAND', '3', null, null);
INSERT INTO lang_keywords VALUES ('173', 'FLD_HOME_PAGE', '3', null, null);
INSERT INTO lang_keywords VALUES ('175', 'FLD_ICQ ', '3', null, null);
INSERT INTO lang_keywords VALUES ('176', 'FLD_MSN', '3', null, null);
INSERT INTO lang_keywords VALUES ('189', 'FLD_NAME', '3', null, null);
INSERT INTO lang_keywords VALUES ('190', 'FLD_DESCRIPTION', '3', null, null);
INSERT INTO lang_keywords VALUES ('191', 'BTN_ADD', '1', null, null);
INSERT INTO lang_keywords VALUES ('192', 'BTN_CLEAR', '1', null, null);
INSERT INTO lang_keywords VALUES ('193', 'BTN_CREATE_ACTOR', '1', null, null);
INSERT INTO lang_keywords VALUES ('194', 'BTN_CREATE_PHOTOGALLERY', '1', null, null);
INSERT INTO lang_keywords VALUES ('195', 'MSG_TTL_WARNING', '7', null, null);
INSERT INTO lang_keywords VALUES ('196', 'MSG_TTL_ERROR', '7', null, null);
INSERT INTO lang_keywords VALUES ('197', 'MSG_TTL_DELETE', '7', null, null);
INSERT INTO lang_keywords VALUES ('198', 'MSG_TTL_STATUS', '7', null, null);
INSERT INTO lang_keywords VALUES ('199', 'MSG_FAILURE', '7', null, null);
INSERT INTO lang_keywords VALUES ('200', 'MSG_PLEASE_WAIT', '7', null, null);
INSERT INTO lang_keywords VALUES ('201', 'MSG_UPLOADING_FILE', '7', null, null);
INSERT INTO lang_keywords VALUES ('202', 'MSG_SAVING_DATA', '7', null, null);
INSERT INTO lang_keywords VALUES ('203', 'MSG_TXT_SERVER_UNREACHBLE', '7', null, null);
INSERT INTO lang_keywords VALUES ('204', 'MSG_TXT_CONFIRM_DELETE', '7', null, null);
INSERT INTO lang_keywords VALUES ('205', 'MSG_TXT_CONFIRM_DELETE_ACTOR', '7', null, null);
INSERT INTO lang_keywords VALUES ('206', 'PNL_EDIT_IMAGE', '2', null, null);
INSERT INTO lang_keywords VALUES ('207', 'PNL_EDIT_ACTOR', '2', null, null);
INSERT INTO lang_keywords VALUES ('208', 'PNL_EDIT_PHOTOGALLERY', '2', null, null);
INSERT INTO lang_keywords VALUES ('209', 'PNL_INFORMATION', '2', null, null);
INSERT INTO lang_keywords VALUES ('210', 'PNL_ZIP_UPLOADIMAGE', '2', null, null);
INSERT INTO lang_keywords VALUES ('211', 'PNL_IMAGE_LIST', '2', null, null);
INSERT INTO lang_keywords VALUES ('212', 'PNL_ACTOR_LIST', '2', null, null);
INSERT INTO lang_keywords VALUES ('213', 'PNL_PHOTOGALLERY_LIST', '2', null, null);
INSERT INTO lang_keywords VALUES ('214', 'FLD_TIMEMGT_FROM', '3', null, null);
INSERT INTO lang_keywords VALUES ('215', 'FLD_TIMEMGT_TO', '3', null, null);
INSERT INTO lang_keywords VALUES ('216', 'FLD_TITLE', '3', null, null);
INSERT INTO lang_keywords VALUES ('217', 'FLD_ORDER', '3', null, null);
INSERT INTO lang_keywords VALUES ('218', 'FLD_FSK', '3', null, null);
INSERT INTO lang_keywords VALUES ('219', 'FLD_URL', '3', null, null);
INSERT INTO lang_keywords VALUES ('220', 'FLD_KEYWORDS_SEO', '3', null, null);
INSERT INTO lang_keywords VALUES ('221', 'FLD_FREE', '3', null, null);
INSERT INTO lang_keywords VALUES ('222', 'FLD_FROM_USER', '3', null, null);
INSERT INTO lang_keywords VALUES ('223', 'FLD_STATUS', '3', null, null);
INSERT INTO lang_keywords VALUES ('224', 'FLD_ACTORS', '3', null, null);
INSERT INTO lang_keywords VALUES ('225', 'FLD_CATEGORY', '3', null, null);
INSERT INTO lang_keywords VALUES ('226', 'FLD_SELECT_ZIP_FILE', '3', null, null);
INSERT INTO lang_keywords VALUES ('227', 'TIP_CLICKTODELETE', '9', null, null);
INSERT INTO lang_keywords VALUES ('228', 'TIP_EDIT_IMAGE', '9', null, null);
INSERT INTO lang_keywords VALUES ('229', 'TIP_DELETE_IMAGE', '9', null, null);
INSERT INTO lang_keywords VALUES ('230', 'TIP_DELETE', '9', null, null);
INSERT INTO lang_keywords VALUES ('231', 'TIP_EDIT', '9', null, null);
INSERT INTO lang_keywords VALUES ('232', 'TIP_CREATE_ACTOR', '9', null, null);
INSERT INTO lang_keywords VALUES ('233', 'TIP_CREATE_PHOTOGALLERY', '9', null, null);
INSERT INTO lang_keywords VALUES ('234', 'HDR_NAME', '6', null, null);
INSERT INTO lang_keywords VALUES ('235', 'HDR_TITLE', '6', null, null);
INSERT INTO lang_keywords VALUES ('236', 'HDR_DESCRIPTION', '6', null, null);
INSERT INTO lang_keywords VALUES ('237', 'HDR_TM_FROM', '6', null, null);
INSERT INTO lang_keywords VALUES ('238', 'HDR_TM_TO', '6', null, null);
INSERT INTO lang_keywords VALUES ('239', 'HDR_USERNAME', '6', null, null);
INSERT INTO lang_keywords VALUES ('240', 'HDR_STATUS', '6', null, null);
INSERT INTO lang_keywords VALUES ('241', 'HDR_SEO', '6', null, null);
INSERT INTO lang_keywords VALUES ('242', 'HDR_URL', '6', null, null);
INSERT INTO lang_keywords VALUES ('243', 'HDR_FREE', '6', null, null);
INSERT INTO lang_keywords VALUES ('244', 'HDR_DATE_CREATED', '6', null, null);
INSERT INTO lang_keywords VALUES ('245', 'HDR_USERID_CREATED', '6', null, null);
INSERT INTO lang_keywords VALUES ('249', 'MSG_LOGIN_CON_ERROR', '7', null, null);
INSERT INTO lang_keywords VALUES ('250', 'MSG_LOGIN_FIELDS_REQUIRED', '7', null, null);
INSERT INTO lang_keywords VALUES ('251', 'MSG_LOGIN_INVALID', '7', null, null);
INSERT INTO lang_keywords VALUES ('252', 'MSG_MAX_LOGIN', '7', null, null);
INSERT INTO lang_keywords VALUES ('253', 'MSG_ACCT_DISABLED', '7', null, null);
INSERT INTO lang_keywords VALUES ('254', 'MSG_USER_LOGGED_IN', '7', null, null);
INSERT INTO lang_keywords VALUES ('255', 'PNL_LOGIN_ERROR', '2', null, null);
INSERT INTO lang_keywords VALUES ('256', 'PNL_LOGIN_TITLE', '2', null, null);
INSERT INTO lang_keywords VALUES ('257', 'BTN_FORGOT_PASSWORD', '1', null, null);
INSERT INTO lang_keywords VALUES ('258', 'BTN_LOGIN', '1', null, null);
INSERT INTO lang_keywords VALUES ('259', 'TIP_FORGOT_PASSWORD', '9', null, null);

-- ----------------------------
-- Table structure for `lang_translation`
-- ----------------------------
DROP TABLE IF EXISTS `lang_translation`;
CREATE TABLE `lang_translation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `keyword_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `translation` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_keyword_id` (`keyword_id`,`country_id`),
  CONSTRAINT `lang_translation_ibfk_1` FOREIGN KEY (`keyword_id`) REFERENCES `lang_keywords` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1864 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of lang_translation
-- ----------------------------
INSERT INTO lang_translation VALUES ('184', '1', '1', 'Save');
INSERT INTO lang_translation VALUES ('185', '2', '1', 'Delete');
INSERT INTO lang_translation VALUES ('186', '3', '1', 'Cancel');
INSERT INTO lang_translation VALUES ('202', '4', '1', 'Update');
INSERT INTO lang_translation VALUES ('206', '5', '1', 'Reset');
INSERT INTO lang_translation VALUES ('716', '86', '1', 'Home');
INSERT INTO lang_translation VALUES ('740', '95', '1', 'Messages');
INSERT INTO lang_translation VALUES ('746', '97', '1', 'Backend News');
INSERT INTO lang_translation VALUES ('812', '110', '1', 'Edit Profile');
INSERT INTO lang_translation VALUES ('818', '112', '1', 'Change Group');
INSERT INTO lang_translation VALUES ('824', '113', '1', 'Navigation Boxes');
INSERT INTO lang_translation VALUES ('915', '129', '1', 'Satistics');
INSERT INTO lang_translation VALUES ('921', '130', '1', 'Updates');
INSERT INTO lang_translation VALUES ('923', '131', '1', 'Logout');
INSERT INTO lang_translation VALUES ('925', '132', '1', 'User List');
INSERT INTO lang_translation VALUES ('927', '133', '1', 'Backend Management');
INSERT INTO lang_translation VALUES ('929', '134', '1', 'Content Management');
INSERT INTO lang_translation VALUES ('931', '135', '1', 'System Management');
INSERT INTO lang_translation VALUES ('933', '136', '1', 'Actors');
INSERT INTO lang_translation VALUES ('935', '137', '1', 'Categories');
INSERT INTO lang_translation VALUES ('937', '138', '1', 'Picture Management');
INSERT INTO lang_translation VALUES ('939', '139', '1', 'WELCOME TO THE ADMINISTRATION PAGE');
INSERT INTO lang_translation VALUES ('945', '140', '1', 'Main Menu');
INSERT INTO lang_translation VALUES ('947', '141', '1', 'News');
INSERT INTO lang_translation VALUES ('949', '142', '1', 'System Setting');
INSERT INTO lang_translation VALUES ('951', '143', '1', 'Faq / Help Text');
INSERT INTO lang_translation VALUES ('953', '144', '1', 'Translation System');
INSERT INTO lang_translation VALUES ('955', '145', '1', 'Subject');
INSERT INTO lang_translation VALUES ('957', '146', '1', 'Date');
INSERT INTO lang_translation VALUES ('959', '147', '1', 'New');
INSERT INTO lang_translation VALUES ('961', '148', '1', 'Read Messages');
INSERT INTO lang_translation VALUES ('963', '149', '1', 'Sent Messages');
INSERT INTO lang_translation VALUES ('965', '150', '1', 'All messages');
INSERT INTO lang_translation VALUES ('967', '151', '1', 'Send New Message');
INSERT INTO lang_translation VALUES ('969', '152', '1', 'Type');
INSERT INTO lang_translation VALUES ('971', '153', '1', 'Sender');
INSERT INTO lang_translation VALUES ('973', '157', '1', 'Picture');
INSERT INTO lang_translation VALUES ('974', '159', '1', 'Hide phone list?');
INSERT INTO lang_translation VALUES ('975', '160', '1', 'Default Language');
INSERT INTO lang_translation VALUES ('976', '161', '1', 'Username');
INSERT INTO lang_translation VALUES ('977', '162', '1', 'Password');
INSERT INTO lang_translation VALUES ('978', '163', '1', 'Group');
INSERT INTO lang_translation VALUES ('979', '164', '1', 'First name *');
INSERT INTO lang_translation VALUES ('985', '165', '1', 'Last name');
INSERT INTO lang_translation VALUES ('986', '166', '1', 'Email *');
INSERT INTO lang_translation VALUES ('987', '167', '1', 'Verify Email *');
INSERT INTO lang_translation VALUES ('988', '168', '1', 'Phone *');
INSERT INTO lang_translation VALUES ('989', '169', '1', 'Mobile Phone ');
INSERT INTO lang_translation VALUES ('990', '170', '1', 'Gender *');
INSERT INTO lang_translation VALUES ('991', '171', '1', 'Date of Birth *');
INSERT INTO lang_translation VALUES ('992', '172', '1', 'Country *');
INSERT INTO lang_translation VALUES ('993', '173', '1', 'Homepage');
INSERT INTO lang_translation VALUES ('994', '175', '1', 'ICQ');
INSERT INTO lang_translation VALUES ('995', '176', '1', 'MSN');
INSERT INTO lang_translation VALUES ('1150', '1', '2', 'Speichern');
INSERT INTO lang_translation VALUES ('1151', '1', '4', 'Iligtas');
INSERT INTO lang_translation VALUES ('1152', '1', '5', 'Uložit');
INSERT INTO lang_translation VALUES ('1153', '1', '6', 'Guardar');
INSERT INTO lang_translation VALUES ('1154', '1', '7', '-');
INSERT INTO lang_translation VALUES ('1155', '3', '2', 'Zurücksetzen');
INSERT INTO lang_translation VALUES ('1156', '3', '4', 'Likod');
INSERT INTO lang_translation VALUES ('1157', '3', '5', 'Zpět');
INSERT INTO lang_translation VALUES ('1158', '3', '6', 'Vuelta');
INSERT INTO lang_translation VALUES ('1159', '3', '7', '-');
INSERT INTO lang_translation VALUES ('1160', '2', '2', 'Entfernen');
INSERT INTO lang_translation VALUES ('1161', '2', '4', 'Alisin');
INSERT INTO lang_translation VALUES ('1162', '2', '5', 'Vymazat');
INSERT INTO lang_translation VALUES ('1163', '2', '6', 'Eliminar');
INSERT INTO lang_translation VALUES ('1164', '2', '7', '-');
INSERT INTO lang_translation VALUES ('1165', '4', '2', 'Speichern');
INSERT INTO lang_translation VALUES ('1166', '4', '4', 'Iligtas');
INSERT INTO lang_translation VALUES ('1167', '4', '5', 'Uložit');
INSERT INTO lang_translation VALUES ('1168', '4', '6', 'Guardar');
INSERT INTO lang_translation VALUES ('1169', '4', '7', '-');
INSERT INTO lang_translation VALUES ('1170', '5', '2', 'Zurücksetzen');
INSERT INTO lang_translation VALUES ('1171', '5', '4', 'Orihinal na setting');
INSERT INTO lang_translation VALUES ('1172', '5', '5', 'Původní nastavení');
INSERT INTO lang_translation VALUES ('1173', '5', '6', 'Configuracion original');
INSERT INTO lang_translation VALUES ('1174', '5', '7', '-');
INSERT INTO lang_translation VALUES ('1175', '86', '2', 'Home');
INSERT INTO lang_translation VALUES ('1176', '86', '4', 'Tahanan');
INSERT INTO lang_translation VALUES ('1177', '86', '5', 'Domů');
INSERT INTO lang_translation VALUES ('1178', '86', '6', 'Casa');
INSERT INTO lang_translation VALUES ('1179', '86', '7', '-');
INSERT INTO lang_translation VALUES ('1180', '95', '2', 'Nachrichten');
INSERT INTO lang_translation VALUES ('1181', '95', '4', 'Balita');
INSERT INTO lang_translation VALUES ('1182', '95', '5', 'Zprávy');
INSERT INTO lang_translation VALUES ('1183', '95', '6', 'Mensaje');
INSERT INTO lang_translation VALUES ('1184', '95', '7', '-');
INSERT INTO lang_translation VALUES ('1185', '97', '2', 'Interna Nachrichten');
INSERT INTO lang_translation VALUES ('1186', '97', '4', 'Panloob na mga ulat');
INSERT INTO lang_translation VALUES ('1187', '97', '5', 'Interní zprávy');
INSERT INTO lang_translation VALUES ('1188', '97', '6', 'Informes internos');
INSERT INTO lang_translation VALUES ('1189', '97', '7', '-');
INSERT INTO lang_translation VALUES ('1190', '110', '2', 'Profil bearbeiten');
INSERT INTO lang_translation VALUES ('1191', '110', '4', 'L-edit ang Profile');
INSERT INTO lang_translation VALUES ('1192', '110', '5', 'Upravit profil');
INSERT INTO lang_translation VALUES ('1193', '110', '6', 'Editar Perfil');
INSERT INTO lang_translation VALUES ('1194', '110', '7', '-');
INSERT INTO lang_translation VALUES ('1195', '112', '2', 'Gruppe ändern');
INSERT INTO lang_translation VALUES ('1196', '112', '4', 'Baguhin grupo');
INSERT INTO lang_translation VALUES ('1197', '112', '5', 'Změnit skupinu');
INSERT INTO lang_translation VALUES ('1198', '112', '6', 'Cambio de grupo');
INSERT INTO lang_translation VALUES ('1199', '112', '7', '-');
INSERT INTO lang_translation VALUES ('1200', '113', '2', 'Boxmanagement');
INSERT INTO lang_translation VALUES ('1201', '113', '4', 'Nabigasyon Box');
INSERT INTO lang_translation VALUES ('1202', '113', '5', 'Navigační box');
INSERT INTO lang_translation VALUES ('1203', '113', '6', 'Caja de navegacion');
INSERT INTO lang_translation VALUES ('1204', '113', '7', '-');
INSERT INTO lang_translation VALUES ('1205', '129', '2', 'Statistik');
INSERT INTO lang_translation VALUES ('1206', '129', '4', 'Statistics');
INSERT INTO lang_translation VALUES ('1207', '129', '5', 'Statistiky');
INSERT INTO lang_translation VALUES ('1208', '129', '6', 'Estadisticas');
INSERT INTO lang_translation VALUES ('1209', '129', '7', '-');
INSERT INTO lang_translation VALUES ('1210', '130', '2', 'Speichern');
INSERT INTO lang_translation VALUES ('1211', '130', '4', 'Iligtas');
INSERT INTO lang_translation VALUES ('1212', '130', '5', 'Uložit');
INSERT INTO lang_translation VALUES ('1213', '130', '6', 'Guardar');
INSERT INTO lang_translation VALUES ('1214', '130', '7', '-');
INSERT INTO lang_translation VALUES ('1215', '132', '2', 'Anwendersliste');
INSERT INTO lang_translation VALUES ('1216', '132', '4', 'Gumagamit');
INSERT INTO lang_translation VALUES ('1217', '132', '5', 'Uživatelé');
INSERT INTO lang_translation VALUES ('1218', '132', '6', 'Usarios');
INSERT INTO lang_translation VALUES ('1219', '132', '7', '-');
INSERT INTO lang_translation VALUES ('1220', '131', '2', 'Abmeldung');
INSERT INTO lang_translation VALUES ('1221', '131', '4', 'Logout');
INSERT INTO lang_translation VALUES ('1222', '131', '5', 'Odhlášení');
INSERT INTO lang_translation VALUES ('1223', '131', '6', 'Cierre desesion');
INSERT INTO lang_translation VALUES ('1224', '131', '7', '-');
INSERT INTO lang_translation VALUES ('1225', '133', '2', 'Interna Management');
INSERT INTO lang_translation VALUES ('1226', '133', '4', 'Internal management');
INSERT INTO lang_translation VALUES ('1227', '133', '5', 'Interní management');
INSERT INTO lang_translation VALUES ('1228', '133', '6', 'Gestion Interna');
INSERT INTO lang_translation VALUES ('1229', '133', '7', '-');
INSERT INTO lang_translation VALUES ('1230', '134', '2', 'Content Management');
INSERT INTO lang_translation VALUES ('1231', '134', '4', 'Nilalaman ng Mga Setting');
INSERT INTO lang_translation VALUES ('1232', '134', '5', 'Nastavení obsahu');
INSERT INTO lang_translation VALUES ('1233', '134', '6', 'Configuracion del contenido');
INSERT INTO lang_translation VALUES ('1234', '134', '7', '-');
INSERT INTO lang_translation VALUES ('1235', '135', '2', 'System Management');
INSERT INTO lang_translation VALUES ('1236', '135', '4', 'Sistema ng mga setting');
INSERT INTO lang_translation VALUES ('1237', '135', '5', 'Systémové nastavení');
INSERT INTO lang_translation VALUES ('1238', '135', '6', 'Configuracion del sistema');
INSERT INTO lang_translation VALUES ('1239', '135', '7', '-');
INSERT INTO lang_translation VALUES ('1240', '136', '2', 'Schauspieler');
INSERT INTO lang_translation VALUES ('1241', '136', '4', 'Actors');
INSERT INTO lang_translation VALUES ('1242', '136', '5', 'Herci');
INSERT INTO lang_translation VALUES ('1243', '136', '6', 'Actores');
INSERT INTO lang_translation VALUES ('1244', '136', '7', '-');
INSERT INTO lang_translation VALUES ('1245', '137', '2', 'Kategorien');
INSERT INTO lang_translation VALUES ('1246', '137', '4', 'Kategorya');
INSERT INTO lang_translation VALUES ('1247', '137', '5', 'Kategorie');
INSERT INTO lang_translation VALUES ('1248', '137', '6', 'Categoria');
INSERT INTO lang_translation VALUES ('1249', '137', '7', '-');
INSERT INTO lang_translation VALUES ('1250', '138', '2', 'Bildmanagement');
INSERT INTO lang_translation VALUES ('1251', '138', '4', 'Setting ng mga file');
INSERT INTO lang_translation VALUES ('1252', '138', '5', 'Nastavení obrázků');
INSERT INTO lang_translation VALUES ('1253', '138', '6', 'Configuracion de los archivos');
INSERT INTO lang_translation VALUES ('1254', '138', '7', '-');
INSERT INTO lang_translation VALUES ('1255', '139', '2', 'Wilkommen Sie an der Verwaltungsseite');
INSERT INTO lang_translation VALUES ('1256', '139', '4', 'Maligayang pagdating sa administrator sa kapaligiran');
INSERT INTO lang_translation VALUES ('1257', '139', '5', 'Vítejte v administrátorském prostředí');
INSERT INTO lang_translation VALUES ('1258', '139', '6', 'Bienvenido al administradosr en el medio ambiente');
INSERT INTO lang_translation VALUES ('1259', '139', '7', '-');
INSERT INTO lang_translation VALUES ('1260', '140', '2', 'Hauptmenü');
INSERT INTO lang_translation VALUES ('1261', '140', '4', 'Main Menu');
INSERT INTO lang_translation VALUES ('1262', '140', '5', 'Hlavní menu');
INSERT INTO lang_translation VALUES ('1263', '140', '6', 'Menu Principal');
INSERT INTO lang_translation VALUES ('1264', '140', '7', '-');
INSERT INTO lang_translation VALUES ('1265', '141', '2', 'Nachrichten');
INSERT INTO lang_translation VALUES ('1266', '141', '4', 'Balita');
INSERT INTO lang_translation VALUES ('1267', '141', '5', 'Novinky');
INSERT INTO lang_translation VALUES ('1268', '141', '6', 'Noticias');
INSERT INTO lang_translation VALUES ('1269', '141', '7', '-');
INSERT INTO lang_translation VALUES ('1270', '142', '2', 'System Einstellung');
INSERT INTO lang_translation VALUES ('1271', '142', '4', 'Sistema ng mga setting');
INSERT INTO lang_translation VALUES ('1272', '142', '5', 'Systémové nastavení');
INSERT INTO lang_translation VALUES ('1273', '142', '6', 'Configuracion del sistema');
INSERT INTO lang_translation VALUES ('1274', '142', '7', '-');
INSERT INTO lang_translation VALUES ('1275', '143', '2', 'FAQ / Helfen Sie Text');
INSERT INTO lang_translation VALUES ('1276', '143', '4', 'FAQ / Tulong');
INSERT INTO lang_translation VALUES ('1277', '143', '5', 'FAQ / Nápověda');
INSERT INTO lang_translation VALUES ('1278', '143', '6', 'FAQ / Ayuda');
INSERT INTO lang_translation VALUES ('1279', '143', '7', '-');
INSERT INTO lang_translation VALUES ('1280', '144', '2', 'Übersetzungsystem');
INSERT INTO lang_translation VALUES ('1281', '144', '4', 'Pagsasalin');
INSERT INTO lang_translation VALUES ('1282', '144', '5', 'Překlad');
INSERT INTO lang_translation VALUES ('1283', '144', '6', 'Traduccion');
INSERT INTO lang_translation VALUES ('1284', '144', '7', '-');
INSERT INTO lang_translation VALUES ('1285', '145', '2', 'Gegenstand');
INSERT INTO lang_translation VALUES ('1286', '145', '4', 'Paksa');
INSERT INTO lang_translation VALUES ('1287', '145', '5', 'Předmět');
INSERT INTO lang_translation VALUES ('1288', '145', '6', 'Sujetos');
INSERT INTO lang_translation VALUES ('1289', '145', '7', '-');
INSERT INTO lang_translation VALUES ('1290', '146', '2', 'Datum');
INSERT INTO lang_translation VALUES ('1291', '146', '4', 'Petsa');
INSERT INTO lang_translation VALUES ('1292', '146', '5', 'Datum');
INSERT INTO lang_translation VALUES ('1293', '146', '6', 'Datos');
INSERT INTO lang_translation VALUES ('1294', '146', '7', '-');
INSERT INTO lang_translation VALUES ('1295', '147', '2', 'Neu');
INSERT INTO lang_translation VALUES ('1296', '147', '4', 'Bagong');
INSERT INTO lang_translation VALUES ('1297', '147', '5', 'Nový');
INSERT INTO lang_translation VALUES ('1298', '147', '6', 'Nuevo');
INSERT INTO lang_translation VALUES ('1299', '147', '7', '-');
INSERT INTO lang_translation VALUES ('1300', '148', '2', 'Nachrichten lesen');
INSERT INTO lang_translation VALUES ('1301', '148', '4', 'Basahin ang ulat');
INSERT INTO lang_translation VALUES ('1302', '148', '5', 'Číst zprávy');
INSERT INTO lang_translation VALUES ('1303', '148', '6', 'Leer el informe');
INSERT INTO lang_translation VALUES ('1304', '148', '7', '-');
INSERT INTO lang_translation VALUES ('1305', '149', '2', 'Nachrichten schicken');
INSERT INTO lang_translation VALUES ('1306', '149', '4', 'Magpadala ng mensahe');
INSERT INTO lang_translation VALUES ('1307', '149', '5', 'Poslat zprávy');
INSERT INTO lang_translation VALUES ('1308', '149', '6', 'Enviar mensaje');
INSERT INTO lang_translation VALUES ('1309', '149', '7', '-');
INSERT INTO lang_translation VALUES ('1310', '150', '2', 'Alle Nachrichten');
INSERT INTO lang_translation VALUES ('1311', '150', '4', 'Lahat ng mga mensahe');
INSERT INTO lang_translation VALUES ('1312', '150', '5', 'Všechny zprávy');
INSERT INTO lang_translation VALUES ('1313', '150', '6', 'Todos los mensajes');
INSERT INTO lang_translation VALUES ('1314', '150', '7', '-');
INSERT INTO lang_translation VALUES ('1315', '151', '2', 'Neues Nachricht schicken');
INSERT INTO lang_translation VALUES ('1316', '151', '4', 'Bagong Mensahe');
INSERT INTO lang_translation VALUES ('1317', '151', '5', 'Nová zpráva');
INSERT INTO lang_translation VALUES ('1318', '151', '6', 'Nuevo mensaje');
INSERT INTO lang_translation VALUES ('1319', '151', '7', '-');
INSERT INTO lang_translation VALUES ('1320', '152', '2', 'Typ');
INSERT INTO lang_translation VALUES ('1321', '152', '4', 'Uri');
INSERT INTO lang_translation VALUES ('1322', '152', '5', 'Typ');
INSERT INTO lang_translation VALUES ('1323', '152', '6', 'Tipo');
INSERT INTO lang_translation VALUES ('1324', '152', '7', '-');
INSERT INTO lang_translation VALUES ('1325', '153', '2', 'Absender');
INSERT INTO lang_translation VALUES ('1326', '153', '4', 'Sender');
INSERT INTO lang_translation VALUES ('1327', '153', '5', 'Odesílatel');
INSERT INTO lang_translation VALUES ('1328', '153', '6', 'Remitente');
INSERT INTO lang_translation VALUES ('1329', '153', '7', '-');
INSERT INTO lang_translation VALUES ('1330', '157', '2', 'Bild');
INSERT INTO lang_translation VALUES ('1331', '157', '4', 'Imahen');
INSERT INTO lang_translation VALUES ('1332', '157', '5', 'Obrázek');
INSERT INTO lang_translation VALUES ('1333', '157', '6', 'Imagen');
INSERT INTO lang_translation VALUES ('1334', '157', '7', '-');
INSERT INTO lang_translation VALUES ('1335', '159', '2', 'Telefonliste verstecken?');
INSERT INTO lang_translation VALUES ('1336', '159', '4', 'Itago ang telepono ng libro?');
INSERT INTO lang_translation VALUES ('1337', '159', '5', 'Schovat telefonní seznam?');
INSERT INTO lang_translation VALUES ('1338', '159', '6', 'Ocultar datos telefonicos?');
INSERT INTO lang_translation VALUES ('1339', '159', '7', '-');
INSERT INTO lang_translation VALUES ('1340', '160', '2', 'Standard Sprache');
INSERT INTO lang_translation VALUES ('1341', '160', '4', 'Default na wika');
INSERT INTO lang_translation VALUES ('1342', '160', '5', 'Výchozí jazyk');
INSERT INTO lang_translation VALUES ('1343', '160', '6', 'Una lengua materna');
INSERT INTO lang_translation VALUES ('1344', '160', '7', '-');
INSERT INTO lang_translation VALUES ('1345', '161', '2', 'Benutzername');
INSERT INTO lang_translation VALUES ('1346', '161', '4', 'Username');
INSERT INTO lang_translation VALUES ('1347', '161', '5', 'Uživatelské jméno');
INSERT INTO lang_translation VALUES ('1348', '161', '6', 'Nombre de usuario');
INSERT INTO lang_translation VALUES ('1349', '161', '7', '-');
INSERT INTO lang_translation VALUES ('1350', '162', '2', 'Kennwort');
INSERT INTO lang_translation VALUES ('1351', '162', '4', 'Kontrasenyas');
INSERT INTO lang_translation VALUES ('1352', '162', '5', 'Heslo');
INSERT INTO lang_translation VALUES ('1353', '162', '6', 'Contraseña');
INSERT INTO lang_translation VALUES ('1354', '162', '7', '-');
INSERT INTO lang_translation VALUES ('1355', '163', '2', 'Gruppe');
INSERT INTO lang_translation VALUES ('1356', '163', '4', 'Grupo');
INSERT INTO lang_translation VALUES ('1357', '163', '5', 'Skupina');
INSERT INTO lang_translation VALUES ('1358', '163', '6', 'Grupo');
INSERT INTO lang_translation VALUES ('1359', '163', '7', '-');
INSERT INTO lang_translation VALUES ('1360', '164', '2', 'Privat name *');
INSERT INTO lang_translation VALUES ('1361', '164', '4', 'Personal na pangalan *');
INSERT INTO lang_translation VALUES ('1362', '164', '5', 'Osobní jméno *');
INSERT INTO lang_translation VALUES ('1363', '164', '6', 'Nombre personal');
INSERT INTO lang_translation VALUES ('1364', '164', '7', '-');
INSERT INTO lang_translation VALUES ('1365', '165', '2', 'Nachname');
INSERT INTO lang_translation VALUES ('1366', '165', '4', 'Apelyido');
INSERT INTO lang_translation VALUES ('1367', '165', '5', 'Příjmení *');
INSERT INTO lang_translation VALUES ('1368', '165', '6', 'Apellido');
INSERT INTO lang_translation VALUES ('1369', '165', '7', '-');
INSERT INTO lang_translation VALUES ('1370', '166', '2', 'Email *');
INSERT INTO lang_translation VALUES ('1371', '166', '4', 'Email *');
INSERT INTO lang_translation VALUES ('1372', '166', '5', 'Email *');
INSERT INTO lang_translation VALUES ('1373', '166', '6', 'Correo electronico *');
INSERT INTO lang_translation VALUES ('1374', '166', '7', '-');
INSERT INTO lang_translation VALUES ('1375', '167', '2', 'Email Wiederholung *');
INSERT INTO lang_translation VALUES ('1376', '167', '4', 'Patunayan ang Email');
INSERT INTO lang_translation VALUES ('1377', '167', '5', 'Ověř email *');
INSERT INTO lang_translation VALUES ('1378', '167', '6', 'Compruebe correo electronico');
INSERT INTO lang_translation VALUES ('1379', '167', '7', '-');
INSERT INTO lang_translation VALUES ('1380', '168', '2', 'Festnetz *');
INSERT INTO lang_translation VALUES ('1381', '168', '4', 'Telepono *');
INSERT INTO lang_translation VALUES ('1382', '168', '5', 'Telefon *');
INSERT INTO lang_translation VALUES ('1383', '168', '6', 'Telefono *');
INSERT INTO lang_translation VALUES ('1384', '168', '7', '-');
INSERT INTO lang_translation VALUES ('1385', '169', '2', 'Mobil ');
INSERT INTO lang_translation VALUES ('1386', '169', '4', 'Cell phone ');
INSERT INTO lang_translation VALUES ('1387', '169', '5', 'Mobil ');
INSERT INTO lang_translation VALUES ('1388', '169', '6', 'Telefono celular ');
INSERT INTO lang_translation VALUES ('1389', '169', '7', '-');
INSERT INTO lang_translation VALUES ('1390', '170', '2', 'Geschlecht *');
INSERT INTO lang_translation VALUES ('1391', '170', '4', 'Kasarian *');
INSERT INTO lang_translation VALUES ('1392', '170', '5', 'Pohlaví *');
INSERT INTO lang_translation VALUES ('1393', '170', '6', 'Sexo *');
INSERT INTO lang_translation VALUES ('1394', '170', '7', '-');
INSERT INTO lang_translation VALUES ('1395', '171', '2', 'Geburtsdatum *');
INSERT INTO lang_translation VALUES ('1396', '171', '4', 'Petsa ng kapanganakan *');
INSERT INTO lang_translation VALUES ('1397', '171', '5', 'Datum narození *');
INSERT INTO lang_translation VALUES ('1398', '171', '6', 'Fecha de nacimiento *');
INSERT INTO lang_translation VALUES ('1399', '171', '7', '-');
INSERT INTO lang_translation VALUES ('1400', '172', '2', 'Land *');
INSERT INTO lang_translation VALUES ('1401', '172', '4', 'Bansa *');
INSERT INTO lang_translation VALUES ('1402', '172', '5', 'Země *');
INSERT INTO lang_translation VALUES ('1403', '172', '6', 'Pais');
INSERT INTO lang_translation VALUES ('1404', '172', '7', '-');
INSERT INTO lang_translation VALUES ('1405', '173', '2', 'Startseite');
INSERT INTO lang_translation VALUES ('1406', '173', '4', 'Tahanan');
INSERT INTO lang_translation VALUES ('1407', '173', '5', 'Domovská stránka');
INSERT INTO lang_translation VALUES ('1408', '173', '6', 'Inicio');
INSERT INTO lang_translation VALUES ('1409', '173', '7', '-');
INSERT INTO lang_translation VALUES ('1410', '175', '2', 'ICQ');
INSERT INTO lang_translation VALUES ('1411', '175', '4', 'ICQ');
INSERT INTO lang_translation VALUES ('1412', '175', '5', 'ICQ');
INSERT INTO lang_translation VALUES ('1413', '175', '6', 'ICQ');
INSERT INTO lang_translation VALUES ('1414', '175', '7', '-');
INSERT INTO lang_translation VALUES ('1415', '176', '2', 'MSN');
INSERT INTO lang_translation VALUES ('1416', '176', '4', 'MSN');
INSERT INTO lang_translation VALUES ('1417', '176', '5', 'MSN');
INSERT INTO lang_translation VALUES ('1418', '176', '6', 'MSN');
INSERT INTO lang_translation VALUES ('1419', '176', '7', '-');
INSERT INTO lang_translation VALUES ('1444', '189', '1', 'Name');
INSERT INTO lang_translation VALUES ('1445', '189', '2', 'Name');
INSERT INTO lang_translation VALUES ('1446', '189', '4', 'Pangalan');
INSERT INTO lang_translation VALUES ('1447', '189', '5', 'Jméno');
INSERT INTO lang_translation VALUES ('1448', '189', '6', 'Nombre');
INSERT INTO lang_translation VALUES ('1449', '189', '7', '-');
INSERT INTO lang_translation VALUES ('1450', '190', '1', 'Description');
INSERT INTO lang_translation VALUES ('1451', '191', '1', 'Add');
INSERT INTO lang_translation VALUES ('1452', '192', '1', 'Clear');
INSERT INTO lang_translation VALUES ('1453', '193', '1', 'Create New Actor');
INSERT INTO lang_translation VALUES ('1454', '194', '1', 'Create New Photo Gallery');
INSERT INTO lang_translation VALUES ('1455', '195', '1', 'Warning!');
INSERT INTO lang_translation VALUES ('1456', '196', '1', 'Error!');
INSERT INTO lang_translation VALUES ('1457', '197', '1', 'Delete');
INSERT INTO lang_translation VALUES ('1458', '198', '1', 'Status');
INSERT INTO lang_translation VALUES ('1459', '199', '1', 'Failure');
INSERT INTO lang_translation VALUES ('1460', '200', '1', 'Please wait...');
INSERT INTO lang_translation VALUES ('1461', '201', '1', 'Uploading file...');
INSERT INTO lang_translation VALUES ('1462', '202', '1', 'Saving data...');
INSERT INTO lang_translation VALUES ('1463', '203', '1', 'Server is unreachle');
INSERT INTO lang_translation VALUES ('1464', '204', '1', 'Are you sure you want to delete');
INSERT INTO lang_translation VALUES ('1465', '205', '1', 'Are you sure you want to delete the actor?');
INSERT INTO lang_translation VALUES ('1466', '206', '1', 'Edit Image');
INSERT INTO lang_translation VALUES ('1467', '207', '1', 'Edit Actor');
INSERT INTO lang_translation VALUES ('1468', '208', '1', 'Edit Photo Gallery');
INSERT INTO lang_translation VALUES ('1469', '209', '1', 'Information');
INSERT INTO lang_translation VALUES ('1470', '210', '1', 'Add Zip file with Images');
INSERT INTO lang_translation VALUES ('1471', '211', '1', 'List of Images');
INSERT INTO lang_translation VALUES ('1472', '212', '1', 'List of Actors');
INSERT INTO lang_translation VALUES ('1473', '213', '1', 'List of Photo Galleries');
INSERT INTO lang_translation VALUES ('1474', '214', '1', 'Time Management From');
INSERT INTO lang_translation VALUES ('1475', '215', '1', 'Time Management To');
INSERT INTO lang_translation VALUES ('1476', '216', '1', 'Title');
INSERT INTO lang_translation VALUES ('1477', '217', '1', 'Order');
INSERT INTO lang_translation VALUES ('1478', '218', '1', 'FSK');
INSERT INTO lang_translation VALUES ('1479', '219', '1', 'URL');
INSERT INTO lang_translation VALUES ('1480', '220', '1', 'Keywords (SEO)');
INSERT INTO lang_translation VALUES ('1481', '221', '1', 'Free');
INSERT INTO lang_translation VALUES ('1482', '222', '1', 'From user');
INSERT INTO lang_translation VALUES ('1483', '223', '1', 'Status');
INSERT INTO lang_translation VALUES ('1484', '224', '1', 'Actors');
INSERT INTO lang_translation VALUES ('1485', '225', '1', 'Category');
INSERT INTO lang_translation VALUES ('1486', '226', '1', 'Select ZIP file');
INSERT INTO lang_translation VALUES ('1487', '227', '1', 'Click to delete');
INSERT INTO lang_translation VALUES ('1488', '228', '1', 'Edit image');
INSERT INTO lang_translation VALUES ('1489', '229', '1', 'Delete image');
INSERT INTO lang_translation VALUES ('1490', '230', '1', 'Delete');
INSERT INTO lang_translation VALUES ('1491', '231', '1', 'Edit');
INSERT INTO lang_translation VALUES ('1492', '232', '1', 'Create new actor');
INSERT INTO lang_translation VALUES ('1493', '233', '1', 'Create new photo gallery');
INSERT INTO lang_translation VALUES ('1494', '234', '1', 'Name');
INSERT INTO lang_translation VALUES ('1495', '235', '1', 'Title');
INSERT INTO lang_translation VALUES ('1496', '236', '1', 'Description');
INSERT INTO lang_translation VALUES ('1497', '237', '1', 'TM From');
INSERT INTO lang_translation VALUES ('1498', '238', '1', 'TM To');
INSERT INTO lang_translation VALUES ('1499', '239', '1', 'Username');
INSERT INTO lang_translation VALUES ('1500', '240', '1', 'Status');
INSERT INTO lang_translation VALUES ('1501', '241', '1', 'SEO');
INSERT INTO lang_translation VALUES ('1502', '242', '1', 'URL');
INSERT INTO lang_translation VALUES ('1503', '243', '1', 'Free');
INSERT INTO lang_translation VALUES ('1504', '244', '1', 'Date Created');
INSERT INTO lang_translation VALUES ('1505', '245', '1', 'UserID Created');
INSERT INTO lang_translation VALUES ('1506', '190', '2', 'Beschreibung');
INSERT INTO lang_translation VALUES ('1507', '191', '2', 'Zugeben');
INSERT INTO lang_translation VALUES ('1508', '192', '2', 'Leeren');
INSERT INTO lang_translation VALUES ('1509', '193', '2', 'Neuen Schauspieler erstellen');
INSERT INTO lang_translation VALUES ('1510', '194', '2', 'Neues Photo erstellen');
INSERT INTO lang_translation VALUES ('1511', '195', '2', 'Warnung!');
INSERT INTO lang_translation VALUES ('1512', '196', '2', 'Error!');
INSERT INTO lang_translation VALUES ('1513', '197', '2', 'Entfernen');
INSERT INTO lang_translation VALUES ('1514', '198', '2', 'Status');
INSERT INTO lang_translation VALUES ('1515', '199', '2', 'Scheitern');
INSERT INTO lang_translation VALUES ('1516', '200', '2', 'Warten Sie bitte...');
INSERT INTO lang_translation VALUES ('1517', '201', '2', 'Dateneinladung');
INSERT INTO lang_translation VALUES ('1518', '202', '2', 'Datenspeicherung...');
INSERT INTO lang_translation VALUES ('1519', '203', '2', 'Server ist unersteigbar');
INSERT INTO lang_translation VALUES ('1520', '204', '2', 'Wollen Sie das wirklich entfernen?');
INSERT INTO lang_translation VALUES ('1521', '205', '2', 'Wollen Sie den Schauspieler wirklich entfernen? ');
INSERT INTO lang_translation VALUES ('1522', '206', '2', 'Bild bearbeiten');
INSERT INTO lang_translation VALUES ('1523', '207', '2', 'Schauspieler bearbeiten');
INSERT INTO lang_translation VALUES ('1524', '208', '2', 'Fotogalerie bearbeiten');
INSERT INTO lang_translation VALUES ('1525', '209', '2', 'Informationen');
INSERT INTO lang_translation VALUES ('1526', '210', '2', 'ZIP-file mit einem Bild zugeben');
INSERT INTO lang_translation VALUES ('1527', '211', '2', 'Bildliste');
INSERT INTO lang_translation VALUES ('1528', '212', '2', 'Schauspielerliste');
INSERT INTO lang_translation VALUES ('1529', '213', '2', 'Fotogalerielist');
INSERT INTO lang_translation VALUES ('1530', '214', '2', 'Zeit Management aus');
INSERT INTO lang_translation VALUES ('1531', '215', '2', 'Zeit Management zu ');
INSERT INTO lang_translation VALUES ('1532', '216', '2', 'Titel');
INSERT INTO lang_translation VALUES ('1533', '217', '2', 'Ordnung');
INSERT INTO lang_translation VALUES ('1534', '218', '2', 'FSK');
INSERT INTO lang_translation VALUES ('1535', '219', '2', 'URL');
INSERT INTO lang_translation VALUES ('1536', '220', '2', 'Stichwörter (SEO)');
INSERT INTO lang_translation VALUES ('1537', '221', '2', 'Frei');
INSERT INTO lang_translation VALUES ('1538', '222', '2', 'Fon Benutzer');
INSERT INTO lang_translation VALUES ('1539', '223', '2', 'Status');
INSERT INTO lang_translation VALUES ('1540', '224', '2', 'Schauspieler');
INSERT INTO lang_translation VALUES ('1541', '225', '2', 'Kategorie');
INSERT INTO lang_translation VALUES ('1542', '226', '2', 'ZIP-Datei auswählen');
INSERT INTO lang_translation VALUES ('1543', '227', '2', 'Klicken und entfernen');
INSERT INTO lang_translation VALUES ('1544', '228', '2', 'Bild bearbeiten');
INSERT INTO lang_translation VALUES ('1545', '229', '2', 'Bild entfernen');
INSERT INTO lang_translation VALUES ('1546', '230', '2', 'Entfernen');
INSERT INTO lang_translation VALUES ('1547', '231', '2', 'Bearbeiten');
INSERT INTO lang_translation VALUES ('1548', '232', '2', 'Neuen Schauspieler erstellen');
INSERT INTO lang_translation VALUES ('1549', '233', '2', 'Neues Foto erstellen');
INSERT INTO lang_translation VALUES ('1550', '234', '2', 'Name');
INSERT INTO lang_translation VALUES ('1551', '235', '2', 'Titel');
INSERT INTO lang_translation VALUES ('1552', '236', '2', 'Beschreibung');
INSERT INTO lang_translation VALUES ('1553', '237', '2', 'TM aus');
INSERT INTO lang_translation VALUES ('1554', '238', '2', 'TM zu');
INSERT INTO lang_translation VALUES ('1555', '239', '2', 'Benutzername');
INSERT INTO lang_translation VALUES ('1556', '240', '2', 'Status');
INSERT INTO lang_translation VALUES ('1557', '241', '2', 'SEO');
INSERT INTO lang_translation VALUES ('1558', '242', '2', 'URL');
INSERT INTO lang_translation VALUES ('1559', '243', '2', 'Frei');
INSERT INTO lang_translation VALUES ('1560', '244', '2', 'Datum erstellt');
INSERT INTO lang_translation VALUES ('1561', '245', '2', 'Benutzer ID erstellt');
INSERT INTO lang_translation VALUES ('1562', '190', '4', 'Paglalarawan');
INSERT INTO lang_translation VALUES ('1563', '191', '4', 'Magdagdag');
INSERT INTO lang_translation VALUES ('1564', '192', '4', 'Malinis');
INSERT INTO lang_translation VALUES ('1565', '193', '4', 'Lumikha ng isang bagong actor');
INSERT INTO lang_translation VALUES ('1566', '194', '4', 'Lumikha ng isang bagong larawan');
INSERT INTO lang_translation VALUES ('1567', '195', '4', 'Babala!');
INSERT INTO lang_translation VALUES ('1568', '196', '4', 'Mali!');
INSERT INTO lang_translation VALUES ('1569', '197', '4', 'Bakat');
INSERT INTO lang_translation VALUES ('1570', '198', '4', 'Katayuan');
INSERT INTO lang_translation VALUES ('1571', '199', '4', 'Mali');
INSERT INTO lang_translation VALUES ('1572', '200', '4', 'Mangyaring maghintay...');
INSERT INTO lang_translation VALUES ('1573', '201', '4', 'Upload file');
INSERT INTO lang_translation VALUES ('1574', '202', '4', 'Imbakan');
INSERT INTO lang_translation VALUES ('1575', '203', '4', 'Server ay hindi magagamit');
INSERT INTO lang_translation VALUES ('1576', '204', '4', 'Sigurado ka bang gusto mong tanggalin ito?');
INSERT INTO lang_translation VALUES ('1577', '205', '4', 'Sigurado ka bang gusto mong tanggalin ang isang actor? ');
INSERT INTO lang_translation VALUES ('1578', '206', '4', 'I-edit ang imahe');
INSERT INTO lang_translation VALUES ('1579', '207', '4', 'Edit actor');
INSERT INTO lang_translation VALUES ('1580', '208', '4', 'Edit gallery');
INSERT INTO lang_translation VALUES ('1581', '209', '4', 'Impormasyon');
INSERT INTO lang_translation VALUES ('1582', '210', '4', 'Magdagdag ng isang ZIP file sa imahe');
INSERT INTO lang_translation VALUES ('1583', '211', '4', 'Mga image');
INSERT INTO lang_translation VALUES ('1584', '212', '4', 'Ihagis');
INSERT INTO lang_translation VALUES ('1585', '213', '4', 'Photo Gallery');
INSERT INTO lang_translation VALUES ('1586', '214', '4', 'Setup ng oras mula sa');
INSERT INTO lang_translation VALUES ('1587', '215', '4', 'Setup ng panahon upang');
INSERT INTO lang_translation VALUES ('1588', '216', '4', 'Pamagat');
INSERT INTO lang_translation VALUES ('1589', '217', '4', 'Order');
INSERT INTO lang_translation VALUES ('1590', '218', '4', 'FSK');
INSERT INTO lang_translation VALUES ('1591', '219', '4', 'URL');
INSERT INTO lang_translation VALUES ('1592', '220', '4', 'Keywords (SEO)');
INSERT INTO lang_translation VALUES ('1593', '221', '4', 'Libre');
INSERT INTO lang_translation VALUES ('1594', '222', '4', 'Sa pamamagitan ng');
INSERT INTO lang_translation VALUES ('1595', '223', '4', 'Katayuan');
INSERT INTO lang_translation VALUES ('1596', '224', '4', 'Actors');
INSERT INTO lang_translation VALUES ('1597', '225', '4', 'Kategorya');
INSERT INTO lang_translation VALUES ('1598', '226', '4', 'Piliin ang ZIP file');
INSERT INTO lang_translation VALUES ('1599', '227', '4', 'I-click ang at tanggalin ang mga');
INSERT INTO lang_translation VALUES ('1600', '228', '4', 'I-edit ang imahe');
INSERT INTO lang_translation VALUES ('1601', '229', '4', 'Tanggalin ang imahe');
INSERT INTO lang_translation VALUES ('1602', '230', '4', 'Alisin');
INSERT INTO lang_translation VALUES ('1603', '231', '4', 'I-edit');
INSERT INTO lang_translation VALUES ('1604', '232', '4', 'Lumikha ng isang bagong artista');
INSERT INTO lang_translation VALUES ('1605', '233', '4', 'Lumikha ng isang bagong larawan');
INSERT INTO lang_translation VALUES ('1606', '234', '4', 'Pangalan');
INSERT INTO lang_translation VALUES ('1607', '235', '4', 'Pamagat');
INSERT INTO lang_translation VALUES ('1608', '236', '4', 'Paglalarawan');
INSERT INTO lang_translation VALUES ('1609', '237', '4', 'TM mula sa');
INSERT INTO lang_translation VALUES ('1610', '238', '4', 'TM para sa');
INSERT INTO lang_translation VALUES ('1611', '239', '4', 'Username');
INSERT INTO lang_translation VALUES ('1612', '240', '4', 'Katayuan');
INSERT INTO lang_translation VALUES ('1613', '241', '4', 'SEO');
INSERT INTO lang_translation VALUES ('1614', '242', '4', 'URL');
INSERT INTO lang_translation VALUES ('1615', '243', '4', 'Libre');
INSERT INTO lang_translation VALUES ('1616', '244', '4', 'Petsa ng Paglikha');
INSERT INTO lang_translation VALUES ('1617', '245', '4', 'User ID created');
INSERT INTO lang_translation VALUES ('1618', '190', '5', 'Popis');
INSERT INTO lang_translation VALUES ('1619', '191', '5', 'Přidat');
INSERT INTO lang_translation VALUES ('1620', '192', '5', 'Vyčistit');
INSERT INTO lang_translation VALUES ('1621', '193', '5', 'Vytvoř nového herce');
INSERT INTO lang_translation VALUES ('1622', '194', '5', 'Vytvoř nové foto');
INSERT INTO lang_translation VALUES ('1623', '195', '5', 'Varování!');
INSERT INTO lang_translation VALUES ('1624', '196', '5', 'Chyba!');
INSERT INTO lang_translation VALUES ('1625', '197', '5', 'Vymaž');
INSERT INTO lang_translation VALUES ('1626', '198', '5', 'Status');
INSERT INTO lang_translation VALUES ('1627', '199', '5', 'Chyba');
INSERT INTO lang_translation VALUES ('1628', '200', '5', 'Čekejte prosím...');
INSERT INTO lang_translation VALUES ('1629', '201', '5', 'Nahrávání souboru');
INSERT INTO lang_translation VALUES ('1630', '202', '5', 'Ukládání dat');
INSERT INTO lang_translation VALUES ('1631', '203', '5', 'Server je nedostupný');
INSERT INTO lang_translation VALUES ('1632', '204', '5', 'Opravdu chcete položku vymazat?');
INSERT INTO lang_translation VALUES ('1633', '205', '5', 'Orpavdu chcete herce vymazat? ');
INSERT INTO lang_translation VALUES ('1634', '206', '5', 'Uprav obrázek');
INSERT INTO lang_translation VALUES ('1635', '207', '5', 'Uprav herce');
INSERT INTO lang_translation VALUES ('1636', '208', '5', 'Uprav fotogalerii');
INSERT INTO lang_translation VALUES ('1637', '209', '5', 'Informace');
INSERT INTO lang_translation VALUES ('1638', '210', '5', 'Přidat souboru ZIP s obrázkem ');
INSERT INTO lang_translation VALUES ('1639', '211', '5', 'Obrázky');
INSERT INTO lang_translation VALUES ('1640', '212', '5', 'Seznam herců');
INSERT INTO lang_translation VALUES ('1641', '213', '5', 'Fotogalerie');
INSERT INTO lang_translation VALUES ('1642', '214', '5', 'Nastavení času od');
INSERT INTO lang_translation VALUES ('1643', '215', '5', 'Nastavení času do');
INSERT INTO lang_translation VALUES ('1644', '216', '5', 'Nadpis');
INSERT INTO lang_translation VALUES ('1645', '217', '5', 'Pořadí');
INSERT INTO lang_translation VALUES ('1646', '218', '5', 'FSK');
INSERT INTO lang_translation VALUES ('1647', '219', '5', 'URL');
INSERT INTO lang_translation VALUES ('1648', '220', '5', 'Klíčová slova (SEO)');
INSERT INTO lang_translation VALUES ('1649', '221', '5', 'Volný');
INSERT INTO lang_translation VALUES ('1650', '222', '5', 'Od uživatele');
INSERT INTO lang_translation VALUES ('1651', '223', '5', 'Status');
INSERT INTO lang_translation VALUES ('1652', '224', '5', 'Herci');
INSERT INTO lang_translation VALUES ('1653', '225', '5', 'Kategorie');
INSERT INTO lang_translation VALUES ('1654', '226', '5', 'Vyber soubor ZIP');
INSERT INTO lang_translation VALUES ('1655', '227', '5', 'Kliknout a vymazat');
INSERT INTO lang_translation VALUES ('1656', '228', '5', 'Upravit obrázek');
INSERT INTO lang_translation VALUES ('1657', '229', '5', 'Vymazat obrázek');
INSERT INTO lang_translation VALUES ('1658', '230', '5', 'Vymazat');
INSERT INTO lang_translation VALUES ('1659', '231', '5', 'Upravit');
INSERT INTO lang_translation VALUES ('1660', '232', '5', 'Vytvořit nového herce');
INSERT INTO lang_translation VALUES ('1661', '233', '5', 'Vytvořit novou fotogalerii');
INSERT INTO lang_translation VALUES ('1662', '234', '5', 'Jméno');
INSERT INTO lang_translation VALUES ('1663', '235', '5', 'Nadpis');
INSERT INTO lang_translation VALUES ('1664', '236', '5', 'Popis');
INSERT INTO lang_translation VALUES ('1665', '237', '5', 'TM od');
INSERT INTO lang_translation VALUES ('1666', '238', '5', 'TM pro');
INSERT INTO lang_translation VALUES ('1667', '239', '5', 'Uživatelské jméno');
INSERT INTO lang_translation VALUES ('1668', '240', '5', 'Status');
INSERT INTO lang_translation VALUES ('1669', '241', '5', 'SEO');
INSERT INTO lang_translation VALUES ('1670', '242', '5', 'URL');
INSERT INTO lang_translation VALUES ('1671', '243', '5', 'Volný');
INSERT INTO lang_translation VALUES ('1672', '244', '5', 'Datum vytvořeno');
INSERT INTO lang_translation VALUES ('1673', '245', '5', 'Uživatelské ID vytvořeno');
INSERT INTO lang_translation VALUES ('1674', '190', '6', 'Descripcion');
INSERT INTO lang_translation VALUES ('1675', '191', '6', 'Anadir');
INSERT INTO lang_translation VALUES ('1676', '192', '6', 'Limpia');
INSERT INTO lang_translation VALUES ('1677', '193', '6', 'Crear un nuevo actor');
INSERT INTO lang_translation VALUES ('1678', '194', '6', 'Crear una nueva foto');
INSERT INTO lang_translation VALUES ('1679', '195', '6', 'Advertencia');
INSERT INTO lang_translation VALUES ('1680', '196', '6', 'Error!');
INSERT INTO lang_translation VALUES ('1681', '197', '6', 'Mancha');
INSERT INTO lang_translation VALUES ('1682', '198', '6', 'Status');
INSERT INTO lang_translation VALUES ('1683', '199', '6', 'Error');
INSERT INTO lang_translation VALUES ('1684', '200', '6', 'Por favor, espere...');
INSERT INTO lang_translation VALUES ('1685', '201', '6', 'Archivo de carga');
INSERT INTO lang_translation VALUES ('1686', '202', '6', 'Almacenamiento');
INSERT INTO lang_translation VALUES ('1687', '203', '6', 'Servidor no esta disponible');
INSERT INTO lang_translation VALUES ('1688', '204', '6', 'Estas seguro que quieres eliminar este? ');
INSERT INTO lang_translation VALUES ('1689', '205', '6', 'Esta seguro que desea eliminar un actor? ');
INSERT INTO lang_translation VALUES ('1690', '206', '6', 'Edicion de imagenes');
INSERT INTO lang_translation VALUES ('1691', '207', '6', 'Edicion Actor');
INSERT INTO lang_translation VALUES ('1692', '208', '6', 'Edicion galeria');
INSERT INTO lang_translation VALUES ('1693', '209', '6', 'Informacion');
INSERT INTO lang_translation VALUES ('1694', '210', '6', 'Agregar archivo ZIP');
INSERT INTO lang_translation VALUES ('1695', '211', '6', 'Imagenes');
INSERT INTO lang_translation VALUES ('1696', '212', '6', 'Reparto');
INSERT INTO lang_translation VALUES ('1697', '213', '6', 'Galeria de fotos');
INSERT INTO lang_translation VALUES ('1698', '214', '6', 'Tiempo de configuracion de');
INSERT INTO lang_translation VALUES ('1699', '215', '6', 'Configuracion de tiempo para');
INSERT INTO lang_translation VALUES ('1700', '216', '6', 'Titulo');
INSERT INTO lang_translation VALUES ('1701', '217', '6', 'Fin');
INSERT INTO lang_translation VALUES ('1702', '218', '6', 'FSK');
INSERT INTO lang_translation VALUES ('1703', '219', '6', 'URL');
INSERT INTO lang_translation VALUES ('1704', '220', '6', 'Palabras clave (SEO)');
INSERT INTO lang_translation VALUES ('1705', '221', '6', 'Libre');
INSERT INTO lang_translation VALUES ('1706', '222', '6', 'Por');
INSERT INTO lang_translation VALUES ('1707', '223', '6', 'Status');
INSERT INTO lang_translation VALUES ('1708', '224', '6', 'Actores');
INSERT INTO lang_translation VALUES ('1709', '225', '6', 'Categoria');
INSERT INTO lang_translation VALUES ('1710', '226', '6', 'Seleccione el archivo ZIP');
INSERT INTO lang_translation VALUES ('1711', '227', '6', 'Clic y eliminar');
INSERT INTO lang_translation VALUES ('1712', '228', '6', 'Edicion de imagenes');
INSERT INTO lang_translation VALUES ('1713', '229', '6', 'Eliminar la imagen');
INSERT INTO lang_translation VALUES ('1714', '230', '6', 'Eliminar');
INSERT INTO lang_translation VALUES ('1715', '231', '6', 'Editar');
INSERT INTO lang_translation VALUES ('1716', '232', '6', 'Crear un nuevo actor');
INSERT INTO lang_translation VALUES ('1717', '233', '6', 'Crear una nueva foto');
INSERT INTO lang_translation VALUES ('1718', '234', '6', 'Nombre');
INSERT INTO lang_translation VALUES ('1719', '235', '6', 'Titulo');
INSERT INTO lang_translation VALUES ('1720', '236', '6', 'Descripcion');
INSERT INTO lang_translation VALUES ('1721', '237', '6', 'TM de');
INSERT INTO lang_translation VALUES ('1722', '238', '6', 'TM por');
INSERT INTO lang_translation VALUES ('1723', '239', '6', 'Nombre de usuario');
INSERT INTO lang_translation VALUES ('1724', '240', '6', 'Status');
INSERT INTO lang_translation VALUES ('1725', '241', '6', 'SEO');
INSERT INTO lang_translation VALUES ('1726', '242', '6', 'URL');
INSERT INTO lang_translation VALUES ('1727', '243', '6', 'Libre');
INSERT INTO lang_translation VALUES ('1728', '244', '6', 'Fecha de creacion');
INSERT INTO lang_translation VALUES ('1729', '245', '6', 'ID de usuario de creacion');
INSERT INTO lang_translation VALUES ('1730', '190', '7', '-');
INSERT INTO lang_translation VALUES ('1731', '191', '7', '-');
INSERT INTO lang_translation VALUES ('1732', '192', '7', '-');
INSERT INTO lang_translation VALUES ('1733', '193', '7', '-');
INSERT INTO lang_translation VALUES ('1734', '194', '7', '-');
INSERT INTO lang_translation VALUES ('1735', '195', '7', '-');
INSERT INTO lang_translation VALUES ('1736', '196', '7', '-');
INSERT INTO lang_translation VALUES ('1737', '197', '7', '-');
INSERT INTO lang_translation VALUES ('1738', '198', '7', '-');
INSERT INTO lang_translation VALUES ('1739', '199', '7', '-');
INSERT INTO lang_translation VALUES ('1740', '200', '7', '-');
INSERT INTO lang_translation VALUES ('1741', '201', '7', '-');
INSERT INTO lang_translation VALUES ('1742', '202', '7', '-');
INSERT INTO lang_translation VALUES ('1743', '203', '7', '-');
INSERT INTO lang_translation VALUES ('1744', '204', '7', '-');
INSERT INTO lang_translation VALUES ('1745', '205', '7', '-');
INSERT INTO lang_translation VALUES ('1746', '206', '7', '-');
INSERT INTO lang_translation VALUES ('1747', '207', '7', '-');
INSERT INTO lang_translation VALUES ('1748', '208', '7', '-');
INSERT INTO lang_translation VALUES ('1749', '209', '7', '-');
INSERT INTO lang_translation VALUES ('1750', '210', '7', '-');
INSERT INTO lang_translation VALUES ('1751', '211', '7', '-');
INSERT INTO lang_translation VALUES ('1752', '212', '7', '-');
INSERT INTO lang_translation VALUES ('1753', '213', '7', '-');
INSERT INTO lang_translation VALUES ('1754', '214', '7', '-');
INSERT INTO lang_translation VALUES ('1755', '215', '7', '-');
INSERT INTO lang_translation VALUES ('1756', '216', '7', '-');
INSERT INTO lang_translation VALUES ('1757', '217', '7', '-');
INSERT INTO lang_translation VALUES ('1758', '218', '7', '-');
INSERT INTO lang_translation VALUES ('1759', '219', '7', '-');
INSERT INTO lang_translation VALUES ('1760', '220', '7', '-');
INSERT INTO lang_translation VALUES ('1761', '221', '7', '-');
INSERT INTO lang_translation VALUES ('1762', '222', '7', '-');
INSERT INTO lang_translation VALUES ('1763', '223', '7', '-');
INSERT INTO lang_translation VALUES ('1764', '224', '7', '-');
INSERT INTO lang_translation VALUES ('1765', '225', '7', '-');
INSERT INTO lang_translation VALUES ('1766', '226', '7', '-');
INSERT INTO lang_translation VALUES ('1767', '227', '7', '-');
INSERT INTO lang_translation VALUES ('1768', '228', '7', '-');
INSERT INTO lang_translation VALUES ('1769', '229', '7', '-');
INSERT INTO lang_translation VALUES ('1770', '230', '7', '-');
INSERT INTO lang_translation VALUES ('1771', '231', '7', '-');
INSERT INTO lang_translation VALUES ('1772', '232', '7', '-');
INSERT INTO lang_translation VALUES ('1773', '233', '7', '-');
INSERT INTO lang_translation VALUES ('1774', '234', '7', '-');
INSERT INTO lang_translation VALUES ('1775', '235', '7', '-');
INSERT INTO lang_translation VALUES ('1776', '236', '7', '-');
INSERT INTO lang_translation VALUES ('1777', '237', '7', '-');
INSERT INTO lang_translation VALUES ('1778', '238', '7', '-');
INSERT INTO lang_translation VALUES ('1779', '239', '7', '-');
INSERT INTO lang_translation VALUES ('1780', '240', '7', '-');
INSERT INTO lang_translation VALUES ('1781', '241', '7', '-');
INSERT INTO lang_translation VALUES ('1782', '242', '7', '-');
INSERT INTO lang_translation VALUES ('1783', '243', '7', '-');
INSERT INTO lang_translation VALUES ('1784', '244', '7', '-');
INSERT INTO lang_translation VALUES ('1785', '245', '7', '-');
INSERT INTO lang_translation VALUES ('1798', '249', '2', 'Verbindungsfehler hat, bitte Versuch wieder stattgefunden.');
INSERT INTO lang_translation VALUES ('1799', '249', '4', 'Koneksyon bigo, mangyaring subukan ulit mamaya.');
INSERT INTO lang_translation VALUES ('1800', '249', '5', 'Spojení selhalo, prosím zkuste se znovu připojit. ');
INSERT INTO lang_translation VALUES ('1801', '249', '6', 'Error de conexion, por favor, intentelo de nuevo mas tarde. ');
INSERT INTO lang_translation VALUES ('1802', '249', '7', '-');
INSERT INTO lang_translation VALUES ('1803', '249', '1', 'Connection error has occured, please try again.');
INSERT INTO lang_translation VALUES ('1804', '250', '2', 'Benutzername und Kennwort Felder können nicht leer sein, bitte versuchen Sie es noch einmal. ');
INSERT INTO lang_translation VALUES ('1805', '250', '4', 'Username at password ay dapat na blangko, mangyaring subukan ulit. ');
INSERT INTO lang_translation VALUES ('1806', '250', '5', 'Uživatelské jméno a heslo nesmí být prázdné, prosím opakujte pokus. ');
INSERT INTO lang_translation VALUES ('1807', '250', '6', 'Nombre de usuario y la contraseña deben estar en blanco, por favor, inténtelo de nuevo. ');
INSERT INTO lang_translation VALUES ('1808', '250', '7', '-');
INSERT INTO lang_translation VALUES ('1809', '250', '1', 'Username and Password fields cannot be empty, please try again.');
INSERT INTO lang_translation VALUES ('1810', '251', '2', 'Ungültige Anmeldung, bitte versuchen Sie wieder!');
INSERT INTO lang_translation VALUES ('1811', '251', '4', 'Maling password, mangyaring subukan muli. ');
INSERT INTO lang_translation VALUES ('1812', '251', '5', 'Nesprávné heslo, prosím opakujte pokus.');
INSERT INTO lang_translation VALUES ('1813', '251', '6', 'Contraseña incorrecta, por favor, inténtelo de nuevo.');
INSERT INTO lang_translation VALUES ('1814', '251', '7', '-');
INSERT INTO lang_translation VALUES ('1815', '251', '1', 'Invalid login, please try again!');
INSERT INTO lang_translation VALUES ('1816', '252', '2', 'Sie haben das Maximum erlaubte Anmeldungen überschritten! Ihr Konto ist vorübergehend unwirksam, bitte kontaktiert den Stellenverwalter gemacht, es zu ermöglichen.');
INSERT INTO lang_translation VALUES ('1817', '252', '4', 'Mo na lumampas sa bilang ng posibleng mga pagtatangka upang mag-log in Ang iyong account ay pansamantalang hindi magagamit para sa pag-access, mangyaring makipag-ugnayan ang mga administrator. ');
INSERT INTO lang_translation VALUES ('1818', '252', '5', 'Byl překročen možný počet pokusů k přihlášení! Váš účet je dočasně nedostupný, pro zpřístupnění prosím kontaktujte administrátora.');
INSERT INTO lang_translation VALUES ('1819', '252', '6', 'Has superado el número de posibles intentos de entrar Su cuenta está temporalmente fuera de servicio para el acceso, por favor ponte en contacto con el administrador. ');
INSERT INTO lang_translation VALUES ('1820', '252', '7', '-');
INSERT INTO lang_translation VALUES ('1821', '252', '1', 'You have exceeded the maximum allowed logins! Your account is temporarily disabled, please contact the site administrator to enable it.');
INSERT INTO lang_translation VALUES ('1822', '253', '2', 'Ihr Konto ist vorübergehend unwirksam, bitte kontaktiert die Stellenadmin gemacht, es zu ermöglichen.');
INSERT INTO lang_translation VALUES ('1823', '253', '4', 'Ang iyong account ay pansamantalang hindi magagamit, mangyaring makipag-ugnayan sa administrator!');
INSERT INTO lang_translation VALUES ('1824', '253', '5', 'Váš účet je dočasně nedostupný, pro zpřístupnění kontaktujte prosím administrátora!');
INSERT INTO lang_translation VALUES ('1825', '253', '6', 'Su cuenta no esta disponible temporalmente, por favor, contacte con el administrador!');
INSERT INTO lang_translation VALUES ('1826', '253', '7', '-');
INSERT INTO lang_translation VALUES ('1827', '253', '1', 'Your account is temporarily disabled, please contact the site admin to enable it.');
INSERT INTO lang_translation VALUES ('1828', '254', '2', 'Verbraucher schon Anmeldung.');
INSERT INTO lang_translation VALUES ('1829', '254', '4', 'Käyttäjä on jo kirjautuneena. ');
INSERT INTO lang_translation VALUES ('1830', '254', '5', 'Uživatel je již přihlášen.');
INSERT INTO lang_translation VALUES ('1831', '254', '6', 'El usuario ya está conectado. ');
INSERT INTO lang_translation VALUES ('1832', '254', '7', '-');
INSERT INTO lang_translation VALUES ('1833', '254', '1', 'User already logged-in.');
INSERT INTO lang_translation VALUES ('1834', '255', '2', 'Anmeldungsfehler');
INSERT INTO lang_translation VALUES ('1835', '255', '4', 'Login Error ');
INSERT INTO lang_translation VALUES ('1836', '255', '5', 'Chyba přihlášení');
INSERT INTO lang_translation VALUES ('1837', '255', '6', 'Entrada de error ');
INSERT INTO lang_translation VALUES ('1838', '255', '7', '-');
INSERT INTO lang_translation VALUES ('1839', '255', '1', 'Login Error');
INSERT INTO lang_translation VALUES ('1840', '256', '2', 'Bitte melden Sie sich an');
INSERT INTO lang_translation VALUES ('1841', '256', '4', 'Mangyaring mag-login ');
INSERT INTO lang_translation VALUES ('1842', '256', '5', 'Přihlašte se prosím');
INSERT INTO lang_translation VALUES ('1843', '256', '6', 'Por favor, identifiquese');
INSERT INTO lang_translation VALUES ('1844', '256', '7', '-');
INSERT INTO lang_translation VALUES ('1845', '256', '1', 'Please log in');
INSERT INTO lang_translation VALUES ('1846', '257', '2', 'Vergessenen Kennwort');
INSERT INTO lang_translation VALUES ('1847', '257', '4', 'Nakalimutang Password');
INSERT INTO lang_translation VALUES ('1848', '257', '5', 'Zapomenuté heslo');
INSERT INTO lang_translation VALUES ('1849', '257', '6', 'Olvido su contrasena');
INSERT INTO lang_translation VALUES ('1850', '257', '7', '-');
INSERT INTO lang_translation VALUES ('1851', '257', '1', 'Forgot Password');
INSERT INTO lang_translation VALUES ('1852', '258', '2', 'Anmeldung');
INSERT INTO lang_translation VALUES ('1853', '258', '4', 'Login');
INSERT INTO lang_translation VALUES ('1854', '258', '5', 'Přihlášení');
INSERT INTO lang_translation VALUES ('1855', '258', '6', 'Iniciar sesion');
INSERT INTO lang_translation VALUES ('1856', '258', '7', '-');
INSERT INTO lang_translation VALUES ('1857', '258', '1', 'Login');
INSERT INTO lang_translation VALUES ('1858', '259', '2', 'Passwort vergessen ? Kein Problem, einfach Link Passwort vergessen anklicken und EMail-Adresse in das Feld eingeben. Wenn wir dich mit dieser Adresse in unserer Datenbank registriert haben, schicken wir dir ein neues Passwort per E-Mail zu. Bitte verwende');
INSERT INTO lang_translation VALUES ('1859', '259', '4', 'Nakalimutang Password ');
INSERT INTO lang_translation VALUES ('1860', '259', '5', 'Zapomenuté heslo');
INSERT INTO lang_translation VALUES ('1861', '259', '6', 'Olvido su contrasena');
INSERT INTO lang_translation VALUES ('1862', '259', '7', '-');
INSERT INTO lang_translation VALUES ('1863', '259', '1', 'Forgotten Password? No Problem! Click on Link Forgotten Password and enter your E-mail adress. If you have entered this E-mail adress in our prime registration, we send you a new Password. Please look in your Mailbox. Please be sure, to use the same spell');

-- ----------------------------
-- Table structure for `login_jnl`
-- ----------------------------
DROP TABLE IF EXISTS `login_jnl`;
CREATE TABLE `login_jnl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `login_dt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of login_jnl
-- ----------------------------
INSERT INTO login_jnl VALUES ('1', '76', '2014-09-22 01:52:13');
INSERT INTO login_jnl VALUES ('2', '76', '2014-09-24 22:23:58');
INSERT INTO login_jnl VALUES ('3', '76', '2014-09-24 22:38:24');
INSERT INTO login_jnl VALUES ('4', '76', '2014-09-24 22:46:22');
INSERT INTO login_jnl VALUES ('5', '76', '2014-09-24 22:49:49');
INSERT INTO login_jnl VALUES ('6', '76', '2014-09-24 23:08:52');
INSERT INTO login_jnl VALUES ('7', '76', '2014-09-25 02:01:14');
INSERT INTO login_jnl VALUES ('8', '76', '2014-09-25 22:40:25');
INSERT INTO login_jnl VALUES ('9', '76', '2014-09-26 05:56:09');
INSERT INTO login_jnl VALUES ('10', '76', '2014-09-26 05:56:28');
INSERT INTO login_jnl VALUES ('11', '76', '2014-09-26 05:56:28');
INSERT INTO login_jnl VALUES ('12', '76', '2014-09-26 06:02:10');
INSERT INTO login_jnl VALUES ('13', '76', '2014-09-26 06:08:29');
INSERT INTO login_jnl VALUES ('14', '76', '2014-09-27 16:57:23');
INSERT INTO login_jnl VALUES ('15', '76', '2014-09-28 16:13:19');
INSERT INTO login_jnl VALUES ('16', '76', '2014-09-29 16:22:04');
INSERT INTO login_jnl VALUES ('17', '76', '2014-09-29 16:47:22');
INSERT INTO login_jnl VALUES ('18', '76', '2014-09-29 17:59:15');
INSERT INTO login_jnl VALUES ('19', '76', '2014-09-29 18:14:39');
INSERT INTO login_jnl VALUES ('20', '76', '2014-10-02 17:35:59');
INSERT INTO login_jnl VALUES ('21', '76', '2014-10-02 23:22:56');
INSERT INTO login_jnl VALUES ('22', '76', '2014-10-03 05:07:02');
INSERT INTO login_jnl VALUES ('23', '76', '2014-10-03 11:39:46');
INSERT INTO login_jnl VALUES ('24', '76', '2014-10-03 13:44:22');
INSERT INTO login_jnl VALUES ('25', '76', '2014-10-03 13:50:37');
INSERT INTO login_jnl VALUES ('26', '76', '2014-10-03 15:19:22');
INSERT INTO login_jnl VALUES ('27', '76', '2014-10-03 15:32:54');
INSERT INTO login_jnl VALUES ('28', '76', '2014-10-03 15:48:52');
INSERT INTO login_jnl VALUES ('29', '76', '2014-10-03 16:12:18');
INSERT INTO login_jnl VALUES ('30', '76', '2014-10-03 16:12:20');
INSERT INTO login_jnl VALUES ('31', '76', '2014-10-03 16:42:16');
INSERT INTO login_jnl VALUES ('32', '76', '2014-10-04 17:05:34');

-- ----------------------------
-- Table structure for `members`
-- ----------------------------
DROP TABLE IF EXISTS `members`;
CREATE TABLE `members` (
  `firstname` varchar(100) NOT NULL,
  `middlename` varchar(100) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `address` varchar(200) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` int(30) NOT NULL,
  `userType` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of members
-- ----------------------------

-- ----------------------------
-- Table structure for `messages`
-- ----------------------------
DROP TABLE IF EXISTS `messages`;
CREATE TABLE `messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `author` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `message` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `isRead` tinyint(1) NOT NULL DEFAULT '0',
  `subject` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of messages
-- ----------------------------
INSERT INTO messages VALUES ('1', 'Patrick Garcia', 'freaked0ut07@yahoo.com.ph', 'I want to order a 3x3 picture', '2014-10-03 11:43:42', '2014-10-03 11:44:37', '1', 'Tarp');
INSERT INTO messages VALUES ('2', 'Patrick Garcia', 'patrickpgarcia07@gmail.com', 'asdjajshd', '2014-10-03 13:51:48', '2014-10-03 13:52:19', '1', 'blah');

-- ----------------------------
-- Table structure for `naviboxes`
-- ----------------------------
DROP TABLE IF EXISTS `naviboxes`;
CREATE TABLE `naviboxes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `position` varchar(1) NOT NULL,
  `order` smallint(2) NOT NULL,
  `type` varchar(50) DEFAULT 'dynamic',
  `xtype` varchar(50) DEFAULT 'panel',
  `active_indicator` smallint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=461 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of naviboxes
-- ----------------------------
INSERT INTO naviboxes VALUES ('27', 'Backend Management', 'L', '0', 'static', 'wdpanelsbackendnavibox', '1');
INSERT INTO naviboxes VALUES ('29', 'System Management', 'R', '0', 'static', 'wdpanelssystemsettingnavibox', '2');
INSERT INTO naviboxes VALUES ('23', 'Frontend Management', 'R', '0', 'dynamic', 'panel', '2');
INSERT INTO naviboxes VALUES ('31', 'Welcome', 'C', '0', 'dynamic', 'wdpanelwelcome', '2');
INSERT INTO naviboxes VALUES ('40', 'Statistics', 'C', '0', 'dynamic', 'wdpanelstatistics', '2');
INSERT INTO naviboxes VALUES ('42', 'News', 'C', '0', 'dynamic', 'wdgridshomenews', '2');
INSERT INTO naviboxes VALUES ('44', 'Activity Stream', 'L', '1', 'static', 'cbnaviboxactivitystream', '2');
INSERT INTO naviboxes VALUES ('46', 'Leads', 'L', '3', 'static', 'cbnaviboxleads', '1');
INSERT INTO naviboxes VALUES ('47', 'Accounts', 'L', '7', 'static', 'cbnaviboxaccounts', '2');
INSERT INTO naviboxes VALUES ('49', 'Contacts', 'L', '8', 'dynamic', 'cbnaviboxcontacts', '2');
INSERT INTO naviboxes VALUES ('45', 'Messages', 'L', '2', 'dynamic', 'cbnaviboxmessages', '2');
INSERT INTO naviboxes VALUES ('460', 'Lead Planning', 'L', '4', 'static', 'cbnaviboxleadplanning', '2');

-- ----------------------------
-- Table structure for `news`
-- ----------------------------
DROP TABLE IF EXISTS `news`;
CREATE TABLE `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subject` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `last_update_dt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `image` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of news
-- ----------------------------
INSERT INTO news VALUES ('1', 'Soap for sale', 'new soaps for gays', '2011-08-24 02:34:47', 'papaya-soap-cristine.jpg');
INSERT INTO news VALUES ('2', 'Free Shampoo', 'no desc', '2011-08-24 02:39:08', null);
INSERT INTO news VALUES ('3', 'news 1', 'descxxxxx', '2011-09-06 22:20:52', null);
INSERT INTO news VALUES ('4', 'fdsfs', 'fdsefsdfsef', '2011-09-09 02:14:59', null);

-- ----------------------------
-- Table structure for `orderlist`
-- ----------------------------
DROP TABLE IF EXISTS `orderlist`;
CREATE TABLE `orderlist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `productID` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `amount` float NOT NULL,
  `orderID` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of orderlist
-- ----------------------------
INSERT INTO orderlist VALUES ('1', '3', '12', '600', '1');
INSERT INTO orderlist VALUES ('2', '3', '1', '50', '2');
INSERT INTO orderlist VALUES ('3', '1', '2', '21', '3');
INSERT INTO orderlist VALUES ('4', '2', '1', '10.5', '4');
INSERT INTO orderlist VALUES ('5', '5', '2', '24', '5');
INSERT INTO orderlist VALUES ('7', '7', '2', '180', '7');
INSERT INTO orderlist VALUES ('14', '22', '10', '150', '15');
INSERT INTO orderlist VALUES ('15', '3', '1', '12', '16');

-- ----------------------------
-- Table structure for `orders`
-- ----------------------------
DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `total_amount` float NOT NULL,
  `customerID` int(11) NOT NULL,
  `paymentstatus` varchar(20) NOT NULL DEFAULT '0' COMMENT 'pending;paid;reserved;',
  `deliverystatus` varchar(20) NOT NULL DEFAULT 'Pending' COMMENT 'pending;delivered;',
  `transactionID` varchar(30) DEFAULT NULL,
  `expiration` date DEFAULT NULL COMMENT 'null if paid',
  `amount_paid` float NOT NULL,
  `email_payer` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of orders
-- ----------------------------
INSERT INTO orders VALUES ('1', '2011-09-21 00:15:12', '2011-09-21 00:16:46', '600', '1', 'Paid', 'Delivered', '70S90237WH9062329', null, '600', null);
INSERT INTO orders VALUES ('2', '2011-09-21 00:48:48', '2011-09-21 00:48:48', '50', '1', 'Reserved', 'Pending', '07N29852JE882864A', '2011-09-24', '25', null);
INSERT INTO orders VALUES ('3', '2011-09-21 13:07:04', '2011-09-21 13:08:07', '21', '1', 'Paid', 'Delivered', '4GX167264E8551345', null, '21', null);
INSERT INTO orders VALUES ('4', '2011-11-22 23:03:22', '2011-11-22 23:03:22', '10.5', '1', 'Paid', 'Delivered', '3M854443WK6326713', null, '10.5', 'poy_cu_1247777456_per@yahoo.com');
INSERT INTO orders VALUES ('5', '2011-11-28 01:28:03', '2011-11-28 01:28:03', '24', '1', 'Paid', 'Delivered', '89F81862AM881680Y', null, '24', 'poy_cu_1247777456_per@yahoo.com');
INSERT INTO orders VALUES ('7', '2011-11-28 15:21:12', '2011-11-28 15:21:12', '180', '1', 'Paid', 'Delivered', '1EK17754404844424', null, '180', 'poy_cu_1247777456_per@yahoo.com');
INSERT INTO orders VALUES ('15', '2011-11-28 17:20:26', '2011-11-28 17:20:26', '150', '7', 'Paid', 'Delivered', '3C766401US805313C', null, '150', 'poy_cu_1247777456_per@yahoo.com');
INSERT INTO orders VALUES ('16', '2011-12-06 13:36:25', '2011-12-06 13:36:25', '12', '1', 'Paid', 'Delivered', '9E618097T46580048', null, '12', 'poy_cu_1247777456_per@yahoo.com');

-- ----------------------------
-- Table structure for `patients`
-- ----------------------------
DROP TABLE IF EXISTS `patients`;
CREATE TABLE `patients` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `fname` varchar(100) NOT NULL,
  `mname` varchar(100) NOT NULL,
  `lname` varchar(100) NOT NULL,
  `address` varchar(255) NOT NULL,
  `pnum` varchar(30) DEFAULT NULL,
  `bday` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of patients
-- ----------------------------
INSERT INTO patients VALUES ('3', '2011-10-18 22:19:16', '2011-10-18 22:19:16', 'jade', 'leonor', 'manalili', 'toril davao city', '2222222', '2011-10-15');
INSERT INTO patients VALUES ('4', '2011-11-03 11:54:11', '2011-11-03 11:54:11', 'alpha', 'a', 'a', 'aaaa', '222222', '2011-11-01');
INSERT INTO patients VALUES ('5', '2011-11-03 23:15:54', '2011-11-03 23:15:54', 'katrin jade', 'leonor', 'manalili', 'toril davao city', '2314143', '1988-10-15');
INSERT INTO patients VALUES ('6', '2011-11-03 23:16:34', '2011-11-03 23:16:34', 'eileen', 'maglana', 'gomia', 'damosa davao city', '3214871', '1990-08-28');
INSERT INTO patients VALUES ('7', '2011-11-03 23:18:22', '2011-11-03 23:18:22', 'eritz von', 'salcedo', 'romero', 'malvar street davao city', '2213451', '1986-09-14');
INSERT INTO patients VALUES ('8', '2011-11-17 13:02:58', '2011-11-17 13:02:58', 'jovitmark', 'bagsican', 'roble', 'malvar street davao city', '09219909012', '1991-06-13');
INSERT INTO patients VALUES ('9', '2011-11-17 13:03:44', '2011-11-17 13:03:44', 'eileen', 'maglana', 'gomia', 'damosa davao city', '09128981829', '1990-08-28');
INSERT INTO patients VALUES ('10', '2011-11-17 13:57:03', '2011-11-17 13:57:03', 'adiza', 'mallorca', 'caya', 'buhangin davao city', '09223456789', '1990-09-05');
INSERT INTO patients VALUES ('11', '2011-11-22 11:20:55', '2011-11-22 11:20:55', 'samplex', 'samplex', 'samplex', 'xxxxxxxx', '1234324234', '2011-11-15');

-- ----------------------------
-- Table structure for `photobooth`
-- ----------------------------
DROP TABLE IF EXISTS `photobooth`;
CREATE TABLE `photobooth` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `customerId` int(11) NOT NULL,
  `event_date` date NOT NULL,
  `setup_time` time NOT NULL,
  `start_time` time NOT NULL,
  `end_time` time NOT NULL,
  `eventType` varchar(100) NOT NULL,
  `theme` varchar(100) NOT NULL,
  `title` varchar(100) NOT NULL,
  `amount_due` float NOT NULL,
  `downPayment` float NOT NULL,
  `balance` float NOT NULL,
  `amountPaid` float NOT NULL,
  `transactionId` varchar(100) NOT NULL,
  `userId` int(11) NOT NULL,
  `venue` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of photobooth
-- ----------------------------
INSERT INTO photobooth VALUES ('1', '2014-09-25 03:41:13', '2014-09-25 03:41:17', '4', '2014-10-08', '15:00:00', '16:00:00', '20:00:00', 'bday', 'sexy', 'sexy bday', '6500', '3500', '3000', '3500', '324234sfsdfsf', '76', '');

-- ----------------------------
-- Table structure for `products`
-- ----------------------------
DROP TABLE IF EXISTS `products`;
CREATE TABLE `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `product_code` varchar(50) NOT NULL,
  `catID` int(11) NOT NULL,
  `image` varchar(200) NOT NULL,
  `description` text NOT NULL,
  `price` float NOT NULL,
  `rquantity` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of products
-- ----------------------------
INSERT INTO products VALUES ('1', 'Cozy Autumn Vanilla Shower Gel', '37711738', '1', 'cozy autumn vanilla.jpg', 'Our exclusive Cozy Autumn Vanilla is a warm blend of fresh vanilla beans, soft caramel and comforting creamy musk. ', '10.5', '48', '2011-09-04 22:52:38', '2011-10-11 00:53:55');
INSERT INTO products VALUES ('2', 'Paris Amour Shimmer Mist ', '074923413056', '1', 'paris Shimmer Mist.jpg', 'This Shimmer Mist will leave skin sparkling with irresistible glamour and lightly fragranced with our exclusive Paris Amour, a dreamy blend of French tulips, apple blossoms and sparkling pink champagne, inspired by a romantic stroll through the streets of Paris.', '10.5', '50', '2011-09-04 22:52:31', '2011-10-11 00:56:32');
INSERT INTO products VALUES ('3', 'Cozy Autumn Vanilla Fragrance Mist', '95506630', '1', 'cozy FM.jpg', 'Lavishly spritz our refreshing Fragrance Mist all over your body to leave skin lightly scented with our exclusive Cozy Autumn Vanilla, a warm blend of fresh vanilla beans, soft caramel and comforting creamy musk.', '12', '77', '2011-09-04 22:52:25', '2011-10-11 00:53:22');
INSERT INTO products VALUES ('4', 'Warm Vanilla Sugar Body Lotion', '4800064610889', '2', 'warm vanilla sugar.jpg', 'Our exclusive Signature Body Lotion formula, fortified with powerful ingredients like super conditioning Shea Butter, fast absorbing Jojoba Oil and protective Vitamin E, hydrates skin with moisture that lasts all day. Our non-greasy formula absorbs quickly and completely to leave skin feeling incredibly soft, smooth and nourished, making it the perfect daily moisturizer.', '10.5', '50', '2011-09-04 22:52:17', '2011-11-03 22:37:00');
INSERT INTO products VALUES ('5', 'Sweet Pea Body Lotion ', '8935001708988', '2', 'sweet pea.jpg', 'Our exclusive Signature Body Lotion formula, fortified with powerful ingredients like super conditioning Shea Butter, fast absorbing Jojoba Oil and protective Vitamin E, hydrates skin with moisture that lasts all day. Our non-greasy formula absorbs quickly and completely to leave skin feeling incredibly soft, smooth and nourished, making it the perfect daily moisturizer.', '12', '48', '2011-09-04 22:55:19', '2011-10-11 00:58:43');
INSERT INTO products VALUES ('6', 'Sweet Cinnamon Pumpkin Fragrance Mist ', '4901140921834', '1', 'Sweet Cinnamon Pumpkin FM.jpg', 'Lavishly spritz our refreshing Fragrance Mist all over your body to leave skin lightly scented with our exclusive Sweet Cinnamon Pumpkin, a cozy blend of fresh pumpkin, crisp apple and spiced vanilla.', '48', '50', '2011-09-20 22:55:48', '2011-10-11 00:58:13');
INSERT INTO products VALUES ('7', 'Paris Amour Fragrance Mist', '4800047865008', '1', 'Paris Amour FM.jpg', 'Lavishly spritz our refreshing Fragrance Mist all over your body to leave skin lightly scented with our exclusive Paris Amour, a dreamy blend of French tulips, apple blossoms and sparkling pink champagne, inspired by a romantic stroll through the streets of Paris.', '90', '48', '2011-09-06 23:44:54', '2011-10-11 00:56:14');
INSERT INTO products VALUES ('8', 'Paris Amour Shower Gel', '123456789', '1', 'Paris Amour.jpg', 'Smooth over skin and enjoy the fragrance of our exclusive Paris Amour, a dreamy blend of French tulips, apple blossoms and sparkling pink champagne.', '400', '50', '2011-09-20 13:54:37', '2011-10-11 00:56:49');
INSERT INTO products VALUES ('9', 'Sensual Amber Shower Gel', '12345678', '1', 'sensual amber.jpg', 'Our exclusive, seductive Sensual Amber fragrance is the exotic scent of pink lotus petals enveloped in golden amber.', '400', '50', '2011-09-20 13:58:09', '2011-10-11 00:57:06');
INSERT INTO products VALUES ('10', 'Sweet Cinnamon Pumpkin Shower Gel', '12345678', '1', 'Sweet Cinnamon Pumpkin.jpg', 'Our exclusive Sweet Cinnamon Pumpkin is a cozy blend of fresh pumpkin, crisp apple and spiced vanilla.', '400', '50', '2011-09-20 13:59:50', '2011-10-11 00:58:29');
INSERT INTO products VALUES ('11', 'Sparkling Berry Bliss Body Cream ', '12345678', '2', 'Sparkling Berry Bliss.jpg', 'Fortified with our exclusive triple moisture complex of conditioning milk proteins, hydrating rice bran oil and protective acai berry extract, our Signature Triple Moisture Body Cream provides deep, 24-hour moisture that softens even the driest skin. Our non-greasy formula deeply conditions, leaving skin moisturized, fragrant, and more beautiful than ever before.', '12.5', '50', '2011-09-21 09:56:44', '2011-10-11 00:57:45');
INSERT INTO products VALUES ('12', 'Japanese Cherry Blossom Body Cream', '23463212', '2', 'japanese.jpg', 'Fortified with our exclusive Triple Moisture Complex of conditioning milk proteins, hydrating rice bran oil and protective acai berry extract, our Signature Triple Moisture Body Cream provides deep, 24-hour moisture that softens even the driest skin. Our non-greasy formula deeply conditions, leaving skin moisturized, fragrant, and more beautiful than ever before.', '12', '50', '2011-09-21 09:58:00', '2011-10-11 00:54:16');
INSERT INTO products VALUES ('13', 'Orchard Leaves Hand Soap', '43567865', '5', 'Orchard Leaves.jpg', 'This powerful germ-killing hand soap is enriched with an exclusive moisturizing blend to help hydrate and soften skin as powerful ingredients leave dirty hands clean and lightly scented with a fresh blend of red apple, spiced cider and cinnamon sticks.', '6', '50', '2011-09-21 10:18:04', '2011-10-11 00:55:52');
INSERT INTO products VALUES ('14', 'Kitchen Lemon Hand Soap', '654323', '5', 'Kitchen Lemon.jpg', 'This powerful germ-killing hand soap is enriched with an exclusive moisturizing blend to help hydrate and soften skin as powerful ingredients leave dirty hands clean and lightly scented with the fresh, crisp scent of lemon.', '6', '50', '2011-09-21 10:22:49', '2011-10-11 00:55:30');
INSERT INTO products VALUES ('15', 'Pink Sugarplum Moisturizing Hand Soap', '431234', '5', 'Pink Sugarplum Moisturizing hand soap.jpg', 'Even with frequent hand washing, our soothing moisturizers hydrate skin. Hands are left soft, germ-free and lightly scented with an addictive blend of sugared plum, sparkling tangerine and vanilla musk.', '5.5', '0', '2011-11-03 22:43:54', '2011-11-03 22:44:29');
INSERT INTO products VALUES ('16', 'Winter Candy Apple Moisturizing Hand Soap', '7223523', '5', 'Winter Candy Apple.jpg', 'Even with frequent hand washing, our soothing moisturizers hydrate skin. Hands are left soft, germ-free and lightly scented with a mouthwatering blend of crisp apple, candied orange and rich cinnamon spice.', '5.5', '0', '2011-11-03 22:46:09', '2011-11-03 22:46:59');
INSERT INTO products VALUES ('17', 'Festive Friends Sanitizing Hand Gel 5-pack', '4452346', '5', 'Festive Friends.jpg', 'Spreading merry, not germs has never been easier or more fun! Natural ingredients and powerful germ killers get hands clean and lightly scented, wherever you go. Includes 5 PocketBacs, one in each of our festive fragrances.', '5', '0', '2011-11-03 22:49:27', '2011-11-03 22:49:27');
INSERT INTO products VALUES ('18', 'Sea Island Cotton Sanitizing Hand Gel', '2523462', '5', 'Sea Island Cotton.jpg', 'Spread love, not germs. Our powerful germ-killing hand gel is enriched with an exclusive blend of moisturizing honey, coconut milk and olive fruit extracts to help nourish and soften skin, leaving hands feeling clean, soft and lightly scented with pure white cotton flows in fresh ocean air.', '5', '0', '2011-11-03 22:51:02', '2011-11-03 22:52:24');
INSERT INTO products VALUES ('19', 'Green Apple Light-Up Coffin Hand Soap', '456765', '5', 'Green Apple.jpg', 'Surprise your guests with a little Halloween fun at every sink! Simply press the pump to dispense our green apple scented hand soap and enjoy the eerie lights and sound. This powerful germ-killing hand soap is enriched with an exclusive moisturizing blend to help hydrate and soften skin as powerful ingredients leave dirty hands clean and lightly scented with fresh picked apples.', '14.5', '0', '2011-11-03 22:57:59', '2011-11-03 22:57:59');
INSERT INTO products VALUES ('20', 'White Citrus HandiBac Moisturizing Hand Lotion', '456232', '5', 'White Citrus moisturizing hand lotion.jpg', 'This moisturizing hand lotion softens and soothes as it kills germs, leaving hands lightly fragranced with our cool, modern citrus scent.', '4', '0', '2011-11-03 23:01:13', '2011-11-03 23:01:42');
INSERT INTO products VALUES ('21', 'Dark Kiss Body Butter', '2355622', '2', 'Dark Kiss Body Butter.jpg', 'Our Intense Moisture Body Butter is proven to provide moisture for over 24 hours, taking hydration to the extreme with Shea and Murumuru butters and nourishing coconut oil. Ultra-conditioning and non-greasy, this formula is supercharged with an advanced blend of Pro-Vitamin B, CO Q10 and antioxidant vitamins C & E to leave skin feeling noticeably softer.', '15', '0', '2011-11-03 23:05:41', '2011-11-03 23:05:41');
INSERT INTO products VALUES ('22', 'Black Amethyst Body Butter', '7616400001115', '2', 'Black Amethyst Body Butter.jpg', 'Our exclusive Black Amethyst fragrance is a hypnotic scent, inspired by chic, sensual and confident women.', '15', '64', '2011-11-03 23:08:31', '2011-11-28 17:17:37');
INSERT INTO products VALUES ('23', 'Dark Kiss Hand Cream', '6342313', '2', 'Dark Kiss Hand Cream.jpg', 'Wrap your hands in cashmere. This ultra-moisturizing hand cream glides on sensually to soothe and nourish skin. Non-greasy, it absorbs quickly, leaving hands soft as cashmere and lightly fragranced with Dark Kiss, a seductive blend of dark berries with a kiss of vanilla to unleash your most primal passions', '10', '100', '2011-11-03 23:10:27', '2011-11-22 11:40:40');
INSERT INTO products VALUES ('24', 'Black Amethyst Hand Cream', '4800371068571', '2', 'Black Amethyst Hand Cream.jpg', 'African Shea Butter deeply moisturizes skin.', '10', '50', '2011-11-03 23:12:09', '2011-11-28 17:26:07');

-- ----------------------------
-- Table structure for `sakits`
-- ----------------------------
DROP TABLE IF EXISTS `sakits`;
CREATE TABLE `sakits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `patient_id` int(11) NOT NULL,
  `sakit` varchar(255) NOT NULL,
  `sakit_desc` text,
  `tambal` varchar(255) NOT NULL,
  `tambal_desc` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sakits
-- ----------------------------
INSERT INTO sakits VALUES ('1', '2011-11-17 14:12:56', '2011-11-17 14:12:56', '8', 'kerker', 'kerker', 'kerker', 'kerker');

-- ----------------------------
-- Table structure for `services`
-- ----------------------------
DROP TABLE IF EXISTS `services`;
CREATE TABLE `services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `serviceName` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of services
-- ----------------------------
INSERT INTO services VALUES ('1', 'Tarpaulin');
INSERT INTO services VALUES ('2', 'Stickers');
INSERT INTO services VALUES ('3', 'T-Shirt Silkscreen');
INSERT INTO services VALUES ('4', 'T-Shirt Heatpress');
INSERT INTO services VALUES ('5', 'Signage');
INSERT INTO services VALUES ('6', 'Business Card');
INSERT INTO services VALUES ('7', 'Layout');
INSERT INTO services VALUES ('8', 'Invitation');
INSERT INTO services VALUES ('9', 'Craft Projects');
INSERT INTO services VALUES ('10', 'Flyers');
INSERT INTO services VALUES ('11', 'OS1');
INSERT INTO services VALUES ('12', 'OS2');
INSERT INTO services VALUES ('13', 'Tarpaulin');

-- ----------------------------
-- Table structure for `settings`
-- ----------------------------
DROP TABLE IF EXISTS `settings`;
CREATE TABLE `settings` (
  `name` varchar(20) NOT NULL,
  `value` varchar(255) DEFAULT NULL,
  `enabled` smallint(1) NOT NULL DEFAULT '0',
  `type` varchar(1) DEFAULT NULL,
  `text_value` text,
  PRIMARY KEY (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of settings
-- ----------------------------
INSERT INTO settings VALUES ('MAX_LOGIN', '4', '1', 'S', null);
INSERT INTO settings VALUES ('LOGIN_TIMEOUT', '2', '1', 'S', null);
INSERT INTO settings VALUES ('REGISTRATION', null, '0', 'M', 'Hello FULLNAME,\n<br>\n<br>Your username is <b>USERNAME</b> and your password is <b>PASSWORD</b><br>Please click this link to activate your account\n<br>\n<b> <a href=\\\\\\\\\\\\\\\"http://dev.maxzando.com/admin/users/confirm/?param=HASH&uname=USERNAME\\\\\\\\\\\\\\\">http://dev.maxzando.com/admin/users/confirm/?param=HASH&uname=USERNAME</a></b>\n<br>\n<br>Thank you,\n<br>Maxzando Team');

-- ----------------------------
-- Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `salt` varchar(50) NOT NULL,
  `group_id` int(11) NOT NULL,
  `active` smallint(1) DEFAULT '0',
  `date_created` datetime NOT NULL,
  `added_by` int(11) DEFAULT NULL,
  `last_password_upd_dt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=95 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO users VALUES ('76', 'a', '939bacc47e0b292c48dde4cb13ca8e77efa9ef72', '#4s;+TN]`i*O_ML2UfhU?0fyJpY3rD(^&qD1\"s5(1Qn7REv|SW', '1', '1', '2011-04-02 06:37:00', '76', '2011-09-16 03:02:30');
INSERT INTO users VALUES ('91', 'poycutex', '1c8273bef017b55d43865d13891dc8a59be0fc4f', '26822c6d1d6054240c9aa99390a1d2a4', '1', '1', '2011-07-19 09:23:34', '76', '2011-07-19 09:23:34');

-- ----------------------------
-- Table structure for `user_profiles`
-- ----------------------------
DROP TABLE IF EXISTS `user_profiles`;
CREATE TABLE `user_profiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `firstname` varchar(60) DEFAULT 'changeme',
  `lastname` varchar(60) DEFAULT 'changeme',
  `email` varchar(60) DEFAULT 'changeme@email.com',
  `phone` varchar(20) DEFAULT '1',
  `mobile_phone` varchar(20) DEFAULT '1',
  `gender` varchar(20) DEFAULT 'Male',
  `date_birth` date DEFAULT NULL,
  `home_address` text,
  `country` varchar(80) DEFAULT 'Philippines',
  `homepage` varchar(100) DEFAULT NULL,
  `icq` varchar(60) DEFAULT NULL,
  `msn` varchar(60) DEFAULT NULL,
  `picture` varchar(255) DEFAULT NULL,
  `hide_numbers` tinyint(1) DEFAULT NULL,
  `default_language` varchar(5) DEFAULT NULL,
  `provisionscheme_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_user_id` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=93 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of user_profiles
-- ----------------------------
INSERT INTO user_profiles VALUES ('76', '76', 'superadmin', 'superadmin', 'superadmin@cleverbuy.com', '1234561', '1234561', 'Male', '2011-04-02', null, 'Philippines', '', '', '', null, null, null, '0');
INSERT INTO user_profiles VALUES ('91', '91', 'x', 'x', 'x@email.com', '1', '1', 'Male', '2011-05-09', null, 'Philippines', '', '', '', null, null, null, '1');

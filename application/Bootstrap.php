<?php


class Bootstrap extends Zend_Application_Bootstrap_Bootstrap {

    private static $_config; // config variable for config.ini

    /**
     * Initializes the Application Auto Loader
     */

    
    
    
    protected function _initAppAutoload() {
        $moduleLoader = new Zend_Application_Module_Autoloader(array(
                    'namespace' => '',
                    'basePath' => APPLICATION_PATH));
    }

    /**
     * Fire PHP logger
     */
    public function _initLogger() {
        $logger = new Zend_Log();
        $writer = new Zend_Log_Writer_Firebug();
        $logger->addWriter($writer);
        Zend_Registry::set('fp', $logger);
    }

    /**
     * Initializes the Action Helpers
     */
    protected function _initActionHelpers() {
        Zend_Controller_Action_HelperBroker::addHelper(
                new WebDav_Controller_Action_Helper_LayoutLoader());

        Zend_Controller_Action_HelperBroker::addHelper(
                new WebDav_Controller_Action_Helper_Fp());
    }

    /**
     * Initializes the Zend Configuration
     */
    protected function _initConfig() {
        self::$_config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', APPLICATION_ENV);
        Zend_Registry::set('config', self::$_config);
    }

    /**
     * Initializes the Database 
     */
    protected function _initDb() {
        $resource = $this->getPluginResource('db');
        $db = $resource->getDbAdapter();
        Zend_Db_Table::setDefaultAdapter($db);
        Zend_Registry::set('db', $db);
    }

    /**
     * Initializes the Doctrine
     */
    protected function _initDoctrine() {
        $this->getApplication()->getAutoloader()
                ->pushAutoloader(array('Doctrine', 'autoload'));
        spl_autoload_register(array('Doctrine', 'modelsAutoload'));

        $doctrineConfig = $this->getOption('doctrine');
        $manager = Doctrine_Manager::getInstance();
        $manager->setAttribute(Doctrine::ATTR_AUTO_ACCESSOR_OVERRIDE, true);
        $manager->setAttribute(
                Doctrine::ATTR_MODEL_LOADING, $doctrineConfig['model_autoloading']
        );

        Doctrine_Core::loadModels(@$doctrineConfig['models_path']);
        $conn = Doctrine_Manager::connection($doctrineConfig['dsn'], 'doctrine');
        $conn->setAttribute(Doctrine::ATTR_USE_NATIVE_ENUM, true);
        return $conn;
        
    }

    /**
     * Salinates GET POST COOKIE slashes if magic quotes is ON
     */
    protected function _initGPC_Salinate() {

        function stripslashes_deep($value) {
            $value = is_array($value) ?
                    array_map('stripslashes_deep', $value) :
                    stripslashes($value);
            return $value;
        }

        if (get_magic_quotes_gpc()) {
            foreach (array_keys($_GET) as $indexGET) {
                $_GET[$indexGET] = stripslashes_deep($_GET[$indexGET]);
            }

            foreach (array_keys($_POST) as $indexPOST) {
                $_POST[$indexPOST] = stripslashes_deep($_POST[$indexPOST]);
            }

            foreach (array_keys($_COOKIE) as $indexCOOKIE) {
                $_COOKIE[$indexCOOKIE] = stripslashes_deep($_COOKIE[$indexCOOKIE]);
            }
        }
    }

    protected function _initACL() {
//        $helper = new WebDav_Controller_Helper_Acl();
//        $helper->setRoles();
//        $helper->setResources();
//        $helper->setPrivileges();
//        $helper->setAcl();
//
//        $frontController = Zend_Controller_Front::getInstance();
//        $frontController->registerPlugin(new WebDav_Controller_Plugin_Acl());
    }

    public function _initLoggerOld() {
        $writer = new Zend_Log_Writer_Stream(self::$_config->logPath);
        $logger = new Zend_Log($writer);
        Zend_Registry::set('logger', $logger);
    }

    /**
     * Initializes Utility Scripts
     */
    protected function _initUtilityScripts() {
        include_once(APPLICATION_PATH . '/scripts/CBUtils.php');
    }
    
    protected function _initCache() {
        WebDav_Model_Cache::init(true, Zend_Registry::get('config')->cachePath, 604800, 'FILE');
        $cache = WebDav_Model_Cache::getInstance();
        Zend_Registry::set('cache', $cache);
    }
}


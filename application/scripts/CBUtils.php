<?php
/**
 * Utility Scripts for Clever Buy
 *
 * @author Ruel
 */

class CBUtils {

    private static function getWeek(){
        $ts = time();
        $year = date('o', $ts);
        $week = date('W', $ts);
        // print week for the current date
        for($i=1; $i<=7; $i++) {
                // timestamp from ISO week date format
                $ts = strtotime($year.'W'.$week.$i);
                $APP_DATE[$i]= date("Y-m-d", $ts);
        }
        return $APP_DATE;
    }

     public static function getAuthId(){
        return Zend_Auth::getInstance()->getStorage()->read()->id;
    }

    public static function findUserId($username) {
        $u = Doctrine_Core::getTable('Model_User')->findOneByUsername($username);
        return $u->id;
    }

    public static function getStartOfWeek(){
        $week = self::getWeek();
        return $week[1];
    }
    
    public static function getEndOfWeek(){
        $week = self::getWeek();
        return $week[7];
    }

    public static function isACLPermitted($resource) {
        $acl  =   Zend_Registry::get('acl');
        $role =   (( Zend_Auth::getInstance()->hasIdentity()) &&
                    (Zend_Auth::getInstance()->getStorage()->read()->role != '')) ?

            Zend_Auth::getInstance()->getStorage()->read()->role : 'guest';

        if (!$acl->has($resource)) return false;
        return $acl->isAllowed($role, $resource);
    }

    public static function print_array($mixed) {
        echo '<pre>';
        print_r($mixed);
        echo '</pre>';
    }

    public static function getMonthsArray(){
        return array('January','February','March','April','May','June','July','August','September','October','November','December');
    }

    public static function getMonthNumber($monthName){
        $monthArray = self::getMonthsArray();
        $key = array_search($monthName, $monthArray) + 1;
        return $key;
    }

    public static function uploadFile($file,$target_path){
        return move_uploaded_file($file['tmp_name'], $target_path);
    }
    
    public static function formatMoney($num,$dec=2){        
        return number_format($num, $dec);
    }
    
    public static function checkImageExtension($ext){
         $allowedExtensions = array(
             "jpg","png","jpeg","bmp","jpe","gif"
         );
         return in_array(strtolower($ext), $allowedExtensions);
    }
    
    public static function getTimeLength($mins){
        $loginlength = "";
        if($mins < 60){
            $loginlength = "$mins minutes ago";
        } else {
            //convert to hours
            $hours = floor($mins / 60);
            if($hours == 1){
                $loginlength = "$hours hour ago";
            }else if($hours > 1 && $hours < 24){
                $loginlength = "$hours hours ago";
            }else{
                //convert to days
                $days = floor($hours/24);
                $loginlength = ($days == 1) ? "1 day ago":"$days days ago";
            }
        }
        return $loginlength;
    }
    
    public static function getStartOfMonth(){
        return $firstdayofmonth = date("Y-m-01");
    }
    
    public static function getLastDayOfMonth(){
        $newdate = strtotime('+1 month',strtotime(self::getStartOfMonth()));
        $newdate = strtotime('-1 day',$newdate);
        return date('Y-m-d',$newdate);
    }
    
    public static function getStartOfPreviousMonth(){
        $newdate = strtotime('-1 month',strtotime(self::getStartOfMonth()));
        return date('Y-m-d',$newdate);
    }
    
    public static function getLastDayOfPreviousMonth(){
        $newdate = strtotime('-1 day',strtotime(self::getStartOfMonth()));
        return date('Y-m-d',$newdate);
    }
}
?>

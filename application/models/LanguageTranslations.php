<?php
class Model_LanguageTranslations extends Model_Base_LanguageTranslations
{
    public function test()
    {
        $q = Doctrine_Query::create()
                ->select('T.translation,K.keyword,C.alias,G.name')
                ->from(__CLASS__.' T')
                ->leftjoin('T.Keyword K')
                ->leftJoin('K.LangGroup G')
                ->leftjoin('T.Country C')
                ->orderBy("K.keyword ASC");
        $records = $q->execute();

        foreach($records as $record){
            $data[$record->Country->alias][][$record->Keyword->keyword] = array(
                $record->keyword_id,
                $record->Keyword->keyword,
                $record->translation,
                $record->Keyword->LangGroup->id,
                $record->Keyword->LangGroup->name);
        }

        $languages =  array_keys($data);
        $i = 0 ;

        foreach($data['us'] as $key=>$value) {
                foreach($value as $k=>$v){
                    foreach($languages as $lang=>$alias)
                    {
                        if($data[$alias][$i])
                        foreach($data[$alias][$i] as $langkey=>$langvalue)
                        {
                            $rows[$i]['kid']  =  $langvalue[0];
                            $rows[$i]['kw']   =  $langvalue[1];
                            $rows[$i]['group_id'] =  $langvalue[3];
                            $rows[$i]['group_name'] =  $langvalue[4];
                            $rows[$i][$alias] =  $langvalue[2];
                        }
                    }
                }
            $i++;
        }
        return $rows ;
    }

    public function getTranslationRecords($groupId = null, $keywordId = null) {

        $q = Doctrine_Query::create()
                ->select('T.translation,K.keyword,C.alias,G.name')
                ->from(__CLASS__.' T')
                ->leftjoin('T.Keyword K')
                ->leftJoin('K.LangGroup G')
                ->innerJoin('T.Country C');

        if($keywordId)
            $q = $q->where('K.id = ?', array($keywordId));
        else if($groupId)
            $q = $q->where('K.group_id = ?', array($groupId));

        $q = $q->orderBy("K.keyword,G.name");
        $records = $q->execute();

        foreach($records as $record){
            $data[$record->Country->alias][][$record->Keyword->keyword] = array(
                $record->keyword_id,
                $record->Keyword->keyword,
                $record->translation,
                $record->Keyword->LangGroup->id,
                $record->Keyword->LangGroup->name);
        }

        $languages =  array_keys($data);
        $i = 0 ;

        foreach($data['us'] as $key=>$value) {
                foreach($value as $k=>$v){
                    foreach($languages as $lang=>$alias)
                    {
                        if($data[$alias][$i])
                        foreach($data[$alias][$i] as $langkey=>$langvalue)
                        {
                            $rows[$i]['kid']  =  $langvalue[0];
                            $rows[$i]['kw']   =  $langvalue[1];
                            $rows[$i]['group_id'] =  $langvalue[3];
                            $rows[$i]['group_name'] =  $langvalue[4];
                            $rows[$i][$alias] =  $langvalue[2];
                        }
                    }                  
                }
            $i++;
        }
        return $rows ;
    }

    
    public function createColumns() {
        $q = Doctrine_Query::create()
                ->select('C.alias,C.name')
                ->from('Model_Countries C');

        $columns    =   $q->execute(array(),Doctrine_Core::HYDRATE_ARRAY);

        $extFields[] = array(
            'id'        =>  'kid',
            'dataIndex' =>  'kid',
            'header'    =>  'ID',
            'width'     =>  40,
            'sortable'  =>  true
        );

        $extFields[] = array(
            'id'        =>  'group_id',
            'dataIndex' =>  'group_id',
            'header'    =>  'Group ID',
            'hidden'    =>  true,
            'sortable'  =>  true
        );

        $extFields[] = array(
            'id'        =>  'group_name',
            'dataIndex' =>  'group_name',
            'header'    =>  'Group',
//            'editor'    =>  array(
//                'xtype' => 'combo',
//                'allowBlank' => false,
//                'store' => array(
//                    'xtype' => 'jsonstore',
//                    'url' => '/admin/combo/translation-groups',
//                    'root' => 'records',
//                    'fields' => array('id', 'name')
//                ),
//                'displayField' => 'name',
//                'valueField' => 'id',
//                'hiddenValue' => 'id',
//                'hiddenName' => 'gid',
//                'triggerAction' => 'all'
//            ),
            'sortable'  =>  true,
            'width'     =>  60
        );
        
        $extFields[] = array(
            'id'        =>  'kw',
            'dataIndex' =>  'kw',
            'header'    =>  'Keyword',
            'editor'    =>  array('xtype'=>'textfield','allowBlank'=>false),
            'sortable'  =>  true
        );

        if ($columns) {
            foreach($columns as $key=>$column) {
                $extFields[] = array (
                    'dataIndex' =>  $column['alias'],
                    'header'    =>  $column['name'],
                    'editor'    =>  array (
                                        'xtype'         =>  'textfield',
                                        'allowBlank'    =>  $column['alias']==='us'?false:true
                                    ),
                    'sortable'=>true
                );
            }
        }
        
        return $extFields ? $extFields : array();
    }
    
    public function createTable() {

        $columns = $this->createColumns();

        foreach ($columns as $column=>$field) {
            $fields[] = $field['dataIndex'] ;
        }

        $gridProperties =   array (
            'height'            =>  370,
            //'frame'             =>  true,
            'columns'           =>  $columns,
            'autoWidth'         =>  true,
            'autoExpandColumn'  =>  'kw',
            'loadMask'          =>  true,
            'fields'            =>  $fields
        );
        return $gridProperties;
    }

    
}
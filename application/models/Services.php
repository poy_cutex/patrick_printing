<?php
class Model_Services extends Model_Base_Services {
    public static function listAll($serviceId=10){
         $q = Doctrine_Query::create()
                ->select()
                 ->from('Model_Services s')
                 ->where("s.id > $serviceId")
                 ->orderby('s.id asc');

         $count = $q->count();
         $servicelist = $q->execute();
         return array("count"=>$count,"list"=>$servicelist->toArray());
     }
     
     public static function persist($record){
          if(isset($record["id"])){
               self::update($record);
          }else {
               self ::create($record);
          }
     }
     
     private static function create($record) {
        $service = new Model_Services();
        $service->set('serviceName', $record['serviceName']); 
        $service->save();
    }
    
    private static function update($record){
        $service = Doctrine::getTable(__CLASS__)->find($record->id);
        $service->set('serviceName', $record->serviceName);
        $service->save();
    }
    
    public static function del($recordId){
        $service = Doctrine::getTable(__CLASS__)->find(json_decode($recordId));
        $service->delete();
    }
} 
 
<?php
class Model_Countries extends Model_Base_Countries
{
    public static function getIdByAlias($alias){
        $t = Doctrine::getTable(__CLASS__);
        $c = $t->findOneBy('alias',$alias);
        return $c->id;
    }
}

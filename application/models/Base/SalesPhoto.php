<?php

abstract class Model_Base_SalesPhoto extends Doctrine_Record {

    public function setTableDefinition() {

        $this->setTableName('salesphoto');
        
        $this->hasColumn('customerId', 'integer', 11, array('type' => 'integer', 'length' => '11'));
        $this->hasColumn('amount', 'float', array('type' => 'integer'));
        $this->hasColumn('photoboothId', 'integer', 11, array('type' => 'integer', 'length' => '11'));
       
    }

    public function setUp() {
        parent::setUp();
        $this->actAs('Timestampable'); 
        $this->hasOne('Model_Customers as Customer', array('local' => 'customerId', 'foreign' => 'id'));
        $this->hasOne('Model_Photobooth as Photobooth', array('local' => 'photoboothId', 'foreign' => 'id'));
    }

}
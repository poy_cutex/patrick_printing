<?php
class Model_Base_LoginJNL extends Doctrine_Record {

    public function setTableDefinition() {

        $this->setTableName('login_jnl');
        $this->hasColumn('user_id','int',11,    
                array('type' => 'int', 'length' => '11'));
        $this->hasColumn('login_dt','datetime');
    }

    public function setUp() {
        parent::setUp();
    }

}


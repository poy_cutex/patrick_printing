<?php

class Model_Base_User extends Doctrine_Record {

    public function setTableDefinition() {

        $this->setTableName('users');

        $this->hasColumn('username', 'string', 255, array('type' => 'string', 'length' => '255'));
        $this->hasColumn('password', 'string', 255, array('type' => 'string', 'length' => '255'));
        $this->hasColumn('salt', 'string', 255, array('type' => 'string', 'length' => '255'));
        $this->hasColumn('group_id', 'integer', null, array('type' => 'integer'));
        $this->hasColumn('active', 'integer', null, array('type' => 'integer'));
        $this->hasColumn('date_created', 'date', null, array('type' => 'date'));
        $this->hasColumn('added_by', 'integer', null, array('type' => 'integer'));
        $this->hasColumn('last_password_upd_dt', 'timestamp');
        $this->hasColumn('firstname', 'string', 100, array('type' => 'string', 'length' => '100'));
        $this->hasColumn('middlename', 'string', 100, array('type' => 'string', 'length' => '100'));
        $this->hasColumn('lastname', 'string', 100, array('type' => 'string', 'length' => '100'));
        $this->hasColumn('address', 'string', 200, array('type' => 'string', 'length' => '200'));
        $this->hasColumn('email', 'string', 100, array('type' => 'string', 'length' => '100'));
        $this->hasColumn('phone', 'string', 30, array('type' => 'string', 'length' => '30'));
    }

    public function setUp() {
        parent::setUp();
        $this->hasOne('Model_Group as Groups', array('local' => 'group_id', 'foreign' => 'id'));
    }

}
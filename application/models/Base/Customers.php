<?php

abstract class Model_Base_Customers extends Doctrine_Record {

    public function setTableDefinition() {

        $this->setTableName('customers');
        
        $this->hasColumn('firstname', 'string', 100, array('type' => 'string', 'length' => '100'));
        $this->hasColumn('middlename', 'string', 100, array('type' => 'string', 'length' => '100'));
        $this->hasColumn('lastname', 'string', 100, array('type' => 'string', 'length' => '100'));
        $this->hasColumn('address', 'string', 200, array('type' => 'string', 'length' => '200'));
        $this->hasColumn('email', 'string', 100, array('type' => 'string', 'length' => '100'));
        $this->hasColumn('phone', 'string', 30,null, array('type' => 'string', 'length' => '30'));
        $this->hasColumn('username', 'string', 30, array('type' => 'string', 'length' => '30'));
        $this->hasColumn('password', 'string', 30, array('type' => 'string', 'length' => '30'));
    }

    public function setUp() {
        parent::setUp();
        $this->actAs('Timestampable'); 
    }

}
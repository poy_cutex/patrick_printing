<?php

class Model_Base_JobOrder extends Doctrine_Record {
    
    public function setTableDefinition() {
        
        $this->setTableName('joborder');
        
        $this->hasColumn('customerId', 'integer', array('type' => 'integer'));
        $this->hasColumn('totalAmount', 'float', array('type' => 'float'));
        $this->hasColumn('datePickup', 'date', array('type' => 'date'));
        $this->hasColumn('serviceId', 'integer', array('type' => 'integer'));
        $this->hasColumn('approveId', 'integer', array('type' => 'integer'));
        $this->hasColumn('workId', 'integer', array('type' => 'integer'));
        $this->hasColumn('downPayment', 'float', array('type' => 'float'));
        $this->hasColumn('balance', 'float', array('type' => 'float'));
        $this->hasColumn('amountPaid', 'float', array('type' => 'float'));
        $this->hasColumn('jobdescription', 'string', 200, array('type' => 'string', 'lenght' => '200'));
        $this->hasColumn('status', 'string', 50, array('type' => 'string','lenght' => '50'));
    }
    
     public function setUp() {
        parent::setUp();
        $this->actAs('Timestampable');
        $this->hasOne('Model_Customers as Customer', array('local' => 'customerId', 'foreign' => 'id'));
        $this->hasOne('Model_User as Approve', array('local' => 'approveId', 'foreign' => 'id'));
        $this->hasOne('Model_User as Work', array('local' => 'workId', 'foreign' => 'id'));
        $this->hasOne('Model_Services as Service', array('local' => 'serviceId', 'foreign' => 'id'));
    }
    
}


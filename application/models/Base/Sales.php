<?php

abstract class Model_Base_Sales extends Doctrine_Record {

    public function setTableDefinition() {

        $this->setTableName('sales');
        
        $this->hasColumn('customerId', 'integer', 11, array('type' => 'integer', 'length' => '11'));
        $this->hasColumn('serviceId', 'integer', 11, array('type' => 'integer', 'length' => '11'));
        $this->hasColumn('amount', 'float', array('type' => 'integer'));
        $this->hasColumn('jobOrderId', 'integer', 11, array('type' => 'integer', 'length' => '11'));
       
    }

    public function setUp() {
        parent::setUp();
        $this->actAs('Timestampable');
        $this->hasOne('Model_Customers as Customer', array('local' => 'customerId', 'foreign' => 'id'));
        $this->hasOne('Model_Services as Service', array('local' => 'serviceId', 'foreign' => 'id'));
        $this->hasOne('Model_JobOrder as JobOrder', array('local' => 'jobOrderId', 'foreign' => 'id'));
    }

}
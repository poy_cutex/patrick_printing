<?php

class Model_Base_Group extends Doctrine_Record
{
    public function setTableDefinition() {        
        $this->setTableName('groups');        
        $this->hasColumn(   'name'                           ,   'string'    ,  255 ,   array(  'type'  =>  'string'    ,   'length'    =>  '255'));
        $this->hasColumn(   'description'                    ,   'string'    ,  255 ,   array(  'type'  =>  'string'    ,   'length'    =>  '255'));
        $this->hasColumn(   'password_reset_interval_days'   ,   'integer'   ,  null,   array(  'type'  =>  'integer'));
        $this->hasColumn(   'password_reset_last_upd_dt'     ,   'date'      ,  1   ,   array(  'type' => 'date'));
    }

    public function setUp() {
        parent::setUp();
        $this->hasMany ('Model_User as Users',  array(  'local' =>  'id', 'foreign' => 'group_id'));
    }
}
<?php
class Model_Base_LanguageTranslations extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('lang_translation');

        $this->hasColumn('keyword_id', 'integer', 11, array(
             'type' => 'integer',
             'length' => '11',
             ));
        
        $this->hasColumn('country_id', 'integer', 11, array(
             'type' => 'integer',
             'length' => '11',
             ));            
        $this->hasColumn('translation', 'string', 255, array(
             'type' => 'string',
             'length' => '255',
             ));
    }
    public function setUp()
    {
        parent::setUp();
        $this->hasOne('Model_Base_LanguageKeywords as Keyword',array(
            'local' => 'keyword_id',
            'foreign'=>'id'
        ));          
        $this->hasOne('Model_Base_Countries as Country',array(
            'local' => 'country_id',
            'foreign'=>'id'
        ));        
    }
}

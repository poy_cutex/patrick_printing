<?php

class Model_Base_Countries extends Doctrine_Record
{
    public function setTableDefinition()
    {
        $this->setTableName('countries');

        $this->hasColumn('name', 'string', 100, array(
             'type' => 'string',
             'length' => '100',
             ));
        $this->hasColumn('alias', 'string', 2, array(
             'type' => 'string',
             'length' => '2',
             ));
    }
    public function setUp()
    {
        parent::setUp();
    }
}

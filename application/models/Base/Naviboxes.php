<?php
/**
 * Description of Uploads
 * @author edd
 */
abstract class Model_Base_Naviboxes extends Doctrine_Record
{
    public function setTableDefinition()
    {
        $this->setTableName('naviboxes');
         
        $this->hasColumn('name', 'string', 100, array(
             'type' => 'string',
             'length' => 100
             ));
        $this->hasColumn('position', 'string', 1, array(
             'type' => 'string',
             'length' => 1
             ));
        $this->hasColumn('order', 'integer', 2, array(
             'type' => 'integer',
             'length' => 2
             ));
        $this->hasColumn('type', 'strin',50, array(
             'type' => 'string',
             'length' => 50
             ));
        $this->hasColumn('xtype', 'strin',100, array(
             'type' => 'string',
             'length' => 100
             ));
        $this->hasColumn('active_indicator', 'integer', 1, array(
             'type' => 'integer',
             'length' => 1
             ));
    }
}
?>
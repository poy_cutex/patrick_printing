<?php
class Model_Base_LanguageGroup extends Doctrine_Record
{

    public function setTableDefinition()
    {
        $this->setTableName('lang_group');
        $this->hasColumn('name', 'string', 255, array(
             'type' => 'string',
             'length' => '255',
             ));
    }
    public function setUp()
    {
        parent::setUp();
        $this->hasMany('Model_Base_LanguageKeywords as Keyword',array(
            'local' => 'id',
            'foreign'=>'group_id'
        ));   
    }
}

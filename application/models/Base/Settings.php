<?php

class Model_Base_Settings extends Doctrine_Record
{
    public function setTableDefinition()
    {
        $this->setTableName('settings');

        $this->hasColumn('name', 'string', 20,
                array('type' => 'string', 'length' => 20, 'primary' => true));
        $this->hasColumn('value', 'string', 255,
                array('type' => 'string', 'length' => 255));
        $this->hasColumn('enabled', 'integer', 1,
                array('type' => 'integer', 'length' => 1));
        $this->hasColumn('type', 'string', 1,
                array('type' => 'string', 'length' => 1));
        $this->hasColumn('text_value', 'string', 1000,
                array('type' => 'string', 'length' => 1000));
    }
}
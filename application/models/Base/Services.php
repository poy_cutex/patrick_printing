<?php
abstract class Model_Base_Services extends Doctrine_Record {

    public function setTableDefinition() {

        $this->setTableName('services');

        $this->hasColumn('serviceName','string', 200,array('type' => 'string','length' => '200'));
    }
    public function setUp() {
        parent::setUp();
    }
}

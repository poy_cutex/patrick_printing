<?php
abstract class Model_Base_Photobooth extends Doctrine_Record {

    public function setTableDefinition() {

        $this->setTableName('photobooth');

        $this->hasColumn('customerId', 'integer', array('type' => 'integer', 'length' => '11'));
        $this->hasColumn('event_date', 'date', array('type' => 'date'));
        $this->hasColumn('setup_time', 'time', array('type' => 'time'));
        $this->hasColumn('start_time', 'time', array('type' => 'time'));
        $this->hasColumn('end_time', 'time', array('type' => 'time'));
        $this->hasColumn('eventType', 'string', 100, array('type' => 'string','length' => '100'));
        $this->hasColumn('theme', 'string', 100, array('type' => 'string','length' => '100'));
        $this->hasColumn('title', 'string', 100, array('type' => 'string','length' => '100'));
        $this->hasColumn('amount_due', 'float', array('type' => 'float'));
        $this->hasColumn('downPayment', 'float', array('type' => 'float'));
        $this->hasColumn('balance', 'float', array('type' => 'float'));
        $this->hasColumn('amountPaid', 'float', array('type' => 'float'));
        $this->hasColumn('transactionId', 'string', array('type' => 'string'));
        $this->hasColumn('userId', 'integer', array('type' => 'integer'));
        $this->hasColumn('venue', 'string', 255, array('type' => 'string','length' => '255'));
    }
    public function setUp() {
        parent::setUp();
        $this->actAs('Timestampable'); 
        $this->hasOne('Model_Customers as Customer', array('local' => 'customerId', 'foreign' => 'id'));
        $this->hasOne('Model_User as User', array('local' => 'userId', 'foreign' => 'id'));
    }
}
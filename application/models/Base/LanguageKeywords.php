<?php
class Model_Base_LanguageKeywords extends Doctrine_Record
{
    public function setTableDefinition()
    {
        $this->setTableName('lang_keywords');

        $this->hasColumn('keyword', 'string', 100, array(
             'type' => 'string',
             'length' => '100',
             ));
        $this->hasColumn('group_id', 'integer', 11, array(
             'type' => 'integer',
             'length' => '11',
             ));
    }
    public function setUp()
    {
        parent::setUp();
        //$this->hasMany('Model_Base_LanguageTranslations as Translation',array(
        //    'local' => 'id',
        //    'foreign'=>'keyword_id'
        //));
        $this->hasOne('Model_Base_LanguageTranslations as Translation',array(
            'local' => 'id',
            'foreign'=>'keyword_id'
        ));
        $this->hasOne('Model_Base_LanguageGroup as LangGroup',array(
            'local' => 'group_id',
            'foreign'=>'id'
        ));
    }
  
}

<?php

abstract class Model_Base_Inventory extends Doctrine_Record {

    public function setTableDefinition() {

        $this->setTableName('printinginventory');
        
        $this->hasColumn('itemName', 'string', 200, array('type' => 'string', 'length' => '200'));
        $this->hasColumn('quantity', 'integer', array('type' => 'integer'));
        $this->hasColumn('cost', 'float', array('type' => 'float'));
    }

    public function setUp() {
        parent::setUp();
        $this->actAs('Timestampable'); 
    }

}
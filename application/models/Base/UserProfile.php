<?php

abstract class Model_Base_UserProfile extends Doctrine_Record
{
    public function setTableDefinition() {
        
        $this->setTableName('user_profiles');

        $this->hasColumn(   'user_id'       ,   'integer'   ,   null    ,   array(  'type'  =>  'integer'));
        $this->hasColumn(   'firstname'     ,   'string'    ,   255     ,   array(  'type'  =>  'string'    ,   'length'    =>  '255'));
        $this->hasColumn(   'lastname'      ,   'string'    ,   255     ,   array(  'type'  =>  'string'    ,   'length'    =>  '255'));
        $this->hasColumn(   'email'         ,   'string'    ,   255     ,   array(  'type'  =>  'string'    ,   'length'    =>  '255'));
        $this->hasColumn(   'phone'         ,   'string'    ,   255     ,   array(  'type'  =>  'string'    ,   'length'    =>  '255'));
        $this->hasColumn(   'mobile_phone'  ,   'string'    ,   255     ,   array(  'type'  =>  'string'    ,   'length'    =>  '255'));
        $this->hasColumn(   'gender'        ,   'string'    ,   255     ,   array(  'type'  =>  'string'    ,   'length'    =>  '255'));
        $this->hasColumn(   'date_birth'    ,   'date'      ,   null    ,   array(  'type'  =>  'date'));
        $this->hasColumn(   'home_address'  ,   'string'    ,   null    ,   array(  'type'  =>  'string'    ,   'length'    =>  '1000'));
        $this->hasColumn(   'country'       ,   'string'    ,   255     ,   array(  'type'  =>  'string'    ,   'length'    =>  '255'));
        $this->hasColumn(   'homepage'      ,   'string'    ,   255     ,   array(  'type'  =>  'string'    ,   'length'    =>  '255'));
        $this->hasColumn(   'icq'           ,   'string'    ,   255     ,   array(  'type'  =>  'string'    ,   'length'    =>  '255'));
        $this->hasColumn(   'msn'           ,   'string'    ,   255     ,   array(  'type'  =>  'string'    ,   'length'    =>  '255'));
        $this->hasColumn(   'picture'       ,   'string'    ,   255     ,   array(  'type'  =>  'string'    ,   'length'    =>  '255'));
        $this->hasColumn(   'hide_numbers'  ,   'integer'   ,   null    ,   array(  'type'  =>  'integer'   ,   'length'    =>  '1'));
        $this->hasColumn(   'default_language', 'integer'   ,   null    ,   array(  'type'  =>  'integer'));
        $this->hasColumn(   'provisionscheme_id', 'integer'   ,   null    ,   array(  'type'  =>  'integer'));
        $this->hasColumn(   'comments'      ,   'string'    ,   500     ,   array(  'type'  =>  'string'    ,   'length'    =>  '500'));    
    }

    public function setUp() {
        parent::setUp();
        $this->hasOne(  'Model_User as User'    ,   array(  'local' =>  'user_id'   ,   'foreign'   =>  'id'));
        $this->hasOne('Model_UserProfilesProvisionSchemes as ProvisionScheme',   
            array('local'=>'user_id',
               'foreign'=>'user_id'));
        
        
    }
}

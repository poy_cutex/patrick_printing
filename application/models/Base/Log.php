<?php
class Model_Base_Log extends Doctrine_Record {

    public function setTableDefinition() {

        $this->setTableName('cb_logs');
        $this->hasColumn('message','string',100,    
                array('type' => 'string', 'length' => '255'));
    }

    public function setUp() {
        parent::setUp();
    }

}


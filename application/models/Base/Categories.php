<?php
abstract class Model_Base_Categories extends Doctrine_Record {

    public function setTableDefinition() {

        $this->setTableName('cb_categories');

        $this->hasColumn('name',
                'string', 100,
                array('type' => 'string',
                      'length' => '100'));
    }
    public function setUp()
    {
        $this->hasOne('Model_Leads as Lead', array(
                'local' => 'id',
                'foreign' => 'category_id'
            )
        );
    }
}

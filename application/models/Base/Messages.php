<?php
abstract class Model_Base_Messages extends Doctrine_Record {

    public function setTableDefinition() {

        $this->setTableName('messages');

        $this->hasColumn('author','string', 100,array('type' => 'string','length' => '100'));
        $this->hasColumn('subject','string', 100,array('type' => 'string','length' => '100'));
        $this->hasColumn('email','string', 100,array('type' => 'string','length' => '100'));
        $this->hasColumn('message','string', 500,array('type' => 'string','length' => '500'));
        $this->hasColumn('isRead','integer', 1,array('type' => 'integer','length' => '1'));
    }
    public function setUp() {
        parent::setUp();
        $this->actAs('Timestampable'); 
    }
}

<?php
/**
 * Description of Uploads
 * @author edd
 */
abstract class Model_Base_GroupsNaviboxes extends Doctrine_Record
{
    public function setTableDefinition()
    {
        $this->setTableName('groups_naviboxes');
         
        $this->hasColumn('group_id', 'integer', 11, array(
             'type' => 'integer',
             'length' => 11
             ));
        $this->hasColumn('navibox_id', 'integer', 11, array(
             'type' => 'integer',
             'length' => 11
             ));            
    }
    public function setUp()
    {
        $this->hasOne('Model_Naviboxes as Naviboxes',array(
            'local' => 'navibox_id',
            'foreign'=>'id'
        ));
    }
}
?>
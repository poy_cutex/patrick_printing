<?php
class Model_Settings extends Model_Base_Settings
{
    public static function fetchAll()
    {
        $fields = Doctrine::getTable('Model_Base_Settings')->findAll();
        return $fields->toArray();
    }

    public static function saveSetting($data)
    {
        $settingsModel = Doctrine::getTable('Model_Settings')->find($data['name']);
        $settingsModel->value = $data['value'];
        $settingsModel->enabled = $data['enabled'];
        $settingsModel->text_value = $data['text_value'];
        $settingsModel->save();
    }

    public static function findSetting($name)
    {
        $tmp = Doctrine::getTable('Model_Settings')->find($name);
        return ($tmp ? $tmp->toArray() : null);
    }
}
<?php
class Model_Categories extends Model_Base_Categories
{
    public static function listAll(){
         $q = Doctrine_Query::create()
                ->select()
                 ->from('Model_Categories c')
                 ->orderby('c.name asc');

         $count = $q->count();
         $categorylist = $q->execute();
         return array("count"=>$count,"list"=>$categorylist->toArray());
     }
} 
 
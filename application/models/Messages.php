<?php
class Model_Messages extends Model_Base_Messages {
    public static function listAll($read=0){
         $q = Doctrine_Query::create()
                ->select("m.*, date_format(created_at,'%b %e, %Y %a') senddate")
                 ->from('Model_Messages m')
                 ->where("m.isRead = $read")
                 ->orderby('m.created_at asc');

         $count = $q->count();
         $messagelist = $q->execute();
         return array("count"=>$count,"list"=>$messagelist->toArray());
     }
         
     public static function create($record) {
        $message = new Model_Messages();
        $message->set('author', $record->author);
        $message->set('subject', $record->subject);
        $message->set('email', $record->email);
        $message->set('message', $record->message);
        $message->save();
    }
    
    public static function readMessage($recordId){
        $message = Doctrine::getTable(__CLASS__)->find(json_decode($recordId));
        $message->set('isRead', 1);
        $message->save();
    }
    
    public static function del($recordId){
        $message = Doctrine::getTable(__CLASS__)->find(json_decode($recordId));
        $message->delete();
    }
} 
 
<?php
class Model_Sales extends Model_Base_Sales {
   public static function listAll(){
         $q = Doctrine_Query::create()
                ->select("s.*,date_format(created_at,'%b %e, %Y %a') date_trans, concat(c.firstname,' ', c.middlename,' ',c.lastname ) custname,serv.serviceName as servicename")
                 ->from('Model_Sales s')
                 ->innerJoin('s.Customer c')
                 ->innerJoin('s.Service serv')
                 ->orderby('s.id asc');

         $count = $q->count();
         $saleslist = $q->execute();
         return array("count"=>$count,"list"=>$saleslist->toArray());
     }
     
          
     public static function create($record) {
        $sales = new Model_Sales();
        $sales->set('customerId', $record['customerId']);
        $sales->set('serviceId', $record['serviceId']);
        $sales->set('amount', $record['amount']);
        $sales->set('jobOrderId', $record['jobOrderId']);
        $sales->save();
    }   
        
     
    
}

 
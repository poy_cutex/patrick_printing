<?php
class Model_User extends Model_Base_User
{
    public static function listAll(){
         $q = Doctrine_Query::create()
                ->select("u.*,g.name as groupname,concat(u.firstname,' ', u.middlename,' ',u.lastname ) fullname")
                 ->from('Model_User u')
                 ->innerJoin("u.Groups g")
                 ->orderby('u.id asc');
         $count = $q->count();
         $userlist = $q->execute();
         return array("count"=>$count,"list"=>$userlist->toArray());
     }
     
     public static function listAllUsersCombo(){
         $q = Doctrine_Query::create()
                ->select("u.id,concat(u.firstname,' ', u.middlename,' ',u.lastname ) name")
                 ->from('Model_Users u')
                 ->orderby('u.id asc');
         $count = $q->count();
         $userlist = $q->execute();
         return array("count"=>$count,"list"=>$userlist->toArray());
     }
     
     public static function persist($record){
         $res = 0;
          if($record["id"] > 0){
               $res = self::update($record);
          }else {
              $usernameOk = self::checkUsername($record['username']);
              if ($usernameOk) {
                  $res = self::create($record);
              } else {
                  $res = -1;
              }
          }
          return $res;
     }
     
     private static function checkUsername($str){
         $user = Doctrine::getTable('Model_User')->findOneByUsername($str);
         if ($user) {
            return false;
         }
         return true;
     }
     
     private static function create($record) {
         
          $user = new Model_User();
          $user->set('firstname', $record['firstname']);
          $user->set('middlename', $record['middlename']);
          $user->set('lastname', $record['lastname']);
          $user->set('address', $record['address']);
          $user->set('email', $record['email']);
          $user->set('phone', $record['phone']);
          $user->set('username', $record['username']);
          
          $clearpassword = $record['password'];
          $salt   = md5(self::_createSalt());
          $hash = $clearpassword.$salt;
          $pwd = sha1($hash);
          $user->set('salt', $salt);
          $user->set('date_created',date('Y-m-d'));
          $user->set('added_by',$record['addedId']);
          $user->set('password', $pwd);
          $groupId = Model_Group::getGroupId($record['groupname']);
          $user->set('group_id',$groupId);
          $user->set('active',1);
          $user->save();
          return $res = 1;
    }
    
     private static function _createSalt(){
          $dynamicSalt = '';
          for ($i = 0; $i < 50; $i++) {
              $dynamicSalt .= chr(rand(33, 126));
          }
          return $dynamicSalt;
     }
    
     private static function update($record){
        $user = Doctrine::getTable(__CLASS__)->find($record['id']);
        $user->set('firstname', $record['firstname']);
        $user->set('middlename', $record['middlename']);
        $user->set('lastname', $record['lastname']);
        $user->set('address', $record['address']);
        $user->set('email', $record['email']);
        $user->set('phone', $record['phone']);
        $groupId = Model_Group::getGroupId($record['groupname']);
        $user->set('group_id',$groupId);
        $user->save();
        return 1;
    }
    
    public static function changePass($record){
          $user = Doctrine::getTable(__CLASS__)->find($record['id']);
          $clearpassword = $record['pwd'];
          $salt   = md5(self::_createSalt());
          $hash = $clearpassword.$salt;
          $pwd = sha1($hash);
          $user->set('salt', $salt);
          $user->set('password', $pwd);          
          $user->save();
       
    }
    public static function del($recordId){
        $user = Doctrine::getTable(__CLASS__)->find(json_decode($recordId));
        $user->delete();
    }
    
    public static function getUsers($where=null){
        $users = Doctrine::getTable('Model_User')->findAll();
        return $users->toArray();
    }
    
     public static function getUser($userID){
        $q = Doctrine_Query::create()
             ->select('u.id,u.username as username,g.name as groupname')
             ->from(__CLASS__.' u')
             ->innerJoin('u.Groups g')
             ->where("u.id = $userID");
        $res = $q->execute(array(),DOCTRINE::HYDRATE_ARRAY); 
        return $res[0];
    } 
}

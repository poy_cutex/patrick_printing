<?php
class Model_SalesPhoto extends Model_Base_SalesPhoto {
   public static function listAll(){
         $q = Doctrine_Query::create()
                ->select("s.*,date_format(created_at,'%b %e, %Y %a') date_trans, concat(c.firstname,' ', c.middlename,' ',c.lastname ) custname")
                 ->from('Model_SalesPhoto s')
                 ->innerJoin('s.Customer c')
                 ->orderby('s.id asc');

         $count = $q->count();
         $salesphotolist = $q->execute();
         return array("count"=>$count,"list"=>$salesphotolist->toArray());
     }

     public static function create($record) {
        $salesphoto = new Model_SalesPhoto();
        $salesphoto->set('customerId', $record['customerId']);
        $salesphoto->set('amount', $record['amount']);
        $salesphoto->set('photoboothId', $record['photoboothId']);
        $salesphoto->save();
    }
    
}
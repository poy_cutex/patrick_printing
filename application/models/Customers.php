<?php
class Model_Customers extends Model_Base_Customers {
    public static function listAll(){
         $q = Doctrine_Query::create()
                ->select()
                 ->from('Model_Customers c')
                 ->orderby('c.id asc');
         $count = $q->count();
         $customerlist = $q->execute();
         return array("count"=>$count,"list"=>$customerlist->toArray());
     }
     
     public static function listAllCustomerCombo(){
         $q = Doctrine_Query::create()
                ->select("c.id,concat(c.firstname,' ', c.middlename,' ',c.lastname ) name")
                 ->from('Model_Customers c')
                 ->orderby('c.id asc');
         $count = $q->count();
         $customerlist = $q->execute();
         return array("count"=>$count,"list"=>$customerlist->toArray());
     }
     
     public static function persist($record){
          if($record["id"] > 0){
               self::update($record);
          }else {
               self ::create($record);
          }
     }
     
     private static function create($record) {
        $customer = new Model_Customers();
        $customer->set('firstname', $record['firstname']);
        $customer->set('middlename', $record['middlename']);
        $customer->set('lastname', $record['lastname']);
        $customer->set('address', $record['address']);
        $customer->set('email', $record['email']);
        $customer->set('phone', $record['phone']);
        $customer->set('username', $record['firstname']);
        $customer->set('password', '123456');
        $customer->save();
    }
    
     private static function update($record){
        $customer = Doctrine::getTable(__CLASS__)->find($record["id"]);
        $customer->set('firstname', $record['firstname']);
        $customer->set('middlename', $record['middlename']);
        $customer->set('lastname', $record['lastname']);
        $customer->set('address', $record['address']);
        $customer->set('email', $record['email']);
        $customer->set('phone', $record['phone']);
        $customer->save();
    }
    
    public static function del($recordId){
        $customer = Doctrine::getTable(__CLASS__)->find(json_decode($recordId));
        $customer->delete();
    }
    
} 
 
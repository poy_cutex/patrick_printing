<?php
class Model_LanguageKeywords extends Model_Base_LanguageKeywords
{
    public static function deleteKeyword($ids)
    {
        $q = Doctrine_Query::create()
            ->delete(__CLASS__)
            ->whereIn('id', $ids);      
        $deleted = $q->execute();
    }
    public static function getKeywords($cid)
    {
        $id = Model_Countries::getIdByAlias($cid);
        $q = Doctrine_Query::create()
            ->select('K.keyword,T.translation')
            ->from(__CLASS__.' K')
            ->leftjoin('K.Translation T')
            ->where('T.country_id=?',array($id));
        $kw = $q->execute();
        foreach($kw as $key=>$value){
            $keywords[$value->keyword] = $value->Translation->translation; 
        }
        return $keywords;
    }

    public static function getKeywordsByGroup($gid)
    {
        $q = Doctrine_Query::create()
            ->select('K.keyword')
            ->from(__CLASS__.' K')
            ->where('K.group_id=?',array($gid))
            ->orderBy('K.keyword');
        $kw = $q->execute();
        return $kw;
    }
}

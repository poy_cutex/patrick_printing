<?php


class Model_LanguageGroup extends Model_Base_LanguageGroup
{
    public static function listGroups($id=null)
    {
        $q = Doctrine_Query::create();
        $q = $q->select()
            ->from('Model_LanguageGroup g');
        if($id)
            $q = $q->where('id = ?', array($id));

        $q->orderBy('g.name ASC');
        return $q->execute();
    }

    public static function saveGroup($id, $name)
    {
        if($id>0)
            $data = Doctrine::getTable('Model_LanguageGroup')->find($id);
        else
            $data = new Model_LanguageGroup();

        $data->name = $name;
        $data->save();
    }

    public static function deleteGroup($id)
    {
        $data = Doctrine::getTable('Model_LanguageGroup')->find($id);
        $data->delete();
    }
}

<?php

class Model_UserProfile extends Model_Base_UserProfile {
    
    public static function saveDealMakerCard($params,$file){
        $newfilename = "";
        if($file != null){
            $ext = end(explode(".",strtolower($file['name'])));
            if(CBUtils::checkImageExtension($ext)){
                $newfilename = "{$params['user_id']}-{$params['firstname']}-{$params['lastname']}.$ext";
                $target_path = "/img/user_profile/$newfilename";
                $up = CBUtils::uploadFile($file, $target_path);
            }
        }else{
            $up = true;
        }
        
            
        if($up){
            $user = Doctrine::getTable(__CLASS__)->findBy('user_id',$params['id']);
            
            $user->firstname = $params['firstname'];
            $user->lastname  = $params['lastname'];
            $user->date_birth = $params['date_birth'];
            $user->home_address = $params['date_birth'];
            $user->comments =   $params['comments'];
            if($newfilename != ""){
                $user->picture = $newfilename;
            }            
            $user->save();
        }        
    }
    
    
}
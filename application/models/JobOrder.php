<?php
class Model_JobOrder extends Model_Base_JobOrder {
    public static function listAll($serviceId){
         $where = $serviceId > 10 ? "s.serviceId > 10":"s.serviceId = $serviceId";
         $q = Doctrine_Query::create()
                ->select("s.*, date_format(datePickup,'%b %e, %Y %a') getdate, concat(c.firstname,' ', c.middlename,' ',c.lastname ) custname")
                 ->from('Model_JobOrder s')
                 ->innerJoin('s.Customer c')
                 ->innerJoin('s.Approve a')
                 ->innerJoin('s.Work w')
                 ->innerJoin('s.Service serv')
                 ->where($where)
                 ->orderby('s.created_at asc');

         $count = $q->count();
         $joborderlist = $q->execute();
         return array("count"=>$count,"list"=>$joborderlist->toArray());
     }
     
     public static function persist($record){
          if(isset($record["id"])){
               self::update($record);
          }else {
               self ::create($record);
          }
     }
     
     private static function create($record) {
        $joborder = new Model_JobOrder();
        
        $joborder->set('customerId', $record['customerId']); 
        $joborder->set('totalAmount', $record['totalAmount']);
        $joborder->set('jobdescription', $record['jobdescription']);
        $joborder->set('datePickup', $record['datePickup']);
        $serviceID = ($record['serviceId1']>10) ? $record['serviceId1']:$record['serviceId'];
        $joborder->set('serviceId', $serviceID);
        $joborder->set('approveId', $record['approveId']);
        $joborder->set('workId', $record['workId']);        
        $joborder->set('amountPaid', $record['amountPaid']);
        $dp = ($record['totalAmount'] - $record['amountPaid']) == 0 ? 0:$record['amountPaid'];
        $joborder->set('downPayment', $dp);
        $joborder->set('balance', ($record['totalAmount'] - $record['amountPaid']));
        $joborder->set('status', $record['status']);
        
        $joborder->save();
        
        $rec['customerId'] = $record['customerId'];
        $rec['serviceId'] = $serviceID;
        $rec['amount'] = $record['amountPaid'];
        $rec['jobOrderId'] = $joborder->id;
        
        Model_Sales::create($rec);
        
    }
    
    private static function update($record){
        $joborder = Doctrine::getTable(__CLASS__)->find($record['id']);
        $joborder->set('customerId', $record->customerId);  
        $joborder->set('totalAmount', $record->totalAmount);
        $joborder->set('jobdescription', $record->jobdescription);
        $joborder->set('datePickup', $record->datePickup);
        $joborder->set('serviceId', $record->serviceId);
        $joborder->set('approveId', $record->approveId);
        $joborder->set('workId', $record->workId);
        $joborder->set('downPayment', $record->downPayment);
        $joborder->set('balance', $record->balance);
        $joborder->set('amountPaid', $record->amountPaid);
        $joborder->set('status', $record->status);
        $joborder->save();
    }
    
     public static function del($recordId){
        $joborder = Doctrine::getTable(__CLASS__)->find(json_decode($recordId));
        $joborder->delete();
    }
    
    public static function payment($record){
        $joborder = Doctrine::getTable(__CLASS__)->find($record['id']);
        $payment =$record['pay'] + $joborder->amountPaid;
        $dp = $joborder->totalAmount == $payment ? 0 : $payment;
        $balance = $joborder->totalAmount - $payment;        
        $joborder->set('downPayment', $dp);
        $joborder->set('balance', $balance);
        $joborder->set('amountPaid', $payment);
        $joborder->save();
        
        // save payment in sales table
        
        $rec['customerId'] = $joborder->customerId;
        $rec['serviceId'] = $joborder->serviceId;
        $rec['amount'] = $record['pay'];
        $rec['jobOrderId'] = $record['id'];
        
        Model_Sales::create($rec);
    }
    
    public static function status($record){
        $joborder = Doctrine::getTable(__CLASS__)->find($record['id']);
        $joborder->set('status', $record['status']);
        $joborder->save();
    }
    }

 
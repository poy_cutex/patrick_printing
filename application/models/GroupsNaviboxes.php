<?php
/**
 * Description of Uploads
 * @author edd
 */
class Model_GroupsNaviboxes extends Model_Base_GroupsNaviboxes
{
    public static function getByGroup($pos){
    $data = array();    
    $group = Zend_Auth::getInstance()->getStorage()->read()->groupname;
    $grp = Doctrine::getTable('Model_Base_Group')->findOneByName($group);
    $grpInfo = $grp->toArray();
        $t = Doctrine_Query::create()
                ->select()
                ->from(__CLASS__.' GN')
                ->leftjoin('GN.Naviboxes N')
                ->where('N.position=?',$pos)
                ->andWhere('N.active_indicator=?',1)
                ->andWhere('GN.group_id=?',$grpInfo['id'])
                ->orderBy('N.order');
            try{
            $records = $t->execute();
            if($records){
                foreach($records as $key=>$record){
                    $data[$key]['id'] = 'NB'.$record->Naviboxes->position.'_'.$record->navibox_id;
                    $data[$key]['xtype'] = $record->Naviboxes->xtype;
                    $data[$key]['title'] = $record->Naviboxes->name;
                    $data[$key]['pos'] = $record->Naviboxes->position;
                    $data[$key]['items'] = array(array('xtype'=>'container','height'=>30));
                }
            }
            return $data;
        }catch(Exception $e){
           // print_r($e);
        }    
    }
}?>
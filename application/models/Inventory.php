<?php
class Model_Inventory extends Model_Base_Inventory {
    public static function listAll(){
         $q = Doctrine_Query::create()
                ->select()
                 ->from('Model_Inventory c')
                 ->orderby('c.id asc');
         $count = $q->count();
         $inventorylist = $q->execute();
         return array("count"=>$count,"list"=>$inventorylist->toArray());
     }
     
     public static function persist($record){
          if(isset($record["id"])){
               self::update($record);
          }else {
               self ::create($record);
          }
     }
     
     private static function create($record) {
        $inventory = new Model_Inventory();
        $inventory->set('itemName',$record['itemName']);
        $inventory->set('quantity', $record['quantity']);
        $inventory->set('cost', $record['cost']);
        $inventory->save();
    }
    
     private static function update($record){
        $inventory = Doctrine::getTable(__CLASS__)->find($record["id"]);
        $inventory->set('itemName',$record['itemName']);
        $inventory->set('quantity', $record['quantity']);
        $inventory->set('cost', $record['cost']);
        $inventory->save();
    }
    
    public static function del($recordId){
        $inventory = Doctrine::getTable(__CLASS__)->find(json_decode($recordId));
        $inventory->delete();
    }
    
} 
 
<?php
class Model_Photobooth extends Model_Base_Photobooth {
    public static function listAll(){
         $q = Doctrine_Query::create()
                ->select("p.*,"
                        . "c.id,"
                        . "concat(c.firstname,' ',c.lastname) as custname,"
                        . "date_format(p.setup_time,'%l:%i %p') setuptime,"
                        . "date_format(p.start_time,'%l:%i %p') starttime,"
                        . "date_format(p.event_date,'%b %e, %Y %a') eventdate,"
                        . "date_format(p.end_time,'%l:%i %p') endtime")
                 ->from('Model_Photobooth p')
                 ->innerJoin("p.Customer c")
                 ->orderby('p.id asc');

         $count = $q->count();
         $photoboothlist = $q->execute();
         return array("count"=>$count,"list"=>$photoboothlist->toArray());
    }
    
    
    
    public static function create($record) {
        $photobooth = new Model_Photobooth();
        $photobooth->set('customerId', $record['customerId']);
        $photobooth->set('event_date', $record['dateofevent']);
        $photobooth->set('setup_time', $record['setup_time']);
        $photobooth->set('start_time', $record['start_time']);
        $photobooth->set('end_time', $record['end_time']);
        $photobooth->set('eventType', $record['eventtype']);
        $photobooth->set('theme', $record['theme']);
        $photobooth->set('title', $record['title']);
        $photobooth->set('amount_due', $record['amount_due']);
        $dp = $record['amount_due'] == $record['amountPaid'] ? 0 :$record['amountPaid']; 
        $bal = $record['amount_due'] == $record['amountPaid'] ? 0 :$record['amount_due'] - $record['amountPaid']; 
        $photobooth->set('downPayment', $dp);
        $photobooth->set('balance', $bal);
        $photobooth->set('amountPaid', $record['amountPaid']);
        $photobooth->set('transactionId', $record['transactionId']);
        $photobooth->set('userId', $record['userId']);
        $photobooth->set('venue', $record['venue']);
        $photobooth->save();
        
        
        $rec['customerId'] = $record['customerId'];
        $rec['amount'] = $record['amountPaid'];
        $rec['photoboothId'] = $photobooth->id;
        
        Model_SalesPhoto::create($rec);
    }
    
    
    
    public static function del($recordId){
        $photobooth = Doctrine::getTable(__CLASS__)->find(json_decode($recordId));
        $photobooth->delete();
    }
    
    public static function payment($record){
        $photobooth = Doctrine::getTable(__CLASS__)->find($record['id']);
        $payment =$record['pay'] + $photobooth->amountPaid;
        $dp = $photobooth->amount_due == $payment ? 0 : $payment;
        $balance = $photobooth->amount_due - $payment;      
        $photobooth->set('downPayment', $dp);
        $photobooth->set('balance', $balance);
        $photobooth->set('amountPaid', $payment);
        $photobooth->save();
        
        $rec['customerId'] = $photobooth->customerId;
        $rec['amount'] = $record['pay'];
        $rec['photoboothId'] = $photobooth->id;
        
        Model_SalesPhoto::create($rec);
    }
}
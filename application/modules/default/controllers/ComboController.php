<?php

class ComboController extends Zend_Controller_Action {

    public function init() {
        $this->authData = Zend_Auth::getInstance()->getStorage()->read();
    }
    
    public function usersAction() {
         $list = Model_User::getUsers();
        die(json_encode(array("count"=>count($list),"list"=>$list)));
    }
    
    public function servicesAction(){
         die(json_encode(Model_Services::listAll()));
    }
    
    public function customersAction(){
         die(json_encode(Model_Customers::listAllCustomerCombo()));
    }
    
    public function groupsAction(){
         die(json_encode(Model_Group::getGroups()));
    }

    
}
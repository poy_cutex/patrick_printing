<?php

class AuthenticationController extends Zend_Controller_Action
{

    public $_user;
    
    public function init() {
        $this->_user = new WebDav_Db_Users();
        $this->_userMapper = new WebDav_Model_UsersMapper();
    }

    public function indexAction() {
    }

    public function infoAction() {
        $this->_helper->layout->disableLayout();
        echo phpinfo();
        die();
    }

    public function loginAction() {
        $this->view->headLink()->appendStylesheet('/css/admin/layout.css');
        $this->view->headLink()->appendStylesheet('/js/ext3/resources/css/ext-all.css');
        $this->view->headLink()->appendStylesheet('/js/ext3/resources/css/xtheme-slickness.css');
        $this->view->headScript()->appendFile('/js/ext3/adapter/ext/ext-base.js');
        $this->view->headScript()->appendFile('/js/ext3/ext-all.js');
        $this->view->headScript()->appendFile('/js/ux/IconCombo.js');
        $this->view->headScript()->appendFile('/js/forms/login.js');
        $this->view->headScript()->appendFile('/js/forms/forgotpassword.js');
        $this->view->headScript()->appendFile('/js/login.js');
        $this->view->headScript()->appendFile('/js/general.js');

        /*  Lets put the locale overrides here */
        $this->view->headScript()->appendFile('/js/ext3/src/locale/ext-lang-en.js');
        $this->view->headScript()->appendFile('/default/locale/index/locale/us');

        $request = $this->getRequest();
        $params = $request->getParams();
        @Zend_Registry::set('locale', $params['locale']);
        if ($request->isPost()) {
                if ($this->process($params)) {
                    // We're authenticated! Redirect to the home page
                    if($this->_isUserActive()){
                        $_SESSION['login_attempts'] = 0;
                        $db = Zend_Registry::get('db');
                        $db->insert('login_jnl', array('user_id' => $this->_user->getIdByUsername($params['username']) ));
                        die(json_encode(array('success'=>true
                            ,'redirect'=>$this->_authRedirect())));
                    }else{
                        Zend_Auth::getInstance()->clearIdentity();
                        die(json_encode(array('success'=>false,'error_code'=>3)));
                    }
                }else{
                    $userId = WebDav_Db_Users::getIdByUsername($params['username']) ;
                    $user = Doctrine::getTable('Model_User')->find($userId);
                    $user = $user ? $user->toArray() : null;
                    if($user && $user['active'] == 0) {
                        die(json_encode(array('success'=>false,'error_code'=>3)));
                    }

                     /*
                     * create a counter here for failed logins, disable accounts when it reaches 3
                     */
                    @session_start();

                    $maxLogin = Model_Settings::findSetting('MAX_LOGIN');
                    $intMaxLoginValue = $maxLogin['value'];
                    $intMaxLoginEnabled = $maxLogin['enabled'];

                    $strLoginAttemptSessionName = 'login_attempts::' . $params['username'];

                    if( !isset($_SESSION[$strLoginAttemptSessionName])){
                        $_SESSION[$strLoginAttemptSessionName]=1;
                    } else{
                        $_SESSION[$strLoginAttemptSessionName]++;
                    }
                    if( $intMaxLoginEnabled &&
                        (isset ($_SESSION[$strLoginAttemptSessionName])
                        && $_SESSION[$strLoginAttemptSessionName]>=$intMaxLoginValue))
                    {
                        if($userId){
                            $this->_user->update(array('active'=>0),'id='.$userId);
                        }

                        unset($_SESSION[$strLoginAttemptSessionName]);
                        die(json_encode(
                                array('success'=>false
                                ,'error_code'=> 2 )));
                    }
                    die(json_encode(array('success'=>false
                      ,'error_code' => 1 // Login is incorrect ...
                      ,'attempts'=>$_SESSION[$strLoginAttemptSessionName])));
                    $this->_redirect('/authentication/login');
                }
        }
    }

    public function logoutAction() {
        Zend_Auth::getInstance()->clearIdentity();
        Zend_Session::destroy();
        $this->_redirect('/admin');
    }

    public function getpasswordAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        
        $request = $this->getRequest();
        if ($request->isPost()) {
            $post = $request->getPost();

            $userMapper = new WebDav_Model_UsersMapper();
            $row = $userMapper->findUserByEmail($post['email']);
            
            if(!is_null(@$row->id)) {
                $userMapper->mailPassword(
                        $row->id,
                        $row->email,
                        ucwords($row->firstname) . ' ' . ucwords($row->lastname));
                echo json_encode(array('success' => true));
            } else {
                echo json_encode(array('success' => false));
            }
        }
    }

    public function changeexpiredpasswordAction() {
        $user = Zend_Auth::getInstance()->getStorage()->read();

        $request = $this->getRequest();
        if($request->isPost()) {
            extract($request->getPost());
            
            $isPasswordValid = $user->password == sha1($curPassword . $user->salt);
            if($isPasswordValid) {
                $this->_userMapper->updatePassword($user->id, $newPassword);
                die(json_encode(array('success' => true)));
            }

            die(json_encode(array('success' => false)));
        }

        die(json_encode(array('success' => false)));
    }

    public function checkpasswordexpirationAction() {
        $user = Zend_Auth::getInstance()->getStorage()->read();
        $userMapper = new WebDav_Model_UsersMapper();

        $res = $userMapper->isPasswordExpired($user->id);
        if($res)
            die(json_encode(array('success' => false)));

        die(json_encode(array('success' => true)));
    }

    protected function process($values) {
        // Get our authentication adapter and check credentials
        $adapter = $this->getAuthAdapter();
        $adapter->setIdentity($values['username']);
        $adapter->setCredential($values['password']);

        $auth = Zend_Auth::getInstance();
        $result = $auth->authenticate($adapter);
        if ($result->isValid()) {
            $user = $adapter->getResultRowObject();

            // Sets the  role & groupname to the session.
            $groups = Doctrine::getTable('Model_Base_Group')->find($user->group_id)->toArray();
            $user->groupname    =   $groups['name'];
            $user->role         = strtolower(str_replace(" ","",$groups['name']));
            
            //set start page to user statistics page
            $_SESSION['current_page'] = "statistics";
            $_SESSION['current_subpage'] = "user_statistics";
            $_SESSION['current_panel'] = "user_statistics";
            
            $auth->getStorage()->write($user);
            return true;
        }
        return false;
    }
    protected function getAuthAdapter() {
        $dbAdapter = Zend_Db_Table::getDefaultAdapter();
        $authAdapter = new Zend_Auth_Adapter_DbTable($dbAdapter);

        $authAdapter->setTableName('users')
                    ->setIdentityColumn('username')
                    ->setCredentialColumn('password')
                    ->setCredentialTreatment('SHA1(CONCAT(?,salt))');

        return $authAdapter;
    }

    protected function _isUserActive(){
        return Zend_Auth::getInstance()
                    ->getStorage()
                    ->read()
                    ->active;
    }
    
    protected function _authRedirect()  {
        $page = $this->_helper->url(
            'index',
            'index',
            'admin',
            $params=array(
                'locale'=>Zend_Registry::get('locale')
                )
        );
        return $page;
    }

}

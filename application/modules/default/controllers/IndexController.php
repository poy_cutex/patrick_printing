<?php

class IndexController extends Zend_Controller_Action {

     public function init() {

     }
    
     public function preDispatch() {
          $this->_helper->layout()->disableLayout(); 
          $this->_helper->viewRenderer->setNoRender(true);
     }

     public function indexAction (){
         $this->view->controller = $this;
         $sent = "noMessage";        
              
         $params = $this->_getAllParams();
         if (isset($params['post'])) {
              $rec = new stdClass();
              $rec->author = $params['author'];
              $rec->email = $params['email'];
              $rec->message = $params['text'];
              $rec->subject = $params['subject'];
              Model_Messages::create($rec);
              $sent = "ok";
         }
         $this->view->sent = $sent;
         $this->render('index');
     }

     public function admAction() {
         die(__METHOD__);
         system('dir', $retval);
         echo $retval;
     }

}


<?php

/**
 * PrivateController
 * @author Gado <jaranza@gmail.com>
 * @version 0.1
 */
class PrivateController extends Zend_Controller_Action {

    public function init() {
        $this->authData = Zend_Auth::getInstance()->getIdentity();
    }

    public function indexAction() {
        $this->_helper->layout->setLayout('default-ext');
        $this->view->user = Zend_Auth::getInstance()->getStorage()->read();
    }

    public function statAction() {
        /* Quick Fix for Stat */
        $a = Zend_Auth::getInstance()->getStorage()->read();
        $o = new WebDav_View_Helper_ViewUtility();
        $totalLogin = $o->getTotalLogin($a->id);
        $date = new DateTime($a->date_created);
        $registeredDate = $date->format('d.m.Y - H:i') . ' Uhr';
        $group = ucfirst($a->groupname);
        $stat = array(
            'totalLogin' => $totalLogin,
            'registeredDate' => $registeredDate,
            'group' => $group
        );
        die(json_encode($stat));
    }

    public function setCurrentPageAction() {
        $current_page = $this->_getParam('cur_page', "home");
        $_SESSION['current_page'] = $current_page;

        $current_subpage = $this->_getParam('cur_subpage', null);
        $_SESSION['current_subpage'] = $current_subpage;

        $current_panel = $this->_getParam('cur_panel', null);
        $_SESSION['current_panel'] = $current_panel;

        die(json_encode(array('success' => true)));
    }

    public function setNaviboxesAction() {
        $pos = $this->_getParam('pos', "L");
        $init = $this->_getParam('init', false);

        if($init && isset($_SESSION['current_page']) && $_SESSION['current_page'] != 'home') {
            $b = array();
            if($pos == "L") {
                $b = $this->getLeftBoxContainer($_SESSION['current_page'], $_SESSION['current_subpage']);
            } elseif($pos == "C") {
                $b = $this->getCenterBoxContainer($_SESSION['current_page'], $_SESSION['current_subpage']);
            }
            die(json_encode(array('success' => true, 'naviboxes' => $b)));
        }
        else {
            try {
                $o = new Model_GroupsNaviboxes();
                $b = $o->getByGroup($pos);
                die(json_encode(array('success' => true, 'naviboxes' => $b)));
            } catch (Exception $e) {
                print_r($e);
            }
            die();
        }
    }

    public function getLeftBoxContainerAction() {
        $page       = $this->_getParam('page', "home");
        $subpage    = $this->_getParam('subpage', null);
        die(json_encode(array('success' => true, 'naviboxes' => $this->getLeftBoxContainer($page, $subpage))));
    }

    protected function getLeftBoxContainer($page, $subpage = null) {
        $box = array();
        $pos = $this->_getParam('pos', "L");
        switch($page) {                       
          case  'printing':
               if(in_array($this->authData->role, array('staff', 'superadmin'))) {
                    $box[] = $this->getNaviboxesArrayVal($pos, $page, 1, 'cb.panels.naviboxes.printingservices', 'Printing Services',false);
               }                
               break ; 
          case  'messages':
               if(in_array($this->authData->role, array('staff', 'superadmin'))) {
                    $box[] = $this->getNaviboxesArrayVal($pos, $page, 1, 'cb.panels.naviboxes.messages', 'Messages',false);
               }                
               break ; 
          case  'sales':
               if(in_array($this->authData->role, array('superadmin'))) {
                    $box[] = $this->getNaviboxesArrayVal($pos, $page, 1, 'cb.panels.naviboxes.salesreports', 'Sales Reports',false);
               }                
               break ; 
        }
        return $box;
    }

    protected function getNaviboxesArrayVal($pos, $page, $count, $xtype, $title, $collapsed = true) {
        return  array(
            'id'        => 'NBL_' . $page . $count,
            'xtype'     => $xtype,
            'title'     => $title,
            'pos'       => $pos,
            'collapsed' => $collapsed,
            'cur_page'  => $page,
            'items'     => array(
                array(
                    'xtype' => 'container',
                    'height' => 30
                )
            )
        );
    }

    public function getCenterBoxContainerAction() {
        $page       = $this->_getParam('page', "home");
        $subpage    = $this->_getParam('subpage', null);
        die(json_encode(array('success' => true, 'naviboxes' => $this->getCenterBoxContainer($page, $subpage))));
    }

    protected function getCenterBoxContainer($page, $subpage = null) {
        $box = array();
        switch($page) {            
            case 'services'  :
                $box[] = array(
                    'collapsed' => false,
                    'bodyStyle' => 'padding:10px;',
                    'style'     => array(
                        'marginLeft'    => '10px',
                        'marginRight'   => '5px',
                    ),
                    'height'    => 200,
                    'title'     => 'Printing Services',
                    'html'      => '<h1>Printing Services</h1>
                        <br/><p>List of Printing Services</p>'
                );
                break; 
           case 'printing'  :
                $box[] = array(
                    'xtype'=>'gridjoborder',
                    'serviceId'=>1,
                    'height' => 400,
                );
                break; 
           case 'messages'  :
                $box[] = array(
                    'xtype'=>'gridmessages',
                    'title'=>'Unread Messages',
                    'isRead' => 0,
                );
                break;   
        }
        return $box;
    }

    public function savenaviboxAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $navibox = new Default_Model_Naviboxes();
        $naviboxMapper = new Default_Model_NaviboxesMapper();

        $request = $this->getRequest();
        if ($request->isPost()) {
            $navibox->setOptions($request->getPost());
            $naviboxMapper->save($navibox);

            echo "{success: true, msg: 'Navibox saved.'}";
            return;
        }

        echo "{success: false, msg: 'Navibox not saved.'}";
    }

    public function deletenaviboxAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $navibox = new Default_Model_Naviboxes();
        $naviboxMapper = new Default_Model_NaviboxesMapper();

        $request = $this->getRequest();
        if ($request->isPost()) {
            $navibox->setOptions($request->getPost());
            $naviboxMapper->delete($navibox);

            echo "Navigation Box deleted.";
            return;
        }

        echo "Navigation Box not deleted.";
    }

    public function naviboxlistAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $naviboxMapper = new Default_Model_NaviboxesMapper();
        $naviboxes = $naviboxMapper->fetchAll();

        $naviboxesArr = array();
        foreach ($naviboxes as $navibox) {
            $naviboxesArr[] = array(
                'box_id' => $navibox->getId(),
                'name' => $navibox->getName(),
                'position' => $navibox->getPosition(),
                'active_indicator' => $navibox->getActiveIndicator(),
                'order' => $navibox->getOrder()
            );
        }

        echo json_encode(array('records' => $naviboxesArr));
    }

    public function savegroupAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $request = $this->getRequest();
        if ($request->isPost()) {
            $post = $request->getPost();

            $groups = new Default_Model_Groups();
            $groupsMapper = new Default_Model_GroupsMapper();

            $groups->setOptions($post);
            $groupId = $groupsMapper->save($groups);
            $groupId = $groupId > 0 ? $groupId : $post['id'];

            $groupNavibox = new Default_Model_GroupsNaviboxes(array('groupId' => $groupId));

            $groupNaviboxMapper = new Default_Model_GroupsNaviboxesMapper();
            $groupNaviboxMapper->delete($groupNavibox);

            $groupNavibox->setGroupId($groupId);
            $postArr = array_slice($post, 3, count($post) - 2);
            foreach ($postArr as $boxId) {
                $groupNavibox->setBoxId($boxId);
                $groupNaviboxMapper->save($groupNavibox);
            }

            echo "{success: true, msg: 'Navibox saved.'}";
            return;
        }

        echo "{success: false, msg: 'Navibox not saved.'}";
    }

    public function grouplistAction() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        $groupMapper = new Default_Model_UserGroupsMapper();
        $groups = $groupMapper->fetchAll();

        $groupsArr = array();
        foreach ($groups as $group) {
            $groupsArr[] = array(
                'group_id' => $navibox->getId(),
                'group_name' => $navibox->getName(),
                'description' => $navibox->getPosition()
            );
        }

        echo json_encode(array('records' => $groupsArr));
    }

}

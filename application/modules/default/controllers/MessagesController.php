<?php

class MessagesController extends Zend_Controller_Action {

     public function init() {

     }
    
     public function listAction() {
          $read = $this->_getParam('isRead',0);
          die(json_encode(Model_Messages::listAll($read)));
     }
     
     public function readAction(){
          $recId = $this->_getParam('recordId',0);
          Model_Messages::readMessage($recId);
          die(json_encode(array('success'=>true)));
     }

     public function deleteAction(){
          $recId = $this->_getParam('recordId',0);
          Model_Messages::del($recId);
          die(json_encode(array('success'=>true)));
     }
}
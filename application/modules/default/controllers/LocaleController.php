<?php
class LocaleController extends Zend_Controller_Action
{
    public function init()
    {
       // $contextSwitch = $this->_helper->getHelper('contextSwitch');
       // $contextSwitch->addActionContext('index', 'json')->initContext();
    }
    public function indexAction()
    {
        error_reporting(0);
        $this->_helper->getHelper('layout')->disableLayout();
        $locale = $this->_getParam('locale','en');
        $keywords = Model_LanguageKeywords::getKeywords($locale);
        header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
        header("Cache-Control: no-cache");
        header("Pragma: no-cache");
        Header("content-type: application/x-javascript");
        $value = is_null($keywords) ? '{}' : json_encode($keywords);
        echo 'var LANG =';
        echo $value;
        echo ";";
        die();
    }
}
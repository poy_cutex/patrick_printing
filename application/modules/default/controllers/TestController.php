<?php

class TestController extends Zend_Controller_Action {

    public function init() {
        $this->_config = Zend_Registry::get('config');
    }

    public function testcsvAction() {
        $this->getHelper('layout')->disableLayout();
        header('Content-type: text/plain');
        header('Content-disposition: attachment; filename="income.csv"');
        $records = Doctrine::getTable('Model_AccountingIncome')->findAll(Doctrine::HYDRATE_ARRAY);
        $csv = '';
        foreach ($records as $record) {
            foreach ($record as $k => $v) {
                $csv .= "{$k},{$v},";
            }
            $csv .= "\n";
        }
        echo $csv;
        die();
    }

    public function statAction() {
        $implementedDomains = array(
            'http://admin.cleverbuy.com.ph',
            'http://beta-admin.cleverbuy.com.ph'
                // 'http://admin.cleverbuy'
        );

        if (!in_array($this->_config->appUrl, $implementedDomains)) {
            $response = array('success' => false,
                'msg' => 'This feature is not yet implemented on  ' . $this->_config->appUrl,
                'records' => array());
            echo json_encode($response);
            die();
        }
        $searchParams = json_decode($this->_getParam('searchParams'));

        $created = isset($searchParams->created_at) ? $searchParams->created_at : null;

        $referrals1 = Model_MagentoawRafInvite::getReferrals(1, $created);
        $referrals2 = Model_MagentoawRafInvite::getReferrals(2, $created);
        $payouts1 = Model_MagentoawRafReferrerDiscount::getRevenues(1, $created);
        $payouts2 = Model_MagentoawRafReferrerDiscount::getRevenues(2, $created);

        $combinedKeys = array_keys(array_merge($referrals1, $referrals2, $payouts1, $payouts2));


        foreach ($combinedKeys as $key => $val) {

            $records[$key]['date'] = $val;

            if (array_key_exists($val, $referrals1))
                $records[$key]['level1'] = $referrals1[$val];
            else
                $records[$key]['level1'] = 0;

            if (array_key_exists($val, $referrals2))
                $records[$key]['level2'] = $referrals2[$val];
            else
                $records[$key]['level2'] = 0;

            if (array_key_exists($val, $payouts1)) {
                $records[$key]['payouts1'] = $payouts1[$val]['total'];
                $records[$key]['revenues1'] = $payouts1[$val]['revenue'] * .40;
            } else {
                $records[$key]['payouts1'] = 0;
                $records[$key]['revenues1'] = 0;
            }

            if (array_key_exists($val, $payouts2)) {
                $records[$key]['payouts2'] = $payouts2[$val]['total'];
                $records[$key]['revenues2'] = $payouts2[$val]['revenue'] * .10;
            } else {
                $records[$key]['payouts2'] = 0;
                $records[$key]['revenues2'] = 0;
            }

            if (array_key_exists($val, $payouts1)) {
                $payout1 = $payouts1[$val]['total'];
                $revenue1 = $payouts1[$val]['revenue'] * .40;
            } else {
                $payout1 = 0;
                $revenue1 = 0;
            }
            if (array_key_exists($val, $payouts2)) {
                $payout2 = $payouts2[$val]['total'];
                $revenue2 = $payouts2[$val]['revenue'] * .10;
            } else {
                $payout2 = 0;
                $revenue2 = 0;
            }

            $records[$key]['payouts_total'] = $payout1 + $payout2;
            $records[$key]['cleverbuy_revenue'] = $revenue1 + $revenue2;
            $records[$key]['revenues_total'] = ($revenue1 + $revenue2) * 2;
        }
        $totalLevel1 = 0;
        $totalLevel2 = 0;

        $totalPayouts1 = 0;
        $totalPayouts2 = 0;
        $totalPayouts = 0;

        $totalRevenues1 = 0;
        $totalRevenues2 = 0;
        $totalRevenues = 0;

        if (isset($records)) {
            foreach ($records as $record) {
                $totalLevel1 += $record['level1'];
                $totalLevel2 += $record['level2'];

                $totalPayouts1 += $record['payouts1'];
                $totalPayouts2 += $record['payouts2'];
                $totalPayouts += $record['payout_total'];

                $totalRevenues1 += $record['revenues1'];
                $totalRevenues2 += $record['revenues2'];
                $totalRevenues += $record['revenues_total'];
            }

            $totals = array('date' => 'TOTAL',
                'level1' => $totalLevel1,
                'level2' => $totalLevel2,
                'payouts1' => $totalPayouts1,
                'payouts2' => $totalPayouts2,
                'payouts_total' => $totalPayouts1 + $totalPayouts2,
                'revenues1' => $totalRevenues1,
                'revenues2' => $totalRevenues2,
                'cleverbuy_revenue' => ($totalRevenues2 + $totalRevenues1),
                'revenues_total' => $totalRevenues,
            );
            array_push($records, $totals);
        } else {
            $records = array();
        }
        $response = json_encode(array('records' => $records));
        echo $response;
        die();
    }

    public function cascadeAction() {
        $parent_id = $this->_getParam('parent_id', null);
        $t = Doctrine::getTable('Model_CascadingCombos');

        if ($parent_id == null)
            $recs = $t->findByDql('parent_id is null', '', Doctrine::HYDRATE_ARRAY);
        else
            $recs = $t->findByDql('parent_id=?', array($parent_id), Doctrine::HYDRATE_ARRAY);

//        if (isset($recs[0]['parent_id']) && $recs[0]['parent_id'] != null) {
//            $addItem = array('id' => -1, 'parent_id' => -1, 'name' => 'Add New');
//            array_push($recs, $addItem);
//        }

        echo json_encode(array('records' => $recs));
        die();
    }

    public function cascadeSourceAction() {
        $parent_id = $this->_getParam('parent_id', null);
        $t = Doctrine::getTable('Model_CascadingCombosSource');

        if ($parent_id == null) {
            $recs = $t->findByDql('parent_id is null', '', Doctrine::HYDRATE_ARRAY);
        } else {
            $recs = $t->findByDql('parent_id=?', array($parent_id), Doctrine::HYDRATE_ARRAY);
        }

        if (isset($recs[0]['parent_id']) && $recs[0]['parent_id'] != null) {
            // $addItem = array('id' => -1, 'parent_id' => -1, 'name' => 'Add New');
            // array_push($recs, $addItem);
        }


        echo json_encode(array('records' => $recs));
        die();
    }

    public function dealAction() {
        $dealId = 65;
        $q = Doctrine_Query::create()
                ->select()
                ->from('Model_Base_MagentoSalesFlatOrder o')
                ->leftJoin('o.Item i')
                ->where('i.product_id=?', $dealId)
                ->andWhereIn('o.status', array('processing', 'complete'));

        $data = $q->execute();
        echo '<pre>';
        var_dump($data->toArray());
        die();
    }

    public function dealInfoAction() {
        $dealId = $this->_getParam('deal_id');
        $record = $this->getDealInfo($dealId);
        echo json_encode(array('record' => $record));
        die();
    }

    private function getDealInfo($dealId) {
        $record = array();
        $query = Doctrine_Query::create()
                ->select()
                ->from('Model_MagentocatalogProductEntity cpe')
                ->leftJoin('cpe.Varchar cpev')
                ->leftJoin('cpev.EavAttribute')
                ->where('cpe.entity_id=?', $dealId);
        $recs = $query->execute();

        if (isset($recs[0]->Varchar)) {
            foreach ($recs[0]->Varchar as $k => $v) {
                // refactor this 
                $data[@$v->EavAttribute->attribute_code] = $v->value;
            }
        }

        if (isset($data)) {
            $record['admin_deal_id'] = $recs[0]->entity_id;
            $record['sku'] = $recs[0]->sku;
            $record['date'] = $recs[0]->created_at;
            $record['name'] = $data['name'];
            if (isset($data['crm_deal_id']))
                $record['crm_deal_id'] = $data['crm_deal_id'];
            else
                $record['crm_deal_id'] = 0;
            $record['deal_from'] = $data['customer_website'];
        }
        return $record;
    }

    public function dealInfoByCrmIdAction() {
        $crmDealId = $this->_getParam('crm_deal_id');
        $query = Doctrine_Query::create()
                ->select()
                ->from('Model_MagentoeavAttribute ea')
                ->leftJoin('ea.Varchar v')
                ->where('ea.attribute_code="crm_deal_id"')
                ->andWhere('v.value=?', $crmDealId);
        $recs = $query->execute();
        $dealId = $recs[0]->Varchar->entity_id;
        $record = $this->getDealInfo($dealId);
        echo json_encode(array('record' => $record));
        die();
    }

    public function loadAction() {
        // die(__METHOD__);
        $file = Zend_Registry::get('config')->uploadPath .
                'controlling' .
                DIRECTORY_SEPARATOR . 'file.csv';
        echo '<pre>';
        //echo $file;
        // die();
        if (!file_exists($file)) {
            throw new Exception('File does not exists');
        }

        $filecontents = file($file);

        foreach ($filecontents as $line) {
            $data[] = explode(';', $line);
        }
        //var_dump($filecontents);
        //die();
        foreach ($data as $k => $datum) {
            if ($k > 0) {

                $ci = Doctrine::getTable('Model_ControlInput')->findOneBy('order_id', trim($datum[0]));

                if (!$ci) {
                    $ci = new Model_ControlInput();
                }


                $ci->order_id = intval($datum[0]);
                $ci->customer_name = $datum[1];
                $ci->product_name = $datum[2];
                $ci->quantity = intval($datum[3]);
                $ci->voucher_number = intval($datum[4]);
                $ci->id_code = intval($datum[5]);
                $ci->id = intval($datum[7]);
                $ci->created_date = date('Y-m-d H:i:s', strtotime($datum[6]));
                $ci->voucher_code = $datum[8];
                $ci->email = $datum[9];
                $ci->status = $datum[10];
                $ci->save();
            }
        }
        die("KILLED");
    }

    public function xmlrpcAction() {
        set_time_limit(0);
        $client = new Zend_XmlRpc_Client('http://cleverbuy/api/xmlrpc');
        try {
            $session = $client->call('login', array('edd', 'jamesd'));
            //$dateEnd     = '2011-01-01';
            $dateStart = '2011-02-30';
            $filters = array(
                array('created_at' => array('lt' => $dateStart))
                    //array('created_at'=>array('gt'=>$dateEnd)),
                    //array('customer_id'=>array('in'=>array(1,2,3,4)))
            );
            $c = $client->call('call', array($session, 'order.list', $filters));
            echo '<pre>';
            var_dump(count($c));
            //echo '<pre>';
            //Zend_Registry::get('logger')->log(print_r($c,true),6);
        } catch (Exception $e) {
            echo $e->getMessage();
        }
        die();
    }

    public function testAction() {
        echo '<pre>';
        $q = Doctrine_Query::create()
                ->select(
                        'i.order_increment_id,
                    i.order_id,oi.sku,oi.name,
                    oi.qty_ordered,
                    oi.price,
                    oi.row_total,
                    p.entity_id,
                    v.attribute_id,
                    v.value,
                    eav.attribute_code')
                ->from('Model_MagentosalesFlatInvoiceGrid as i')
                ->leftJoin('i.OrderItem as oi')
                ->leftJoin('oi.Product p')
                ->leftJoin('p.Varchar v')
                ->leftJoin('v.EavAttribute eav')
                ->orderBy('i.order_increment_id desc')
                ->whereIn('i.order_increment_id', array(100001801, 100001800, 100001799, 100001798))
                ->limit(5)
        ;
        $recs = $q->execute();
        $data = array();
        foreach ($recs as $key => $rec) {

            $data[$key]['increment_id'] = $rec->order_increment_id;
            $data[$key]['sku'] = $rec->OrderItem->sku;
            $data[$key]['qty'] = $rec->OrderItem->qty_ordered;
            $data[$key]['price'] = $rec->OrderItem->price;
            $data[$key]['admin_deal_id'] = $rec->OrderItem->Product->entity_id;
            foreach ($rec->OrderItem->Product->Varchar as $v) {
                if ($v->EavAttribute->attribute_code == 'crm_deal_id')
                    $data[$key]['crm_deal_id'] = $v->value;
            }
        }
        var_dump($data);
        die('killed');
    }

    public function zdbAction() {
        $config = Zend_Registry::get('config');
        $magentoDb = Zend_Db::factory(
                        $config->magentodb->adapter, $config->magentodb->params
        );

        echo '<pre>';
        $db = $magentoDb;
        $q = $db->select()->from('magentosales_flat_invoice');
        var_dump($q->query()->fetch());
        die();
    }

    public function listAction() {
        for ($i = 0; $i < 20; $i++)
            $records[] = array('value' => $i, 'text' => 'Text ' . $i);
        shuffle($records);
        die(json_encode(array('records' => $records)));
    }

    public function resetPasswordsCronAction() {
        $umap = new WebDav_Model_UsersMapper();
        $usersList = WebDav_Db_Profiles::getAllNonStaticPasswords();
        $procList = array();
        foreach ($usersList as $user) {
            $procList[] = $umap->mailPassword($user["id"], $user["email"], $user["name"]);
        }
        CBUtils::print_array($procList);
        die;
    }

    public function orderAction() {
        $q = Doctrine_Query::create()
                ->select('o.quote_id,
                    o.increment_id,
                    o.status,
                    i.sku,
                    i.qty,
                    i.price,
                    i.row_total,
                    i.product_id,
                    p.sku,
                    vc.value,
                    eav.attribute_code')
                ->from('Model_Base_MagentoSalesFlatOrder o')
                ->leftJoin('o.QuoteItem i')
                ->leftJoin('i.Product p')
                ->leftJoin('p.Varchar vc')
                ->leftJoin('vc.EavAttribute eav')
                ->where('i.sku !=""')
                //->andWhere('o.increment_id=?',100001802)
                ->andWhereIn('o.increment_id', array('100001801', '100001802', '100001787'))
                ->limit(10);
        $results = $q->execute(array(), Doctrine::HYDRATE_ARRAY);

        foreach ($results as $key => $result) {
            // increment_id == order_id in magento/admin/sales/order
            $records[$key]['increment_id'] = $result['increment_id'];
            $records[$key]['status'] = $result['status'];
            $records[$key]['sku'] = $result['QuoteItem']['sku'];
            $records[$key]['price'] = $result['QuoteItem']['price'];
            $records[$key]['qty'] = $result['QuoteItem']['qty'];
            $records[$key]['admin_deal_id'] = $result['QuoteItem']['product_id'];
            // $records[$key]['attributes'] = $result['QuoteItem']['Product']['Varchar'];
            foreach ($result['QuoteItem']['Product']['Varchar'] as $attribute) {
                if ($attribute['EavAttribute']['attribute_code'] == 'crm_deal_id')
                    $records[$key]['crm_deal_id'] = $attribute['value'];
            }
        }
        //Zend_Debug::dump($records);
        //Zend_Debug::dump($results);
        die();
    }

    public function bottomAction() {
        /**
         * SELECT
          `magentosales_flat_order`.`increment_id`
          , `magentosales_flat_quote_item`.`quote_id`
          , `magentosales_flat_quote_item`.`sku`
          #, `magentosales_flat_order`.`status`
          #,`magentosales_flat_quote_item`.`qty`
          , SUM(`magentosales_flat_quote_item`.`qty`) Qty
          ,`magentosales_flat_quote_item`.`price`
          ,`magentosales_flat_quote_item`.`base_row_total`
          , SUM(`magentosales_flat_quote_item`.`base_row_total`) Total
          FROM
          `dev_cleverbuy`.`magentosales_flat_order`
          INNER JOIN `dev_cleverbuy`.`magentosales_flat_quote_item`
          ON (`magentosales_flat_order`.`quote_id` = `magentosales_flat_quote_item`.`quote_id`)
          WHERE `magentosales_flat_order`.`increment_id` IN (100001801, 100001802, 100001787)
          GROUP BY `magentosales_flat_quote_item`.`sku`
         */
        $q = Doctrine_Query::create()
                ->select('o.quote_id,
                    o.increment_id,
                    i.sku,
                    sum(i.qty) qty,
                    i.price,
                    sum(i.base_row_total) base_row_total')
                ->from('Model_Base_MagentoSalesFlatOrder o')
                ->leftJoin('o.QuoteItem i')
                ->andWhereIn('o.increment_id', array('100001801', '100001802', '100001787'))
                ->groupBy('i.sku')
                ;

        $results = $q->execute(array(), Doctrine::HYDRATE_ARRAY);

        foreach ($results as $key => $result) {
            $records[$key]['order_id'] = $result['increment_id'];
            $records[$key]['sku'] = $result['QuoteItem']['sku'];
            $records[$key]['qty'] = $result['QuoteItem']['qty'];
            $records[$key]['price'] = $result['QuoteItem']['price'];
            $records[$key]['total'] = $result['QuoteItem']['base_row_total'];
        }
        Zend_Debug::dump($records);

        $gt = Doctrine_Query::create()
                ->select('sum(i.base_row_total) grandtotal')
                ->from('Model_Base_MagentoSalesFlatOrder o')
                ->leftJoin('o.QuoteItem i')
                ->andWhereIn('o.increment_id', array('100001801', '100001802', '100001787'))
        ;

        $grandtotal = $gt->execute(array(), Doctrine::HYDRATE_ARRAY);
        Zend_Debug::dump($grandtotal[0]);
        die();
    }

}

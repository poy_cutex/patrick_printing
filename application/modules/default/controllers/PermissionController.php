<?php
class PermissionController extends Zend_Controller_Action
{
    private $acl;
    private $role;
    
    public function init(){
        $this->acl  =   Zend_Registry::get('acl');
        $this->role =   (( Zend_Auth::getInstance()->hasIdentity()) &&
                    (Zend_Auth::getInstance()->getStorage()->read()->role != '')) ?

            Zend_Auth::getInstance()->getStorage()->read()->role : 'guest';
    }

    public function isAllowedAction() {
        $resource = $this->_getParam('resource');
        
        if (!$this->acl->has($resource)) return false;
        return $this->acl->isAllowed($this->role, $resource);
    }
}
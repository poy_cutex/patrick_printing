<?php
class Default_Model_NaviboxMapper
{
    protected $_dbTable;
 
    public function setDbTable($dbTable)
    {
        if (is_string($dbTable)) {
          $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
          throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }
 
    public function getDbTable()
    {
        if (null === $this->_dbTable) {
          $this->setDbTable('Default_Model_DbTable_Navibox');
        }
        return $this->_dbTable;
    }
 
    public function save(Default_Model_Navibox $navibox)
    {
        $id = $navibox->getId();
        $data = array(
            'name' => $navibox->getName(),
            'position' => $navibox->getPosition(),
            'active_indicator' => $navibox->getActiveIndicator(),
            'order' => $navibox->getOrder()
        );

        if (0 >= $id) {
            unset($data['id']);
            $res = $this->getDbTable()->insert($data);
        } else {
            $res = $this->getDbTable()->update($data, array('box_id = ?' => $id));
        }
    }

    public function fetchAll()
    {
        $resultSet = $this->getDbTable()->fetchAll();
        $entries   = array();
        foreach ($resultSet as $row) {
          $entry = new Default_Model_Navibox();
          $entry->setId($row->box_id)
                ->setName($row->name)
                ->setPosition($row->position)
                ->setActiveIndicator($row->active_indicator)
                ->setOrder($row->order);
          $entries[] = $entry;
        }
        return $entries;
    }

    public function find($id, Default_Model_Navibox $employee)
    {
        $result = $this->getDbTable()->find($id);
        if (0 == count($result)) {
          return;
        }
        $row = $result->current();
        $employee->setId($row->id)
                 ->setEncryptedPassword($row->password)
                 ->setFirstName($row->first_name)
                 ->setLastName($row->last_name);
    }

    public function delete($id) {
        $this->getDbTable()->delete('box_id = ' . $id);
    }

}

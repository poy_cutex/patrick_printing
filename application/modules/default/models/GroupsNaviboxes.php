<?php
/*
 *@package default
 *@author Kristel Magnaye
 */
class Default_Model_GroupsNaviboxes
{
  protected $_id;
  protected $_groupId;
  protected $_boxId;
 
  public function __construct(array $options = null)
  {
    if (is_array($options)) {
      $this->setOptions($options);
    }
  }
 
  public function __set($name, $value)
  {
    $method = 'set' . $name;
    if (('mapper' == $name) || !method_exists($this, $method)) {
      throw new Exception('Invalid property');
    }
    $this->$method($value);
  }
 
  public function __get($name)
  {
    $method = 'get' . $name;
    if (('mapper' == $name) || !method_exists($this, $method)) {
      throw new Exception('Invalid property');
    }
    return $this->$method();
  }
 
  public function setOptions(array $options)
  {
    $methods = get_class_methods($this);
    foreach ($options as $key => $value) {
      $method = 'set' . ucfirst($key);
      if (in_array($method, $methods)) {
        $this->$method($value);
      }
    }
    return $this;
  }

  public function setId($id)
  {
    $this->_id = $id;
    return $this;
  }

  public function getId()
  {
    return $this->_id;
  }
 
  public function setGroupId($groupId)
  {
    $this->_groupId = $groupId;
    return $this;
  }
 
  public function getGroupId()
  {
    return $this->_groupId;
  }

  public function setBoxId($boxId)
  {
    $this->_boxId = $boxId;
    return $this;
  }

  public function getBoxId()
  {
    return $this->_boxId;
  }

}

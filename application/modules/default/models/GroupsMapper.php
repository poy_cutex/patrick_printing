<?php
class Default_Model_GroupsMapper
{
    protected $_dbTable;
 
    public function setDbTable($dbTable)
    {
        if (is_string($dbTable)) {
          $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
          throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }
 
    public function getDbTable()
    {
        if (null === $this->_dbTable) {
          $this->setDbTable('Default_Model_DbTable_Groups');
        }
        return $this->_dbTable;
    }
 
    public function save(Default_Model_Groups $groups)
    {
        $id = $groups->getId();
        $data = array(
            'name' => $groups->getName(),
            'description' => $groups->getDescription()
        );

        if (0 >= $id) {
            unset($data['id']);
            $res = $this->getDbTable()->insert($data);
        } else {
            $res = $this->getDbTable()->update($data, array('id = ?' => $id));
        }

        return $res;
    }

    public function fetchAll()
    {
        $resultSet = $this->getDbTable()->fetchAll();
        $entries   = array();
        foreach ($resultSet as $row) {
          $entry = new Default_Model_Groups();
          $entry->setId($row->id)
                ->setName($row->name)
                ->setDescription($row->description);
          $entries[] = $entry;
        }
        return $entries;
    }

}

<?php
class Default_Model_GroupsNaviboxesMapper
{
    protected $_dbTable;
 
    public function setDbTable($dbTable)
    {
        if (is_string($dbTable)) {
          $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
          throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }
 
    public function getDbTable()
    {
        if (null === $this->_dbTable) {
          $this->setDbTable('Default_Model_DbTable_GroupsNaviboxes');
        }
        return $this->_dbTable;
    }
 
    public function save(Default_Model_GroupsNaviboxes $groupNavibox)
    {
        $data = array(
            'group_id' => $groupNavibox->getGroupId(),
            'navibox_id' => $groupNavibox->getBoxId()
        );

        $res = $this->getDbTable()->insert($data);
    }
    
    public function delete(Default_Model_GroupsNaviboxes $groupNavibox)
    {
        $this->getDbTable()->delete('group_id = ' .$groupNavibox->getGroupId());
    }

    public function fetchAll()
    {
        $resultSet = $this->getDbTable()->fetchAll();
        $entries   = array();
        foreach ($resultSet as $row) {
          $entry = new Default_Model_GroupsNaviboxes();
          $entry->setId($row->id)
                ->setGroupId($row->group_id)
                ->setBoxId($row->navibox_id);
          $entries[] = $entry;
        }
        return $entries;
    }

}

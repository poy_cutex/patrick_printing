<?php
/*
 *@package default
 *@author Kristel Magnaye
 */
class Default_Model_Navibox
{
  protected $_id;
  protected $_name;
  protected $_position;
  protected $_activeIndicator;
  protected $_order;
 
  public function __construct(array $options = null)
  {
    if (is_array($options)) {
      $this->setOptions($options);
    }
  }
 
  public function __set($name, $value)
  {
    $method = 'set' . $name;
    if (('mapper' == $name) || !method_exists($this, $method)) {
      throw new Exception('Invalid property');
    }
    $this->$method($value);
  }
 
  public function __get($name)
  {
    $method = 'get' . $name;
    if (('mapper' == $name) || !method_exists($this, $method)) {
      throw new Exception('Invalid property');
    }
    return $this->$method();
  }
 
  public function setOptions(array $options)
  {
    $methods = get_class_methods($this);
    foreach ($options as $key => $value) {
      $method = 'set' . ucfirst($key);
      if (in_array($method, $methods)) {
        $this->$method($value);
      }
    }
    return $this;
  }
 
  public function setId($id)
  {
    $this->_id = $id;
    return $this;
  }
 
  public function getId()
  {
    return $this->_id;
  }

  public function setName($name)
  {
      $this->_name =  $name;
      return $this;
  }

  public function getName()
  {
      return $this->_name;
  }

  public function setPosition($position)
  {
      $this->_position =  $position;
      return $this;
  }

  public function getPosition()
  {
      return $this->_position;
  }

  public function setActiveIndicator($activeIndicator)
  {
      $this->_activeIndicator =  $activeIndicator;
      return $this;
  }

  public function getActiveIndicator()
  {
      return $this->_activeIndicator;
  }

  public function setOrder($order)
  {
      $this->_order =  $order;
      return $this;
  }

  public function getOrder()
  {
      return $this->_order;
  }

}

<?php
/*
 *@package default
 *@author Kristel Magnaye
 */
class Default_Model_DbTable_UserGroups extends Zend_Db_Table_Abstract {

  /** Table name */
  protected $_name    = 'user_groups';

}
<?php
/*
 *@package default
 *@author Kristel Magnaye
 */
class Default_Model_DbTable_Groups extends Zend_Db_Table_Abstract {

  /** Table name */
  protected $_name    = 'groups';

}
<?php
/**
 * Class Admin_Model_Users
 *
 * @category   Admin
 * @package    Admin_Models
 * @copyright  Copyright (c) 2010 Frozynart Designs. (http://www.frozynart.com)
 * @license    http://www.frozynart.com/license/new-bsd New BSD License
 * @author jaranza <jaranzaa@gmail.com>
 * @version v0.1 2010/03/10 11:32:00 GMT+0800
 */

/**
 * This class is used for managing the creation,modifications,deletions of users.
 */

/**
 * @category Admin
 * @package Admin_Models
 */
class Admin_Model_Users extends Doctrine_Record
{
    /**
     * Returned if user is not found
     */
    const NOT_FOUND = 1;
    /**
     * Returned if passwords are mismatched
     */
    const PASSWORD_MISMATCH = 2;

    /**
     * Set the user table
     * @return void
     */


    public function setTableDefinition() {
        $this->setTableName('admin_users');
        $this->hasColumn('username','text', array('unique', 'notblank','nospace'));
        $this->hasColumn('password','text', array('notblank','nospace'));
        $this->hasColumn('password_salt','text', array('notblank','nospace'));
        $this->hasColumn('role','text', array('notblank','nospace'));
    }

    public static function authenticate($username, $password) {
        $siteId = Zend_Registry::get('config')->siteUniqueId;
 
        $theUser = Doctrine::getTable('Admin_Model_Users')->findOneByUsername($username);
        if (!$theUser) {
            throw new Exception(self::NOT_FOUND);
        }

        if ( self::passwordMatch($password,$theUser->password,$theUser->password_salt,$siteId) != 1 ){
            throw new Exception(self::PASSWORD_MISMATCH);
        }
        return $theUser;
    }
    /**
     *
     * @param <type> $username
     * @return object user object having the password and password-salt properties.
     *
     */
    public static function getUser($username){
        $userObj = new stdClass();
        $userData = Doctrine::getTable('Admin_Model_Users')->findOneByUsername($username);
        if($userData){
            $userData = $userData->toArray();
            foreach($userData as $k=>$v){
                if(!in_array($k, array('password','password_salt'))){
                    $userObj->{$k} = $v;
                }
            }
            return $userObj;
        }
        return null;
    }
    /**
     * Matches user supplied password against her password stored on DB
     * @param string $passwd 
     * @param string $storePasswd
     * @param string $salt
     * @param string $siteId
     * @return bool true on match
     */
    private function passwordMatch($passwd,$storePasswd,$salt,$siteId){
        $p1 = md5($passwd).$siteId.$salt;
        $p2 = $storePasswd.$siteId.$salt;
        return ($p1 == $p2) ? true : false;
    }
}
?>
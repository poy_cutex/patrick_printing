<?php
/**
 * Zend_Auth adapter for an extended Doctrine users record.
 *
 *@package admin
 *@author Ed Aranza <jaranza@gmail.com>
 */
class Admin_Model_Auth implements Zend_Auth_Adapter_Interface
{
    public $username; 
    public $password;
    public $user;

    const NOT_FOUND_MESSAGE = "The username you entered could not be found.";
    const CREDINTIALS_MESSAGE = "The password you entered is not correct.";
    const UNKNOWN_FAILURE = "Authentication failed for an unknown reason.";

    public function __construct($username, $password) {
        $this->username = $username;
        $this->password = $password;
    }

    /**
     * Zend_Auth hook.
     */
    public function authenticate() {
        try {
            $this->user = Admin_Model_Users::authenticate($this->username, $this->password);
        } catch (Exception $e) {
            if ($e->getMessage() == Admin_Model_Users::NOT_FOUND) {
                return $this->notFound();
            }

            if ($e->getMessage() == Admin_Model_Users::PASSWORD_MISMATCH) {
                return $this->passwordMismatch();
            }

            return $this->failed($e);
        }
        return $this->success();
    }

    /**
     * Factory for Zend_Auth_Result
     *
     *@param integer    The Result code, see Zend_Auth_Result
     *@param mixed      The Message, can be a string or array
     *@return Zend_Auth_Result
     */
    public function result($code, $messages = array()) {
        if (!is_array($messages)) {
            $messages = array($messages);
        }

        return new Zend_Auth_Result(
            $code,
            $this->username,
            $messages
        );
    }

    /**
     * "User not found" wrapper for $this->result()
     */
    public function notFound() {
        return $this->result(Zend_Auth_Result::FAILURE_IDENTITY_NOT_FOUND,self::NOT_FOUND_MESSAGE);
    }

    /**
     * "Password does not match" wrapper for $this->result()
     */
    public function passwordMismatch() {
        return $this->result(Zend_Auth_Result::FAILURE_IDENTITY_NOT_FOUND,self::CREDINTIALS_MESSAGE);
    }

    /**
     * General or Unknown failure wrapper for $this->result()
     */
    public function failed(Exception $e) {
        return $this->result(Zend_Auth_Result::FAILURE, self::UNKNOWN_FAILURE);
    }

    /**
     * "Success" wrapper for $this->result()
     */
    public function success() {
        return $this->result(Zend_Auth_Result::SUCCESS);
    }

    public function getResultRowObject(){
        return Admin_Model_Users::getUser($this->username) ;
    }
}
?>
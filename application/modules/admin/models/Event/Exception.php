<?php
/**
 * 
 *
 * @category   Admin
 * @package    Model_Event
 * @copyright  Copyright (c) 2010-2011 Frozynart Designs. (http://www.frozynart.com)
 * @license    http://www.frozynart.com/license/new-bsd New BSD License
 * @author Edgar Aranza <jaranza@gmail.com>
 * @version $id: Exception.php, v0.1 2010/14/21 10:48:00 GMT+0800 $
 */

/**
 * @see Zend_Exception
 */
require_once 'Zend/Exception.php';

/**
 * @category Froz
 * @package Froz_Db
 */
class Admin_Model_Event_Exception extends Zend_Exception
{}
<?php
/**
 * Class Admin_Model_Users
 *
 * @category   Admin
 * @package    Admin_Models
 * @copyright  Copyright (c) 2010 Frozynart Designs. (http://www.frozynart.com)
 * @license    http://www.frozynart.com/license/new-bsd New BSD License
 * @author jaranza <jaranzaa@gmail.com>
 * @version v0.1 2010/03/10 11:32:00 GMT+0800
 */

/**
 * This class is used for managing the creation,modifications,deletions of users.
 */

/**
 * @category Admin
 * @package Admin_Models
 */
class Admin_Model_Event_Events extends Doctrine_Record
{
    private $_e = null;
    public function setTableDefinition() {
        $this->setTableName('admin_events');
        $this->hasColumn('dtstart','timestamp');
        $this->hasColumn('dtend','timestamp');
        $this->hasColumn('title','string');
        $this->hasColumn('location','string');
        $this->hasColumn('details','string');
    }

    /**
     *
     * @return events.
     *
     */
    public static function getEvents(Zend_Controller_Request_Abstract $request){
        if( !$request->isXmlHttpRequest()){ 
            throw new Admin_Model_Event_Exception('Bad Request!');
        }
        $q = Doctrine_Query::create()
            ->select()
            ->from('Admin_Model_Event_Events');
        $events = $q->execute(array(), Doctrine::HYDRATE_ARRAY);
        return $events;
    }

    public function saveEvent(Zend_Controller_Request_Abstract $request)
    {
          $success = true;
//        if(!$request instanceof Zend_Controller_Request_Abstract){
//            throw new Admin_Model_Event_Exception('Argument 1 must be an instance'.
//                    'of Zend_Controller_Request_Abstract,<br/> type'.gettype($request)
//                    .' was given.');
//        }
//        if( !$request->isXmlHttpRequest()){
//            throw new Admin_Model_Event_Exception('Bad Request!');
//        }
        $params = $request->getParam('extparams');
        $params = $params['data'][0];
        if(isset($params['id']) && $params['id']){
          $event = Doctrine::getTable('Admin_Model_Event_Events')->findOneById($params['id']);
        }else{
           $event = new Admin_Model_Event_Events();
        }
        foreach ($params as $param=>$val){
            $event->{$param} = $val ;
        }
//        $event->dtstart  =   $params['dtstart'];
//        $event->dtend    =   $params['dtend'];
//        $event->title    =   $params['title'];
//        $event->location =   $params['location'];
//        $event->details  =   $params['details'];

        try{
            $event->save();
        }catch(Exception $e){
            $this->_e =  $e->getMessage();
            $success = false;
        }
        return $event->toArray();
    }
    public function updateEvent(Zend_Controller_Request_Abstract $request)
    {
        $params = $request->getParams();
        $EventsTable = Doctrine::getTable('Admin_Model_Event_Events');
        $event = $EventsTable->findOneById($params['eventId']);
        $event->dtstart = $params['newEventStart'];
        $event->dtend = $params['newEventEnd'];
        try{
            $event->save();
            $sucess = true;
        }catch(Exception $e){
             $this->_e =  $e->getMessage();
             $success = false ;
        }
        return array('success'=>true);
    }
    public function deleteEvent(Zend_Controller_Request_Abstract $request)
    {
        $params = $request->getParam('extparams');
        $eid = $params['data'][0];
        $event = Doctrine::getTable('Admin_Model_Event_Events')->findOneById($eid);
        $event->delete();
        return array('success'=>true);
    }
}
?>
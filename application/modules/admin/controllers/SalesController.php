<?php
class Admin_SalesController extends Zend_Controller_Action
{
    public function init()
    {
        
        $this->auth = Zend_Auth::getInstance();
        $this->authInfo = $this->auth->getStorage()->read();
        if (!$this->auth->hasIdentity()) {
            $this->_redirect("/default/authentication/login");
        }
    }   
    public function indexAction()
    {
         
    }
    public function listAction(){
        die(json_encode(Model_Sales::listAll()));
    }
    
    public function listspAction(){
        die(json_encode(Model_SalesPhoto::listAll()));
    }
    
    
}
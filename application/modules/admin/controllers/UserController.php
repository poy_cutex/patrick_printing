<?php
class Admin_UserController extends Zend_Controller_Action
{
    public function init()
    {
        
        $this->auth = Zend_Auth::getInstance();
        $this->authInfo = $this->auth->getStorage()->read();
        if (!$this->auth->hasIdentity()) {
            $this->_redirect("/default/authentication/login");
        }
    }   
    public function indexAction()
    {
         
    }
    public function listAction(){
        die(json_encode(Model_User::listAll()));
    }
    
    public function saveAction(){
        $recs = $this->_getAllParams();
        $recs['addedId'] = $this->authInfo->id;
        $res = Model_User::persist($recs);
        $msg = $res == -1 ? "Username already exist!" : "An error has occured.";
        
        die(json_encode(array('success'=>$res > 0,"msg"=>$msg)));
    }
    
    public function deleteAction(){
        Model_User::del($this->_getParam('recordId'));
        die(json_encode(array('success'=>true)));
    }

    public function changePassAction(){
        $recs = $this->_getAllParams();
        Model_User::changePass($recs);        
        die(json_encode(array('success'=>true)));
    }
}
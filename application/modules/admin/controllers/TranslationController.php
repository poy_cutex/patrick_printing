<?php
class Admin_TranslationController extends Zend_Controller_Action
{

    public function init()
    {
        $this->translationMapper = new WebDav_Model_TranslationMapper();
    }

    public function indexAction()
    {
        die(json_encode(array('success'=>true)));
    }

    public function listAction()
    {
        $category = $this->_getParam('category');
        $status = $this->_getParam('status');
        $records = $this->translationMapper->getAllTranslations($category, $status);
        die(json_encode(array('records'=>$records)));
    }

    public function saveAction()
    {
        $request = $this->getRequest();
        if($request->isPost()) {
            $post = $request->getPost();

            if (get_magic_quotes_gpc()) {
                $records = json_decode(stripslashes($post['recordsToInsertUpdate']));
            } else {
                $records = json_decode($post['recordsToInsertUpdate']);
            }

            foreach ($records as $record) {
                if(is_null($record->keyword)) continue;
                $lastInsertId = $this->translationMapper->saveKeyword(
                        array('id' => $record->keyword_id,
                            'keyword' => $record->keyword,
                            'category' => $record->category,
                            'url' => $record->url,
                            'status' => $record->status));
                $keywordId = !is_null($record->keyword_id) ? $record->keyword_id : $lastInsertId;

                $data = null;
                foreach($record as $key => $value) {
                    if('keyword_id' == $key || 'keyword' == $key)
                        continue;

                    $tmp = explode('_t', $key);
                    $data['keyword_id'] = $keywordId;
                    $data['country_id'] = $tmp[1];
                    if('id' == $tmp[0]) {
                        $data['id'] = $value;
                    } elseif('translation' == $tmp[0]) {
                        $data['translation'] = $value;
                        if(!is_null($data['translation'])) $this->translationMapper->save($data);
                        $data = null;
                    }
                }
            }
        }

        die(json_encode(array('success' => 'false')));
    }

    public function deleteAction()
    {
        $request = $this->getRequest();
        if($request->isPost()) {
            $post = $request->getPost();
            $this->translationMapper->delete($post['id']);
        }
        die(json_encode(array('success' => true)));
    }

    public function savekeywordAction()
    {
        $request = $this->getRequest();
        if($request->isPost()) {
            $result = $this->translationMapper->saveKeyword($request->getPost());
            $msg = $result ? null : 'Please enter a unique keyword.';
            die(json_encode(array('success' => $result, 'msg' => $msg)));
        }
        die(json_encode(array('success' => false)));
    }

    public function deletekeywordAction()
    {
        $request = $this->getRequest();
        if($request->isPost()) {
            $result = $this->translationMapper->deleteKeyword($request->getPost());
            die(json_encode(array('success' => true)));
        }
        die(json_encode(array('success' => false)));
    }

    public function getcountriesAction()
    {
        $mapper = new WebDav_Model_CountriesMapper();
        $countries = $mapper->fetchAll();
        die(json_encode(array('success' => true, 'countries' => $countries->toArray())));
    }
    public function getTableAction()
    {
       $t = new Model_LanguageTranslations();
       $tbl =  $t->createTable();
       die(json_encode($tbl));
    }
    public function getRecordsAction()
    {
       $t = new Model_LanguageTranslations();
       $recs =  $t->getTranslationRecords(
               $this->getNodeId($this->_getParam('groupId'), null),
               $this->_getParam('keywordId'), null);
       die(Zend_Json::encode(array('records'=>$recs)));
        
    }
    public function insertUpdateAction()
    {
        
        $request = $this->_request;
        if($request->isPost()){
            $recordsToInsertUpdate = $this->_getParam('recordsToInsertUpdate');
        }
        $records = json_decode(stripslashes($recordsToInsertUpdate));
        foreach($records as $record)
        {
           $keywordType = substr($record->kw,0,3);
           if(!in_array($keywordType,array(
                'BTN','FAQ','PNL','HDR','MSG','COL','TAB','FLD', 'TIP','EML')))
           {
               die(json_encode(array('success'=>false,'e'=>$record->kw)));
           }
           if($record->kid){
                $keyword = Doctrine::getTable('Model_Base_LanguageKeywords')->find($record->kid);
                $keyword->set('keyword',$record->kw);
                if($record->group_name > 0)
                        $keyword->set('group_id',$record->group_name);
                $keyword->save();
                foreach($record as $key=>$val)
                {
                    if( !in_array($key,array('kid','kw', 'group_id', 'group_name')) ){
                        $t = Doctrine::getTable('Model_LanguageTranslations');
                        $translations = $t->findByKeyword_idAndCountry_id(
                            $record->kid,Model_Countries::getIdByAlias($key));
                        if($translations->count()){
                            $translations[0]->set('translation',$val);
                            $translations[0]->save();
                        }else{
                           $translation = new Model_LanguageTranslations();
                           $translation->set('translation',$val);
                           $translation->set('keyword_id',$record->kid);
                           $translation->set('country_id',Model_Countries::getIdByAlias($key));
                           $translation->save();
                        }
                    }
                }
           }else{
                $keyword = new Model_Base_LanguageKeywords();   
                $keyword->set('keyword',$record->kw);
                $keyword->set('group_id', $record->group_name);
                $keyword->save();
                $keywordId = $keyword->getIncremented();
                if($keywordId){
                    foreach($record as $key=>$val){
                        if( !in_array($key,array('kid','kw')) ){
                        $cid = Model_Countries::getIdByAlias($key);
                            if(!empty($val) && $cid){
                                $translation = new Model_LanguageTranslations();
                                $translation->set('translation',$val);
                                $translation->set('keyword_id',$keywordId);
                                $translation->set('country_id',$cid);
                                $translation->save();
                            }
                        }
                    }
                }
                             
           }
        }
        die(json_encode(array('success'=>true)));
    }
    public function deleteRowsAction(){
        $ids = $this->_getParam('ids');
        $ids = json_decode(stripslashes($ids));
        Model_LanguageKeywords::deleteKeyword($ids);
        die(json_encode(array('success'=>true)));
    }

    public function gettreechildrenAction()
    {
        $nodeId = $this->_getParam('node');
        if($nodeId == '0') {
            $result = Model_LanguageGroup::listGroups();
            $textCol = 'name';
        } else {
            $nodeId = $this->getNodeId($nodeId);
            $result = Model_LanguageKeywords::getKeywordsByGroup($nodeId);
            $textCol = 'keyword';
        }

        $isLeaf = $nodeId ? true : false;
        $recs = array();
        foreach ($result as $rec) {
            $recs[] = array(
                'id' => $isLeaf ? $rec['id'] : $rec['id'] . '_' ,
                'text' => $rec[$textCol],
                'leaf' => $isLeaf,
                'editable' => !$isLeaf);
        }
        die(json_encode($recs));
    }

    public function saveGroupAction()
    {
        extract($this->_getAllParams());
        Model_LanguageGroup::saveGroup($this->getNodeId($id), $name);
        die();
    }

    public function deleteGroupAction()
    {
        $groupId = $this->getNodeId($this->_getParam('id'));
        $groupItemCnt = count(Model_LanguageKeywords::getKeywordsByGroup($groupId));
        if($groupItemCnt > 0) {
            die(json_encode(array('success' => false)));
        } else {
            Model_LanguageGroup::deleteGroup($groupId);
            die(json_encode(array('success' => true)));
        }
    }

    private function getNodeId($id)
    {
        return $this->strstrb($id, '_');
    }

    private function strstrb($h,$n)
    {
        return array_shift(explode($n,$h,2));
    }
}
?>

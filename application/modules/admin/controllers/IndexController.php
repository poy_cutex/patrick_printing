<?php

class Admin_IndexController extends Zend_Controller_Action {

    protected $_libPath;
    protected $_jsPath;

    public function init() {
        $this->auth = Zend_Auth::getInstance();

        if (!$this->auth->hasIdentity()) {
            $this->_redirect("/default/authentication/login");
        }
        $this->_libPath = Zend_Registry::get('config')->includePaths->library;
        $this->_jsPath  = realpath(Zend_Registry::get('config')->publicPath);

        }

    public function indexAction() {

        $this->view->headLink()->appendStylesheet('/js/ext3/examples/calendar/resources/css/calendar.css');
        $this->view->headLink()->appendStylesheet('/js/ext3/resources/css/ext-all.css');
        $this->view->headLink()->appendStylesheet('/js/ext3/resources/css/xtheme-access.css');
        //calendar
        $jScript = 'var USER_ID=' . $this->auth->getIdentity()->id . ' ;';
        $jScript .= 'var ROLE="' . $this->auth->getIdentity()->role . '" ;';

        //Locale

        $localeFile = 'ext-lang-' . Zend_Registry::get('locale') . '.js';
        if (file_exists($this->_jsPath . $localeFile)) {
            $this->view->headScript()->appendFile('/js/ext3/src/locale/' . $localeFile);
        }
        $this->view->headScript()->appendFile('/default/locale/index/locale/' . Zend_Registry::get('locale'));

        $this->view->headLink()->appendStylesheet('/css/admin/layout.css');
        $this->view->headLink()->appendStylesheet('/css/backenduser.css');
        $this->view->headLink()->appendStylesheet('/css/boxselect.css');
        $this->view->headLink()->appendStylesheet('/css/MultiSelect.css');
        $this->view->headLink()->appendStylesheet('/css/gridsummary.css');
        $this->view->headLink()->appendStylesheet('/js/ext3/examples/ux/css/RowEditor.css');
        $this->view->headLink()->appendStylesheet('/js/ext3/examples/ux/fileuploadfield/css/fileuploadfield.css');
        $this->view->headLink()->appendStylesheet('/js/ext3/examples/organizer/organizer.css');

	$this->view->headScript()->appendFile('/js/ext3/adapter/ext/ext-base.js');
	$this->view->headScript()->appendFile('/js/ext3/ext-all.js');

    }

    public function minAction() {
        include_once($this->_libPath.'/jsmin.php');
        Zend_Layout::getMvcInstance()->disableLayout();
        $concatFile = '';

        $files = array(
            //common
            //'/js/ext3/adapter/ext/ext-base.js',
            //'/js/ext3/ext-all.js',
            '/js/zeroclipboard/ZeroClipboard.js',
            '/js/zeroclipboard/CopyButton.js',
            '/js/CB/Utils.js',
            '/js/utils/ObjectSize.js',
            '/js/general.js',
            '/js/ext3/examples/ux/RowEditor.js',
            '/js/ext3/examples/ux/GroupSummary.js"',
            '/js/panels/Translation.js',
            '/js/panels/systemtranslation.js',
            '/js/panels/backendmanagement.js',
            '/js/panels/systemsetting.js',
            //plugins
            '/js/CB/plugins/Fittoparent.js',
            '/js/ux/Exporter-all.js',
            '/js/ux/gridsummary.js',
            '/js/CB/layouts/Message.js',
            '/js/CB/Layout.js',
            '/js/CB/Main.js',
            
            '/js/panels/Welcome.js',
            '/js/CB/toolbars/HeaderToolbar.js',
            '/js/ext3/examples/ux/fileuploadfield/FileUploadField.js',
            '/js/ux/BoxSelect.js',
            '/js/ux/MultiSelect.js',
            '/js/ux/ItemSelector.js',
            '/js/ux/ImageDragZone.js',
            '/js/ux/GridSearch.js',
            '/js/ux/PagingToolbarResizer.js',
            '/js/ux/MultiSelect.js',
            '/js/ext3/examples/ux/DataView-more.js',
            '/js/forms/changepassword.js',
            '/js/forms/forgotpassword.js',
            '/js/forms/users.js',
            '/js/forms/groups.js',
            '/js/forms/navibox.js',
            '/js/forms/news.js',
            '/js/forms/passwordsettings.js',
            '/js/forms/mailsettings.js',
            '/js/forms/keyword.js',
            '/js/grids/users.js',
            '/js/grids/userlist.js',
            '/js/grids/groups.js',
            '/js/grids/navibox.js',
            '/js/panels/userdetails.js',
            '/js/panels/navibox.js',
            '/js/panels/passwordsettings.js',
            '/js/panels/categories.js',
            '/js/CB/panels/NaviboxesContainer.js',
            '/js/panels/Statistics.js',
            //naviboxes            
            '/js/CB/panels/naviboxes/PrintingServices.js',
            '/js/CB/panels/naviboxes/Messages.js',
            '/js/CB/panels/naviboxes/SalesReports.js',
            //combo boxes
            '/js/CB/forms/combo/Services.js',
            '/js/CB/forms/combo/Customers.js',
            '/js/CB/forms/combo/Users.js',
            '/js/CB/forms/combo/UserGroups.js',
            //grids
            '/js/CB/grids/User.js',
            '/js/CB/grids/Services.js',
            '/js/CB/grids/Customers.js',
            '/js/CB/grids/Photobooth.js',
            '/js/CB/grids/JobOrder.js',
            '/js/CB/grids/Messages.js',   
            '/js/CB/grids/Sales.js',
            '/js/CB/grids/SalesPhoto.js',
            '/js/CB/grids/Inventory.js',
            //forms
            '/js/CB/forms/JobOrder.js',
            '/js/CB/forms/Button.js',
            '/js/CB/plugins/Button.js',
            '/js/CB/forms/Photobooth.js',
            '/js/CB/forms/Customer.js',
            '/js/CB/forms/User.js',
            '/js/CB/forms/ChangePassword.js',
            '/js/CB/forms/ForgotPassword.js',
            '/js/CB/forms/BalancePayment.js',
            '/js/CB/forms/Inventory.js',
            '/js/CB/forms/JobOrderStatus.js',

            '/js/CB/forms/ReservationPayment.js',
            '/js/CB/forms/PrintingServices.js'            
            );


        foreach ($files as $k=>$file) {
            $string = file_get_contents($this->_jsPath .$file);
            $concatFile .= "\n" . '/* ' .$k.' '. $file . ' */' . "\n";
            $concatFile .= $string . "\n";
        }

        header("Content-type: application/x-javascript");
        ob_start("ob_gzhandler");
        $cache   = Zend_Registry::get('cache');
        $cacheId = 'combinedjs';
        $combinedJs = $cache->load($cacheId);
        if(!$combinedJs){
//            if( APPLICATION_ENV !== 'production'){
//                 $combinedJs = $concatFile;
//            }else{
//                 $combinedJs = JSMin::minify($concatFile);
//                 $cache->save($combinedJs,$cacheId);
//            }
            $combinedJs = $concatFile;
        }
        echo $combinedJs;

        die();
    }
    public function minCssAction(){

    }
}
?>

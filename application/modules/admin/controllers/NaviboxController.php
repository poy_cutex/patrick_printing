<?php
class Admin_NaviboxController extends Zend_Controller_Action
{
    public function init()
    {
        $this->mapper = new WebDav_Model_NaviboxesMapper();
    }

    public function indexAction()
    {
        die(json_encode(array('success'=>true)));
    }

    public function listAction()
    {
        $whereParams['position'] = $this->_getParam('position',null);
        $whereParams['active_indicator'] = $this->_getParam('active_indicator',0);
        
        $records = $this->mapper->fetchAll($whereParams);

        die(json_encode(array('records'=>$records->toArray())));
    }

    public function saveAction()
    {
        $request = $this->getRequest();
        if($request->isPost()) {
            $post = $request->getPost();

            if (get_magic_quotes_gpc()) {
                $records = json_decode(stripslashes($post['recordsToInsertUpdate']));
            } else {
                $records = json_decode($post['recordsToInsertUpdate']);
            }
            
            foreach ($records as $record) {
                $data = array(
                    'id' => $record->id,
                    'name' => $record->name,
                    'position' => $record->position,
                    'active_indicator' => $record->activeIndicator
                   // 'order' => $record->order
                );
                $this->mapper->save($data);
            }
            die(json_encode(array('success' => 'true')));
        }

        die(json_encode(array('success' => 'false')));
    }

    public function deleteAction()
    {
        $request = $this->getRequest();
        if($request->isPost()) {
            $post = $request->getPost();
            if($post['id']) {
                $this->mapper->delete($post['id']);
                die(json_encode(array('success' => 'true')));
            }
        }
        die(json_encode(array('success' => 'false')));
    }
}
?>
<?php
class Admin_GroupsController extends Zend_Controller_Action
{
    public function init()
    {
        $this->view->headLink()->appendStylesheet('/js/ext3/css/ext-all.css');
        $this->view->headLink()->appendStylesheet('/css/admin/layout.css');

        $this->view->headScript()->appendFile('/js/ext3/adapter/ext/ext-base.js');
        $this->view->headScript()->appendFile('/js/ext3/ext-all.js');

        $this->groupMapper = new WebDav_Model_GroupsMapper();
        $this->db = Zend_Registry::get('db');
        $this->authInfo = Zend_Auth::getInstance()->getStorage()->read();

    }

    public function indexAction()
    {
        //$this->view->headScript()->appendFile('/js/grids/users.js');
        die(json_encode(array('success'=>true)));
    }

    public function listAction()
    {
        $start = $this->_getParam('start',0);
        $limit = $this->_getParam('limit',20);
        $count = $this->groupMapper->fetchAll();
        $groups = $this->groupMapper->fetchAll($start,$limit);
        die(json_encode(array('count'=>count($count),'records'=>$groups->toArray())));
    }

    public function listallAction()
    {
        $groups = $this->groupMapper->fetchAll();
        die(json_encode(array('success' => true, 'records' => $groups->toArray())));
    }

    public function getgroupnaviboxesAction()
    {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        
        extract($this->getRequest()->getPost());

        $groupNaviboxesMapper = new WebDav_Model_GroupsNaviboxesMapper();
        $naviboxes = $groupNaviboxesMapper->getGroupNaviboxes($groupId);

        $naviboxesArr = array();
        foreach ($naviboxes as $navibox) {
            $naviboxesArr[] = $navibox['navibox_id'];
        }

        die(json_encode($naviboxesArr));
    }

    public function deleteAction()
    {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        extract($this->getRequest()->getPost());

        if (Model_Group::blnGroupsHasUsers($groupId)) {
            die('Group cannot be deleted because it contains users.');
        }
        else {
            $groupMapper = new WebDav_Model_GroupsMapper();

            $group = new Default_Model_Groups(array('id' => $groupId));
            $groupMapper->delete($group);

            die('Group deleted.');
        }
    }

    public function updatepasswordresetintervalAction()
    {
        $request = $this->getRequest();
        if($request->isPost()) {
            $post = $request->getPost();
            foreach($post as $id => $passwordResetDays) {
                $group = new WebDav_Model_Groups(array(
                    'id' => $id,
                    'passwordResetDays' => $passwordResetDays
                ));
                $groupMapper = new WebDav_Model_GroupsMapper();
                $groupMapper->updatePasswordResetFields($group);
            }

            die(json_encode(array('success' => true)));
        }

        die(json_encode(array('success' => false)));
    }

     public function getGroupsAction(){
        $groups = Model_Group::getGroups();
        die(json_encode(array('succes'=>true,'records'=>$groups)));
    }

    public function resetallpasswordsAction()
    {
        $usersMapper = new WebDav_Model_UsersMapper();
        $usersMapper->resetAllPasswords();
        die(json_encode(array('success' => true)));
    }
}
?>
<?php
class Admin_JobOrderController extends Zend_Controller_Action
{
    public function init()
    {
    }   
    public function indexAction()
    {
         
    }
    public function listAction(){
        $serviceId = $this->_getParam('serviceId');
        die(json_encode(Model_JobOrder::listAll($serviceId)));
    }    
    
    public function saveAction(){
        $recs = $this->_getAllParams();
        Model_JobOrder::persist($recs);
        die(json_encode(array('success'=>true)));
    }
    
    public function statusAction(){
        $recs = $this->_getAllParams();
        Model_JobOrder::status($recs);
        die(json_encode(array('success'=>true)));
    }
    
    public function deleteAction(){
        Model_JobOrder::del($this->_getParam('recordId'));
        die(json_encode(array('success'=>true)));
    }
    
    public function paymentAction(){
        $record = $this->_getAllParams();
        Model_User::payment($record);
        die(json_encode(array('success'=>true)));
    }
    
    public function updateAction(){
        $record = $this->_getAllParams();
        Model_JobOrder::payment($record);
        die(json_encode(array('success'=>true)));
    }
}
?>
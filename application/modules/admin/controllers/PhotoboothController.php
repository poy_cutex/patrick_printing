<?php
class Admin_PhotoboothController extends Zend_Controller_Action
{
    public function init()
    {
    }   
    public function indexAction()
    {
         
    }
    public function listAction(){
        die(json_encode(Model_Photobooth::listAll()));
    }
    
    public function saveAction(){
        $recs = $this->_getAllParams();
        $recs['userId'] = $this->authInfo->id;
        Model_Photobooth::create($recs);
        die(json_encode(array('success'=>true)));
    }
    
    public function deleteAction(){
        Model_Photobooth::del($this->_getParam('recordId'));
        die(json_encode(array('success'=>true)));
    }
    
     public function paymentAction(){
        $record = $this->_getAllParams();
        Model_Photobooth::payment($record);
        die(json_encode(array('success'=>true)));
    }
}
?>
<?php
class Admin_ServicesController extends Zend_Controller_Action
{
    public function init()
    {
    }   
    public function indexAction()
    {
         
    }
    public function listAction(){
        $serviceId = $this->_getParam('serviceId',10);
        die(json_encode(Model_Services::listAll($serviceId)));
    }
    
    public function saveAction(){
        $recs = $this->_getAllParams();
        Model_Services::persist($recs);
        die(json_encode(array('success'=>true)));
    }
    
    public function deleteAction(){
        Model_Services::del($this->_getParam('recordId'));
        die(json_encode(array('success'=>true)));
    }

    
}
?>
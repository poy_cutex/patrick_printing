<?php
class Admin_ComboController extends Zend_Controller_Action
{
    public function init()
    {
    }   
    public function indexAction()
    {
    }
    public function groupsAction(){
        $q = Doctrine_Query::create()->select('id,name')->from('Model_Group')->orderBy('name ASC');;
        $records   =   $q->execute()->toArray();
        return die(json_encode(array('success'=>true,'records'=>$records)));
    }

    public function leadsAction(){
        $q = Doctrine_Query::create()->select('id,merchant')->from('Model_Leads')->orderBy('merchant ASC');;
        $records   =   $q->execute()->toArray();
        return die(json_encode(array('success'=>true,'records'=>$records)));
    }

    public function countriesAction(){
        $countriesMapper = new WebDav_Model_CountriesMapper();
        $records = $countriesMapper->fetchAll();
        die(json_encode(array('success'=>true,'records'=>$records->toArray())));
    }

    public function keywordsAction(){
        $translations = new WebDav_Model_TranslationMapper();
        $records = $translations->getAllKeywords();
        die(json_encode(array('success'=>true,'records'=>$records)));
    }
    
    public function userstatusAction(){
        $records = array(
            array('id'=>0,'name'=>'Not Active')
            ,array('id'=>1,'name'=>'Active')
            ,array('id'=>2,'name'=>'Confirmed')
            ,array('id'=>3,'name'=>'Blocked')
        );
        return die(json_encode(array('success'=>true,'records'=>$records)));
    }

    public function languagesAction(){
        $records = array(
            array('id'=>1,'name'=>'English')
            ,array('id'=>2,'name'=>'German')
            ,array('id'=>3,'name'=>'Czech')
        );
        return die(json_encode(array('success'=>true,'records'=>$records)));
    }

    public function translationGroupsAction(){
        return die(json_encode(array('records' => Model_LanguageGroup::listGroups($this->_getParam('id'))->toArray())));
    }
}
?>
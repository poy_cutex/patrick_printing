<?php
class Admin_InventoryController extends Zend_Controller_Action
{
    public function init()
    {
    }   
    public function indexAction()
    {
         
    }
    public function listAction(){
        
        die(json_encode(Model_Inventory::listAll()));
    }
    
    public function saveAction(){
        $recs = $this->_getAllParams();
        Model_Inventory::persist($recs);
        die(json_encode(array('success'=>true)));
    }
    
    public function deleteAction(){
        Model_Inventory::del($this->_getParam('recordId'));
        die(json_encode(array('success'=>true)));
    }

}
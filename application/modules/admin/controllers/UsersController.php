<?php
class Admin_UsersController extends Zend_Controller_Action
{
    public function init()
    {
        $this->view->headLink()->appendStylesheet('/js/ext3/css/ext-all.css');
        $this->view->headLink()->appendStylesheet('/css/admin/layout.css');

        $this->view->headScript()->appendFile('/js/ext3/adapter/ext/ext-base.js');
        $this->view->headScript()->appendFile('/js/ext3/ext-all.js');

        $this->users = new WebDav_Db_Users();
        $this->profiles = new WebDav_Db_Profiles();
        $this->db = Zend_Registry::get('db');
        $this->authInfo = Zend_Auth::getInstance()->getStorage()->read();

    }   
    public function indexAction()
    {
        //$this->view->headScript()->appendFile('/js/grids/users.js');
        die(json_encode(array('success'=>true)));
    }

    public function addAction()
    {
        $params = $this->_getAllParams();

        $userId = @$params['id'];
        $isAdd = @$userId ? false : true;
        $hasUsername = isset($params['username']) ? true : false;

        $adapter = new Zend_File_Transfer_Adapter_Http();
        //$adapter->addValidator('IsImage');
        $adapter->addValidator('Extension', false, array(
            'tif', 'tiff', 'gif', 'jpeg', 'jpg', 'jif', 'jfif', 'pcd', 'png'));
        $adapter->setDestination('img/user_profile/');

        if($adapter->isUploaded()) {
            if($adapter->isValid()) {
                $hasValidPicture = true;
                $target = 'img/user_profile/' . $profileInfo['user_id'] . '.img';
                $adapter->addFilter('Rename', array(
                    'target' => $target,
                    'overwrite' => true));
                $adapter->receive();
                $profileInfo['picture'] = $target;
            } else
                die(json_encode(array('success'=>false, 'msg' => 'Please upload a valid image file.')));
        }
    
        /*
         * Data mapper to follow
         */
        if($hasUsername) {
            $usernameExists = WebDav_Db_Users::recordExist($params['username']);
            if(($isAdd && $usernameExists) || (!$isAdd && $usernameExists && ($usernameExists != $userId))) {
                die(json_encode(array('success'=>false,'error'=> $params['username'].' already exists')));
            }

            $userInfo['username']   =   $params['username'];

            if(@$params['role'])
                $userInfo['group_id']   =   $params['role'];
            

            if($isAdd) {
                $clearpassword = $this->_createRandomPassword();
                $salt   = md5($this->_createSalt());
                $hash = $clearpassword.$salt;

                $userInfo['password']   = new Zend_Db_Expr('SHA1(\''.$hash.'\')');
                $userInfo['salt']       = $salt;
                $userInfo['date_created'] = new Zend_Db_Expr('now()');
                $userInfo['added_by'] = $this->authInfo->id;
                $userInfo['static_password'] = @$params["static_password"] == "on" ? 1:0;

                $result = $this->db->insert('users',$userInfo);
            } else {

                if (isset($params['password'])) {

                    $objWebDav_DbTable_Users = new WebDav_Model_DbTable_Users();
                    $userData = $objWebDav_DbTable_Users->fetchRow($objWebDav_DbTable_Users->select()->where('id = ?', $userId));

                    if ($userData->password != $params['password'])  {
                        $objUserMapper = new WebDav_Model_UsersMapper();
                        $objUserMapper->updatePassword($userId, $params['password']);
                    }
                }

                if(@$params['active'])
                    $userInfo['active'] = $params['active'];
                $userInfo['static_password'] = @$params["static_password"] == "on" ? 1:0;
                $result = $this->db->update('users', $userInfo, 'id = ' . $userId);
            }
        }

        if(@$result || @$userId || !$hasUsername) {
            $err_reporting = error_reporting();
            error_reporting(0);
            $profileInfo['firstname'] = $params['firstname'];
            $profileInfo['lastname'] = $params['lastname'];
            $profileInfo['email'] = $params['email'];
            $profileInfo['phone'] = $params['phone'];
            $profileInfo['mobile_phone'] = $params['mobile_phone'];
            $profileInfo['gender'] = $params['gender'];
            $profileInfo['date_birth'] = date('Y-m-d',strtotime($params['date_birth']));
            $profileInfo['icq'] = $params['icq'];
            $profileInfo['msn'] = $params['msn'];
            $profileInfo['homepage'] = $params['homepage'];
            $profileInfo['country'] = $params['country'];
            $profileInfo['default_language'] = $params['default_language'];
            $profileInfo['provisionscheme_id'] = Model_ProvisionScheme::findSchemeId($params['scheme']);
            error_reporting($err_reporting);
            if(!$hasUsername) {
                $hasProfile = WebDav_Db_Profiles::getUserDetails($this->authInfo->id);
                $profileInfo['user_id'] = $this->authInfo->id;

                if(is_null($hasProfile)) {
                    $this->db->insert('user_profiles', $profileInfo);
                    }
                else {
                    $profileInfo['hide_numbers'] = isset ($params['hide_numbers']) ? $params['hide_numbers'] : null;
                    $this->db->update('user_profiles', $profileInfo, 'user_id = ' . $this->authInfo->id);
                }

                
                die(json_encode(array('success'=>true)));
            } elseif($isAdd) {
                $profileInfo['user_id'] = $this->db->lastInsertId();
                if($this->db->insert('user_profiles',$profileInfo)){
                    $this->mailPassword($params['email']
                            ,$params['firstname'].' '.$params['lastname']
                            ,$userInfo['username']
                            ,$clearpassword
                            ,$hash);
                    die(json_encode(array('success'=>true)));

                }
            } else {
                $this->db->update('user_profiles', $profileInfo, 'user_id = ' . $userId);
                die(json_encode(array('success'=>true)));
            }
        }

        die(json_encode(array('success'=>false)));

    }

    public function listsAction()
    {
        $start = $this->_getParam('start',0);
        $limit = $this->_getParam('limit',20);
        $count = $this->profiles->getAll();
        $users = $this->profiles->getAll($start,$limit);
        die(json_encode(array('count'=>count($count),'records'=>$users)));
    }

    public function deleteAction()
    {
        $userId = $this->_getParam('userId', 0);

        if($userId > 0) {
            $this->db->delete('users', 'id = ' . $userId);
            $this->db->delete('user_profiles', 'user_id = ' . $userId);
            die('User successfully deleted.');
        }

        die('Failed to delete the user.');
    }

    public function getuserdetailsAction()
    {
        $userId = $this->_getParam('userId', 0);

        if($userId == 0) {
            $userId = $this->authInfo->id;
        }

        if($userId > 0) {
            $userDetails = $this->profiles->getUserDetails($userId);
            die(json_encode(array('success' => true, 'data' => @$userDetails[0], 'userdetails' => $userDetails)));
        }

        die(json_encode(array('success' => false)));
    }

    public function getlatestloginsAction()
    {
        $limit = $this->_getParam('limit');
        $userId = $this->_getParam('userId', 0);

        if($userId == 0) {
            $userId = $this->authInfo->id;
        }

        $loginJnl = new WebDav_Model_LoginJnlMapper();
        $recs = $loginJnl->getLatestLogins($userId, $limit);
        die(json_encode(array('success' => true, 'records' => $recs->toArray())));
    }

    public function mailPassword($recipient,$name,$username,$password,$hash)
    {
        $config = array('auth' => 'login',
                'username' => 'maxzandomail@gmail.com',
                'password' => 'maxzmail',
                'ssl'=>"tls",
                'port'=> 587
            );
                
        $transport = new Zend_Mail_Transport_Smtp('smtp.gmail.com',$config);
        /* save tpl to keyword db and fetch value from there ... */
        $emailSetting = Model_Settings::findSetting('REGISTRATION');
        $emailTpl  = $emailSetting['text_value'];

        $placeHolders = array('FULLNAME','USERNAME','PASSWORD','HASH');
        $replacements = array($name,$username,$password,$hash);        
        $htmlBody = str_replace($placeHolders,$replacements,$emailTpl);

        $mail = new Zend_Mail();
        $mail->setBodyText('Hello '.$name.' ,');
        $mail->setBodyHtml($htmlBody);
        $mail->setFrom('maxzandomail@gmail.com', 'Edgar Aranza');
        $mail->addTo($recipient, $name);
        $mail->setSubject('Your Maxzando Login Account');
        $mail->send($transport);
    }


    private function _createRandomPassword() {
        $chars = "abcdefghijkmnopqrstuvwxyz023456789";
        srand((double)microtime()*1000000);
        $i = 0;
        $pass = '' ;
        while ($i <= 7) {
            $num = rand() % 33;
            $tmp = substr($chars, $num, 1);
            $pass = $pass . $tmp;
            $i++;
        }
        return $pass;
    }
    private function _createSalt(){
        $dynamicSalt = '';
        for ($i = 0; $i < 50; $i++) {
            $dynamicSalt .= chr(rand(33, 126));
        }
        return $dynamicSalt;
    }
    public function confirmAction(){
        $param  = $this->_getParam('param');
        $username  = $this->_getParam('uname');
        $userId = WebDav_Db_Users::getIdByUsername($username);
        $userInfo = $this->users->find($userId)->current()->toArray();
        if($userInfo){
            if($userInfo['password']==sha1($param)){
                $data['active'] = 1 ;
                $where = $this->db->quoteInto('id=?',$userId);

                if($this->users->update($data,$where)){
                    $this->_redirect('/admin/users/success');
                }
                
            }
           $this->_redirect('/admin/users/failed');
        }
    }
    public function successAction(){
        
    }
    public function getUsersAction(){
        $users = Model_User::getUsers();
        die(json_encode(array('succes'=>true,'records'=>$users)));
    }

    public function isSuperAdminAction() {
        $groupname = strtolower(Zend_Auth::getInstance()->getStorage()->read()->groupname);
        $result = 'false';
        if (($groupname == 'superadmin') || ($groupname == 'superadmins')|| ($groupname == 'programmers')|| ($groupname == 'programmer')) {
            $result = 'true';
        }
        
        die(json_encode(array('result'=>$result)));

    }
    
    public function isAdminAction() {
        $groupname = strtolower(Zend_Auth::getInstance()->getStorage()->read()->groupname);
        $result = 'false';
        if (($groupname == 'admin') || ($groupname == 'admins')|| ($groupname == 'superadmin')|| ($groupname == 'superadmins') || ($groupname == 'sales manager')|| ($groupname == 'sales manager')) {
            $result = 'true';
        }        
        die(json_encode(array('result'=>$result)));
    }
    
    public function getSalesPersonsAction(){
        $users = Model_User::getSalesPersons();
        $forFilter = $this->_getParam('forFilter', 0);
        if($forFilter==0){
            $users[] = array('username'=>'Not Set', 'id'=>null);
        }
        die(json_encode(array('success'=>true,'list'=>$users)));
    }

    public function getSalesManagersAction(){
        $users = Model_User::getSalesManagers();
        array_push($users,array('id'=>81,'username'=>'christian_bayer'));
        array_push($users,array('id'=>96,'username'=>'david_staudigel'));
        die(json_encode(array('success'=>true,'list'=>$users)));
    }
    
    public function getSalesPersonnelAction(){
        $users = Model_User::getSalesPersonnel();
        die(json_encode(array('success'=>true,'list'=>$users)));
    }
    public function getDealMakersAction(){
        $users = Model_User::getDealMakers();
        die(json_encode(array('success'=>true,'list'=>$users)));
    }
    
    public function getUserStatsAction(){
        
        $data = array();
        $userID = $this->_getParam('user', Zend_Auth::getInstance()->getIdentity()->id);
        $user = Model_User::getUser($userID);
        $lastlogin = Model_User::getLastLogin($userID);
        if($lastlogin != null){
            $data["lastlogindate"] = $lastlogin["lastlogindate"];
            $data["lastlogin"] = CBUtils::getTimeLength($lastlogin["lastlogin"]);
        }else{
            $data["lastlogindate"] = "N/A";
            $data["lastlogin"] = "N/A";
        }        
        $userProfileModel = new Model_UserProfile();
        $userProfile = $userProfileModel->getTable()->findOneByUserId($userID);
        $data["accountsNum"] = Model_Leads::getAccountCount('A',true,$userID);
        $data["leadsNum"] = Model_Leads::getLeadsCountByUser($userID);  
        $data["userdetails"] = "{$userProfile->firstname} {$userProfile->lastname}  ({$user['username']}), {$user['groupname']}";        
        die(json_encode(array('success'=>true,'data'=>$data)));        
    }
    
    public function saveDealMakerCardAction(){
        $params = $this->_getAllParams();        
        die(json_encode(array('success'=>true)));
    }
}
?>

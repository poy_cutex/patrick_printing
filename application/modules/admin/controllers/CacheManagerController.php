<?php
class Admin_CacheManagerController extends Zend_Controller_Action
{
    private $_cache;
    
    public function init(){
        $this->_cache = Zend_Registry::get('cache');
    }
    public function indexAction(){
        die();
    }
    public function flushAction(){
        $this->_cache->clean();
        die('cached has been flushed');
    }
    
}
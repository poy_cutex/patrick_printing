<?php
class Admin_CustomersController extends Zend_Controller_Action
{
    public function init()
    {
    }   
    public function indexAction()
    {
         
    }
    public function listAction(){
        
        die(json_encode(Model_Customers::listAll()));
    }
    
    public function saveAction(){
        $recs = $this->_getAllParams();
        Model_Customers::persist($recs);
        die(json_encode(array('success'=>true)));
    }
    
    public function deleteAction(){
        Model_Customers::del($this->_getParam('recordId'));
        die(json_encode(array('success'=>true)));
    }

    
}
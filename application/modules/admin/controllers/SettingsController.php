<?php
class Admin_SettingsController extends Zend_Controller_Action
{
    public function init()
    {
    }

    public function indexAction()
    {
    }

    public function listFieldsAction()
    {
        $fields = Doctrine::getTable('Model_Base_Settings')->findByType($this->_getParam('type'));
        die(json_encode($fields->toArray()));
    }

    public function saveAction()
    {
        $request = $this->getRequest();
        if($request->isPost()) {
            $groupMapper = new WebDav_Model_GroupsMapper();
            $post = $request->getPost();
            foreach($post as $id => $value) {
                if(strstr($id, '_enabled') || strstr($id, '_text')) continue;

                $isGroup = $this->strstrb($id, '_g');
                if($isGroup > 0) {
                    $group = new WebDav_Model_Groups(array(
                        'id' => $isGroup,
                        'passwordResetDays' => $value
                    ));
                    $groupMapper->updatePasswordResetFields($group);
                } else {
                    $valueField = $post[$id . '_text'] ? 'text_value' : 'value';
                    Model_Settings::saveSetting(array(
                        'name' => $id,
                        $valueField => $value,
                        'enabled' => $post[$id . '_enabled']
                    ));
                }
            }

            die(json_encode(array('success' => true)));
        }
    }

    public function getAction()
    {
        $request = $this->getRequest();
        if($request->isPost()) {
            $post = $request->getPost();
            die(json_encode(Model_Settings::findSetting($post['name'])));
        }
        die();
    }

    private function strstrb($h,$n)
    {
        return array_shift(explode($n,$h,2));
    }
}
?>